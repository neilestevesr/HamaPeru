<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */
    'accepted'             => ':attribute debe ser aceptado.',
    'active_url'           => ':attribute no es una URL válida.',
    'after'                => ':attribute debe ser una fecha posterior a :date.',
    'alpha'                => ':attribute solo debe contener letras.',
    'alpha_dash'           => ':attribute solo debe contener letras, números y guiones.',
    'alpha_num'            => ':attribute solo debe contener letras y números.',
    'array'                => ':attribute debe ser un conjunto.',
    'before'               => ':attribute debe ser una fecha anterior a :date.',
    'between'              => [
        'numeric' => ':attribute tiene que estar entre :min - :max.',
        'file'    => ':attribute debe pesar entre :min - :max kilobytes.',
        'string'  => ':attribute tiene que tener entre :min - :max caracteres.',
        'array'   => ':attribute tiene que tener entre :min - :max ítems.',
    ],
    'boolean'              => 'El campo :attribute debe tener un valor verdadero o falso.',
    'confirmed'            => 'La confirmación de :attribute no coincide.',
    'date'                 => ':attribute no es una fecha válida.',
    'date_format'          => ':attribute no corresponde al formato :format.',
    'different'            => ':attribute y :other deben ser diferentes.',
    'digits'               => ':attribute debe tener :digits dígitos.',
    'digits_between'       => ':attribute debe tener entre :min y :max dígitos.',
    'distinct'             => 'El campo :attribute contiene un valor duplicado.',
    'email'                => ':attribute no es un correo válido',
    'exists'               => ':attribute es inválido.',
    'filled'               => 'El campo :attribute es obligatorio.',
    'image'                => 'El archivo debe ser una imagen(jpeg, png).',
    'in'                   => ':attribute es inválido.',
    'in_array'             => 'El campo :attribute no existe en :other.',
    'integer'              => ':attribute debe ser un número entero.',
    'ip'                   => ':attribute debe ser una dirección IP válida.',
    'json'                 => 'El campo :attribute debe tener una cadena JSON válida.',
    'max'                  => [
        'numeric' => ':attribute no debe ser mayor a :max.',
        'file'    => ':attribute no debe ser mayor que :max kilobytes.',
        'string'  => ':attribute no debe ser mayor que :max caracteres.',
        'array'   => ':attribute no debe tener más de :max elementos.',
    ],
    'mimes'                => ':attribute debe ser un archivo con formato: :values.',
    'mimetypes'             => 'Debe ser un archivo con formato: :values.',
    'min'                  => [
    'numeric' => 'El tamaño de :attribute debe ser de al menos :min.',
    'file'    => 'El tamaño de :attribute debe ser de al menos :min kilobytes.',
    'string'  => ':attribute debe contener al menos :min caracteres.',
    'array'   => ':attribute debe tener al menos :min elementos.',
    ],
    'not_in'               => ':attribute es inválido.',
    'numeric'              => ':attribute debe ser numérico.',
    'present'              => 'El campo :attribute debe estar presente.',
    'regex'                => 'El formato de :attribute es inválido.',
    'required'             => 'El campo :attribute es obligatorio.',
    'required_if'          => 'El campo :attribute es obligatorio cuando :other es :value.',
    'required_unless'      => 'El campo :attribute es obligatorio a menos que :other esté en :values.',
    'required_with'        => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_with_all'    => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_without'     => 'El campo :attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de :values estén presentes.',
    'same'                 => ':attribute y :other deben coincidir.',
    'size'                 => [
    'numeric' => 'El tamaño de :attribute debe ser :size.',
    'file'    => 'El tamaño de :attribute debe ser :size kilobytes.',
    'string'  => 'El campo :attribute debe contener :size caracter(es).',
    'array'   => ':attribute debe contener :size elementos.',
    ],
    'string'               => 'El campo :attribute debe ser una cadena de caracteres.',
    'timezone'             => 'El :attribute debe ser una zona válida.',
    'unique'               => ':attribute ya ha sido registrado.',
    'url'                  => 'El formato :attribute es inválido.',
    'captcha'              => 'El código captcha ingresado no es correcto',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom'               => [
    'attribute-name' => [
    'rule-name' => 'custom-message',
    ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes'           => [
    'name'                  => 'Nombre',
    'username'              => 'usuario',
    'email'                 => 'correo electrónico',
    'first_name'            => 'nombre',
    'last_name'             => 'apellido',
    'password'              => 'contraseña',
    'password_confirmation' => 'confirmación de la contraseña',
    'city'                  => 'ciudad',
    'country'               => 'país',
    'address'               => 'dirección',
    'phone'                 => 'teléfono',
    'mobile'                => 'celular',
    'age'                   => 'edad',
    'sex'                   => 'sexo',
    'gender'                => 'género',
    'year'                  => 'año',
    'month'                 => 'mes',
    'day'                   => 'día',
    'hour'                  => 'hora',
    'minute'                => 'minuto',
    'second'                => 'segundo',
    'title'                 => 'título',
    'body'                  => 'contenido',
    'description'           => 'descripción',
    'excerpt'               => 'extracto',
    'date'                  => 'fecha',
    'time'                  => 'hora',
    'subject'               => 'asunto',
    'message'               => 'mensaje',
    'required'              => 'Requerido',
    'old_password'          => 'Actual contraseña',
    'new_password'          => 'Nueva contraseña',

    'ebar'                  => 'Ebar',
    'active_code'           => 'Cod. Activo Fijo',
    'address'               => 'Dirección',
    'district'              => 'Distrito',
    'service_management'    => 'Gerencia de servicio',
    'service_center'        => 'Centro de servicio',
    'electric_supply'       => 'Suministro eléctrico',
    'hired_potency'         => 'Potencia contratada',
    'feeding'               => 'Alimentación',
    'rate'                  => 'Tarifa',
    'concessionaire'        => 'Concesionario de energía',
    'area'                  => 'Área del terreno(m2)',
    'perimeter'             => 'Perímetro(m)',
    'input_diameter'        => 'Diámetro de ingreso(pulg)',
    'output_diameter'       => 'Diámetro de impulsión(pulg)',
    'pumping_capacity'      => 'Capacidad de bombeo(actual - L/S)',
    'odor_control'          => 'Control de olores',
    'solids_elevator'       => 'Elevador de solidos',
    'equipment_number'      => 'Nº de Equipo',
    'brand'                 => 'Marca',
    'model'                 => 'Modelo',
    'serial_number'         => 'N. serie',
    'material'              => 'Material',
    'type'                  => 'Tipo',
    'installed_at'          => 'Instalado en',
    'potency_hp'            => 'Potencia(HP)',
    'potency_kw'            => 'Potencia(KW)',
    'volt'                  => 'Volt.',
    'amp'                   => 'Apm.',
    'rpm'                   => 'Rpm',
    'f_p'                   => 'F.P.',
    'f_s'                   => 'F.S.',
    'suction_diameter'      => 'Diámetro de succión(pulg)',
    'discharging_diameter'  => 'Diámetro de descarga(pulg)',
    'q'                     => 'Q(L/s)',
    'hdt'                   => 'Hdt',
    'installation_type'     => 'Tipo de instalación',
    'status'                => 'Estado',
    'condition'             => 'Condición',
    'potency_stand'         => 'Potencia stand by(kw)',
    'fuel_tank_dimension'   => 'Dimensión del tanque de combustible',
    'fuel_tank_shape'       => 'Forma del tanque de combustible',
    'fuel_tank_installation_type' => 'Tipo de instalaciòn',
    'generator_brand'       => 'Marca generador',
    'generator_model'       => 'Modelo generador',
    'generator_potency_kw'  => 'potencia(KW) generador',
    'kva'                   => 'KVA',
    't_nominal'             => 'T. nominal(Volt.)',
    'current_nominal'       => 'Corriente nominal(Amp.)',
    'frequency'             => 'Frecuencia(Hz)',
    'card_avr'              => 'Tarjeta avr',
    'generator_status'      => 'Estado generador',
    'motor_brand'           => 'Marca motor',
    'motor_model'           => 'Modelo motor',
    'motor_serial_number'   => 'N. serie motor',
    'motor_fuel_type'       => 'Tipo de combustible',
    'motor_refrigeration'   => 'Refrigeración',
    'motor_consumption'     => 'Consumo(L/hr)',
    'motor_status'          => 'Estado motor',
    'pumping_station_id'    => 'Estaciòn de bombeo',
    'image'                 => 'Imagen'

    ],
];
