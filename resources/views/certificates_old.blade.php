<html lang="es"><head>
    <title>HAMA PERU - DOC</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Free-Template.co">
    <link rel="shortcut icon" href="ftco-32x32.png">

    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/certificates/custom-bs.css') }}">
    <link rel="stylesheet" href="{{ asset('css/certificates/jquery.fancybox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/certificates/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/certificates/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/certificates/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/style_2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style_3.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style_4.css') }}">
    <style media="screen">
    .invalid-feedback {
        display: block;
        display: none;
        width: 100%;
        margin-top: 0.25rem;
        font-size: 80%;
        color: #dc3545;
    }
    .is-invalid
    {
        border-color: #dc3545;
        padding-right: calc(1.5em + 0.75rem);
        background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23dc3545' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23dc3545' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E);
        background-repeat: no-repeat;
        background-position: center right calc(0.375em + 0.1875rem);
        background-size: calc(0.75em + 0.375rem) calc(0.75em + 0.375rem);
    }
    </style>
</head>
<body id="top">

    <div id="overlayer" style="display: none;"></div>

    <div class="site-wrap">
        <div class="site-mobile-menu site-navbar-target">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
        </div>

        <header class="site-navbar mt-3">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="site-logo col-6"><a href="index.html">HAMA | PERU</a></div>

                    <nav class="mx-auto site-navigation">
                        <ul class="site-menu js-clone-nav d-none d-xl-block ml-0 pl-0"></ul>
                    </nav>

                    <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
                        <div class="ml-auto">
                            <a href="https://hamaperu.com" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Sito Web Oficial de Hama</a>
                            <a href="https://capacitacioneshamaperu.com/aulavirtual/Login.php" class="btn btn-danger border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Aula Virtual</a>
                        </div>
                        <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
                    </div>
                </div>
            </div>
        </header>

        <!-- HOME -->
        <section class="home-section section-hero overlay bg-image" style="background-image: url('/images/hero_1.png');" id="home-section">

            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-12">
                        <div class="mb-5 text-center">
                            <h1 class="text-white font-weight-bold">BIENVENIDO AL PORTAL DE EMISION DE DOCUMENTOS DEL 2018 AL 2020-I</h1>
                            <p>CONSULTA POR NUMERO DE DNI Y DESCARGA TU CARTAS DE COMPROMISO ,ANEXO 4 Y CERTIFICADOS.</p>
                        </div>
                        <form action="{{ route('certificates_old.index') }}#next" method="get" class="search-jobs-form">
                            <div class="row mb-5">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0"></div>

                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0">
                                    <input type="text" name="dni" class="form-control form-control-lg {{ $errors->has('dni') ? 'is-invalid' : '' }}"
                                    placeholder="ESCRIBE TU NUMERO DE DNI" value="{{ old('dni') }}">
                                    @if($errors->has('dni'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('dni') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0">
                                    <button type="submit" class="btn btn-danger btn-lg btn-block text-white btn-search"><span class="icon-search icon mr-2"></span>BUSCA TU DOCUMENTO</button>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12 popular-keywords">
                                    <h3>CONTENIDO DE DOCUMENTOS:</h3>
                                    <ul class="keywords list-unstyled m-0 p-0">
                                        <li><a href="#" class="">Cartas de Compromiso</a></li>
                                        <li><a href="#" class="">Certificados</a></li>
                                        <li><a href="#" class="">Anexo4</a></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <a href="#next" class="scroll-button smoothscroll">
                <span class=" icon-keyboard_arrow_down"></span>
            </a>

        </section>

        <section class="py-5 bg-image overlay-primary fixed overlay" id="next" style="background-image: url('/images/hero_1.png');">
            <div class="container">
                <div class="row mb-5 justify-content-center">
                    <div class="col-md-7 text-center">
                        <h2 class="section-title mb-2 text-white">RESULTADOS</h2>
                        <p class="lead text-white">Descarga tus Documentos.</p>
                    </div>
                </div>
            </div>
        </section>


        <section class="site-section">
            <div class="container">
                <center>
                    @php
                        $i = 0;
                        $now = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD');
                    @endphp
                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th scope="col"><center>#</center></th>
                                <th scope="col"><center>APELLIDOS<br>Y<br>NOMBRES</center></th>
                                <th scope="col"><center>EMPRESA</center></th>
                                <th scope="col"><center>CURSO</center></th>
                                <th scope="col" style="width:150px;"><center>FECHA</center></th>
                                <!--<th scope="col">EXAMEN</th>-->
                                <th scope="col"><center>CERTIFICADO</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($certifications as $key => $certification)
                                @php
                                    $fechaFin = \Carbon\Carbon::createFromFormat('Y-m-d', $certification->fechainicio,'America/Lima')->addYear()->isoFormat('YYYY-MM-DD');
                                @endphp
                                <tr role="row" class="odd">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $certification->apellidopaterno . ' ' . $certification->apellidomaterno . ' ' . $certification->nombres }}</td>
                                    <td>{{ $certification->nombreempresa }}</td>
                                    <td>{{ $certification->curso }}</td>
                                    <td><center>{{ $certification->fechainicio }}<center></td>
                                    <td align="center" valign="middle">
                                        @if ($now <= $fechaFin)
                                            <a href="{{ route('certificates_old.download',['file_path' => $certification->file_path]) }}" class="rgba-white-sligh"><img src="{{ asset('images/certificado.png') }}" width='25' height='25'/></a>
                                        @else
                                            <img src="{{ asset('images/doc_bloq.jpg') }}" width='25' height='25'/>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </center>
                </div>
            </section>

            <section class="py-5 bg-image overlay-primary fixed overlay" style="background-image: url('/images/hero_1.png');">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h2 class="text-white">Encontraste tus Certificados?</h2>
                            <p class="mb-0 text-white lead">Si no encontraste tus certificados comunicate con los supervisores responsables.</p>
                        </div>
                        <div class="col-md-3 ml-auto">
                            <a href="#" class="btn btn-warning btn-block btn-lg">Ir al Inicio</a>
                        </div>
                    </div>
                </div>
            </section>

            <footer class="site-footer">

                <a href="#top" class="smoothscroll scroll-top">
                    <span class="icon-keyboard_arrow_up"></span>
                </a>

                <div class="container">
                    <div class="row mb-5">
                        <div class="col-6 col-md-3 mb-4 mb-md-0">
                            <h3>HAMA | PERU</h3>
                            <ul class="list-unstyled">
                                <li><a href="#">Descarga tus certificados</a></li>
                                <li><a href="../aulavirtual/Login.php">Portal de Capacitaciones Virtuales</a></li>
                                <li><a href="https://hamaperu.com">Pagina Web Oficial de Hama Peru</a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="row text-center">
                        <div class="col-12">
                            <p class="copyright">
                                <small>
                                    HAMA | PERU ©<script>document.write(new Date().getFullYear());</script>2020 Derechos Reservados | Plataforma Oficial de Hama Peru <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://hamaperu.com" target="_blank">HAMAPERU.COM</a>
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>

        </div>

        <!-- SCRIPTS -->
        <script src="{{ asset('js/certificates/jquery.min.js') }}"></script>
        <script src="{{ asset('js/certificates/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/certificates/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/certificates/stickyfill.min.js') }}"></script>
        <script src="{{ asset('js/certificates/jquery.fancybox.min.js') }}"></script>
        <script src="{{ asset('js/certificates/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('js/certificates/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('js/certificates/jquery.animateNumber.min.js') }}"></script>
        <script src="{{ asset('js/certificates/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/certificates/bootstrap-select.min.js') }}"></script>
        <script src="{{ asset('js/certificates/custom.js') }}"></script>

    </body>
    </html>
