@extends('layouts.app')

@section('content_main')
    <div class="content">
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form action="{{ route('login') }}" method="post" class="login100-form validate-form">
                        {{ csrf_field() }}
                        &nbsp;&nbsp;&nbsp;&nbsp;<img width="250px" height="50px" src="{{ asset('images/logo.png') }}">
                        <br><br>
                        <span class="login100-form-title p-b-43">
                            Bienvenido a HAMA PERÚ. Para continuar por favor ingresa con tu usuario y contraseña.
                        </span>

                        <div class="validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                            <span class="">Documento de Identidad</span>
                            <input type="text" name="dni" class="form-control {{ $errors->has('dni') ? 'is-invalid' : '' }}"
                            value="{{ old('dni') }}" placeholder="DNI" autofocus>
                        </div>
                        <br>
                        <div class="validate-input" data-validate="Password is required">
                            <span class="">Contraseña</span>
                            <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Contraseña">
                        </div>
                        <br>
                        <div class="validate-input" >
                            <span class="">Rol de Usuario</span>
                            <select name="rol" class="form-control" id="rol">
                                <option value="MASTER">Master Administrador</option>
                                <option value="ADMIN">Administrador</option>
                                <option value="SUP">Supervisor</option>
                                <option value="INS">Instructor</option>
                            </select>
                        </div>
                        <br><br>

                        <div class="container-login100-form-btn">
                            <button type=submit class="login100-form-btn">INICIAR SESION</button>
                        </div>

                        <br>
                        <div class="login100-form-social flex-c-m">
                            <a href="https://www.facebook.com/HamaPeruOficial/" class="login100-form-social-item flex-c-m bg1 m-r-5">
                                <i class="fab fa-facebook-f"></i>
                            </a>

                            <a href="https://www.linkedin.com/company/hama-per%C3%BA/?viewAsMember=true" class="login100-form-social-item flex-c-m bg2 m-r-5">
                                <i class="fab fa-linkedin-in"></i>
                            </a>

                            <a href="https://www.instagram.com/hamaperu/" class="login100-form-social-item flex-c-m bg2 m-r-5">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                    </form>

                    <div class="login100-more bg-img-admin">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('app_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login_main.css') }}">
@stop

@section('app_js')
    <script src="login_2/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="login_2/vendor/animsition/js/animsition.min.js"></script>
    <script src="login_2/vendor/bootstrap/js/popper.js"></script>
    <script src="login_2/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="login_2/vendor/select2/select2.min.js"></script>
    <script src="login_2/vendor/daterangepicker/moment.min.js"></script>
    <script src="login_2/vendor/daterangepicker/daterangepicker.js"></script>
    <script src="login_2/vendor/countdowntime/countdowntime.js"></script>
    <script src="login_2/js/main.js"></script>
@stop
