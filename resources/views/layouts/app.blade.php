<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="google" content="notranslate"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no shrink-to-fit=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('meta_tags')

    <title>@yield('title',config('app.name', 'HamaPeru'))</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('app_css')
</head>

<body class="">
    <div class="wrapper" id="app">
        @hasSection('header')
            <div class="main-header">
                @yield('header')
            </div>
        @endif

        @hasSection('aside')
                @yield('aside')
        @endif

        <div class="content-wrapper">
            @hasSection('content_header')
                <div class="content-header">
                    @yield('content_header')
                </div>
            @endif

            @yield('content_main')
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    @yield('app_js')

    @yield('app_plugins')

</body>
</html>
