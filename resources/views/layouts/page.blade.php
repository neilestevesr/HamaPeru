@extends('layouts.app')

@section('app_css')
    <style media="screen">
		h2, .h2 {
			font-size: 1.35rem;
		}

		.h5, h5 {
			font-size: .9125rem;
		}

		h1,h2,h3,h4,h5,h6,
		.h1,.h2,.h3,.h4,.h5,.h6 {
			margin-bottom: 0.5rem;
			font-weight: 500;
			line-height: 1.4;
		}

		body {
			font-family: Lato,sans-serif;
		}

        .main-header {
            /* z-index: 0; */
            height: 62px;
        }

	</style>

    @stack('css')
    @yield('css')
@stop

@section('header')
    <div class="logo-header" data-background-color="blue">
        <a href="#" class="logo">
            <img src="{{ asset('images/logo_blanco.png') }}"  height="30" width="160" alt="navbar brand" class="navbar-brand">
        </a>
        <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">
                <i class="icon-menu"></i>
            </span>
        </button>
        <button class="more" onclick="showAndroidToast('Desea salir de aula virtual ?')">	<img src="{{ asset('images/ICONO-SALIR.png') }}" alt="..."  width="30" height="30"></button>
    </div>

    <nav class="navbar navbar-header navbar-expand-lg" data-background-color="red2">
        <div class="container-fluid">
            <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                <li class="nav-item toggle-nav-search hidden-caret"></li>
                <li class="nav-item dropdown hidden-caret">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                        <div class="avatar-sm">
                            <img src="{{ asset('images/profile.jpg') }}" alt="..." class="avatar-img rounded-circle">
                        </div>
                    </a>
                    {{-- <ul class="dropdown-menu dropdown-user animated fadeIn">
                        <div class="dropdown-user-scroll scrollbar-outer">
                            <li>
                                <div class="user-box">
                                    <div class="avatar-lg"><img src="{{ asset('images/profile.jpg') }}" alt="image profile" class="avatar-img rounded"></div>
                                    <div class="u-text">
                                        <h4>{{ \Auth::user()->full_name }}</h4>
                                        <p class="text-muted">{{ \Auth::user()->email }}</p><a href="{{ route('classroom.profile.index') }}" class="btn btn-xs btn-secondary btn-sm">View Profile</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">My Profile</a>
                                <a class="dropdown-item" href="#">My Balance</a>
                                <a class="dropdown-item" href="#">Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Logout</a>
                            </li>
                        </div>
                    </ul> --}}
                </li>
            </ul>
        </div>
    </nav>
@stop

@section('aside')
    <div class="sidebar sidebar-style-2">
        <div class="sidebar-wrapper scrollbar scrollbar-inner">
            <div class="sidebar-content">
                <div class="user">
                    <div class="avatar-sm float-left mr-2">
                        <img src="{{ asset('images/profile.jpg') }}" alt="..." class="avatar-img rounded-circle">
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                            <span>
                                {{ \Auth::user()->name }}
                                <span class="user-level">{{ \Auth::user()->email }}</span>
                                <span class="caret"></span>
                            </span>
                        </a>
                        <div class="collapse in" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="{{ route('classroom.profile.index') }}">
                                        <span class="link-collapse"><strong>Mi Perfil</strong></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="nav nav-primary">
                    <li class="nav-section">
                        <span class="sidebar-mini-icon">
                            <i class="fa fa-ellipsis-h"></i>
                        </span>
                        <h4 class="text-section"><strong>MENU</strong></h4>
                    </li>
                    
                    @if( \Auth::user()->profile_survey  == 'S')
                    
                    @else
                     @can (['notInstructor','notSupervisor'])
                    <li class="nav-item">
                        <a href="{{ route('classroom.virtual_surveys.profile_survey_index') }}">
                            <img src="{{ asset('images/noticias.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>ENCUESTA DE PERFIL</strong></p>
                        </a>
                    </li>
                    @endcan
                    @endif
                     
                    @can ('notSupervisor')
                    <li class="nav-item">
                        <a href="{{ route('classroom.elearnings.index') }}">
                            <img src="{{ asset('images/icon_doc.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>E-Learnings</strong></p>
                        </a>
                    </li>
                    @endcan
                    @can (['notInstructor','notSupervisor'])
                    <li class="nav-item">
                        <a href="{{ route('classroom.courses.index') }}">
                            <img src="{{ asset('images/noticias.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>Cursos Inscritos</strong></p>
                        </a>
                    </li>
                    @endcan
                    
                    @can ('notSupervisor')
                    <li class="nav-item">
                        <a href="{{ route('classroom.digital_signatures.index') }}">
                            <img src="{{ asset('images/icono_firma.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>Firma Digital</strong></p>
                        </a>
                    </li>
                     @endcan
                    
                    @can ('notSupervisor')
                    <li class="nav-item">
                        <a href="{{ route('classroom.courses.live_list') }}">
                            <img src="{{ asset('images/aulavirtual.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>Curso en Vivo</strong></p>
                        </a>
                    </li>
                    @endcan
                    
                    @can (['notInstructor','notSupervisor'])
                    <li class="nav-item">
                        <a href="{{ route('classroom.courses.retrieval') }}">
                            <img src="{{ asset('images/alerta.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>Sala de Recuperación</strong></p>
                        </a>
                    </li>
                    @endcan
                    @can (['onlyInstructor'])
                        <li class="nav-item">
                            <a href="{{ route('classroom.events.index') }}">
                                <i class="far fa-calendar" style="font-size: 2em; color: #bdbebf; font-family: 'Font Awesome 5 Regular';"></i>
                                <p><strong>Eventos</strong></p>
                            </a>
                        </li>
                    @endcan
                    
                     @can (['onlySupervisor'])
                        <li class="nav-item">
                            <a href="{{ route('classroom.courses.live_security_list') }}">
                                <i class="far fa-calendar" style="font-size: 2em; color: #bdbebf; font-family: 'Font Awesome 5 Regular';"></i>
                                <p><strong>Eventos por firmar</strong></p>
                            </a>
                        </li>
                    @endcan
                    
                    
                    @can (['notInstructor','notSupervisor'])
                    <li class="nav-item">
                        <a href="{{ route('classroom.virtual_evaluations.index') }}">
                            <img src="{{ asset('images/hamanet.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>Evaluacion Virtual</strong></p>
                        </a>
                    </li>
                    @endcan
                    {{-- @can ('notInstructor')
                    <li class="nav-item">
                        <a href="{{ route('classroom.virtual_surveys.courses_index') }}">
                            <img src="{{ asset('images/hamanet.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>Encuesta Sintomatológica</strong></p>
                        </a>
                    </li>
                    @endcan
                    @can ('notInstructor')
                    <li class="nav-item">
                        <a href="{{ route('classroom.virtual_surveys.evaluations_index') }}">
                            <img src="{{ asset('images/hamanet.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>Encuesta de Satisfacción</strong></p>
                        </a>
                    </li>
                    @endcan --}}
                    <li class="nav-item">
                        <a class=""
                           href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                           <img src="{{ asset('images/icono_salir2.png') }}" alt="..."  width="30" height="30">
                           &nbsp;&nbsp;&nbsp;&nbsp;
                           <p><strong>Salir</strong></p>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                        {{-- <a href="javascript:showAndroidToast('Desea salir de aula virtual ?');">
                            <img src="{{ asset('images/icono_salir2.png') }}" alt="..."  width="30" height="30">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <p><strong>Salir</strong></p>
                        </a> --}}
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop



@section('content_main')
    <div class="main-panel">
        <div class="content">

            <div class="panel-header bg-danger-gradient">
        		<div class="page-inner py-5">
        			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row ">
        				<div class="col-md-6 ml-auto mr-auto">
        					<center><h2 class="text-white pb-2 fw-bold"><center>@yield('header_title')</center><br> @yield('header_body')</h2></center>
        					<hr align="center" noshade="noshade" size="2" color="white" width="80%" />
        					<h5 class="text-white op-7 mb-2"><center><i>"Pensar en tu seguridad es pensar <br> en el bien mayor, tu familia"<center></i>
        					</h5>
        				</div>
        			</div>
        		</div>
        	</div>

            @yield('content')
        </div>

        @hasSection('footer')
            <footer class="footer">
                @yield('footer')
            </footer>
        @endif
    </div>
@stop

@section('app_js')
    <script>
        WebFont.load({
            google: {"families":["Lato:300,400,700,900"]},
            custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['assets/css/fonts.min.css']},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    @stack('js')
    @yield('js')
@stop

@section('app_plugins')
    @yield('plugins')
    @stack('plugins')
@stop
