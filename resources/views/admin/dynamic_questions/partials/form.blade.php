<div class="card-body">
    <div class="form-group">
        <label for="statement">Enunciado</label>
        <input type="text" name="statement" class="form-control {{ $errors->has('statement') ? 'is-invalid' : '' }}"
        value="{{ old('statement',$dynamic_question->statement) }}" placeholder="Ingresa enunciado">
        @if($errors->has('statement'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('statement') }}</strong>
            </div>
        @endif
    </div>
</div>


@section('js')
    <script>
        $(function () {
            $('#type').select2()
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@stop
