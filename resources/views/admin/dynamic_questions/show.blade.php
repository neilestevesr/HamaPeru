@extends('adminlte::page')

@section('plugins.Bs-custom-file-input',true)

@section('title', 'Pregunta Dinamica')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Pregunta Dinamica'])
    @php
        $dynamic_question->load("exam");
    @endphp
    <div class="card-header">
        <a href="{{ $dynamic_question->exam->exam_type == 'test' ? route('dynamic_test_exams.show',$dynamic_question->exam_id) : route('dynamic_exams.show',$dynamic_question->exam_id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Examen</a>
        <a href="{{ route('dynamic_exams.dynamic_questions.edit',$dynamic_question->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $dynamic_question->statement }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Puntos</td>
                        <td>{{ $dynamic_question->points ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Alternativa Correcta</td>
                        <td>{{ $dynamic_question->correctDynamicAlternative->description ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $dynamic_question->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $dynamic_question->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @include('admin.dynamic_questions.partials.section_multimedia')
    @include('admin.dynamic_alternatives.index')
@stop
