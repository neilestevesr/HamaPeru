@extends('adminlte::page')

@section('plugins.Bs-custom-file-input',true)

@section('title', 'Examen|Preguntas')

@section('content_header')
    <h1>CREAR PREGUNTA DINAMICA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('dynamic_exams.dynamic_questions.store',$dynamic_exam->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('admin.dynamic_questions.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar Pregunta</button>
            </div>
        </form>
    </div>
@stop
