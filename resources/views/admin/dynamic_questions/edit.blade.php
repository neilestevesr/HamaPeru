@extends('adminlte::page')

@section('title', 'Pregunta Dinamica')

@section('plugins.Bs-custom-file-input',true)

@section('content_header')
    <h1>EDITAR PREGUNTA DINAMICA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('dynamic_exams.dynamic_questions.update',$dynamic_question->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @include('admin.dynamic_questions.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
