@extends('adminlte::page')

@section('title', 'Pregunta')

@section('content_header')
    <h1>EDITAR PREGUNTA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('exams.questions.update',$question->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.questions.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
