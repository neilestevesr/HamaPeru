<div class="card-body">
    <div class="form-group">
        <label for="statement">Enunciado</label>
        <input type="text" name="statement" class="form-control {{ $errors->has('statement') ? 'is-invalid' : '' }}"
        value="{{ old('statement',$question->statement) }}" placeholder="Ingresa enunciado">
        @if($errors->has('statement'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('statement') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="alternative_a">Alternativa A</label>
        <input type="text" name="alternative_a" class="form-control {{ $errors->has('alternative_a') ? 'is-invalid' : '' }}"
        value="{{ old('alternative_a',$question->alternative_a) }}" placeholder="Ingresa Alternativa A">
        @if($errors->has('alternative_a'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('alternative_a') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="alternative_b">Alternativa B</label>
        <input type="text" name="alternative_b" class="form-control {{ $errors->has('alternative_b') ? 'is-invalid' : '' }}"
        value="{{ old('alternative_b',$question->alternative_b) }}" placeholder="Ingresa Alternativa B">
        @if($errors->has('alternative_b'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('alternative_b') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="alternative_c">Alternativa C</label>
        <input type="text" name="alternative_c" class="form-control {{ $errors->has('alternative_c') ? 'is-invalid' : '' }}"
        value="{{ old('alternative_c',$question->alternative_c) }}" placeholder="Ingresa Alternativa C">
        @if($errors->has('alternative_c'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('alternative_c') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="alternative_d">Alternativa D</label>
        <input type="text" name="alternative_d" class="form-control {{ $errors->has('alternative_d') ? 'is-invalid' : '' }}"
        value="{{ old('alternative_d',$question->alternative_d) }}" placeholder="Ingresa Alternativa D">
        @if($errors->has('alternative_d'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('alternative_d') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Alternativa Correcta</label>
        <select id="alternative_correct" name="alternative_correct" class="form-control select2 select2-hidden-accessible {{ $errors->has('alternative_correct') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Alternativa Correcta">
            @foreach (config('parameters.alternatives') as $key => $alternative)
                <option value="{{ $key }}" {{ $question->alternative_correct  == $key ? 'selected' :'' }}>{{ $alternative }}</option>
            @endforeach
        </select>
        @if($errors->has('alternative_correct'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('alternative_correct') }}</strong>
            </div>
        @endif
    </div>
</div>


@section('js')
    <script>
        $(function () {
            $('#alternative_correct').select2()
        })
    </script>
@stop
