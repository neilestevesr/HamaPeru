@extends('adminlte::page')

@section('title', 'Pregunta')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Pregunta'])
    <div class="card-header">
        <a href="{{ route('exams.show',$question->exam_id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Examen</a>
        <a href="{{ route('exams.questions.edit',$question->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $question->statement }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Alternativa A</td>
                        <td>{{ $question->alternative_a ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Alternativa B</td>
                        <td>{{ $question->alternative_b ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Alternativa C</td>
                        <td>{{ $question->alternative_c ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Alternativa D</td>
                        <td>{{ $question->alternative_d ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Alternativa Correcta</td>
                        <td>{{ $question->alternative_correct ? config('parameters.alternatives')[$question->alternative_correct] : '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
