<div class="card-body">
    <div class="form-group">
        <label for="exampleInputEmail1">Descripción</label>
        <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
        value="{{ old('description',$miningUnit->description) }}" placeholder="Ingresa descripción">
        @if($errors->has('description'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Titular</label>
        <input type="text" name="owner" class="form-control {{ $errors->has('owner') ? 'is-invalid' : '' }}"
        value="{{ old('owner',$miningUnit->owner) }}" placeholder="Ingresa descripción">
        @if($errors->has('owner'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('owner') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Distrito</label>
        <input type="text" name="district" class="form-control {{ $errors->has('district') ? 'is-invalid' : '' }}"
        value="{{ old('district',$miningUnit->district) }}" placeholder="Ingresa descripción">
        @if($errors->has('district'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('district') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Provincia</label>
        <input type="text" name="Province" class="form-control {{ $errors->has('Province') ? 'is-invalid' : '' }}"
        value="{{ old('Province',$miningUnit->Province) }}" placeholder="Ingresa descripción">
        @if($errors->has('Province'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('Province') }}</strong>
            </div>
        @endif
    </div>
</div>
