@extends('adminlte::page')

@section('title', 'Unidades Mineras')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Unidad Minera'])
    <h1 class="card-title">UNIDADES MINERAS</h1> <a href="{{ route('mining_units.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Unidades Mineras</h3>
        </div>
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th class="sorting">Descripción</th>
                                    <th class="sorting">Titular</th>
                                    <th class="sorting">Distrito</th>
                                    <th class="sorting">Provincia</th>
                                    <th style="width: 30px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($miningUnits as $key => $miningUnit)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $miningUnit->id }}</td>
                                        <td>{{ $miningUnit->description }}</td>
                                        <td>{{ $miningUnit->owner }}</td>
                                        <td>{{ $miningUnit->district }}</td>
                                        <td>{{ $miningUnit->Province }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('mining_units.edit',$miningUnit->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $miningUnit)
                                                <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$miningUnit->id}}"
                                                    href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    @include('components.deleteModal',[
                                                        'url_delete' => route('mining_units.destroy',$miningUnit->id),
                                                        'object' => 'Unidad Minera',
                                                        'id' => $miningUnit->id,
                                                        'description' => $miningUnit->description
                                                    ])
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('js')
        <script>
            $(function () {
                $("#example1").DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por página",
                        "zeroRecords": "No se encontró registros",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de _MAX_ registros)",
                        "search":"Buscar",
                        "searchPlaceholder":"Ingrese texto",
                        "paginate": {
                            "first": "Primero",
                            "previous": "Anterior",
                            "next": "Siguiente",
                            "last": "Último",
                        }
                    },
                    "order": []
                });
            });
        </script>
    @stop
