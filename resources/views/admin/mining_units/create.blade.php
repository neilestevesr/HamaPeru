@extends('adminlte::page')

@section('title', 'Unidad Minera')

@section('content_header')
    <h1>CREAR UNIDAD MINERA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('mining_units.store') }}" method="post">
            @csrf
            @include('admin.mining_units.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
