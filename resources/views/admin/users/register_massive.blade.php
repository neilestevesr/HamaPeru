@extends('adminlte::page')

@section('title', 'Usuario')

@section('plugins.Bs-custom-file-input',true)

@section('content_header')
    @include('components.alerts',['object' => 'Usuarios'])
    @include('components.failures_table')
    <h1>REGISTRO MASIVO DE USUARIO</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <a href="{{ route('users.import.download_template') }}" class="btn btn-success" style="margin-left:10px;">Descargar Plantilla <i class="fas fa-download"></i></a>
            </div>
        </div>
        <form action="{{ route('users.store_massive') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputFile">Archivo</label>
                    <div class="input-group {{ $errors->has('file') ? 'is-invalid' : '' }}">
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input"id="exampleInputFile" value="{{ old('file') }}">
                            <label class="custom-file-label" for="exampleInputFile">Elegir archivo</label>
                        </div>
                    </div>
                    @if($errors->has('file'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('file') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script type="text/javascript">
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@stop
