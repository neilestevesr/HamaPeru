@extends('adminlte::page')

@section('title', 'Usuario')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Usuario'])
    <div class="card-header">
        <a href="{{ route('users.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Usuarios</a>
        @can ('canUpdate', $user)
        <a href="{{ route('users.edit',$user->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
        @endcan
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $user->name }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>DNI</td>
                        <td>{{ $user->dni ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Nombre</td>
                        <td>{{ $user->name ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Apellido Paterno</td>
                        <td>{{ $user->paternal ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Apellido Materno</td>
                        <td>{{ $user->maternal ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>E-mail</td>
                        <td>{{ $user->email ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Teléfono</td>
                        <td>{{ $user->telephone ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Rol</td>
                        <td>{{ config('parameters.roles')[$user->role] ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>CIP</td>
                        <td>{{ $user->cip ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Firma</td>
                        <td>{{ $user->signature == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Activo</td>
                        <td>{{ $user->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Unidades Mineras</td>
                        <td>
                            @if ($user->miningUnits->count() > 0)
                                @foreach ($user->miningUnits as $key => $mining_unit)
                                    <li>{{ $mining_unit->description }}</li>
                                @endforeach
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Empresa</td>
                        <td>{{ $user->company->description ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Cargo</td>
                        <td>{{ $user->position ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $user->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $user->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
