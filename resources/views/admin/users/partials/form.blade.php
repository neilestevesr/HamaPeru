<div class="card-body">
    <div class="form-group">
        <label for="dni">DNI</label>
        <input type="text" name="dni" class="form-control {{ $errors->has('dni') ? 'is-invalid' : '' }}"
        value="{{ old('dni',$user->dni) }}" placeholder="Ingresa dni">
        @if($errors->has('dni'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('dni') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="name">Nombre</label>
        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
        value="{{ old('name',$user->name) }}" placeholder="Ingresa nombre">
        @if($errors->has('name'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="paternal">Apellido paterno</label>
        <input type="text" name="paternal" class="form-control {{ $errors->has('paternal') ? 'is-invalid' : '' }}"
        value="{{ old('paternal',$user->paternal) }}" placeholder="Ingresa apellido paterno">
        @if($errors->has('paternal'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('paternal') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="maternal">Apellido Materno</label>
        <input type="text" name="maternal" class="form-control {{ $errors->has('maternal') ? 'is-invalid' : '' }}"
        value="{{ old('maternal',$user->maternal) }}" placeholder="Ingresa apellido materno">
        @if($errors->has('maternal'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('maternal') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="email">E-Mail</label>
        <input type="text" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
        value="{{ old('email',$user->email) }}" placeholder="Ingresa email">
        @if($errors->has('email'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
        value="" placeholder="Ingresa Password">
        @if($errors->has('password'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('password') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="telephone">Telefono</label>
        <input type="text" name="telephone" class="form-control {{ $errors->has('telephone') ? 'is-invalid' : '' }}"
        value="{{ old('telephone',$user->telephone) }}" placeholder="Ingresa telefono">
        @if($errors->has('telephone'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('telephone') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Rol</label>
        <select id="role" name="role" class="form-control select2 select2-hidden-accessible {{ $errors->has('role') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Rol">
            @foreach ($roles as $key => $role)
                <option value="{{ $key }}" {{ $user->role  == $key ? 'selected' :'' }}>{{ $role }}</option>
            @endforeach
        </select>
        @if($errors->has('role'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('role') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="cip">CIP</label>
        <input type="text" name="cip" class="form-control {{ $errors->has('cip') ? 'is-invalid' : '' }}"
        value="{{ old('cip',$user->cip) }}" placeholder="Ingresa codigo CIP">
        @if($errors->has('cip'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('cip') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Unidades Mineras</label>
        <select id="mining_unit" multiple name="mining_units[]" class="form-control select2 blue select2-hidden-accessible" data-placeholder="Seleccione unidades mineras" style="width: 100%;"  tabindex="-1" aria-hidden="true">
            @foreach ($mining_units as $key => $mining_unit)
                <option value="{{ $mining_unit->id }}" {{ $user->miningUnits->pluck('id')->contains($mining_unit->id) ? 'selected':'' }} >{{ $mining_unit->description }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Empresa</label>
        <select id="company" name="company_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('company_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione empresa">
            <option></option>
            @foreach ($companies as $key => $company)
                <option value="{{ $company->id }}" {{ $user->company ? ($user->company->id  == $company->id ? 'selected' :'') : '' }}>{{ $company->description }}</option>
            @endforeach
        </select>
        @if($errors->has('company_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('company_id') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="position">Cargo</label>
        <input type="text" name="position" class="form-control {{ $errors->has('position') ? 'is-invalid' : '' }}"
        value="{{ old('position',$user->position) }}" placeholder="Ingresa Cargo">

        @if($errors->has('position'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('position') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="active" class="custom-control-input" {{ $user->active == 'S' ? 'checked':'' }} id="customSwitch1">
            <label class="custom-control-label" for="customSwitch1">Activo</label>
        </div>
    </div>

</div>


@section('js')
    <script>
        $(function () {
            $('#mining_unit').select2();
            $('#role').select2();
            // $('#company').select2()
            $('#company').select2({ allowClear: true });
        })
    </script>
@stop
