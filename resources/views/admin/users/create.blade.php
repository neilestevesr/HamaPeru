@extends('adminlte::page')

@section('title', 'Usuario')

@section('content_header')
    <h1>CREAR USUARIO</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('users.store') }}" method="post">
            @csrf
            @include('admin.users.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
