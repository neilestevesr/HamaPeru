@extends('adminlte::page')

@section('title', 'Usuario')

@section('content_header')
    <h1>EDITAR USUARIO</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('users.update',$user->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.users.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
