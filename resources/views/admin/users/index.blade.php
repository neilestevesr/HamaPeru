@extends('adminlte::page')

@section('title', 'Usuarios')

@section('plugins.Datatables', true)

@section('content_header')
    @include('components.alerts',['object' => 'Usuario'])
    <h1 class="card-title">USUARIOS</h1> <a href="{{ route('users.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Usuarios</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">

                        <table id="table_index_user" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info"
                            data-url="{{ route('users.index') }}">

                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">ID</th>
                                    <th style="width: 50px;">DNI</th>
                                    <th>Nombre</th>
                                    <th>E-mail</th>
                                    <th>Rol</th>
                                    <th>Empresa</th>
                                    <th style="width: 50px;">Acciones</th>
                                </tr>
                            </thead>

                            {{-- <tbody>
                                @foreach ($users as $key => $user)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td class="sorting_1">{{ $user->dni }}</td>
                                        <td>
                                            <a href="{{ route('users.show',$user->id) }}">
                                                {{ $user->name }}
                                            </a>
                                        </td>
                                        <td class="sorting_1">{{ $user->email }}</td>
                                        <td class="sorting_1">{{ config('parameters')['roles'][$user->role] ?? '-' }}</td>
                                        <td class="sorting_1">{{ $user->company->description ?? '-' }}</td>
                                        <td style="text-align:center;">
                                            @can ('canUpdate', $user)
                                            <a href="{{ route('users.edit',$user->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @endcan
                                            @can ('canDelete', $user)
                                            <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$user->id}}"
                                                href="javascript:;">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                            @include('components.deleteModal',[
                                                'url_delete' => route('users.destroy', $user->id),
                                                'object' => 'Usuario',
                                                'id' => $user->id,
                                                'description' => $user->description
                                            ])
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody> --}}

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')

    {{-- <script src="https://cdn.datatables.net/v/bs4/dt-1.13.7/r-2.5.0/datatables.min.js"></script> --}}
    <script type="module" src="{{ asset('js/admin/users.js') }}"></script>

    {{-- <script>



    $(function () {
        $("#table_index").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No se encontró registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "search":"Buscar",
                "searchPlaceholder":"Ingrese texto",
                "paginate": {
                    "first": "Primero",
                    "previous": "Anterior",
                    "next": "Siguiente",
                    "last": "Último",
                }
            },
            "order": []
        });
    });
    </script> --}}
@stop
