@extends('adminlte::page')

@section('plugins.Moment',true)
@section('plugins.Daterangepicker',true)
@section('plugins.Tempusdominus',true)
@section('plugins.Bs-custom-file-input',true)

@section('title', 'E-Learning')

@section('content_header')
    <h1>EDITAR E-LEARNING</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('elearnings.update',$elearning->id) }}" method="post"  enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @include('admin.elearnings.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script type="text/javascript">
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@stop
