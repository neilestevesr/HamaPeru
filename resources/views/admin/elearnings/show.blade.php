@extends('adminlte::page')

@section('title', 'E-Learning')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'E-Learning'])
    <div class="card-header">
        <a href="{{ route('elearnings.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> E-Learnings</a>
        <a href="{{ route('elearnings.edit',$elearning->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    @if ($elearning->files->count() > 0)
        <div class="card div-img-elearning">
            <img src="{{ $elearning->files->where('file_type', 'imagenes')->last()->file_url ?? '#' }}" class="img-elearning" alt="" >
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $elearning->title }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Descripción</td>
                        <td>{{ $elearning->description ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Activo</td>
                        <td>{{ $elearning->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $elearning->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $elearning->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @include('admin.contents.index',['section' => 'Videos'])
    @include('admin.contents.index',['section' => 'Contenidos'])
    @include('admin.contents.index',['section' => 'Casos'])
    @include('admin.contents.index',['section' => 'RVI'])
    @include('admin.contents.index',['section' => 'Juegos'])
    @include('admin.contents.index',['section' => 'Archivos'])

@stop

@section('css')
    <style media="screen">
        .div-img-elearning{
            padding: 15px;
            max-width: 650px;
            max-height: 300px;
        }
        .img-elearning{
            width: auto;
            height: auto;
            max-width: 635px;
            max-height: 270px;
        }
    </style>
@stop
