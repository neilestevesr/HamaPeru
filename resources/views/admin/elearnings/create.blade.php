@extends('adminlte::page')

@section('plugins.Moment',true)
@section('plugins.Daterangepicker',true)
@section('plugins.Tempusdominus',true)
@section('plugins.Bs-custom-file-input',true)

@section('title', 'E-Learning')

@section('content_header')
    <h1>CREAR E-LEARNING</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('elearnings.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('admin.elearnings.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
