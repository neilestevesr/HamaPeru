<div class="card-body">
    <div class="form-group">
        <label for="title">Título</label>
        <input type="text" name="title" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
        value="{{ old('title',$elearning->title) }}" placeholder="Ingresa título">
        @if($errors->has('title'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('title') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="description">Descripción</label>
        <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
        value="{{ old('description',$elearning->description) }}" placeholder="Ingresa descripción">
        @if($errors->has('description'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </div>
        @endif
    </div>
    
    <div class="form-group">
        <label for="exampleInputFile">Imagen</label>
        <div class="input-group {{ $errors->has('image') ? 'is-invalid' : '' }}">
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="exampleInputFile" value="{{ old('image') }}">
                <label class="custom-file-label" for="exampleInputFile">Elegir Imagen</label>
            </div>
        </div>
        @if($errors->has('image'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('image') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="active" class="custom-control-input" {{ $elearning->active == 'S' ? 'checked':'' }} id="customSwitch1">
            <label class="custom-control-label" for="customSwitch1">Activo</label>
        </div>
    </div>


</div>

@push('css')
    <style media="screen">
        .custom-file-label::after {
            content: "Archivo";
        }
        .custom-file-label2::after {
            content: "Documento";
        }
    </style>
@endpush

@section('js')
    <script>
        $(function() {
            $('#date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#timepicker_start').datetimepicker({
                format: 'HH:mm'
            });

            $('#timepicker_end').datetimepicker({
                format: 'HH:mm'
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@stop
