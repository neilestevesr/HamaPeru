@extends('adminlte::page')

@section('title', 'E-Learnings')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'E-Learning'])
    <h1 class="card-title">E-LEARNINGS</h1> <a href="{{ route('elearnings.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de E-Leanings</h3>
        </div>
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th class="sorting" rowspan="1" colspan="1">Título</th>
                                    <th class="sorting" rowspan="1" colspan="1">Descripción</th>
                                    <th class="sorting" rowspan="1" colspan="1">Activo</th>
                                    <th class="sorting" rowspan="1" colspan="1">Fecha Creación</th>
                                    <th class="sorting" rowspan="1" colspan="1">Fecha Actualización</th>
                                    <th style="width: 30px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($elearnings as $key => $elearning)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $elearning->id }}</td>
                                        <td><a href="{{ route('elearnings.show',$elearning->id) }}">{{ $elearning->title }}</a></td>
                                        <td>{{ $elearning->description ?? '-' }}</td>
                                        <td class="sorting_1">{{ $elearning->active == 'S' ? 'Si' : 'No' }}</td>
                                        <td class="sorting_1">{{ $elearning->created_at ?? '-' }}</td>
                                        <td class="sorting_1">{{ $elearning->updated_at ?? '-' }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('elearnings.edit',$elearning->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $elearning)
                                                <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$elearning->id}}"
                                                    href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    @include('components.deleteModal',[
                                                        'url_delete' => route('elearnings.destroy',$elearning->id),
                                                        'object' => 'E-Learning',
                                                        'id' => $elearning->id,
                                                        'description' => $elearning->title
                                                    ])
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('js')
        <script>
            $(function () {
                $("#example1").DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por página",
                        "zeroRecords": "No se encontró registros",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de _MAX_ registros)",
                        "search":"Buscar",
                        "searchPlaceholder":"Ingrese texto",
                        "paginate": {
                            "first": "Primero",
                            "previous": "Anterior",
                            "next": "Siguiente",
                            "last": "Último",
                        }
                    },
                    "order": []
                });
            });
        </script>
    @stop
