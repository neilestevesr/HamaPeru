@extends('adminlte::page')

@section('title', 'Pregunta')

@section('content_header')
    <h1>EDITAR PREGUNTA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('statements.update',$statement->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.statements.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
