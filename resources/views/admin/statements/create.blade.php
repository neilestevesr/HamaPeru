@extends('adminlte::page')

@section('title', 'Grupo|Preguntas')

@section('content_header')
    <h1>CREAR PREGUNTA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('statements.store',$group->id) }}" method="post">
            @csrf
            @include('admin.statements.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
