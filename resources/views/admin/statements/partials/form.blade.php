<div class="card-body">
    <div class="form-group">
        <label for="description">Pregunta</label>
        <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
        value="{{ old('description',$statement->description) }}" placeholder="Ingresa Pegunta">
        @if($errors->has('description'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="desc">Descripción</label>
        <input type="text" name="desc" class="form-control {{ $errors->has('desc') ? 'is-invalid' : '' }}"
        value="{{ old('desc',$statement->desc) }}" placeholder="Ingresa Descripcion">
        @if($errors->has('desc'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('desc') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label>Tipo de Respuesta</label>
        <select id="type" name="type" class="form-control select2 select2-hidden-accessible {{ $errors->has('type') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Tipo de Respuesta">
            @foreach (config('parameters.statement_types') as $key => $type)
                <option value="{{ $key }}" {{ $statement->type  == $key ? 'selected' :'' }}>{{ $type }}</option>
            @endforeach
        </select>
        @if($errors->has('type'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('type') }}</strong>
            </div>
        @endif
    </div>
</div>


@section('js')
    <script>
        $(function () {
            $('#type').select2()
        })
    </script>
@stop
