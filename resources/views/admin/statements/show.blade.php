@extends('adminlte::page')

@section('title', 'Pregunta')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => session('option_message') ? 'Opción' : 'Pregunta'])
    <div class="card-header">
        <a href="{{ route('groups.show',$statement->group_id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Grupo</a>
        <a href="{{ route('statements.edit',$statement->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $statement->description }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Descripción</td>
                        <td>{{ $statement->desc ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Tipo de Respuesta</td>
                        <td>{{ $statement->type ? config('parameters.statement_types')[$statement->type] : '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @can ('createOption', $statement)
        @include('admin.options.index')
    @endcan

@stop
