@extends('adminlte::page')


@section('title', 'Home')


@section('content')
<div class="page-wrapper" style="padding-top: 15px;">
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('images/2.png') }}" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/3.jpg') }}" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/2.png') }}" alt="Third slide">
                        </div>

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-info color-white widget-stat">
                    <div class="ibox-body">
                        <h2 class="m-b-5 font-strong">{{ $events_total }}</h2>
                        <div class="m-b-5">TOTAL DEL EVENTOS</div><i class="ti-bar-chart widget-stat-icon"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-warning color-white widget-stat">
                    <div class="ibox-body">
                        <h2 class="m-b-5 font-strong">{{ $courses_total }}</h2>
                        <div class="m-b-5">TOTAL DE CURSOS </div><i class="ti-bar-chart widget-stat-icon"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-success color-white widget-stat">
                    <div class="ibox-body">
                        <h2 class="m-b-5 font-strong">{{ $certifications_approved }}</h2>
                        <div class="m-b-5">APROBADOS EN EL MES</div><i class="ti-user widget-stat-icon"></i>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-danger color-white widget-stat">
                    <div class="ibox-body">
                        <h2 class="m-b-5 font-strong">{{ $certifications_disapproved }}</h2>
                        <div class="m-b-5">DESAPROBADOS EN EL MES</div><i class="ti-user widget-stat-icon"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-info color-white widget-stat">
                    <div class="ibox-body">
                        <br>
                        <a href="https://form.jotform.com/240494885564672"> <div class="m-b-5">FICHA DE EVALUACIÓN</div><i class="widget-stat-icon"><img src="{{ asset('images/Cuestionario.png') }}" alt="..."  width="40" height="40"></i> </a>
                        <br>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-warning color-white widget-stat">
                    <div class="ibox-body">
                        <br>
                        {{-- <iframe title="GESTION DE CAPACITACION" width="1140" height="541.25" src="https://app.powerbi.com/reportEmbed?reportId=39b21d9f-9c3b-4b89-b063-fc01db974c8b&autoAuth=true&ctid=3209b50b-b79b-43dc-9fc4-8d42c406dd61" frameborder="0" allowFullScreen="true"></iframe> --}}
                        <a href="https://app.powerbi.com/reportEmbed?reportId=39b21d9f-9c3b-4b89-b063-fc01db974c8b&autoAuth=true&ctid=3209b50b-b79b-43dc-9fc4-8d42c406dd61">  <div class="m-b-5">GESTIÓN DE CAPACITACIÓN</div><i class="widget-stat-icon"><img src="{{ asset('images/GestiondeCapacitacion.png') }}" alt="..."  width="40" height="40"></i></a>
                        <br>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-success color-white widget-stat">
                    <div class="ibox-body">
                        <br>
                        <a style="text-decoration:none" href="https://app.powerbi.com/reportEmbed?reportId=6f39c505-e875-43d2-94a0-fbb410d24a17&autoAuth=true&ctid=3209b50b-b79b-43dc-9fc4-8d42c406dd61"> <div class="m-b-5">GESTIÓN DE SATISFACCIÓN</div><i class="widget-stat-icon"><img src="{{ asset('images/GestiondeSatisfacion.png') }}" alt="..."  width="40" height="40"></i></a>
                        <br>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-danger color-white widget-stat">
                    <div class="ibox-body">
                        <br>
                        <a href="https://app.powerbi.com/reportEmbed?reportId=14be6516-c168-4f25-9688-e0aada5708fc&autoAuth=true&ctid=3209b50b-b79b-43dc-9fc4-8d42c406dd61"> <div class="m-b-5">TEST DE KOLB Y TEST DISC</div><i class="widget-stat-icon"><img src="{{ asset('images/PerfilProfesional.png') }}" alt="..."  width="40" height="40"></i></a>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
    <link href="{{ asset('css/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/main.min.css') }}" rel="stylesheet" />
    <style media="screen">
        A:link {text-decoration: none; color: #FFF; }
        A:visited {color: #FFF;  font-family: arial; text-decoration: none }
        A:hover { color: #007bff; text-decoration: none }
    </style>
@stop


@section('js')

@stop
