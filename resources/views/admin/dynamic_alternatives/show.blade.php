@extends('adminlte::page')

@section('title', 'Pregunta Dinamica')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Alternativa Dinamica'])
    <div class="card-header">
        <a href="{{ route('dynamic_exams.dynamic_questions.show',$dynamic_alternative->dynamic_question_id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Pregunta</a>
        <a href="{{ route('dynamic_questions.dynamic_alternatives.edit',$dynamic_alternative->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $dynamic_alternative->description }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $dynamic_alternative->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $dynamic_alternative->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
