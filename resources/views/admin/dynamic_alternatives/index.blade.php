<div class="card">
    <div class="card-header">
        <h1 class="card-title" style="padding-top: 10px;">Alternativas Dinamicas</h1>
        {{--@can('createQuestion', $dynamic_exam)--}}
        <a href="{{ route('dynamic_questions.dynamic_alternatives.create',$dynamic_question->id) }}" class="btn btn-primary" style="margin-left:10px;"><i class="far fa-plus-square" aria-hidden="true"></i> Agregar</a>
        {{--@endcan--}}
    </div>
    <div class="card-body">
        <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12">
                    <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                        <thead>
                            <tr role="row">
                                <th style="width: 50px;">Nro</th>
                                <th class="sorting" rowspan="1" colspan="1">Descripción</th>
                                <th class="sorting" rowspan="1" colspan="1" style="width: 150px;">Alternativa Correcta</th>
                                <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 0 @endphp
                            @if ($dynamic_question->dynamicAlternatives->count() > 0)
                                @foreach ($dynamic_question->dynamicAlternatives as $key => $dynamic_alternative)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td>{{ $dynamic_alternative->description }}</td>
                                        <td style="text-align: center;">
                                            @if ($dynamic_question->correct_alternative_id == $dynamic_alternative->id)
                                                <i class="fas fa-check" style="color: #00bb2d;" aria-hidden="true"></i>
                                            @else
                                                <form action="{{ route('dynamic_questions.dynamic_alternatives.select_correct',$dynamic_alternative->id) }}" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn waves-effect"  title="Seleccionar"><i class="fas fa-check" style="color: #888888;"></i></button>
                                                </form>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('dynamic_questions.dynamic_alternatives.edit',$dynamic_alternative->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$dynamic_alternative->id}}"
                                                href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                @include('components.deleteModal',[
                                                    'url_delete' => route('dynamic_questions.dynamic_alternatives.destroy',$dynamic_alternative->id),
                                                    'object' => 'Alternativa Dinamica',
                                                    'id' => $dynamic_alternative->id,
                                                    'description' => $dynamic_alternative->description
                                                ])
                                            </td>
                                        </tr>
                                    @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@section('js')
    <script>
    $(function () {
        $("#table_index").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No se encontró registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "search":"Buscar",
                "searchPlaceholder":"Ingrese texto",
                "paginate": {
                    "first": "Primero",
                    "previous": "Anterior",
                    "next": "Siguiente",
                    "last": "Último",
                }
            },
            "searching": false,
            "lengthChange": false,
            "paging": false,
        });
        /*$('#example1').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
    });*/
    });
    </script>
@stop
