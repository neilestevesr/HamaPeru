@extends('adminlte::page')

@section('title', 'Pregunta')

@section('content_header')
    <h1>EDITAR ALTERNATIVA DINAMICA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('dynamic_questions.dynamic_alternatives.update',$dynamic_alternative->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.dynamic_alternatives.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
