@extends('adminlte::page')

@section('title', 'Examen|Preguntas')

@section('content_header')
    <h1>CREAR ALTERNATIVA DINAMICA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('dynamic_questions.dynamic_alternatives.store',$dynamic_question->id) }}" method="post">
            @csrf
            @include('admin.dynamic_alternatives.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar Alternativa</button>
            </div>
        </form>
    </div>
@stop
