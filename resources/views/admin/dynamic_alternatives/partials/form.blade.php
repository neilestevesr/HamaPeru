<div class="card-body">
    <div class="form-group">
        <label for="description">Alternativa</label>
        <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
        value="{{ old('description',$dynamic_alternative->description) }}" placeholder="Ingresa alternativa">
        @if($errors->has('description'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </div>
        @endif
    </div>
</div>
