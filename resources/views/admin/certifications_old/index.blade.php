@extends('adminlte::page')

@section('title', 'Periodo 2018 - 2020-1')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Documento'])
    <h1 class="card-title">CERTIFICADOS 2018 - 2020-1</h1> <br>
@stop

@section('content')
    @include('components.filters',[
        'route' => route('certification_old.index'),
        'dni_filter' => true,
    ])

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Certificados</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        @php $i = 0 @endphp
                        <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 30px;">#</th>
                                    <th>DNI</th>
                                    <th>APELLIDOS Y NOMBRES</th>
                                    <th>CURSO</th>
                                    <th>FECHA</th>
                                    <th style="width: 30px;">CERTIFICADO</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($certifications as $key => $certification)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td class="sorting_1">{{ $certification->dni }}</td>
                                        <td class="sorting_1">{{ $certification->apellidopaterno . ' ' . $certification->apellidomaterno . ' ' . $certification->nombres }}</td>
                                        <td class="sorting_1">{{ $certification->curso }}</td>
                                        <td class="sorting_1">{{ $certification->fechainicio }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('certification_old.download',['file_path' => $certification->file_path]) }}" class="rgba-white-sligh"><img src="{{ asset('images/certificado.png') }}" width='25' height='25'/></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <style media="screen">
        .custom-file-label::after {
            content: "Archivo";
        }
    </style>
@endpush

@section('js')
    <script>
        $(function () {
            $("#table_index").DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se encontró registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros)",
                    "search":"Buscar",
                    "searchPlaceholder":"Ingrese texto",
                    "paginate": {
                        "first": "Primero",
                        "previous": "Anterior",
                        "next": "Siguiente",
                        "last": "Último",
                    }
                },
                "order": []
            });
        });

    </script>
@stop
