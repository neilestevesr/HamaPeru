@extends('adminlte::page')

@section('title', 'Evento')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Evento'])
    <h1 class="card-title">EVENTOS</h1>
    @can ('onlyAdmin')
        <a href="{{ route('events.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="far fa-plus-square" aria-hidden="true"></i>Crear</a>
    @endcan
    <br>
@stop

@section('content')

    <div id="filter_box_container">

        @include('components.filters',[
            'route' => route('events.index'),
            'instructor_filter' => true,
            'course_filter' => true,
            'date_from_filter' => true,
            'date_to_filter' => true,
        ])

    </div>


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Eventos</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" name="is_recovery" value="false">
                        <table id="table_index_event" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info"
                            data-url="{{ route('events.index') }}">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th>Descripción</th>
                                    <th>Tipo</th>
                                    <th>Fecha</th>
                                    <th>Curso</th>
                                    <th>Instructor</th>
                                    <th>Responsable</th>
                                    <th>Activo</th>
                                    <th>Asistencias</th>
                                    <th style="width: 50px;">Acciones</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')

    <script type="module" src="{{ asset('js/admin/events.js') }}"></script>
    
@stop
