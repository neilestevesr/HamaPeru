@extends('adminlte::page')

@section('title', 'Evento')

@section('plugins.Bs-custom-file-input',true)

@section('content_header')
    @include('components.alerts',['object' => 'Participantes'])
    @include('components.failures_table')
    <h1>AREA Y OBSERVACIONES MASIVO DE PARTICIPANTES</h1>
    <a href="{{ route('events.show',$event->id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Evento</a>
@stop

@section('content')

     @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
     @endforeach
    <div class="card">
        <form action="{{ route('events.store_massive_participants_area',$event->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputFile">Archivo</label>
                    <div class="input-group {{ $errors->has('file') ? 'is-invalid' : '' }}">
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input"id="exampleInputFile" value="{{ old('file') }}">
                            <label class="custom-file-label" for="exampleInputFile">Elegir archivo</label>
                        </div>
                    </div>
                    @if($errors->has('file'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('file') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">ACTUALIZAR </button>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script type="text/javascript">
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@stop
