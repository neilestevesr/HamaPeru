@extends('adminlte::page')

@section('title', 'Evento')

@section('plugins.Moment',true)
@section('plugins.Daterangepicker',true)


@section('content_header')
    <h1>CREAR EVENTO</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('events.store') }}" method="post">
            @csrf
            @include('admin.events.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
