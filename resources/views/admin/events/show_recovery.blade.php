@extends('adminlte::page')

@section('title', 'Evento')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => session('object') ? session('object') : 'Evento'])
   
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $event->description }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Codigo</td>
                        <td>{{ $event->id ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Tipo</td>
                        <td>{{ config('parameters.event_types')[$event->type] ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha</td>
                        <td>{{ $event->date ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Capacitador</td>
                        <td>{{ $event->instructor->full_name_complete ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>DNI Capacitador</td>
                        <td>{{ $event->instructor->dni ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Responsable</td>
                        <td>{{ $event->responsable->full_name_complete ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>DNI Responsable</td>
                        <td>{{ $event->responsable->dni ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Sala</td>
                        <td><a href="{{ route('rooms.show', $event->room->id) }}">{{ $event->room->description ?? '-' }} </a> </td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Capacidad</td>
                        <td>{{$event->room->capacity ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Examen</td>
                        <td><a href="{{ route('exams.show', $event->exam->id) }}">{{ $event->exam->title ?? '-' }} </a> </td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Examen Completado</td>
                        <td>{{ $event->exam->is_complete ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Curso</td>
                        <td><a href="{{ route('courses.show', $event->exam->course->id) }}">{{ $event->exam->course->description ?? '-' }} </a> </td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>SubTítulo</td>
                        <td>{{ $event->exam->course->subtitle ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Activo</td>
                        <td>{{ $event->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Asistencias</td>
                        <td>{{ $event->flg_asist == 'S' ? 'Habilitado' : 'Deshabilitado' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Encuestas Ficha Sintomatologica</td>
                        <td>{{ $event->flg_survey_course == 'S' ? 'Habilitado' : 'Deshabilitado' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Encuestas de Satisfacción</td>
                        <td>{{ $event->flg_survey_evaluation == 'S' ? 'Habilitado' : 'Deshabilitado' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $event->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $event->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @include('admin.events.partials.index_participants_recovery',['certifications' => $event->certifications])
@stop
