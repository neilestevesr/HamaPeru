@extends('adminlte::page')

@section('title', 'Evento')

@section('plugins.Moment',true)
@section('plugins.Daterangepicker',true)

@section('content_header')
    <h1>EDITAR EVENTO</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('events.update',$event->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.events.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
