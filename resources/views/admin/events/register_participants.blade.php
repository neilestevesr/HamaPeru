@extends('adminlte::page')

@section('title', 'Evento')

@section('content_header')
    <h1>REGISTRAR PARTICIPANTES</h1>
    <a href="{{ route('events.show',$event->id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Evento</a>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('events.store_participants',$event->id) }}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>Participantes</label>
                    <select id="participants" multiple name="participants[]" class="form-control select2 blue select2-hidden-accessible {{ $errors->has('participants.*') || $errors->has('participants') ? 'is-invalid' : '' }}" data-placeholder="Seleccione participantes" style="width: 100%;"  tabindex="-1" aria-hidden="true">
                        @foreach ($users as $key => $user)
                            <option value="{{ $user->dni }}">{{ $user->dni .' - '.  $user->full_name}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('participants.0'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('participants.0') }}</strong>
                        </div>
                    @endif
                    @if($errors->has('participants'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('participants') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </div>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script>
    $(function () {
        $('#participants').select2()
    })
    </script>
@stop
