<div class="card">
    <div class="card-header">
        <h3 class="card-title">Lista de Participantes para Prueba</h3>
    </div>
    <div class="card-body">
        <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12">
                    @php $i = 0 @endphp
                    <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                        <thead>
                            <tr role="row">
                                <th style="width: 30px;">#</th>
                                <th style="width: 30px;">Id</th>
                                <th class="sorting" rowspan="1" colspan="1">DNI</th>
                                <th class="sorting" rowspan="1" colspan="1">Participante</th>
                                <th class="sorting" rowspan="1" colspan="1">Empresa</th>
                                {{-- <th class="sorting" rowspan="1" colspan="1">Evento</th>
                                <th class="sorting" rowspan="1" colspan="1">Curso</th> --}}
                                {{-- <th class="sorting" rowspan="1" colspan="1">Fecha</th> --}}
                                <th class="sorting" rowspan="1" colspan="1">Nota</th>
                                <th class="sorting" rowspan="1" colspan="1">Nota Fin</th>
                                <th class="sorting" rowspan="1" colspan="1">Recuperación</th>
                                <th class="sorting" rowspan="1" colspan="1">Estado</th>
                                <th class="sorting" rowspan="1" colspan="1">Habilitado</th>
                                <th class="sorting" rowspan="1" colspan="1" style="">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($certifications as $key => $certification)
                                <tr role="row" class="odd">
                                    <td class="sorting_1">{{ ++$i }}</td>
                                    <td class="sorting_1">{{ $certification->id }}</td>
                                    <td class="sorting_1">{{ $certification->user->dni }}</td>
                                    <td class="sorting_1"><a href="{{ route('users.show',$certification->user->id) }}">{{ $certification->user->full_name_complete }}</a></td>
                                    <td class="sorting_1"><a href="{{ route('companies.show',$certification->company->id) }}">{{ $certification->company->description }}</a></td>
                                    {{-- <td class="sorting_1"><a href="{{ route('events.show',$certification->event->id) }}">{{ $certification->event->description }}</a></td>
                                    <td class="sorting_1"><a href="{{ route('courses.show',$certification->event->exam->course->id) }}">{{ $certification->event->exam->course->description }}</a></td> --}}
                                    {{-- <td class="sorting_1">{{ $certification->event->date ?? '-' }}</td> --}}
                                    <td class="sorting_1">{{ $certification->score ?? '-' }}</td>
                                    <td class="sorting_1">{{ $certification->score_fin ?? '-' }}</td>
                                    <td class="sorting_1">{{ $certification->recovered_at ?? '-' }}</td>
                                    <td class="sorting_1">{{ config('parameters.certification_status')[$certification->status] ?? '-' }}</td>
                                    <td class="sorting_1">
                                        <ul>
                                            @foreach ($certification->is_enable_evaluation as $key => $message)
                                                <li>{{ $message }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td class="sorting_1">
                                        <a href="{{ route('certifications.show',$certification->id) }}" class="rgba-white-sligh action" title="Ver detalles"><i class="far fa-eye"></i></a>
                                        <a href="{{ route('certifications.edit',$certification->id) }}" class="rgba-white-sligh action" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a href="#" class="rgba-white-sligh action" title="Reiniciar" onclick="submitResetForm('{{ route('certifications.reset',$certification->id) }}')"><i class="fas fa-redo-alt" aria-hidden="true"></i></a>
                                        <a href="#" class="rgba-white-sligh action" title="Eliminar" onclick="submitCertificationDeleteForm('{{ route('certifications.destroy',$certification->id) }}','{{ $certification->user->full_name_complete ?? '-' }}')" data-toggle="modal" data-target="#certification-delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

<form id="resetForm" action="#" method="post">
    @csrf
</form>

<div class="modal fade" id="certification-delete">
    <div class="modal-dialog" style="width:400px;" role="document">
        <div class="modal-content timeout-modal">
            <div class="modal-body">
                <button class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="text-center h4 mb-3">Eliminación de Participante</div>
                <p class="text-center mb-4">¿Estás seguro que deseas eliminar al participante: <strong id="participant"> </strong> ? </p>
                <div id="timeout-activate-box">
                    <div class="row text-center" style="display: inline;">
                        <form id="certificationDeleteForm" action="#" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary" name="delete_modal">SI, deseo eliminar</button>
                            <button class="btn btn-danger btn-fix btn-air" data-dismiss="modal" >Cancelar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('css')
    <style media="screen">
        .action{
            padding: 3px;
        }
    </style>
@endpush

@section('js')
    <script>
        $(function () {
            $("#table_index").DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se encontró registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros)",
                    "search":"Buscar",
                    "searchPlaceholder":"Ingrese texto",
                    "paginate": {
                        "first": "Primero",
                        "previous": "Anterior",
                        "next": "Siguiente",
                        "last": "Último",
                    }
                }
            });
        });
        function submitResetForm(route) {
            $("#resetForm").attr("action", route);
            $("#resetForm").submit();
        }
        function submitCertificationDeleteForm(route,full_name) {
            $("#certificationDeleteForm").attr("action", route);
            $("#participant").text(full_name);
        }

    </script>
@stop
