<div class="card-body">
    <div class="form-group">
        <label for="description">Descripción</label>
        <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
        value="{{ old('description',$event->description) }}" placeholder="Ingresa descripción">
        @if($errors->has('description'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Tipo</label>
        <select id="type" name="type" class="form-control select2 select2-hidden-accessible {{ $errors->has('type') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Tipo">
            @foreach ($types as $key => $type)
                <option value="{{ $key }}" {{ $event->type  == $key ? 'selected' :'' }}>{{ $type }}</option>
            @endforeach
        </select>
        @if($errors->has('type'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('type') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Fecha</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input id="date" type="text" name="date" class="form-control float-right {{ $errors->has('date') ? 'is-invalid' : '' }}"
            value="{{ old('date',$event->date) }}">
            @if($errors->has('date'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('date') }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label>Instructor</label>
        <select id="user" name="user_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('user_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Instructor">
            @foreach ($users as $key => $user)
                <option value="{{ $user->id }}" {{ $event->instructor ? ($event->instructor->id  == $user->id ? 'selected' :'') : '' }}>{{ $user->full_name }}</option>
            @endforeach
        </select>
        @if($errors->has('user_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('user_id') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Responsable</label>
        <select id="responsable" name="responsable_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('responsable_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Responsable">
            @foreach ($responsables as $key => $responsable)
                <option value="{{ $responsable->id }}" {{ $event->responsable ? ($event->responsable->id  == $responsable->id ? 'selected' :'') : '' }}>{{ $responsable->full_name }}</option>
            @endforeach
        </select>
        @if($errors->has('responsable_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('responsable_id') }}</strong>
            </div>
        @endif
    </div>
    
    
   <!-- <div class="form-group">
        <label>Gerente de Seguridad / Ingeniero de Seguridad (Atacocha) </label>
        <select id="security" name="security_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('security_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Ger/Ing de Seguridad">
            @foreach ($seguridades as $key => $seguridad)
               
                <option value="{{ $seguridad->id }}" {{ $event->seguridad ? ($event->seguridad->id  == $seguridad->id ? 'selected' :'') : '' }}>{{ $seguridad->full_name }}</option>
            @endforeach
        </select>
        @if($errors->has('security_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('security_id') }}</strong>
            </div>
        @endif
    </div>
    
     <div class="form-group">
        <label>Gerente de Seguridad / Ingeniero de Seguridad (El Porvenir) </label>
        <select id="security_por" name="security_por_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('security_por_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Ger/Ing de Seguridad">
            @foreach ($seguridades as $key => $seguridad)
                <option value="{{ $seguridad->id }}" {{ $event->seguridad ? ($event->seguridad->id  == $seguridad->id ? 'selected' :'') : '' }}>{{ $seguridad->full_name }}</option>
            @endforeach
        </select>
        @if($errors->has('security_por_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('security_por_id') }}</strong>
            </div>
        @endif
    </div> -->
    
    
    
    <div class="form-group">
        <label>Sala</label>
        <select id="room" name="room_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('room_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Sala">
            @foreach ($rooms as $key => $room)
                <option value="{{ $room->id }}" {{ $event->room ? ($event->room->id  == $room->id ? 'selected' :'') : '' }}>{{ $room->description }}</option>
            @endforeach
        </select>
        @if($errors->has('room_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('room_id') }}</strong>
            </div>
        @endif
    </div>

 <div class="form-group">
        <label>Empresa Titular</label>
        <select id="company" name="owner_companies_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('owner_companies_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Empresa">
            @foreach ($companys as $key => $company)
                <option value="{{ $company->id }}" {{ $event->EmpresaTitular ? ($event->EmpresaTitular->id  == $company->id ? 'selected' :'') : '' }}>{{ $company->name }}</option>
            @endforeach
        </select>
        @if($errors->has('owner_companies_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('owner_companies_id') }}</strong>
            </div>
        @endif
    </div>


    <div class="form-group">
        <label>Examen</label>
        <select id="exam" name="exam_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('exam_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Examen">
            @foreach ($exams->where('exam_type','dynamic') as $key => $exam)
                <option value="{{ $exam->id }}" {{ $event->exam ? ($event->exam->id  == $exam->id ? 'selected' :'') : '' }}>{{ $exam->title }}</option>
            @endforeach
        </select>
        @if($errors->has('exam_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('exam_id') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label>Examen de Prueba</label>
        <select id="test_exam" name="test_exam_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('test_exam_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Examen de Prueba">
            @foreach ($exams->where('exam_type','test') as $key => $exam)
                <option value="{{ $exam->id }}" {{ $event->testExam ? ($event->testExam->id  == $exam->id ? 'selected' :'') : '' }}>{{ $exam->title }}</option>
            @endforeach
        </select>
        @if($errors->has('test_exam_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('test_exam_id') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label>E-Learning</label>
        <select id="elearning" name="elearning_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('elearning_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione E-Learning">
            @foreach ($elearnings as $key => $elearning)
                <option value="{{ $elearning->id }}" {{ $event->elearning ? ($event->elearning->id  == $elearning->id ? 'selected' :'') : '' }}>{{ $elearning->title }}</option>
            @endforeach
        </select>
        @if($errors->has('elearning_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('elearning_id') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="active" class="custom-control-input" {{ $event->active == 'S' ? 'checked':'' }} id="customSwitch1">
            <label class="custom-control-label" for="customSwitch1">Activo</label>
        </div>
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="flg_test_exam" class="custom-control-input" {{ $event->flg_test_exam == 'S' ? 'checked':'' }} id="customSwitch2">
            <label class="custom-control-label" for="customSwitch2">Examen de Prueba Activo</label>
        </div>
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="flg_asist" class="custom-control-input" {{ $event->flg_asist == 'S' ? 'checked':'' }} id="customSwitch3">
            <label class="custom-control-label" for="customSwitch3">Asistencias</label>
        </div>
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="flg_survey_course" class="custom-control-input" {{ $event->flg_survey_course == 'S' ? 'checked':'' }} id="customSwitch4">
            <label class="custom-control-label" for="customSwitch4">Encuestas Ficha Sintomatológica</label>
        </div>
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="flg_survey_evaluation" class="custom-control-input" {{ $event->flg_survey_evaluation == 'S' ? 'checked':'' }} id="customSwitch5">
            <label class="custom-control-label" for="customSwitch5">Encuestas de Satisfacción</label>
        </div>
    </div>
</div>


@section('js')
    <script>
        $(function () {
            $('#type').select2()
            $('#user').select2()
            $('#responsable').select2()
            $('#security').select2()
            $('#security_por').select2()
            $('#room').select2()
            $('#company').select2()
            $('#exam').select2()
            $('#test_exam').select2()
            $('#elearning').select2()

            $('#date').daterangepicker({
              singleDatePicker: true,
              showDropdowns: true,
              minYear: 2000,
              maxYear: parseInt(moment().format('YYYY'),10),
              locale: {
                format: 'YYYY-MM-DD'
              }
            });
        });
    </script>
@stop
