@extends('adminlte::page')

@section('title', 'Empresa')

@section('content_header')
    <h1>EDITAR EMPRESA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('companies.update',$company->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.companies.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
