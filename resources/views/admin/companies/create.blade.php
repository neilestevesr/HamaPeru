@extends('adminlte::page')

@section('title', 'Empresa')

@section('content_header')
    <h1>CREAR EMPRESA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('companies.store') }}" method="post">
            @csrf
            @include('admin.companies.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
