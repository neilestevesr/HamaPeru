<div class="card-body">
    <div class="form-group">
        <label for="exampleInputEmail1">Descripción</label>
        <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
        value="{{ old('description',$company->description) }}" placeholder="Ingresa descripción">
        @if($errors->has('description'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Abreviación</label>
        <input type="text" name="abbreviation" class="form-control {{ $errors->has('abbreviation') ? 'is-invalid' : '' }}"
        value="{{ old('abbreviation',$company->abbreviation) }}" placeholder="Ingresa abreviación">
        @if($errors->has('abbreviation'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('abbreviation') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Ruc</label>
        <input type="text" name="ruc" class="form-control {{ $errors->has('ruc') ? 'is-invalid' : '' }}"
        value="{{ old('ruc',$company->ruc) }}" placeholder="Ingresa ruc">
        @if($errors->has('ruc'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('ruc') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Dirección</label>
        <input type="text" name="address" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}"
        value="{{ old('address',$company->address) }}" placeholder="Ingresa direccion">
        @if($errors->has('address'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('address') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Teléfono</label>
        <input type="text" name="telephone" class="form-control {{ $errors->has('telephone') ? 'is-invalid' : '' }}"
        value="{{ old('telephone',$company->telephone) }}" placeholder="Ingresa telefono">
        @if($errors->has('telephone'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('telephone') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Nombre de Referencia</label>
        <input type="text" name="name_ref" class="form-control {{ $errors->has('name_ref') ? 'is-invalid' : '' }}"
        value="{{ old('name_ref',$company->name_ref) }}" placeholder="Ingresa nombre de referencia">
        @if($errors->has('name_ref'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('name_ref') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Teléfono de Referencia</label>
        <input type="text" name="telephone_ref" class="form-control {{ $errors->has('telephone_ref') ? 'is-invalid' : '' }}"
        value="{{ old('telephone_ref',$company->telephone_ref) }}" placeholder="Ingresa teléfono de referencia">
        @if($errors->has('telephone_ref'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('telephone_ref') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Correo de Referencia</label>
        <input type="text" name="email_ref" class="form-control {{ $errors->has('email_ref') ? 'is-invalid' : '' }}"
        value="{{ old('email_ref',$company->email_ref) }}" placeholder="Ingresa correo de referencia">
        @if($errors->has('email_ref'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('email_ref') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="active" class="custom-control-input" {{ $company->active == 'S' ? 'checked':'' }} id="customSwitch1">
            <label class="custom-control-label" for="customSwitch1">Activo</label>
        </div>
    </div>
</div>
