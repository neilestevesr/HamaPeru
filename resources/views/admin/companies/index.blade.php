@extends('adminlte::page')

@section('title', 'Empresa')

@section('plugins.Datatables', true)

@section('content_header')
    @include('components.alerts', ['object' => 'Empresa'])
    <h1 class="card-title">EMPRESAS</h1> <a href="{{ route('companies.create') }}" class="btn btn-primary"
        style="margin-left:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Empresas</h3>

        </div>
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="table_index_company" class="table table-bordered table-striped dataTable" role="grid"
                            aria-describedby="example1_info" data-url="{{ route('companies.index') }}">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th>Descripción</th>
                                    <th>R.U.C.</th>
                                    <th style="width: 30px;">Acciones</th>
                                </tr>
                            </thead>
                            {{-- <tbody>
                                @foreach ($companies as $key => $company)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $company->id }}</td>
                                        <td><a
                                                href="{{ route('companies.show', $company->id) }}">{{ $company->description }}</a>
                                        </td>
                                        <td>{{ $company->ruc ?? '-' }}</td>

                                        <td style="text-align:center;">
                                            <a href="{{ route('companies.edit', $company->id) }}"
                                                class="rgba-white-sligh"><i class="fa fa-pencil-square-o"
                                                    aria-hidden="true"></i></a>
                                            @can('canDelete', $company)
                                                <a class="btn waves-effect" data-toggle="modal"
                                                    data-target="#eliminacion-registro-{{ $company->id }}"
                                                    href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                @include('components.deleteModal', [
                                                    'url_delete' => route('companies.destroy', $company->id),
                                                    'object' => 'Empresa',
                                                    'id' => $company->id,
                                                    'description' => $company->description,
                                                ])
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody> --}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script type="module" src="{{ asset('js/admin/companies.js') }}"></script>

    {{-- <script>
        $(function() {
            $("#example1").DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se encontró registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros)",
                    "search": "Buscar",
                    "searchPlaceholder": "Ingrese texto",
                    "paginate": {
                        "first": "Primero",
                        "previous": "Anterior",
                        "next": "Siguiente",
                        "last": "Último",
                    }
                },
                "order": []
            });
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script> --}}
@stop
