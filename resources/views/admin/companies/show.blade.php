@extends('adminlte::page')

@section('title', 'Empresa')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Empresa'])
    <div class="card-header">
        <a href="{{ route('companies.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Empresas</a>
        <a href="{{ route('companies.edit',$company->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $company->description }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Abreviación</td>
                        <td>{{ $company->abbreviation ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Ruc</td>
                        <td>{{ $company->ruc ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Dirección</td>
                        <td>{{ $company->address ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Teléfono</td>
                        <td>{{ $company->telephone ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Nombre de Referencia</td>
                        <td>{{ $company->name_ref ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Teléfono de Referencia</td>
                        <td>{{ $company->telephone_ref ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Correo de Referencia</td>
                        <td>{{ $company->email_ref ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Activo</td>
                        <td>{{ $company->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $company->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $company->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
