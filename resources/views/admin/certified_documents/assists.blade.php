@extends('adminlte::page')

@section('title', 'Certificado')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Anexo'])
    <h1 class="card-title">REGISTROS DE ASISTENCIAS</h1> <br>
@stop

@section('content')
    @include('components.filters',[
        'route' => route('certified_documents.assists'),
        'instructor_filter' => true,
        'course_filter' => true,
        'date_from_filter' => true,
        'date_to_filter' => true,
    ])

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Asistencias</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        @php $i = 0;@endphp
                        <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">#</th>
                                    <th class="sorting" rowspan="1" colspan="1">EVENTO</th>
                                    <th class="sorting" rowspan="1" colspan="1">CURSO</th>
                                    <th class="sorting" rowspan="1" colspan="1">INSTRUCTOR</th>
                                    <th class="sorting" rowspan="1" colspan="1">FECHA</th>
                                    <th class="sorting" rowspan="1" colspan="1">ARCHIVO</th>
                                    <th class="sorting" rowspan="1" colspan="1">CATEGORIA</th>
                                    <th class="sorting" rowspan="1" colspan="1">FECHA DE CARGA</th>
                                    <th class="sorting" rowspan="1" colspan="1">DESCARGA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($events as $key => $event)
                                    @foreach ($event->files as $key => $file)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ ++$i }}</td>
                                            <td class="sorting_1">{{ $event->description ?? '-' }}</td>
                                            <td class="sorting_1">{{ $event->exam->course->description ?? '-' }}</td>
                                            <td class="sorting_1">{{ $event->instructor->full_name_complete ?? '-' }}</td>
                                            <td class="sorting_1">{{ $event->date ?? '-' }}</td>
                                            <td class="sorting_1">{{ $file->name }}</td>
                                            <td class="sorting_1">{{ $file->category }}</td>
                                            <td class="sorting_1">{{ $file->created_at }}</td>
                                            <td style="text-align: center;">
                                                <a href="{{ route('files.download',$file->id) }}" class="rgba-white-sligh"><i class="fas fa-download"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(function () {
            $("#table_index").DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se encontró registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros)",
                    "search":"Buscar",
                    "searchPlaceholder":"Ingrese texto",
                    "paginate": {
                        "first": "Primero",
                        "previous": "Anterior",
                        "next": "Siguiente",
                        "last": "Último",
                    }
                },
                "order": []
            });
        });

    </script>
@stop
