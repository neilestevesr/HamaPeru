@extends('adminlte::page')

@section('title', 'Examenes')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Examen'])
    <h1 class="card-title">EXAMENES</h1> <a href="{{ route('exams.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="far fa-plus-square" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Examenes</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th class="sorting" rowspan="1" colspan="1">Título</th>
                                    <th class="sorting" rowspan="1" colspan="1">Empresa Titular</th>
                                    <th class="sorting" rowspan="1" colspan="1">Curso</th>
                                    <th class="sorting" rowspan="1" colspan="1">Subtítulo</th>
                                    <th class="sorting" rowspan="1" colspan="1">Duración(minutos)</th>
                                    <th class="sorting" rowspan="1" colspan="1">Activo</th>
                                    <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($exams as $key => $exam)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $exam->id }}</td>
                                        <td><a href="{{ route('exams.show',$exam->id) }}">{{ $exam->title ?? '-' }}</a></td>
                                        <td class="sorting_1">{{ $exam->ownerCompany->name ?? '-' }}</td>
                                        <td class="sorting_1">{{ $exam->course->description ?? '-' }}</td>
                                        <td class="sorting_1">{{ $exam->course->subtitle ?? '-' }}</td>
                                        <td class="sorting_1">{{ $exam->exam_time ?? '-' }}</td>
                                        <td class="sorting_1">{{ $exam->active == 'S' ? 'Si' : 'No' }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('exams.edit',$exam->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $exam)
                                            <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$exam->id}}"
                                                href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                @include('components.deleteModal',[
                                                    'url_delete' => route('exams.destroy',$exam->id),
                                                    'object' => 'Examen',
                                                    'id' => $exam->id,
                                                    'description' => $exam->title
                                                ])
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
    $(function () {
        $("#table_index").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No se encontró registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "search":"Buscar",
                "searchPlaceholder":"Ingrese texto",
                "paginate": {
                    "first": "Primero",
                    "previous": "Anterior",
                    "next": "Siguiente",
                    "last": "Último",
                }
            },
            "order": []
        });
    });
    </script>
@stop
