@extends('adminlte::page')

@section('title', 'Examen')

@section('content_header')
    <h1>EDITAR EXAMEN</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('exams.update',$exam->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.exams.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
