@extends('adminlte::page')

@section('title', 'Examen')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => session('question_message') ? 'Pregunta' : 'Examen'])
    <div class="card-header">
        <a href="{{ route('exams.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Examenes</a>
        <a href="{{ route('exams.edit',$exam->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $exam->title }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Empresa Titular</td>
                        <td>{{ $exam->ownerCompany->name ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Curso</td>
                        <td>{{ $exam->course->description ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>SubTítulo</td>
                        <td>{{ $exam->course->subtitle ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Duración (minutos)</td>
                        <td>{{ $exam->exam_time ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Activo</td>
                        <td>{{ $exam->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $exam->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $exam->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @include('admin.questions.index')

@stop
