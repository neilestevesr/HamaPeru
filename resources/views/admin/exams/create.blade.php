@extends('adminlte::page')

@section('title', 'Examen')

@section('content_header')
    <h1>CREAR EXAMEN</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('exams.store') }}" method="post">
            @csrf
            @include('admin.exams.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
