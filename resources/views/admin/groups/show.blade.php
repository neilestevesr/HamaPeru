@extends('adminlte::page')

@section('title', 'Grupo de Preguntas')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => session('statement_message') ? 'Pregunta' : 'Grupo de Pregunta'])
    <div class="card-header">
        <a href="{{ route('surveys.show',$group->survey_id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Encuesta</a>
        <a href="{{ route('groups.edit',$group->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $group->name }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Descripción</td>
                        <td>{{ $group->description ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @include('admin.statements.index')
@stop
