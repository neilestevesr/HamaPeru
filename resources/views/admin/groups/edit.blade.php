@extends('adminlte::page')

@section('title', 'Grupo')

@section('content_header')
    <h1>EDITAR GRUPO DE PREGUNTAS</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('groups.update',$group->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.groups.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
