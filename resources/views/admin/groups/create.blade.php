@extends('adminlte::page')

@section('title', 'Encuesta|Grupos')

@section('content_header')
    <h1>CREAR GRUPO DE PREGUNTAS</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('groups.store',$survey->id) }}" method="post">
            @csrf
            @include('admin.groups.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
