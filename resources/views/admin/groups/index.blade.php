<div class="card">
    <div class="card-header">
        <h1 class="card-title" style="padding-top: 10px;">Grupos de Preguntas</h1>
        <a href="{{ route('groups.create',$survey->id) }}" class="btn btn-primary" style="margin-left:10px;"><i class="far fa-plus-square" aria-hidden="true"></i>Crear</a>
    </div>
    <div class="card-body">
        <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12">
                    <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                        <thead>
                            <tr role="row">
                                <th style="width: 50px;">Nro</th>
                                <th class="sorting" rowspan="1" colspan="1">Grupo de Preguntas</th>
                                {{-- <th class="sorting" rowspan="1" colspan="1">Descripción</th> --}}
                                <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 0 @endphp
                            @if ($survey->groups->count() > 0)
                                @foreach ($survey->groups as $key => $group)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td><a href="{{ route('groups.show',$group->id) }}">{{ $group->name }}</a></td>
                                        {{-- <td>{{ $group->description ?? '-' }}</td> --}}
                                        <td>
                                            <a href="{{ route('groups.edit',$group->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $group)
                                            <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$group->id}}"
                                                href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                @include('components.deleteModal',[
                                                    'url_delete' => route('groups.destroy',$group->id),
                                                    'object' => 'Grupo de Pregunta',
                                                    'id' => $group->id,
                                                    'description' => $group->name
                                                ])
                                            @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@section('js')
    <script>
    $(function () {
        $("#table_index").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No se encontró registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "search":"Buscar",
                "searchPlaceholder":"Ingrese texto",
                "paginate": {
                    "first": "Primero",
                    "previous": "Anterior",
                    "next": "Siguiente",
                    "last": "Último",
                }
            },
            "searching": false,
            "lengthChange": false,
            "paging": false,
        });
        /*$('#example1').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
    });*/
    });
    </script>
@stop
