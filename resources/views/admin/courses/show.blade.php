@extends('adminlte::page')

@section('title', 'Curso')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Curso'])
    <div class="card-header">
        <a href="{{ route('courses.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Cursos</a>
        <a href="{{ route('courses.edit',$course->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    @if ($course->files->count() > 0)
        <div class="card div-img-course">
            <img src="{{ $course->files->where('file_type', 'imagenes')->last()->file_url ?? '#' }}" class="img-course" alt="" >
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $course->description }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>SubTítulo</td>
                        <td>{{ $course->subtitle ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha</td>
                        <td>{{ $course->date ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Horas</td>
                        <td>{{ $course->hours ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Hora Inicio</td>
                        <td>{{ $course->time_start_format ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Hora Fin</td>
                        <td>{{ $course->time_end_format ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Activo</td>
                        <td>{{ $course->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $course->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $course->updated_at ?? '-' }}</td>
                    </tr>
                    @foreach ($course->files->where('file_type','link') as $key => $file)
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>URL Video</td>
                        <td>{{ $file->file_path ?? '-' }}</td>

                        

                        <td><a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$file->id}}"
                                                    href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    @include('components.deleteModal',[
                                                        'url_delete' => route('files.destroy',$file->id),
                                                        'object' => 'Url',
                                                        'id' => $file->id,
                                                        'description' => $file->file_url
                                                    ])
                                                    </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>

@stop

@section('css')
    <style media="screen">
        .div-img-course{
            padding: 15px;
            max-width: 650px;
            max-height: 300px;
        }
        .img-course{
            width: auto;
            height: auto;
            max-width: 635px;
            max-height: 270px;
        }
    </style>
@stop
