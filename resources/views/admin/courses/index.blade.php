@extends('adminlte::page')

@section('title', 'Cursos')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Curso'])
    <h1 class="card-title">CURSOS</h1> <a href="{{ route('courses.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Cursos</h3>
        </div>
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th class="sorting" rowspan="1" colspan="1">Descripción</th>
                                    <th class="sorting" rowspan="1" colspan="1">SubTítulo</th>
                                    <th class="sorting" rowspan="1" colspan="1">Fecha</th>
                                    <th class="sorting" rowspan="1" colspan="1">Hora Inicio</th>
                                    <th class="sorting" rowspan="1" colspan="1">Hora Fin</th>
                                    <th style="width: 30px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses as $key => $course)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $course->id }}</td>
                                        <td><a href="{{ route('courses.show',$course->id) }}">{{ $course->description }}</a></td>
                                        <td>{{ $course->subtitle ?? '-' }}</td>
                                        <td class="sorting_1">{{ $course->date ?? '-' }}</td>
                                        <td class="sorting_1">{{ $course->time_start_format ?? '-' }}</td>
                                        <td class="sorting_1">{{ $course->time_end_format ?? '-' }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('courses.edit',$course->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $course)
                                                <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$course->id}}"
                                                    href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    @include('components.deleteModal',[
                                                        'url_delete' => route('courses.destroy',$course->id),
                                                        'object' => 'Curso',
                                                        'id' => $course->id,
                                                        'description' => $course->description
                                                    ])
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('js')
        <script>
            $(function () {
                $("#example1").DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por página",
                        "zeroRecords": "No se encontró registros",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de _MAX_ registros)",
                        "search":"Buscar",
                        "searchPlaceholder":"Ingrese texto",
                        "paginate": {
                            "first": "Primero",
                            "previous": "Anterior",
                            "next": "Siguiente",
                            "last": "Último",
                        }
                    },
                    "order": []
                });
            });
        </script>
    @stop
