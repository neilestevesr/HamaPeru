<div class="card-body">
    <div class="form-group">
        <label for="description">Descripción</label>
        <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
        value="{{ old('description',$course->description) }}" placeholder="Ingresa descripción">
        @if($errors->has('description'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="subtitle">SubTítulo</label>
        <input type="text" name="subtitle" class="form-control {{ $errors->has('subtitle') ? 'is-invalid' : '' }}"
        value="{{ old('subtitle',$course->subtitle) }}" placeholder="Ingresa subtítulo">
        @if($errors->has('subtitle'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('subtitle') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Fecha</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input id="date" type="text" name="date" class="form-control float-right {{ $errors->has('date') ? 'is-invalid' : '' }}"
            value="{{ old('date',$course->date) }}">
            @if($errors->has('date'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('date') }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="hours">Horas</label>
        <input type="text" name="hours" class="form-control {{ $errors->has('hours') ? 'is-invalid' : '' }}"
        value="{{ old('hours',$course->hours) }}" placeholder="Ingresa horas">
        @if($errors->has('hours'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('hours') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Hora inicio:</label>
        <div class="input-group date" id="timepicker_start" data-target-input="nearest">
            <div class="input-group-append" data-target="#timepicker_start" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="far fa-clock"></i></div>
            </div>
            <input type="text" name="time_start" class="form-control datetimepicker-input {{ $errors->has('time_start') ? 'is-invalid' : '' }}"
            data-target="#timepicker_start" value="{{ old('time_start',$course->time_start) }}" placeholder="Ingresa Hora de inicio">
            @if($errors->has('time_start'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('time_start') }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label>Hora fin:</label>
        <div class="input-group date" id="timepicker_end" data-target-input="nearest">
            <div class="input-group-append" data-target="#timepicker_end" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="far fa-clock"></i></div>
            </div>
            <input type="text" name="time_end" class="form-control datetimepicker-input {{ $errors->has('time_end') ? 'is-invalid' : '' }}"
            data-target="#timepicker_end" value="{{ old('time_end',$course->time_end) }}" placeholder="Ingresa Hora de fin">
            @if($errors->has('time_end'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('time_end') }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label for="exampleInputFile">Imagen</label>
        <div class="input-group {{ $errors->has('image') ? 'is-invalid' : '' }}">
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="exampleInputFile" value="{{ old('image') }}">
                <label class="custom-file-label" for="exampleInputFile">Elegir Imagen</label>
            </div>
        </div>
        @if($errors->has('image'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('image') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="exampleInputFile">Archivo (.PPT - Power Point)</label>
        <div class="input-group {{ $errors->has('file') ? 'is-invalid' : '' }}">
            <div class="custom-file">
                <input type="file" name="file" class="custom-control-input" id="exampleInputFileArch" value="{{ old('file') }}">
                <label class="custom-file-label" for="exampleInputFileArch">Elegir Archivo</label>
            </div>
        </div>
        @if($errors->has('image'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('image') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="url">URL Video  (https://www.youtube.com/embed/CODIGO)</label>
        <input type="text" name="url" class="form-control"
        value="{{ old('url') }}" placeholder="Ingresa Url Video">
        
    </div>
    <div class="form-group">
        <label for="dsc_url">Descripcion Video </label>
        <input type="text" name="dsc_url" class="form-control"
        value="{{ old('dsc_url') }}" placeholder="Ingresa Url Video">
        
    </div>

    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="active" class="custom-control-input" {{ $course->active == 'S' ? 'checked':'' }} id="customSwitch1">
            <label class="custom-control-label" for="customSwitch1">Activo</label>
        </div>
    </div>

    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="flg_public" class="custom-control-input" {{ $course->flg_public == 'S' ? 'checked':'' }} id="customSwitch2">
            <label class="custom-control-label" for="customSwitch2">Mostrar en Publico</label>
        </div>
    </div>

</div>

@push('css')
    <style media="screen">
        .custom-file-label::after {
            content: "Archivo";
        }
        .custom-file-label2::after {
            content: "Documento";
        }
    </style>
@endpush

@section('js')
    <script>
        $(function() {
            $('#date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#timepicker_start').datetimepicker({
                format: 'HH:mm'
            });

            $('#timepicker_end').datetimepicker({
                format: 'HH:mm'
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@stop
