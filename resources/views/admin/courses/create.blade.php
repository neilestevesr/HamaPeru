@extends('adminlte::page')

@section('plugins.Moment',true)
@section('plugins.Daterangepicker',true)
@section('plugins.Tempusdominus',true)
@section('plugins.Bs-custom-file-input',true)

@section('title', 'Curso')

@section('content_header')
    <h1>CREAR CURSO</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('courses.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('admin.courses.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
