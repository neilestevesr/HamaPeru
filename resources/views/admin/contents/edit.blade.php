@extends('adminlte::page')

@section('title', $content->section)

@section('plugins.Bs-custom-file-input',true)

@section('content_header')
    <h1>EDITAR {{ strtoupper($content->section) }}</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('elearnings.contents.update',$content->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @include('admin.contents.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
