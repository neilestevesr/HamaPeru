@extends('adminlte::page')

@section('plugins.Bs-custom-file-input',true)

@section('title', 'Contenido')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Contenido'])
    <div class="card-header">
        <a href="{{ route('elearnings.show',$content->elearning_id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> E-Learning</a>
        <a href="{{ route('elearnings.contents.edit',$content->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $content->title }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Sección</td>
                        <td>{{ $content->section ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Tipo</td>
                        <td>{{ $content->type ?? '-' }}</td>
                    </tr>
                    @if ($content->type == 'URL')
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Url</td>
                        <td>{{ $content->url ?? '-' }}</td> 
                    </tr>
                    @endif
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $content->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $content->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @if ($content->type == 'Imagen' || $content->type == 'Archivo' || $content->type == 'Video')
        @include('admin.contents.partials.section_multimedia')
    @endif
@stop
