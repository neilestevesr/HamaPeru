@extends('adminlte::page')

@section('title', "E-Learning|$section")

@section('plugins.Bs-custom-file-input',true)

@section('content_header')
    <h1>CREAR {{ strtoupper($section) }}</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('elearnings.contents.store',$elearning->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('admin.contents.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>
    </div>
@stop
