<div class="card-body">
    <div class="form-group">
        <label for="statement">Título</label>
        <input type="text" name="title" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
        value="{{ old('title',$content->title) }}" placeholder="Ingresa título">
        @if($errors->has('title'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('title') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <input type="hidden" name="section" class="form-control" value="{{ old('section',$section ?? $content->section) }}">
    </div>
    <div class="form-group">
        <label>Tipo</label>
        <select id="type" name="type" class="form-control select2 select2-hidden-accessible {{ $errors->has('type') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Tipo">
            @foreach ($types as $key => $type)
                <option value="{{ $type }}" {{ $content->type ? ($content->type == $type ? 'selected' :'') : '' }}>{{ $type }}</option>
            @endforeach
        </select>
        @if($errors->has('type'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('type') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group multi">
        <label>Archivos</label>
        <div class="input-group {{ $errors->has('files') ? 'is-invalid' : '' }}">
            <div class="custom-file">
                <input type="file" multiple name="files[]" class="custom-file-input" value="{{ old('files') }}">
                <label class="custom-file-label">Elegir Archivos</label>
            </div>
        </div>
        @if($errors->has('files'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('files') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group video">
        <label>Video</label>
        <div class="input-group {{ $errors->has('video') ? 'is-invalid' : '' }}">
            <div class="custom-file">
                <input type="file" name="video" class="custom-file-input" value="{{ old('video') }}">
                <label class="custom-file-label">Elegir Video</label>
            </div>
        </div>
        @if($errors->has('video'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('video') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group url">
        <label for="statement">Url</label>
        <input type="text" name="url" class="form-control {{ $errors->has('url') ? 'is-invalid' : '' }}"
        value="{{ old('url',$content->url) }}" placeholder="Ingresa url">
        @if($errors->has('url'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('url') }}</strong>
            </div>
        @endif
    </div>
</div>

@push('css')
    <style media="screen">
        .custom-file-label::after {
            content: "Archivo";
        }
    </style>
@endpush
@section('js')
    <script>
        $(function () {
            $('#type').select2();
            
            if ($('#type').val() == 'URL' )
            {
                $(".multi").css("display","none");
                $(".video").css("display","none");
                $(".url").css("display","block");
            }
            else if($('#type').val() == 'Video')
            {
                $(".multi").css("display","none");
                $(".video").css("display","block");
                $(".url").css("display","none");
            }
            else
            {
                $(".multi").css("display","block");
                $(".video").css("display","none");
                $(".url").css("display","none");
            };

            $('#type').change(function() {
                if ($('#type').val() == 'URL' )
                {
                    $(".multi").css("display","none");
                    $(".video").css("display","none");
                    $(".url").css("display","block");
                }
                else if($('#type').val() == 'Video')
                {
                    $(".multi").css("display","none");
                    $(".video").css("display","block");
                    $(".url").css("display","none");
                }
                else
                {
                    $(".multi").css("display","block");
                    $(".video").css("display","none");
                    $(".url").css("display","none");
                };
            });
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });

    </script>
@stop

