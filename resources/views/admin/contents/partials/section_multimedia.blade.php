<div class="card">
    <div class="card-body">
        <div id="table_multimedia_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12">
                    <table id="table_multimedia" class="table table-bordered table-striped dataTable" style="text-align:center; vertical-align: center;" role="grid" aria-describedby="table_multimedia_info">
                        <thead>
                            <tr role="row">
                                <th style="width: 50px;">Nro</th>
                                <th class="sorting" rowspan="1" colspan="1" style="width: 200px;">Previsualizacion</th>
                                <th class="sorting" rowspan="1" colspan="1">Nombre</th>
                                <th class="sorting" rowspan="1" colspan="1">Tipo de archivo</th>
                                <th class="sorting" rowspan="1" colspan="1">Fecha de Carga</th>
                                <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 0 @endphp
                            @foreach ($content->files as $key => $file)
                                <tr role="row" class="odd">
                                    <td class="sorting_1" style="">{{ ++$i }}</td>
                                    <td>
                                        <div class="" style="width:160px; height:120px;">
                                            @if ($file->file_type == 'imagenes')
                                                <img src="{{ $file->file_url }}" alt="" width="100%" height="100%">
                                            @elseif ($file->file_type == 'archivos')
                                                <img src="{{ asset('images/icon_doc.png') }}" width='auto' height='auto'/>
                                            @elseif ($file->file_type == 'videos')
                                                <video src="{{ $file->file_url }}" controls width="100%" height="100%"></video>
                                            @endif

                                        </div>
                                    </td>
                                    <td>{{ $file->name }}</td>
                                    <td>{{ $file->file_type }}</td>
                                    <td>{{ $file->created_at }}</td>
                                    <td>
                                        {{-- <a href="{{ route('files.download',$file->id) }}" class="rgba-white-sligh"><i class="fas fa-download"></i></a> --}}
                                        <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$file->id}}"
                                            href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            @include('components.deleteModal',[
                                            'url_delete' => route('files.destroy',$file->id),
                                            'object' => 'Archivo',
                                            'id' => $file->id,
                                            'description' => $file->name
                                            ])
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @section('js')
        <script>
            $(function () {
                $("#table_multimedia").DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por página",
                        "zeroRecords": "No se encontró registros",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de _MAX_ registros)",
                        "search":"Buscar",
                        "searchPlaceholder":"Ingrese texto",
                        "paginate": {
                            "first": "Primero",
                            "previous": "Anterior",
                            "next": "Siguiente",
                            "last": "Último",
                        }
                    },
                    "searching": false,
                    "lengthChange": false,
                    "paging": false,
                });

            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                bsCustomFileInput.init();
            });
        </script>
    @stop
