<div class="card">
    <div class="card-header">
        <h1 class="card-title" style="padding-top: 10px;">{{ $section }}</h1>
        <a href="{{ route('elearnings.contents.create',['elearning' => $elearning->id,'seccion' => $section]) }}" class="btn btn-primary" style="margin-left:10px;"><i class="far fa-plus-square" aria-hidden="true"></i> Agregar</a>
    </div>
    <div class="card-body">
        <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12">
                    <table id="table_index" class="table table-bordered table-striped dataTable table_index_class" role="grid" aria-describedby="table_index_info">
                        <thead>
                            <tr role="row">
                                <th style="width: 50px;">Nro</th>
                                <th class="sorting" rowspan="1" colspan="1">Título</th>
                                <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">Sección</th>
                                <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 0 @endphp
                            @php $contents = $elearning->contents->where('section',$section) @endphp
                            @if ($contents->count() > 0)
                                @foreach ($contents as $key => $content)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td><a href="{{ route('elearnings.contents.show',$content->id) }}">{{ $content->title }}</a></td>
                                        <td class="sorting_1">{{ $content->section ?? '-' }}</td>
                                        <td>
                                            <a href="{{ route('elearnings.contents.edit',$content->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$content->id}}"
                                                href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                @include('components.deleteModal',[
                                                    'url_delete' => route('elearnings.contents.destroy',$content->id),
                                                    'object' => 'Contenido',
                                                    'id' => $content->id,
                                                    'description' => $content->title
                                                ])
                                            </td>
                                        </tr>
                                    @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@section('js')
    <script>
    $(function () {
        $(".table_index_class").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No se encontró registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "search":"Buscar",
                "searchPlaceholder":"Ingrese texto",
                "paginate": {
                    "first": "Primero",
                    "previous": "Anterior",
                    "next": "Siguiente",
                    "last": "Último",
                }
            },
            "searching": false,
            "lengthChange": false,
            "paging": false,
        });
        /*$('#example1').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
    });*/
    });
    </script>
@stop
