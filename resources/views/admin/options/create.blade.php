@extends('adminlte::page')

@section('title', 'Enunciado|Opciones')

@section('content_header')
    <h1>CREAR OPCIÓN</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('options.store',$statement->id) }}" method="post">
            @csrf
            @include('admin.options.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
