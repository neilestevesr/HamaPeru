@extends('adminlte::page')

@section('title', 'Opciones')

@section('content_header')
    <h1>EDITAR OPCIÓN</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('options.update',$option->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.options.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
