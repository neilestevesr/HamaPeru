@extends('adminlte::page')

@section('title', 'Sala')

@section('content_header')
    <h1>CREAR SALA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('rooms.store') }}" method="post">
            @csrf
            @include('admin.rooms.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
