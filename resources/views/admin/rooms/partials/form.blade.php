<div class="card-body">
    <div class="form-group">
        <label for="description">Descripción</label>
        <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
        value="{{ old('description',$room->description) }}" placeholder="Ingresa descripción">
        @if($errors->has('description'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="capacity">Capacidad</label>
        <input type="text" name="capacity" class="form-control {{ $errors->has('capacity') ? 'is-invalid' : '' }}"
        value="{{ old('capacity',$room->capacity) }}" placeholder="Ingresa descripción">
        @if($errors->has('capacity'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('capacity') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="url_zoom">Url Zoom</label>
        <input type="text" name="url_zoom" class="form-control {{ $errors->has('url_zoom') ? 'is-invalid' : '' }}"
        value="{{ old('url_zoom',$room->url_zoom) }}" placeholder="Ingresa url">
        @if($errors->has('url_zoom'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('url_zoom') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="active" class="custom-control-input" {{ $room->active == 'S' ? 'checked':'' }} id="customSwitch1">
            <label class="custom-control-label" for="customSwitch1">Activo</label>
        </div>
    </div>
</div>
