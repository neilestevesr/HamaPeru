@extends('adminlte::page')

@section('title', 'Sala')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Sala'])
    <div class="card-header">
        <a href="{{ route('rooms.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Salas</a>
        <a href="{{ route('rooms.edit',$room->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $room->description }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Capacidad</td>
                        <td>{{ $room->capacity ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Url Zoom</td>
                        <td>{{ $room->url_zoom ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Url Activo</td>
                        <td>{{ $room->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $room->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $room->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
