@extends('adminlte::page')

@section('title', 'Sala')

@section('content_header')
    <h1>EDITAR SALA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('rooms.update',$room->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.rooms.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
