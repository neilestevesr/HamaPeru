@extends('adminlte::page')

@section('title', 'Salas')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Sala'])
    <h1 class="card-title">SALAS</h1> <a href="{{ route('rooms.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Salas</h3>

        </div>
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        @php $i = 0 @endphp
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" style="width: 50px;">#</th>
                                    <th class="sorting">Descripción</th>
                                    <th class="sorting">Capacidad</th>
                                    <th class="sorting">Activo</th>
                                    <th style="width: 30px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rooms as $key => $room)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td>
                                            <a href="{{ route('rooms.show',$room->id) }}">{{ $room->description ?? '-' }}</a>
                                        </td>
                                        <td class="sorting_1">{{ $room->capacity ?? '-' }}</td>
                                        <td class="sorting_1">{{ $room->active == 'S' ? 'Si' : 'No' }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('rooms.edit',$room->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $room)
                                                <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$room->id}}"
                                                    href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    @include('components.deleteModal',[
                                                        'url_delete' => route('rooms.destroy',$room->id),
                                                        'object' => 'Sala',
                                                        'id' => $room->id,
                                                        'description' => $room->description
                                                    ])
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('js')
        <script>
            $(function () {
                $("#example1").DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por página",
                        "zeroRecords": "No se encontró registros",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de _MAX_ registros)",
                        "search":"Buscar",
                        "searchPlaceholder":"Ingrese texto",
                        "paginate": {
                            "first": "Primero",
                            "previous": "Anterior",
                            "next": "Siguiente",
                            "last": "Último",
                        }
                    },
                    "order": []
                });
            });
        </script>
    @stop
