@extends('adminlte::page')

@section('title', 'Examen Dinamico')

@section('content_header')
    <h1>CREAR EXAMEN DINAMICO</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('dynamic_exams.store') }}" method="post">
            @csrf
            @include('admin.dynamic_exams.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
