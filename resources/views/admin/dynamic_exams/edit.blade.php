@extends('adminlte::page')

@section('title', 'Examen')

@section('content_header')
    <h1>EDITAR EXAMEN DINAMICO</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('dynamic_exams.update',$dynamic_exam->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.dynamic_exams.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
