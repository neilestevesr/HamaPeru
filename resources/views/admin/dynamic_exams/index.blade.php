@extends('adminlte::page')

@section('title', 'Examenes')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Examen'])
    <h1 class="card-title">EXAMENES DINAMICOS</h1> <a href="{{ route('dynamic_exams.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="far fa-plus-square" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Examenes</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th class="sorting" rowspan="1" colspan="1">Título</th>
                                    <th class="sorting" rowspan="1" colspan="1">Empresa Titular</th>
                                    <th class="sorting" rowspan="1" colspan="1">Curso</th>
                                    <th class="sorting" rowspan="1" colspan="1">Subtítulo</th>
                                    <th class="sorting" rowspan="1" colspan="1">Duración(minutos)</th>
                                    <th class="sorting" rowspan="1" colspan="1">Completado</th>
                                    <th class="sorting" rowspan="1" colspan="1">Activo</th>
                                    <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dynamic_exams as $key => $dynamic_exam)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $dynamic_exam->id }}</td>
                                        <td><a href="{{ route('dynamic_exams.show',$dynamic_exam->id) }}">{{ $dynamic_exam->title ?? '-' }}</a></td>
                                        <td class="sorting_1">{{ $dynamic_exam->ownerCompany->name ?? '-' }}</td>
                                        <td class="sorting_1">{{ $dynamic_exam->course->description ?? '-' }}</td>
                                        <td class="sorting_1">{{ $dynamic_exam->course->subtitle ?? '-' }}</td>
                                        <td class="sorting_1">{{ $dynamic_exam->exam_time ?? '-' }}</td>
                                        <td class="sorting_1">{{ $dynamic_exam->is_complete ? 'Si' : 'No' }}</td>
                                        <td class="sorting_1">{{ $dynamic_exam->active == 'S' ? 'Si' : 'No' }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('dynamic_exams.edit',$dynamic_exam->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $dynamic_exam)
                                            <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$dynamic_exam->id}}"
                                                href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                @include('components.deleteModal',[
                                                    'url_delete' => route('dynamic_exams.destroy',$dynamic_exam->id),
                                                    'object' => 'Examen',
                                                    'id' => $dynamic_exam->id,
                                                    'description' => $dynamic_exam->title
                                                ])
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
    $(function () {
        $("#table_index").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No se encontró registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "search":"Buscar",
                "searchPlaceholder":"Ingrese texto",
                "paginate": {
                    "first": "Primero",
                    "previous": "Anterior",
                    "next": "Siguiente",
                    "last": "Último",
                }
            },
            "order": []
        });
    });
    </script>
@stop
