<div class="card-body">
    <div class="form-group">
        <label for="title">Título</label>
        <input type="text" name="title" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
        value="{{ old('title',$dynamic_test_exam->title) }}" placeholder="Ingresa title">
        @if($errors->has('title'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('title') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Empresa Titular</label>
        <select id="owner_company" name="owner_company_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('owner_company_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Empresa Titular">
            @foreach ($owner_companies as $key => $owner_company)
                <option value="{{ $owner_company->id }}" {{ $dynamic_test_exam->ownerCompany ? ($dynamic_test_exam->ownerCompany->id  == $owner_company->id ? 'selected' :'') : '' }}>{{ $owner_company->name }}</option>
            @endforeach
        </select>
        @if($errors->has('owner_company_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('owner_company_id') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label>Curso</label>
        <select id="course" name="course_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('course_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Curso">
            @foreach ($courses as $key => $course)
                <option value="{{ $course->id }}" {{ $dynamic_test_exam->course ? ($dynamic_test_exam->course->id  == $course->id ? 'selected' :'') : '' }}>{{ $course->description . ( $course->subtitle ? ' (' . $course->subtitle .')' : '' )}}</option>
            @endforeach
        </select>
        @if($errors->has('course_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('course_id') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exam_time">Duración (minutos)</label>
        <input type="text" name="exam_time" class="form-control {{ $errors->has('exam_time') ? 'is-invalid' : '' }}"
        value="{{ old('exam_time',$dynamic_test_exam->exam_time) }}" placeholder="Ingresa duración">
        @if($errors->has('exam_time'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('exam_time') }}</strong>
            </div>
        @endif
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="active" class="custom-control-input" {{ $dynamic_test_exam->active == 'S' ? 'checked':'' }} id="customSwitch1">
            <label class="custom-control-label" for="customSwitch1">Activo</label>
        </div>
    </div>
</div>


@section('js')
    <script>
        $(function () {
            $('#course').select2()
            $('#owner_company').select2()
        })
    </script>
@stop
