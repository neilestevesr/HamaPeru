@extends('adminlte::page')

@section('title', 'Examen de Prueba')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => session('question_message') ? 'Pregunta' : 'Examen de Prueba'])
    <div class="card-header">
        <a href="{{ route('dynamic_test_exams.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Examenes de pruebas</a>
        <a href="{{ route('dynamic_test_exams.edit',$dynamic_test_exam->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $dynamic_test_exam->title }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Empresa Titular</td>
                        <td>{{ $dynamic_test_exam->ownerCompany->name ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Curso</td>
                        <td>{{ $dynamic_test_exam->course->description ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>SubTítulo</td>
                        <td>{{ $dynamic_test_exam->course->subtitle ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Duración (minutos)</td>
                        <td>{{ $dynamic_test_exam->exam_time ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Completado</td>
                        <td>{{ $dynamic_test_exam->is_complete ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Activo</td>
                        <td>{{ $dynamic_test_exam->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $dynamic_test_exam->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $dynamic_test_exam->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @php
        $dynamic_exam = $dynamic_test_exam;
    @endphp

    @include('admin.dynamic_questions.index')

@stop
