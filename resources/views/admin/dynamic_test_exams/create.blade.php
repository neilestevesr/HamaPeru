@extends('adminlte::page')

@section('title', 'Examen de Prueba')

@section('content_header')
    <h1>CREAR EXAMEN DE PRUEBA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('dynamic_test_exams.store') }}" method="post">
            @csrf
            @include('admin.dynamic_test_exams.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
