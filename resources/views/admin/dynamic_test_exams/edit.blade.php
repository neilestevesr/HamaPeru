@extends('adminlte::page')

@section('title', 'Examen de Prueba')

@section('content_header')
    <h1>EDITAR EXAMEN DE PRUEBA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('dynamic_test_exams.update',$dynamic_test_exam->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.dynamic_test_exams.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
