@extends('adminlte::page')

@section('title', 'Empresas Titular')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'EMPRESAS TITULAR'])
    <h1 class="card-title">EMPRESAS TITULAR</h1> <a href="{{ route('owner_companies.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Owner Companies</h3>
        </div>
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th >Descripción</th>
                                    <th style="width: 30px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($ownerCompanies as $key => $ownerCompany)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $ownerCompany->id }}</td>
                                        <td>{{ $ownerCompany->name }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('owner_companies.edit',$ownerCompany->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $ownerCompany)
                                                <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$ownerCompany->id}}"
                                                    href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    @include('components.deleteModal',[
                                                        'url_delete' => route('owner_companies.destroy',$ownerCompany->id),
                                                        'object' => 'Owner Company',
                                                        'id' => $ownerCompany->id,
                                                        'description' => $ownerCompany->name
                                                    ])
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('js')
        <script>
            $(function () {
                $("#example1").DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por página",
                        "zeroRecords": "No se encontró registros",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de _MAX_ registros)",
                        "search":"Buscar",
                        "searchPlaceholder":"Ingrese texto",
                        "paginate": {
                            "first": "Primero",
                            "previous": "Anterior",
                            "next": "Siguiente",
                            "last": "Último",
                        }
                    },
                    "order": []
                });
            });
        </script>
    @stop
