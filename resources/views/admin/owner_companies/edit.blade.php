@extends('adminlte::page')

@section('title', 'Owner Company')

@section('content_header')
    <h1>EDITAR EMPRESA TITULAR</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('owner_companies.update',$ownerCompany->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.owner_companies.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
