<div class="card-body">
    <div class="form-group">
        <label for="exampleInputEmail1">Nombre</label>
        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
        value="{{ old('name',$ownerCompany->name) }}" placeholder="Ingresa nombre">
        @if($errors->has('name'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </div>
        @endif
    </div>
</div>
