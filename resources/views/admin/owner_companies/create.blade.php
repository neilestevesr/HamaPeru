@extends('adminlte::page')

@section('title', 'Owner Company')

@section('content_header')
    <h1>CREAR EMPRESA TITULAR</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('owner_companies.store') }}" method="post">
            @csrf
            @include('admin.owner_companies.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
