<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Examen - HAMA</title>
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
    <style media="screen">
    @@font-face {
    font-family: arial;
    src: url('fonts/ARI.ttf');
}
.content{
    font-family: helvetica;
    width: 100%
}
.top{
    text-align: center;
    font-size: 12px;
    font-weight: bold;
    width: inherit;
}
.data{
    width: inherit;
    height: 80px;
    margin-top: 10px;
    text-align: center;
    font-size: 10px;
}
.questions-answers{
    width: inherit;
    margin-top: 20px;
}
.div-logo{
    position: absolute;
}

.logo-nexa{
    height: 16px;
    width: auto;
}
.section-data{
margin-top: 5px;
}
.section-data-text{
margin-top: 3px;
}

.data-1{
    float: left;
    height: inherit;
    width: 310px;
    padding-right: 10px;
}
.data-2{
    float: left;
    height: inherit;
    width: 205px;
    padding-left: 10px;
    padding-right: 10px;
}
.data-3{
    float: left;
    width: 150px;
    height: inherit;
}
.nota{
    font-size: 24px;
    font-weight: bold;
    margin-top: 5px;
    padding-top: 1px;
}
.border{
    border: 1px solid;
    padding-top: 1px;
    padding-bottom: 1px;
}

.question{
    margin-bottom: 20px;
}
.statement{
    font-size: 14px;
    font-style: italic;
    /* font-style: oblique; */
}
.question-cuerpo{
    margin-top: 10px;
    margin-left: 25px;
}
.alternatives{
    font-size: 11px;
}
.alternative{
    margin-top: 5px;
    margin-bottom: 5px;
}
.answer{
    font-size: 11px;
    font-weight: bold;
}

.signature{
    margin-top: 60px;
    margin-left: 25px;
    width: 250px;
    text-align: center;
}
.signature-img{
    height: 50px;
    width: auto;
}
.signature-description{
    font-size: 8px;
    width: auto;
}

.signature-bot{
    font-size: 9px;
}
hr.style1 {
  border-top: 0.1px solid;
}

.files{
    position: inherit;
    background-color: red;
    height: 90px;
    /* margin-left: 25px; */
}

.files-content{
    height: auto;
    width: 170px;
    display: inline-block;
    margin-left: 5px;
    vertical-align: top;
    text-align: center;
}

.files-img{
    height: 90px;
    width: auto;
}
</style>
</head>
<body>
    <div class="content">
        <div class="top">
            <div class="div-logo">
                        @if (empty($certification->event->EmpresaTitular))
                                  <img src="{{ asset('images/nexalogo.png') }}" class="logo-nexa" alt="">
                         @else
                              @if ($certification->event->EmpresaTitular->id == '2')
                             <img src="{{ asset('images/nexalogo.png') }}" class="logo-nexa" alt="">
                              @else
                               <img src="{{ asset('images/logo.png') }}" class="logo-nexa" alt="">
                              @endif
                         @endif



            </div>
            <div class="">
                        @if (empty($certification->event->EmpresaTitular))
                                 EXAMEN DE {{ strtoupper($certification->event->exam->course->description) }} PARA PERSONAL NUEVO NEXA
                         @else
                              @if ($certification->event->EmpresaTitular->id == '2')
                             EXAMEN DE {{ strtoupper($certification->event->exam->course->description) }} PARA PERSONAL NUEVO NEXA
                              @else
                              EXAMEN DE {{ strtoupper($certification->event->exam->course->description) }}
                              @endif
                         @endif

            </div>
        </div>

        <div class="data">
            <div class="data-1">
                <div class="section-data">
                    <div class="border" >
                        NOMBRES Y APELLIDOS
                    </div>
                    <div class="section-data-text">
                        {{ strtoupper($certification->user->full_name_complete_reverse) }}
                    </div>
                </div>
                <div class="section-data">
                    <div class="border">
                        EMPRESA
                    </div>
                    <div class="section-data-text">
                        {{ strtoupper($certification->company->description) }}
                    </div>
                </div>
            </div>
            <div class="data-2">
                <div class="section-data" style="width:120px; display: inline-block;">
                    <div class="border">
                        FECHA
                    </div>
                    <div class="section-data-text">
                        {{ $certification->event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}
                    </div>
                </div>

                <div class="section-data" style="width:80px; display: inline-block;">
                    <div class="border">
                        DNI
                    </div>
                    <div class="section-data-text">
                        {{ $certification->user->dni }}
                    </div>
                </div>

                <div class="section-data">
                    <div class="border">
                        CARGO
                    </div>
                    <div class="section-data-text">
                        {{ strtoupper($certification->user->position ?? '-') }}
                    </div>
                </div>
            </div>
            <div class="data-3">
                <div class="section-data">
                    <div class="border">
                        Nota
                    </div>
                    <div class="nota border">
                        {{ strtoupper($certification->score) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="questions-answers">
            @foreach ($certification->evaluations as $key => $evaluation)

                <div class="question">
                    <div class="statement">
                        {{ $evaluation->question_order . '.- ' }} {{ $evaluation->statement }}
                    </div>
                    @foreach ($evaluation->dynamicQuestion->files as $key => $file)
                        <div class="files-content" >
                            <img class="files-img" src="{{ $file->file_url }}" >
                        </div>
                        @if ( ($key+1) % 4 == 0)
                            <br>
                        @endif
                    @endforeach
                    <div class="question-cuerpo">
                        <div class="alternatives">
                            @foreach ($evaluation->dynamicQuestion->dynamicAlternatives as $key => $dynamic_alternative)
                                <div class="alternative">
                                    <span>{{$key+1}}) {{ $dynamic_alternative->description }}</span><br>
                                </div>
                            @endforeach
                        </div>
                        <div class="answer">
                            <span>Respuesta correcta: {{$evaluation->correct_alternative}}</span><br>
                            <span>Respuesta Marcada: {{$evaluation->selected_alternative}}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="signature">
            <div class="">
                <img src="{{ $certification->user->signature_url}}" class="signature-img" align="middle" alt="">
            </div>

            <div class="signature-description">
                <span>Documento firmado el </span><br>
                <span>{{ $certification->event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</span><br>
                <span>por el usuario</span><br>
                <span>con DNI:  {{ $certification->user->dni }}</span>
            </div>
            <hr class="style1">
            <div class="signature-bot">
                Firma del Trabajador
            </div>
        </div>
    </div>
</body>
</html>
