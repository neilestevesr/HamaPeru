<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta charset="utf-8">
		
    <title>Anexo 4</title>

   <style media="screen">
    @@font-face {
    font-family: arial;
    src: url('fonts/ARI.ttf');
}
.content{
    font-family: helvetica;
    margin-top: -15px;
    margin-left: -20px;
    margin-right: -20px;
    height: inherit;
    padding-bottom: 20px;
}
.page_break { page-break-before: always; }
.footer-page {
    position: absolute;
    bottom: 0px;
    margin-top: 10px;
    /* left: 44%; */
    margin-left: 44%;
    font-size: 10px;
    text-align: center;
}
.page-number:after { content: counter(page); }

.top-section{

}
.title{
    text-align: center;
    background-color: #DBDBDB;
    font-size: 10px;
    font-weight: bold;
    padding-top: 10px;
    padding-bottom: 10px;
}
.course{
    margin-top: 7px;
    font-size: 12px;
}
.details{
    margin-top: 25px;
}
.themes{
    height: 100px;
    width: 342px;
    display: inline-block;
    padding-right: 10px;
}
.activity{
    height: 100px;
    width: 140;
    display: inline-block;
    padding-left: 10px;
    padding-right: 10px;
}
.schedules{
    height: 100px;
    width: 140;
    display: inline-block;
    padding-left: 10px;
}
.details-title{
    border: 1px solid black;
    background-color: #DBDBDB;
    font-size: 10px;
    text-align: center;
    padding-bottom: 1px;
    padding-top: 1px;
}

.details-body{
    font-size: 10px;
    margin-top: 10px;
}
div.details-body div {
    margin-top: 5px;
}

div.activity div.details-body {
    font-weight: bold;
}

div.schedules div.details-body {
    font-weight: bold;
}
.total{
    font-size: 10px;
    width: 342px;
}
.header{
    margin-top: 10px;
    font-size: 10px;
    text-align: center;
}
div.header div {
    background-color: #DBDBDB;
    border: 1px solid black;
    padding-bottom: 1px;
}
.nombres {
    width: 340px;
    display: inline-block;
}
.dni {
    width: 80px;
    margin-left: 20px;
    display: inline-block;
}
.sede {
    width: 80px;
    margin-left: 10px;
    display: inline-block;
}
.area {
    width: 90px;
    margin-left: 10px;
    display: inline-block;
}
.firma {
    width: 104px;
    margin-left: 10px;
    display: inline-block;
}

.assists-section{
    font-size: 10px;
    margin-top: 15px;
    height: 48px;
}
.nombres-data {
    width: 340px;
    display: inline-block;
    padding-top: 20px;
}
div.nombres-data span {
    margin-left: 20px;
}
.dni-data {
    width: 50px;
    margin-left: 40px;
    display: inline-block;
    padding-top: 20px;
    text-align: center;
}
.sede-data {
    width: 78px;
    margin-left: 30px;
    display: inline-block;
    padding-top: 20px;
    text-align: center;
}
.area-data {
    width: 80px;
    margin-left: 20px;
    display: inline-block;
    padding-top: 20px;
    text-align: center;
}
.firma-data {
    width: 104px;
    margin-left: 15px;
    display: inline-block;
}



.bot-section{
    margin-top: 60px;
    position: absolute;
    bottom: 0px;
    page-break-inside: always;
}
.observation{}

.responsables{
    margin-top: 10px;
    height: 80px;
}

.responsables-header {margin-top: -5px;}
.org-header {
    width: 260px;
    display: inline-block;
}
.org-signature-header {
    width: 115px;
    display: inline-block;
}
.res-header {
    width: 253px;
    display: inline-block;
}
.res-sig-header {
    width: 115px;
    display: inline-block;
}
.responsables-body {margin-top: -10px;}

.org-body {
    width: 260px;
    display: inline-block;
    text-align: center;
}
.org-signature-body {
    width: 115px;
    display: inline-block;
}
.res-body {
    width: 253px;
    display: inline-block;
}
div.res-body div {
    padding-left: 25px;
    font-weight: bold;
}

.res-sig-body {
    width: 115px;
    display: inline-block;
}

.img-signature{
    height: auto;
    width: 100px;
}

span { font-size: 10px; }
b { font-size: 13px; }

table, td, th,tr {
  border: 3px solid black;
    
 
}


table {
    
    border-collapse: collapse;
    width: 100%;
}
</style>
</head>
<body>
   @php
        use Illuminate\Support\Str;
        $increment = 0;
        $tope = 11;
        // $theme_flag = $event->exam->course->id == 1;
        $theme_flag = Str::is('*INDUCCI*', strtoupper($event->exam->course->description) )
    @endphp
    <div class="content">

        <div class="details">
            <div class="details-table">
			
			    <table border="2">
				    <tr>
                          <td style="width:25px">
						  <img src="https://proactivo.com.pe/wp-content/uploads/2019/01/nexa-resources.png" alt="Girl in a jacket" width="170" height="70">
						  </td>
                          <td>
						  <center><b>REGISTRO DE ASISTENCIA</b></center>
						  </td>
						  <td>Código: DD-ATA-SDH-SDH-021-ES<br>
											Versión: 1.0<br>
											Area : DHO<br>
											Pagina: 1
						  </td>
						 
                      </tr>
				
				
				
				</table>
				<br>
				
                <table border="2">
					  <tr>
                          <td colspan="2"><span><center>RUC. 20100123500<br>
								Av. Circunvalación del Club Golf Los Incas Nro. 170<br>
								Urb. Club Golf Los Incas (Torre El Golf - Block A piso<br>
								22) Santiago De Surco - Lima.</center></span>
						  </td>
                          <td colspan="2"><span>Nexa Resources Atacocha S.A.A.<br>
						  Carretera La Oroya -Huanuco Nº 150 Pasco - Yanacancha<br>
						  Act. Economica : Extraccion otros minerales metaliferos no ferrosos</span>
						  </td>
						  <td colspan="1"><span>Nº Trabajadores en Centro laboral:</span>
						  </td>
						  <td colspan="1"><center><span><b>REGISTRO Nº</b></span></center></td>
                      </tr>
                      
            </table>
            <table border="2">          
					
					  <tr>
					      <td colspan="6">
               <br>                              
						  <img src="{{ asset('images/pdf/check-blanco.png') }}" alt="Girl in a jacket" width="20" height="20"><b>REUNION</b>
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 <img src="{{ asset('images/pdf/check-blanco.png') }}" alt="Girl in a jacket" width="20" height="20"><b>ENTRENAMIENTO</b>
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
						 <img src="{{ asset('images/pdf/check-blanco.png') }}" alt="Girl in a jacket" width="20" height="20"><b>CAPACITACION</b>
						 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 <img src="{{ asset('images/pdf/check-blanco.png') }}" alt="Girl in a jacket" width="20" height="20"><b>SIMULACRO DE EMERGENCIA</b>
						 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 </td>
                         
                      </tr>
					   <tr>
					      <td colspan="6">
                                                                     <br>
						  
              @if($theme_flag == true)
              <img src="{{ asset('images/pdf/check-aspa.png') }}" alt="Girl in a jacket" width="20" height="20">
              @else
               <img src="{{ asset('images/pdf/check-blanco.png') }}" alt="Girl in a jacket" width="20" height="20">  
              @endif
              
              <b>INDUCCION
                         ANEXO 4</b>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;           
						 
						 <img src="{{ asset('images/pdf/check-blanco.png') }}" alt="Girl in a jacket" width="20" height="20"><b>INDUCCION
                         ANEXO 5</b>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 <img src="{{ asset('images/pdf/check-blanco.png') }}" alt="Girl in a jacket" width="20" height="20"><b>REINDUCCION</b>
						 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    
						  @if($theme_flag == false)
              <img src="{{ asset('images/pdf/check-aspa.png') }}" alt="Girl in a jacket" width="20" height="20">
              @else
               <img src="{{ asset('images/pdf/check-blanco.png') }}" alt="Girl in a jacket" width="20" height="20">  
              @endif<b>OTROS</b>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 </td>
                         
                      </tr>
					  <tr>
                          <td colspan="6"><b>TEMA:</b> {{ strtoupper($event->exam->course->description) }}</td>
                      </tr>
					  <tr>
                          <td colspan="4"><b>EXPOSITOR:</b> {{ strtoupper($event->instructor->full_name_complete) }}</td>
                          <td colspan="2"><b>HORA INICIO:</b>{{ date("g:i a", strtotime($event->exam->course->time_start)) }}</td>
                      </tr>
					  <tr>
                          <td colspan="4"><b>FECHA:</b> {{ $event->date_carbon_instance->isoFormat('DD-MM-YYYY') }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>AREA:</b> SUPERFICIE.</td>
                          <td colspan="2"><b>HORA TERMINO:</b>{{ date("g:i a", strtotime($event->exam->course->time_end)) }} </td>
                      </tr>
					  <tr>
                          <td colspan="4"><b>LUGAR:</b> Modalidad virtual.</td>
                          <td colspan="2"><b>DURACION:</b>{{ $event->exam->course->hours }} horas</td>
                      </tr>
					  <tr>
                          <td colspan="6"></td>
                      </tr>
					  <tr>
						<th style="width:15px; background-color: rgb(204, 207, 205);" >Nº</th>
						<th style="background-color: rgb(204, 207, 205);">APELLIDOS Y NOMBRES</th>
						<th style="background-color: rgb(204, 207, 205);">DNI Nº</th> 
						<th style="background-color: rgb(204, 207, 205);">ÁREA/ E.E</th>
						<th style="background-color: rgb(204, 207, 205);">FIRMA</th>
						<th style="background-color: rgb(204, 207, 205);">OBSERVACIONES</th>
						
					  </tr>
					  
					         @foreach ($certifications as $key => $certification)
                     @if ($certification->user->um_sede == 'Atacocha' )                                    
                                                         
							      	@if (++$increment == $tope)
								      	@php
      									$tope = 14;
      									$increment = 1;
      									@endphp
									
									
								     @endif
								
    								  <tr>
    									<td style="width:15px">{{ $key+1 }}</td>
    									<td>{{strtoupper($certification->user->full_name_complete_reverse)}}</td>
    									<td><center>{{ $certification->user->dni }}</center></td> 
    									<td>{{ $certification->area }}</td>
    									<td>
                      @if ($certification->user->signature == 'S' )
    											<center><img src="{{ $certification->user->file->file_url ?? '#' }}"  style="width: 50px; height:40px;" alt=""></center>
										  @else
											<br>
											<span style="margin-left: 15px;">No tiene firma.</span>
										  @endif
                      </td>
									<td><center>{{ $certification->observation }}</center></td>
						
                     </tr>
                      @elseif($certification->user->um_sede == 'Complejo Pasco')
                      	@if (++$increment == $tope)
								      	@php
      									$tope = 14;
      									$increment = 1;
      									@endphp
									
									
								     @endif
								
    								  <tr>
    									<td style="width:15px">{{ $key+1 }}</td>
    									<td>{{strtoupper($certification->user->full_name_complete_reverse)}}</td>
    									<td><center>{{ $certification->user->dni }}</center></td> 
    									<td>{{ $certification->area }}</td>
    									<td>
                      @if ($certification->user->signature == 'S' )
    											<center><img src="{{ $certification->user->file->file_url ?? '#' }}"  style="width: 50px; height:40px;" alt=""></center>
										  @else
											<br>
											<span style="margin-left: 15px;">No tiene firma.</span>
										  @endif
                      </td>
									<td><center>{{ $certification->observation }}</center></td>
						
                     </tr>
                      @endif
								
								
						  	@endforeach
					  
			
                 </table>
                 
                 <div class="pie">

                     
                     <footer class="footer">
                     <table>
                     <tr>
                          <td colspan="3"><b>CAPACITADOR:</b></td>
                          <td colspan="3"><b>RESPONSABLE DEL REGISTRO:</b></td>
                      </tr>
					             <tr>
                          <td colspan="2"><b>NOMBRE:</b>{{ strtoupper($event->instructor->full_name_complete) }}</td>
						  <td rowspan="2"><img src="{{ $event->instructor->signature_url ?? '#' }}" class="img-signature" style="margin-left: 20px; width: 115px; height:52px;" alt=""></td>
                          <td colspan="3"><b>NOMBRE:</b>{{ strtoupper($event->responsable->full_name_complete) }}</td>
                      </tr>
					            <tr>
                          <td colspan="2"><b>CARGO: Capacitador</b></td>
						  
                          <td colspan="3"><b>CARGO:</b>{{ $event->responsable->position }}</td>
                      </tr>
					             <tr>
                          <td colspan="2"><b>FECHA:</b>{{ $event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</td>
						              <td colspan="1"><b><center>FIRMA</center></b></td>
                          <td colspan="3"><b>FECHA:</b>{{ $event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</td>
                      </tr>
                      </table>
                    </footer>
                  
               
                
                </div>
                 
                 
                 
            </div>
        </div>

    </div>
</body>
</html>
