<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Certificado</title>

    <style media="screen">
    @@font-face {
    font-family: arial;
    src: url('fonts/ARI.ttf');
}
.content{
    font-family: helvetica;
}
.section-1{
    position: absolute;
    left: 0%;
    width: 22%;
    margin-left: -20px;
}
.section-2{
    position: absolute;
    left: 26%;
    width: 70%;
    font-family: helvetica;
    margin-left: 28px;
}
.line-vertical {
    position: absolute;
    left: 25%;
    border-left: 4px solid red;
    height: 875px;
    width: 0px;
    top: 29px;
}
.signature{
    height: 120px;
    width: auto;
}

.logo{
    height: 22px;
    width: auto;
}

.emision{
    margin-top: 327px;
}
.text-center{
    text-align: center;
}

.text-left{
    text-align: left;
}
.date{
    margin-left: 17px;
    text-align: left;
}

.font-size-hama-detail{
    font-size: 14px;
}
.font-size-hama-title{
    margin-top: 30px;
    font-size: 28px;
    font-weight: bold;
}
.font-size-date{
    font-size: 11px;
}

.cuerpo-subtitle{
    font-size: 18px;
    margin-top: 30px;
}

.cuerpo-object{
    font-size: 18px;
    margin-top: 30px;
    font-weight: bold;
}

.user-cip{
    font-size: 15px;
    margin-top: 30px;
    font-weight: bold;
}

.cuerpo{
    margin-top: 50px;
}
</style>
</head>
<body>
    <div class="content">
        <div class="section-1">
            <div class="emision">
                <div class="text-center font-size-hama-detail">
                    <div class="">
                        <img src="{{ asset('images/logo.png') }}" class="logo" alt="" align=”middle”>
                    </div>
                    <div class="" style="margin-top:30px;">
                        <span>www.hamaperu.com</span>
                    </div>
                    <div class="" style="margin-top:25px;">
                        <span>Lima, Perú</span>
                    </div>
                </div>

                <div class="date font-size-date" style="margin-top:25px;">
                    <div class="">
                        <span>F. Emisión &nbsp;&nbsp;&nbsp;&nbsp;: {{ $certification->event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</span>
                    </div>
                    <div class="">
                        <span>F. Caducidad : {{ $certification->event->date_carbon_instance->addYear(1)->isoFormat('DD-MM-YYYY') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="line-vertical"></div>

        <div class="section-2">
            <div class="cuerpo text-center">
                <div class="">
                    <span>Pensar en tu propia seguridad es pensar en el bien mayor...</span>
                </div>
                <div class="" style="margin-top: 20px;">
                    <span>¡TU FAMILIA!</span>
                </div>
                <div class="font-size-hama-title">
                        <span>HAMA Perú</span>
                </div>
                <div class="cuerpo-subtitle">
                    <span>Otorga la presente constancia a:</span>
                </div>
                <div class="cuerpo-object">
                        <span>{{ strtoupper($certification->user->full_name_complete_reverse) }}</span>
                </div>
                <div class="cuerpo-subtitle">
                    <span>Por haber participado y aprobado</span><br>
                    <span>satisfactoriamente el curso de:</span>
                </div>

                <div class="cuerpo-object" style="font-size: 22px;">
                        <span>{{ strtoupper($certification->event->exam->course->description) }}</span>
                        @if ($certification->event->exam->course->subtitle)
                            <br><span>({{ strtoupper($certification->event->exam->course->subtitle) }})</span>
                        @endif
                </div>
                <div class="cuerpo-subtitle">
                   @if (empty($certification->event->EmpresaTitular))
                                <span>Válido para la Unidad Minera {{ $certification->mining_units_line ?? '-' }}</span>
                         @else
                              @if ($certification->event->EmpresaTitular->id == '2')
                             <span>Válido para la Unidad Minera {{ $certification->mining_units_line ?? '-' }}</span>
                              @else
                               <span>Válido para la empresa :</span>
                              @endif
                         @endif
                </div>
                <div class="cuerpo-object">
                       @if (empty($certification->event->EmpresaTitular))
                         <span>NEXA RESOURCES</span>
                         @else
                          @if ($certification->event->EmpresaTitular->id == '2')
                          <span>NEXA RESOURCES</span>
                         @else
                         <span>{{ strtoupper($certification->event->EmpresaTitular->name) }}</span>
                         @endif
                         @endif
                </div>
                <div class="cuerpo-subtitle text-left" style="margin-left: 170px;">

                   @if ( strtoupper($certification->event->exam->course->description) == 'MANEJO DEFENSIVO' )

                      @if($certification->score_fin == null)
                         <div class="">
                             <span>Duración : 4 horas (Teórico)</span>
                         </div>
                         <div class="" style="margin-top: 10px;">
                             <span style="top: 25px;">Nota&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $certification->score }}</span>
                         </div>
                       @else
                         <div class="">
                             <span>Duración : 4 horas (Teórico)<br>
                                               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                               4 horas (Práctico)

                             </span>
                         </div>
                         <div class="" style="margin-top: 10px;">
                             <span style="top: 25px;">Nota Final&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ round(($certification->score)*0.4 + ($certification->score_fin)*0.6) }}</span>
                         </div>
                       @endif


                    @else
                      <div class="">
                        <span>Duración : {{ $certification->event->exam->course->hours }} horas</span>
                    </div>
                    <div class="" style="margin-top: 10px;">
                        <span style="top: 25px;">Nota&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $certification->score }}</span>
                    </div>

                    @endif

                </div>
                <div class="cuerpo-subtitle">
                    <span>Pasco, {{ $certification->event->date_carbon_spanish }}</span>
                </div>

                <div class="" style="margin-top: 30px;">
                    <img src="{{ $certification->event->instructor->signature_url }}" class="signature" alt="">
                </div>
                <div class="" style="font-size: 14px;">
                    <span>Instructor SSO</span>
                </div>
                <div class="cuerpo-object" style="margin-top: 0px;">
                    <span>{{ strtoupper($certification->event->instructor->full_name_complete_reverse) }}</span>
                </div>
                @if ($certification->event->instructor->cip)
                    <div class="user-cip" style="margin-top: 0px;">
                        <span>{{ 'CIP '. $certification->event->instructor->cip }}</span>
                    </div>

                @endif
            </div>
        </div>
    </div>
</body>
</html>
