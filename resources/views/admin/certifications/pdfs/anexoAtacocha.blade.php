<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Anexo 4</title>

    <style media="screen">
    @@font-face {
    font-family: arial;
    src: url('fonts/ARI.ttf');
}
.content{
    margin-top: 20px;
    font-family: helvetica;
}

.div-sello-parada{
    position: absolute;
    transform: translateX(550px) translateY(0px);
}

.img-sello-parada{
    width: 140px;
    height: auto;
}


.center{
    text-align: center;
}
.top-1{
    font-size: 18px;
}
.top-2{
    font-size: 14px;
}
.details{
    margin-top: 15px;
}
.details-top{
    text-align: center;
    font-size: 14px;
}
.details-table{
    margin-top: 5px;
    font-size: 12px;
}
.imagenes{
    margin-top: 20px;
    margin-left: -20px;
    margin-right: -20px;
    height: 550px;
}

.nexa-vb{
  margin: 0;
  position: absolute;
  top: 430%;
  left: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}



.div-sello{
    position: absolute;
    transform: translateX(0px) translateY(180px);
    height: 200px;
    width: 110px;
}
.img-sello{
    width: 110px;
    height: auto;
}
.img-firma-sello{
    width: 110px;
    height: auto;
}
.txt-firma-sello{
    font-size: 8px;
    font-weight: bold;
    text-align: center;
}


.div-anexo{
    position: absolute;
    transform: translateX(110px) translateY(0px);
}
.img-anexo{
    width: 600px;
    height: auto;
}
.cuerpo-subtitle{
    font-size: 18px;
    margin-top: 10px;
    text-align: center;
}
.signature-section{
    margin-top: 20px;
}

.signature-employee{
    margin-top: 0px;
    margin-left: 60px;
    width: 250px;
    text-align: center;
    display: inline-block;
    float: left;
}
.signature-employee-img{
    height: 70px;
    width: auto;
}
.signature-employee-description{
    font-size: 10px;
    width: auto;
}

.signature-employee-bot{
    font-size: 12px;
}

.signature-manager{
    margin-top: 0px;
    margin-right: 50px;
    width: 250px;
    text-align: center;
    font-size: 10px;
    display: inline-block;
    float: right;
}

.signature-manager-img{
    height: 70px;
    width: auto;
}

.signature-manager-description{
    margin-top: 5px;
    font-size: 10px;
}

hr.style1 {
  border-top: 0.1px solid;
}


table, td, th {
  border: 1px solid black;

}
td{
    width: 50%;
    padding: 3px;
}

table {
  border-collapse: collapse;
  width: 100%;
}

</style>
</head>
<body>

       @if ($certification->event->exam->course->id == 74)
           <div class="div-sello-parada">
            <img src="{{ asset('images/pdf/sello_te.png') }}" class="img-sello-parada" alt="">
            </div>
      @elseif ($certification->event->exam->course->id == 15)
           <div class="div-sello-parada">
            <img src="{{ asset('images/pdf/sello_parada_planta.png') }}" class="img-sello-parada" alt="">
        </div>
      @else
          <div class="div-sello-parada">
            <img src="{{ asset('images/pdf/sello_anexo_4.png') }}" class="img-sello-parada" alt="">
        </div>
      @endif
    <div class="content">

        <div class="center">
            <div class="top-1">
                <strong>ANEXO N° 4</strong>
            </div>
            <div class="top-2">
                <span>INDUCCIÓN Y ORIENTACIÓN BÁSICA</span>
            </div>
            <div class="details-top">
                PARA USO DE LA GERENCIA DE SEGURIDAD Y SALUD OCUPACIONAL
            </div>
        </div>
        <div class="details">
            <div class="details-table">
                <table>
                    <tr>
                        <td>Titular: {{ strtoupper($mining_unit->owner) }}</td>
                        <td>Trabajador: {{ strtoupper($certification->user->full_name_complete_reverse) }}</td>
                    </tr>
                    <tr>
                        <td>ECM : {{ $certification->company->abbreviation }} </td>
                        <td>Fecha de Ingreso: {{ $certification->event->date_carbon_instance->addDay(1)->isoFormat('DD/MM/YYYY') }}</td>
                    </tr>
                    <tr>
                        <td>Unidad de Producción: {{ strtoupper($mining_unit->description) }}</td>
                        <td>Registro o N° de Fotocheck: {{ $certification->user->dni }}</td>
                    </tr>
                    <tr>
                        <td>Distrito: {{ strtoupper($mining_unit->district) }}</td>
                        <td>Ocupación: {{ strtoupper($certification->user->position) }}</td>
                    </tr>
                    <tr>
                        <td>Provincia: {{ strtoupper($mining_unit->Province) }}</td>
                        <td>Area de Trabajo: MINA / SUPERFICIE</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="imagenes">
            <div class="div-sello">
                @php
                    $year = explode('-',$certification->event->date)[0];
                @endphp
                <img src="{{ asset("images/pdf/sello_$year.png") }}" class="img-sello" alt="">
                <div class="">
                    @if ($certification->event->instructor->signature == 'S' )
                        <img src="{{ $certification->event->instructor->signature_url }}" class="img-firma-sello" alt="">
                    @else
                        <span style="margin-left: 15px; font-size: 12px; font-weight: normal;">No tiene firma.</span>
                    @endif

                </div>
                <div class="txt-firma-sello">
                    Documento firmado el <br>
                    {{ $certification->event->date_carbon_instance->isoFormat('DD-MM-YYYY') }} <br>
                    por el instructor S.S.O <br>
                    <span>{{ 'con CIP: '. $certification->event->instructor->cip }}</span>
                </div>
            </div>


            <div class="div-anexo">
                <img src="{{ asset('images/pdf/anexo4.png') }}" class="img-anexo" alt="">
            </div>


        </div>

        <div class="">
            <div class="cuerpo-subtitle">
                <span>Pasco, {{ $certification->event->date_carbon_spanish }}</span>
            </div>
        </div>

        <div class="signature-section">
            <div class="signature-employee">
                <div class="">
                    <img src="{{ $certification->user->signature_url}}" class="signature-employee-img" align="middle" alt="">
                </div>

                <div class="signature-employee-description">
                    <span>Documento firmado el</span><br>
                    <span>{{ $certification->event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</span><br>
                    <span>por el usuario</span><br>
                    <span>con DNI:  {{ $certification->user->dni }}</span>
                </div>
                <hr class="style1">
                <div class="signature-employee-bot">
                    Firma del Trabajador
                </div>
            </div>
                 <div class="nexa-vb">
                    @if ( strtoupper($mining_unit->description) == 'ATACOCHA' )

                       <img src="{{ asset('images/pdf/vb-nexa.png') }}" class="signature-manager-img" alt="">

                    @endif
                  </div>

            <div class="signature-manager">
                <div class="">

                    @if ( strtoupper($mining_unit->description) == 'ATACOCHA' )

                     <img src="https://hamaperubucketproduction.s3.amazonaws.com/imagenes/firmas/{{ $certification->event->id }}_{{  $certification->event->seguridad->dni }}.png" class="signature-manager-img" align="middle" alt="">

                    @endif





                </div>

                 <div class="signature-manager-description">

                    @if ( strtoupper($mining_unit->description) == 'ATACOCHA' )
                     <span>Documento firmado el </span><br>
                    <span>{{ $certification->event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</span><br>
                    <span>por: {{ $certification->event->seguridad->name }} {{ $certification->event->seguridad->paternal }} {{ $certification->event->seguridad->maternal }}</span><br>
                    <span>con CIP:  {{ $certification->event->seguridad->cip }}</span>
            	    @endif



                </div>
                <hr class="style1">
                <span>VºBº del Gerente de Seguridad y Salud</span><br>
                <span>Ocupacional o Ingeniero de Seguridad</span>
            </div>
        </div>
    </div>
</body>
</html>
