<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Carta de Compromiso</title>

    <style media="screen">
    @@font-face {
    font-family: arial;
    src: url('fonts/ARI.ttf');
}
.content{
    font-family: helvetica;
}
.page_break { page-break-before: always; }
.center{ text-align: center; }
.title{
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 10px;
}
.fecha{
    margin-top: 20px;
    margin-bottom: 10px;
}
.usuario{
    margin-top: 15px;
    margin-left: 70px;
}
.presentacion{
    margin-top: 15px;
    margin-bottom: 15px;
}
.datos{

}
.div-compromiso1{
    margin-top: 25px;
    margin-left: 70px;
}
.img-compromiso{
    height: auto;
    width: 620px;
}

.signature-section{

}
.signature-manager{
    margin-top: 20px;
    height: 40px;
}
.manager-nombre{
    position: absolute;
    /* transform: translateX(50px) translateY(10px); */
    text-align: center;
    float: left;
    margin-left: 65px;
    width: 300px;
    height: inherit;

}
.manager-firma{
    position: absolute;
    /* transform: translateX(350px) translateY(10px); */
    float: right;
    margin-right: 65px;
    width: 230px;
    height: inherit;
    text-align: center;
    font-size: 14px;
}
.signature-user{
    margin-top: 10px;
}
.user-datos{
    font-size: 16px;
    margin-left: 65px;
}
.user-firma{
    margin-top: 5px;
    font-size: 10px;

}
.user-firma-detalles{
    display: inline-block;
    margin-left: 370px;
}
.div-img-firma{
    display: inline-block;
}

.signature-user-img{
    height: 70px;
    width: auto;
}
hr.style1 {
    border-top: 0.1px solid;
}
</style>
</head>
<body>
    @php
    use Illuminate\Support\Str;

    if( Str::is( '*PORVENIR*', strtoupper($mining_unit->description)) ){
        $url_image_1 = asset('images/pdf/porvenir_old.png');
        $url_image_2 = asset('images/pdf/porvenir2_old.png');
    }
    elseif( Str::is( '*ATACOCHA*', strtoupper($mining_unit->description)) )
    {
        $url_image_1 = asset('images/pdf/atacocha_old.png');
        $url_image_2 = asset('images/pdf/atacocha2_old.png');
    }
    elseif( Str::is( '*CERRO*', strtoupper($mining_unit->description)) )
    {
        $url_image_1 = asset('images/pdf/cerro1.png');
        $url_image_2 = asset('images/pdf/cerro2.png');
    }
    @endphp
    <div class="content">
        <div class="">
            <div class="">
                <div class="title center">
                    CARTA COMPROMISO - REGLAS DE ORO, REGLAS POR LA VIDA
                </div>
                <div class="fecha center">
                    {{ ucwords(strtolower($mining_unit->district)) }}, {{ $certification->event->date_carbon_spanish }}
                </div>
                <div class="usuario">
                    <div class="presentacion">
                        Señor/a
                    </div>
                    <div class="datos">
                        <span>
                            {{ strtoupper($certification->user->full_name_complete_reverse) }}
                        </span>
                        <span style="margin-left:15px;">
                            DNI: {{ $certification->user->dni }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="div-compromiso1">
                <img src="{{ $url_image_1 }}" class="img-compromiso" alt="">
            </div>
        </div>
        <div class="page_break">
            <div class="div-compromiso1">
                <img src="{{ $url_image_2 }}" class="img-compromiso" alt="">
            </div>
            <div class="signature-section">
                <div class="signature-manager">
                    <div class="manager-nombre">
                        @if($certification->event->date >= '2022-05-09' && $certification->event->date < '2022-08-16')
                        <span style="font-weight: ; font-style: italic;">
                            Julio Luna
                        </span><br>
                        @elseif ($certification->event->date >= '2022-08-16')
                        <span style="font-weight: ; font-style: italic;">
                            Carlos Garcia
                        </span><br>
                        @else
                        <span style="font-weight: ; font-style: italic;">
                         @if( Str::is( '*CERRO*', strtoupper($mining_unit->description)) )
                              Carlos Garcia
                          @else
                              Jorge Bonilla
                          @endif
                            
                        
                        </span><br>
                        @endif
                        
                        
                        <span style="font-size: 13px;">
                          @if( Str::is( '*PORVENIR*', strtoupper($mining_unit->description)) )
                            Gerente General del Complejo Pasco
                          @elseif( Str::is( '*ATACOCHA*', strtoupper($mining_unit->description)) )
                            Gerente General del Complejo Pasco
                          @elseif( Str::is( '*CERRO*', strtoupper($mining_unit->description)) )
                            Gerente General de Cerro Lindo 
                          @endif
                        </span>
                    </div>
                    <div class="manager-firma">
                        <hr class="style1">
                        <span>Residente Empresa Especializada</span>
                    </div>
                </div>

                <div class="signature-user">
                    <div class="user-datos">
                        <div class="">
                            <span>
                                Apellidos y Nombres: {{ strtoupper($certification->user->full_name_complete_reverse) }}
                            </span>
                            <span style="margin-left:15px;">
                                DNI: {{ $certification->user->dni }}
                            </span>
                        </div>
                        <div class="">
                            Empresa Especializada: {{ strtoupper($certification->company->abbreviation) }}
                        </div>
                        <div class="">
                            <span>
                                Fecha: {{ $certification->event->date_carbon_instance->isoFormat('DD / MM / YYYY') }}
                            </span>
                            <span style="margin-left:150px;">
                                Firma:
                            </span>
                        </div>
                    </div>

                    <div class="user-firma">
                        <div class="user-firma-detalles">
                            <span>Documento firmado el </span><br>
                            <span>{{ $certification->event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</span><br>
                            <span>por el usuario</span><br>
                            <span>con DNI:  {{ $certification->user->dni }}</span>
                        </div>
                        <div class="div-img-firma">
                            <img src="{{ $certification->user->signature_url}}" class="signature-user-img" style="height:70px; width: auto;" align="middle" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

