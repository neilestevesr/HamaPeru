<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Asistencias</title>

    <style media="screen">
    @@font-face {
    font-family: arial;
    src: url('fonts/ARI.ttf');
}
.content{
    font-family: helvetica;
    margin-top: -15px;
    margin-left: -20px;
    margin-right: -20px;
    height: inherit;
    padding-bottom: 20px;
}
.page_break { page-break-before: always; }
.footer-page {
    position: absolute;
    bottom: 0px;
    margin-top: 10px;
    /* left: 44%; */
    margin-left: 44%;
    font-size: 12px;
    text-align: center;
}
.page-number:after { content: counter(page); }

.top-section{

}
.title{
    text-align: center;
    background-color: #DBDBDB;
    font-size: 10px;
    font-weight: bold;
    padding-top: 10px;
    padding-bottom: 10px;
}
.course{
    margin-top: 7px;
    font-size: 12px;
}
.details{
    margin-top: 25px;
}
.themes{
    height: 100px;
    width: 342px;
    display: inline-block;
    padding-right: 10px;
}
.activity{
    height: 100px;
    width: 140;
    display: inline-block;
    padding-left: 10px;
    padding-right: 10px;
}
.schedules{
    height: 100px;
    width: 140;
    display: inline-block;
    padding-left: 10px;
}
.details-title{
    border: 1px solid black;
    background-color: #DBDBDB;
    font-size: 10px;
    text-align: center;
    padding-bottom: 1px;
    padding-top: 1px;
}

.details-body{
    font-size: 10px;
    margin-top: 10px;
}
div.details-body div {
    margin-top: 5px;
}

div.activity div.details-body {
    font-weight: bold;
}

div.schedules div.details-body {
    font-weight: bold;
}
.total{
    font-size: 10px;
    width: 342px;
}
.header{
    margin-top: 10px;
    font-size: 10px;
    text-align: center;
}
div.header div {
    background-color: #DBDBDB;
    border: 1px solid black;
    padding-bottom: 1px;
}
.nombres {
    width: 340px;
    display: inline-block;
}
.dni {
    width: 80px;
    margin-left: 20px;
    display: inline-block;
}
.sede {
    width: 80px;
    margin-left: 10px;
    display: inline-block;
}
.area {
    width: 90px;
    margin-left: 10px;
    display: inline-block;
}
.firma {
    width: 104px;
    margin-left: 10px;
    display: inline-block;
}

.assists-section{
    font-size: 10px;
    margin-top: 15px;
    height: 48px;
}
.nombres-data {
    width: 340px;
    display: inline-block;
    padding-top: 20px;
}
div.nombres-data span {
    margin-left: 20px;
}
.dni-data {
    width: 50px;
    margin-left: 40px;
    display: inline-block;
    padding-top: 20px;
    text-align: center;
}
.sede-data {
    width: 78px;
    margin-left: 30px;
    display: inline-block;
    padding-top: 20px;
    text-align: center;
}
.area-data {
    width: 80px;
    margin-left: 20px;
    display: inline-block;
    padding-top: 20px;
    text-align: center;
}
.firma-data {
    width: 104px;
    margin-left: 15px;
    display: inline-block;
}



.bot-section{
    margin-top: 60px;
    position: absolute;
    bottom: 0px;
    page-break-inside: always;
}
.observation{}

.responsables{
    margin-top: 10px;
    height: 80px;
}

.responsables-header {margin-top: -5px;}
.org-header {
    width: 260px;
    display: inline-block;
}
.org-signature-header {
    width: 115px;
    display: inline-block;
}
.res-header {
    width: 253px;
    display: inline-block;
}
.res-sig-header {
    width: 115px;
    display: inline-block;
}
.responsables-body {margin-top: -10px;}

.org-body {
    width: 260px;
    display: inline-block;
    text-align: center;
}
.org-signature-body {
    width: 115px;
    display: inline-block;
}
.res-body {
    width: 253px;
    display: inline-block;
}
div.res-body div {
    padding-left: 25px;
    font-weight: bold;
}

.res-sig-body {
    width: 115px;
    display: inline-block;
}

.img-signature{
    height: auto;
    width: 100px;
}


table, td, th {
  border: 1px solid black;

}
td{
    width: 50%;
    padding: 3px;
}

table {
  border-collapse: collapse;
  width: 100%;
}
</style>
</head>
<body>
    @php
        use Illuminate\Support\Str;
        $increment = 0;
        $tope = 11;
        // $theme_flag = $event->exam->course->id == 1;
        $theme_flag = Str::is('*INDUCCI*', strtoupper($event->exam->course->description) )
    @endphp
    <div class="content">
        <div class="top-section">
            <div class="title">
                REGISTRO DE ASISTENCIA
            </div>
            <div class="course">
                <table>
                    <tr>
                        <td style="background-color: #DBDBDB; width: 25%;">NOMBRE DE LA ACTIVIDADDD</td>
                        <td style="text-align: center;">{{ strtoupper($event->exam->course->description) }}</td>
                    </tr>
                </table>
            </div>
            <div class="details">
                <div class="themes">
                    <div class="details-title">
                        TEMAS DESARROLLADOS
                    </div>
                    <div class="details-body">
                        <div>1.- {{ $theme_flag ? 'Introducción al curso' : 'Objetivo, alcance y referencia legal.' }}</div>
                        <div>2.- {{ $theme_flag ? 'Conociendo la unidad.' : 'Definiciones.' }}</div>
                        <div>3.- {{ $theme_flag ? 'Gestión de la seguridad.' : 'Peligros y riesgos.' }}</div>
                        <div>4.- {{ $theme_flag ? 'Trabajos de alto riesgo en la unidad minera.' : 'Controles Operacionales.' }}</div>
                        <div>5.- {{ $theme_flag ? 'Higiene ocupacional: agentes físicos, químicos y biológicos.' : 'Controles criticos.' }}</div>
                    </div>
                </div>
                <div class="activity">
                    <div class="details-title">
                        TIPO DE ACTIVIDAD
                    </div>
                    <div class="details-body">
                        <div>Inducción ({{ $theme_flag ? 'X' : ' ' }})</div>
                        <div>Capacitación ({{ $theme_flag ? ' ' : 'X' }})</div>
                        <div>Simulacro de emergencia( )</div>
                        <div>Reunión ( )</div>
                        <div>Otros ( )</div>
                    </div>
                </div>
                <div class="schedules">
                    <div class="details-title">
                        PROGRAMACIÓN
                    </div>
                    <div class="details-body">
                        <div>Fecha: {{ $event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</div>
                        <div>Hora inicio: {{ date("g:i a", strtotime($event->exam->course->time_start)) }}</div>
                        <div>Hora final: {{ date("g:i a", strtotime($event->exam->course->time_end)) }}</div>
                        <div>Duración: {{ $event->exam->course->hours }} horas</div>
                        <div>Lugar: PASCO</div>
                    </div>
                </div>
            </div>
            <div class="total">
                <table>
                    <tr>
                        <td style="background-color: #DBDBDB; width: 75%; padding: 1px;">N° TOTAL DE TRABAJADORES EN LA EMPRESA</td>
                        <td style="text-align: center; width: 25%; padding: 1px;">{{ $certifications->count() }}</td>
                    </tr>
                </table>
            </div>
            <div class="header">
                <div class="nombres">
                    APELLIDOS Y NOMBRES
                </div>
                <div class="dni">
                    DNI
                </div>
                <div class="sede">
                    UM/SEDE
                </div>
                <div class="area">
                    AREA
                </div>
                <div class="firma">
                    FIRMA
                </div>
            </div>
        </div>

        @foreach ($certifications as $key => $certification)
        @if ($certification->user->um_sede == 'Atacocha' )
          @if (++$increment == $tope)
                @php
                $tope = 14;
                $increment = 1;
                @endphp
                <div class="footer-page">
                    <span class="page-number">Página </span>
                </div>
                <div class="page_break"></div>
            @endif
            <div class="assists-section">
                <div class="nombres-data">
                    <span>{{ $key+1 . ') ' . strtoupper($certification->user->full_name_complete_reverse) }}</span>
                </div>
                <div class="dni-data">
                    {{ $certification->user->dni }}
                </div>
                <div class="sede-data">
                    {{ $certification->user->um_sede }}
                </div>
                <div class="area-data">
                    Superficie/mina
                </div>
                <div class="firma-data">
                    @if ($certification->user->signature == 'S' )
                        <img src="{{ $certification->user->file->file_url ?? '#' }}" class="img-signature" style="width: 115px; height:60px;" alt="">
                    @else
                        <br>
                        <span style="margin-left: 15px;">No tiene firma.</span>
                    @endif
                </div>
            </div>
         @elseif($certification->user->um_sede == 'Complejo Pasco')
           @if (++$increment == $tope)
                @php
                $tope = 14;
                $increment = 1;
                @endphp
                <div class="footer-page">
                    <span class="page-number">Página </span>
                </div>
                <div class="page_break"></div>
            @endif
            <div class="assists-section">
                <div class="nombres-data">
                    <span>{{ $key+1 . ') ' . strtoupper($certification->user->full_name_complete_reverse) }}</span>
                </div>
                <div class="dni-data">
                    {{ $certification->user->dni }}
                </div>
                <div class="sede-data">
                    Atacocha
                </div>
                <div class="area-data">
                    Superficie/mina
                </div>
                <div class="firma-data">
                    @if ($certification->user->signature == 'S' )
                        <img src="{{ $certification->user->file->file_url ?? '#' }}" class="img-signature" style="width: 115px; height:60px;" alt="">
                    @else
                        <br>
                        <span style="margin-left: 15px;">No tiene firma.</span>
                    @endif
                </div>
            </div>
        
        @endif
       

         
        @endforeach

        <div class="bot-section">
            <!--<div class="observation">
                <table style="border-collapse: none;border-style:none; border: 1px;">
                    <tr>
                        <td style="background-color: #DBDBDB; width: 25%; padding: 1px; font-size: 9px;">OBSERVACIONES </td>
                        <td style="width: 75%; padding: 1px;"> </td>
                    </tr>
                </table>
            </div>-->
            <div class="responsables">
                <div class="responsables-header">
                    <div class="details-title org-header">
                        CAPACITADOR ORGANIZADOR
                    </div>
                    <div class="details-title org-signature-header">
                        FIRMA
                    </div>
                    <div class="details-title res-header">
                        RESPONSABLE DE REGISTRO
                    </div>
                    <div class="details-title res-sig-header">
                        FIRMA
                    </div>
                </div>

                <div class="responsables-body">
                    <div class="details-body org-body">
                        <!--{{ strtoupper($event->instructor->full_name_complete) }}-->
                    </div>
                    <div class="org-signature-body">
                        <img src="{{ $event->instructor->signature_url ?? '#' }}" class="img-signature" style="margin-left: 20px; width: 115px; height:52px;" alt="">
                    </div>
                    <div class="details-body res-body">
                        <div>Nombre: {{ strtoupper($event->responsable->full_name_complete) }}</div>
                        <div>Cargo: {{ $event->responsable->position }}</div>
                        <div>Fecha: {{ $event->date_carbon_instance->isoFormat('DD-MM-YYYY') }}</div>
                    </div>
                    <div class="res-sig-body">
                        <img src="{{ $event->responsable->signature_url ?? '#' }}" class="img-signature" style="margin-left: 20px; width: 115px; height:52px;" alt="">
                    </div>
                </div>
                <div class="" style="font-size:12px; margin-left: 44%;">
                    <span class="page-number">Página </span>
                </div>

            </div>

        </div>
    </div>
</body>
</html>
