@extends('adminlte::page')

@section('title', 'Asistencia')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Asistencia'])
    <h1 class="card-title">ASISTENCIAS PERSONALES</h1> <br>
@stop

@section('content')

    @include('components.filters',[
        'route' => route('certifications.individual_assist'),
        'dni_filter' => true,
        'company_filter' => true,
        'course_filter' => true,
        'date_from_filter' => true,
        'date_to_filter' => true,
    ])

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Participantes</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        @php $i = 0;@endphp
                        <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 30px;">#</th>
                                    <th class="sorting" rowspan="1" colspan="1">DNI</th>
                                    <th class="sorting" rowspan="1" colspan="1">APELLIDOS Y NOMBRES</th>
                                    <th class="sorting" rowspan="1" colspan="1">EMPRESA</th>
                                    <th class="sorting" rowspan="1" colspan="1">ID EVENTO</th>
                                    <th class="sorting" rowspan="1" colspan="1">EVENTO</th>
                                    <th class="sorting" rowspan="1" colspan="1">CURSO</th>
                                    <th class="sorting" rowspan="1" colspan="1">FECHA</th>
                                    <th class="sorting" rowspan="1" colspan="1">NOTA</th>
                                    <th class="sorting" rowspan="1" colspan="1" style="width: 30px;">ASISTENCIAS</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($certifications as $key => $certification)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td class="sorting_1">{{ $certification->user->dni }}</td>
                                        <td class="sorting_1">{{ $certification->user->full_name_complete_reverse }}</td>
                                        <td class="sorting_1">{{ $certification->company->description }}</td>
                                          <td class="sorting_1">{{ $certification->event->id }}</td>
                                        <td class="sorting_1">{{ $certification->event->description }}</td>
                                        <td class="sorting_1">{{ $certification->event->exam->course->description }}</td>
                                        <td class="sorting_1">{{ $certification->event->date ?? '-' }}</td>
                                        <td class="sorting_1">{{ $certification->score ?? '-' }}</td>
                                        <td style="text-align:center;"><a href="{{ route('certifications.individual_assist_pdf',$certification->id) }}"><img src="{{ asset('images/pdf.png') }}" width='25' height='25'/></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(function () {
            $("#table_index").DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se encontró registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros)",
                    "search":"Buscar",
                    "searchPlaceholder":"Ingrese texto",
                    "paginate": {
                        "first": "Primero",
                        "previous": "Anterior",
                        "next": "Siguiente",
                        "last": "Último",
                    }
                },
                "order": []
            });
        });

    </script>
@stop
