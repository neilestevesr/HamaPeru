@extends('adminlte::page')

@section('title', 'Certificado')

@section('plugins.Datatables', true)

@section('content_header')
    @include('components.alerts', ['object' => 'Certificado'])
    <h1 class="card-title">CERTIFICADOS</h1> <br>
@stop

@section('content')
    <div id="filter_box_container">
        @include('components.filters', [
            'route' => route('certifications.index'),
            'dni_filter' => true,
            'company_filter' => true,
            'course_filter' => true,
            'date_from_filter' => true,
            'date_to_filter' => true,
        ])
    </div>


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Certificados</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="table_index_certification" class="table table-bordered table-striped dataTable"
                            role="grid" data-url="{{ route('certifications.index') }}"
                            aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 30px;">Id</th>
                                    <th>DNI</th>
                                    <th>APELLIDOS Y NOMBRES</th>
                                    <th>EMPRESA</th>
                                    <th>CURSO</th>
                                    <th>FECHA</th>
                                    <th>NOTA</th>
                                    <th>EXAMEN</th>
                                    <th>CERTIFICADO</th>
                                    <th>ANEXO 4 ATACOCHA</th>
                                    <th>ANEXO 4 EL PORVENIR</th>
                                    <th>ANEXO 4 SINAYCOCHA</th>
                                    <th>ANEXO 4 CERRO LINDO</th>
                                    <th>CARTA COMPROMISO_1</th>
                                    <th>CARTA COMPROMISO_2</th>
                                    <th>CARTA COMPROMISO_3</th>
                                    <th>CARTA COMPROMISO_4</th>
                                </tr>
                            </thead>
                            {{-- <tbody>
                                @foreach ($certifications as $key => $certification)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $certification->id }}</td>
                                        <td class="sorting_1">{{ $certification->user->dni }}</td>
                                        <td class="sorting_1">{{ $certification->user->full_name_complete_reverse }}</td>
                                        <td class="sorting_1">{{ $certification->company->description }}</td>
                                        <td class="sorting_1">{{ $certification->event->exam->course->description }}</td>
                                        <td class="sorting_1">{{ $certification->event->date ?? '-' }}</td>
                                        <td class="sorting_1">{{ $certification->score ?? '0' }}</td>

                                        
                                        <td style="text-align:center;">
                                            @can('canDownloadExam', $certification)
                                                <a href="{{ route('certifications.exam_pdf', $certification->id) }}"><img
                                                        src="{{ asset('images/examen.png') }}" width='25'
                                                        height='25' /></a>
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>


                                        <td style="text-align:center;">
                                            @can('canDownloadCertificate', $certification)
                                                @if (empty($certification->event->EmpresaTitular))
                                                    <a
                                                        href="{{ route('certifications.certificate_pdf', $certification->id) }}"><img
                                                            src="{{ asset('images/certificado.png') }}" width='25'
                                                            height='25' /></a>
                                                @else
                                                    @if ($certification->event->EmpresaTitular->id == '2')
                                                        <a
                                                            href="{{ route('certifications.certificate_pdf', $certification->id) }}"><img
                                                                src="{{ asset('images/certificado.png') }}" width='25'
                                                                height='25' /></a>
                                                    @else
                                                        <a
                                                            href="{{ route('certifications.certificate_pdf_externos', $certification->id) }}"><img
                                                                src="{{ asset('images/certificado.png') }}" width='25'
                                                                height='25' /></a>
                                                    @endif
                                                @endif
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>
                                        <td style="text-align:center;">
                                            @can('canDownloadAnexo', [$certification, 'atacocha'])
                                                @if ($certification->event->id < '3420')
                                                    <a href="{{ route('certifications.anexo4old', [$certification->id, 'atacocha']) }}"
                                                        download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                            height='25' /></a>
                                                @elseif($certification->event->id >= '3420')
                                                    <a href="{{ route('certifications.anexo4Atacocha', [$certification->id, 'atacocha']) }}"
                                                        download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                            height='25' /></a>
                                                @endif
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>

                                        <td style="text-align:center;">
                                            @can('canDownloadAnexo', [$certification, 'porvenir'])
                                                @if ($certification->event->id < '3420')
                                                    <a href="{{ route('certifications.anexo4old', [$certification->id, 'porvenir']) }}"
                                                        download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                            height='25' /></a>
                                                @elseif($certification->event->id >= '3420')
                                                    <a href="{{ route('certifications.anexo4Porvenir', [$certification->id, 'porvenir']) }}"
                                                        download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                            height='25' /></a>
                                                @endif
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>

                                        <td style="text-align:center;">
                                            @can('canDownloadAnexo', [$certification, 'sinaycocha'])
                                                <a href="{{ route('certifications.anexo4old', [$certification->id, 'sinaycocha']) }}"
                                                    download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                        height='25' /></a>
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>

                                        <td style="text-align:center;">
                                            @can('canDownloadAnexo', [$certification, 'cerro'])
                                                <a href="{{ route('certifications.anexo4old', [$certification->id, 'cerro']) }}" download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                        height='25' /></a>
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>

                                        <td style="text-align:center;">
                                            @can('canDownloadAnexo', [$certification, 'atacocha'])
                                                @if ($certification->company->id == '6')
                                                    <a href="{{ route('certifications.compromiso', [$certification->id, 'atacocha']) }}"
                                                        download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                            height='25' /></a>
                                                @else
                                                    <a href="{{ route('certifications.compromisoOld', [$certification->id, 'atacocha']) }}"
                                                        download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                            height='25' /></a>
                                                @endif
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>

                                        <td style="text-align:center;">
                                            @can('canDownloadAnexo', [$certification, 'porvenir'])
                                                @if ($certification->company->id == '6')
                                                    <a href="{{ route('certifications.compromiso', [$certification->id, 'porvenir']) }}"
                                                        download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                            height='25' /></a>
                                                @else
                                                    <a href="{{ route('certifications.compromisoOld', [$certification->id, 'porvenir']) }}"
                                                        download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                            height='25' /></a>
                                                @endif
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>

                                        <td style="text-align:center;">
                                            @can('canDownloadAnexo', [$certification, 'sinaycocha'])
                                                <a href="{{ route('certifications.compromiso', [$certification->id, 'sinaycocha']) }}"
                                                    download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                        height='25' /></a>
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>

                                        <td style="text-align:center;">
                                            @can('canDownloadAnexo', [$certification, 'cerro'])
                                                <a href="{{ route('certifications.compromiso', [$certification->id, 'cerro']) }}"
                                                    download><img src="{{ asset('images/pdf.png') }}" width='25'
                                                        height='25' /></a>
                                            @else
                                                {{ '-' }}
                                            @endcan
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody> --}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script type="module" src="{{ asset('js/admin/certifications.js') }}"></script>

    {{-- <script>
        $(function () {
            $("#table_index").DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se encontró registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros)",
                    "search":"Buscar",
                    "searchPlaceholder":"Ingrese texto",
                    "paginate": {
                        "first": "Primero",
                        "previous": "Anterior",
                        "next": "Siguiente",
                        "last": "Último",
                    }
                },
                "order": []
            });
        });

    </script> --}}
@stop
