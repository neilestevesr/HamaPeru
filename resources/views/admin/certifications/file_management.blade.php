@extends('adminlte::page')

@section('title', 'Archivos')

@section('plugins.Datatables',true)
@section('plugins.Bs-custom-file-input',true)

@section('content_header')
    @include('components.alerts',['object' => 'Archivos'])
    <h1 class="card-title">ARCHIVOS</h1> <br>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('files.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="exampleInputFile">Archivos</label>
                    <div class="input-group {{ $errors->has('file') ? 'is-invalid' : '' }}">
                        <div class="custom-file">
                            <input type="file" multiple name="files[]" class="custom-file-input" style="width: auto;" id="exampleInputFile" value="{{ old('files') }}">
                            {{-- <label class="custom-file-label">Elegir Archivos</label> --}}

                            <label class="custom-file-label" for="exampleInputFile">
                                <span class="d-inline-block text-truncate w-75">Elegir Archivos</span>
                            </label>

                        </div>
                    </div>
                    @if($errors->has('files'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('files') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Cargar Archivos</button>
                </div>
            </form>
        </div>
    </div>


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Archivos</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        @php $i = 0 @endphp
                        <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 30px;">#</th>
                                    <th class="sorting" rowspan="1" colspan="1">ARCHIVO</th>
                                    <th class="sorting" rowspan="1" colspan="1">CATEGORÍA</th>
                                    <th class="sorting" rowspan="1" colspan="1">FECHA DE CARGA</th>
                                    <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">ACCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($files as $key => $file)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td class="sorting_1">{{ $file->name }}</td>
                                        <td class="sorting_1">{{ $file->category == 'esp' ? 'cursos de especialización' : $file->category  }}</td>
                                        <td class="sorting_1">{{ $file->created_at }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('files.download',$file->id) }}" class="rgba-white-sligh"><i class="fas fa-download"></i></a>
                                            <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$file->id}}"
                                                href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                @include('components.deleteModal',[
                                                'url_delete' => route('files.destroy',$file->id),
                                                'object' => 'Archivo',
                                                'id' => $file->id,
                                                'description' => $file->name
                                                ])
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @push('css')
        <style media="screen">
            .custom-file-label::after {
                content: "Archivo";
            }
        </style>
    @endpush

    @section('js')
        <script>
            $(function () {
                $('#category').select2();
                $("#table_index").DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por página",
                        "zeroRecords": "No se encontró registros",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de _MAX_ registros)",
                        "search":"Buscar",
                        "searchPlaceholder":"Ingrese texto",
                        "paginate": {
                            "first": "Primero",
                            "previous": "Anterior",
                            "next": "Siguiente",
                            "last": "Último",
                        }
                    },
                    "order": []
                });
            });

        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                bsCustomFileInput.init();
            });
        </script>
    @stop
