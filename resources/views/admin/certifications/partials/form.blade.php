<div class="card-body">
    <div class="form-group">
        <label>Participante</label>
        <select id="user" name="user_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('user_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Usuario">
            @foreach ($users as $key => $user)
                <option value="{{ $user->id }}" {{ $certification->user ? ($certification->user->id  == $user->id ? 'selected' :'') : '' }}>{{ $user->full_name }}</option>
            @endforeach
        </select>
        @if($errors->has('user_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('user_id') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label>Unidades Mineras</label>
        <select id="mining_unit" multiple name="mining_units[]" class="form-control select2 blue select2-hidden-accessible" data-placeholder="Seleccione unidades mineras" style="width: 100%;"  tabindex="-1" aria-hidden="true">
            @foreach ($mining_units as $key => $mining_unit)
                <option value="{{ $mining_unit->id }}" {{ $certification->miningUnits->pluck('id')->contains($mining_unit->id) ? 'selected':'' }} >{{ $mining_unit->description }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Evento</label>
        <select id="event" name="event_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('event_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Evento">
            @foreach ($events as $key => $event)
                <option value="{{ $event->id }}" {{ $certification->event ? ($certification->event->id  == $event->id ? 'selected' :'') : '' }}>{{ $event->description }}</option>
            @endforeach
        </select>
        @if($errors->has('event_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('event_id') }}</strong>
            </div>
        @endif
    </div>


    <div class="form-group">
        <label>Empresa</label>
        <select id="company" name="company_id" class="form-control select2 select2-hidden-accessible {{ $errors->has('company_id') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Empresa">
            @foreach ($companies as $key => $company)
                <option value="{{ $company->id }}" {{ $certification->company ? ($certification->company->id  == $company->id ? 'selected' :'') : '' }}>{{ $company->description }}</option>
            @endforeach
        </select>
        @if($errors->has('company_id'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('company_id') }}</strong>
            </div>
        @endif
    </div>
  <div class="form-group">
        <label for="area">Area</label>
        <input type="text" name="area" class="form-control {{ $errors->has('area') ? 'is-invalid' : '' }}"
        value="{{ old('area',$certification->area) }}" placeholder="Ingresa Area del Participante">
        @if($errors->has('area'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('area') }}</strong>
            </div>
        @endif
    </div>
    
    <div class="form-group">
        <label for="observation">Observacion</label>
        <input type="text" name="observation" class="form-control {{ $errors->has('observation') ? 'is-invalid' : '' }}"
        value="{{ old('observation',$certification->observation) }}" placeholder="Observacion del Participante">
        @if($errors->has('observation'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('observation') }}</strong>
            </div>
        @endif
    </div>  
    
     <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="assist_user" class="custom-control-input" {{ $certification->assist_user == 'S' ? 'checked':'' }} id="customSwitch2">
            <label class="custom-control-label" for="customSwitch2">Asistencia</label>
        </div>
    </div>  
    
    
    
</div>


@section('js')
    <script>
        $(function () {
            $('#user').select2()
            $('#mining_unit').select2();
            $('#event').select2()
            $('#company').select2()

        });
    </script>
@stop
