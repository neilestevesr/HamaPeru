<table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
    <thead>
        <tr role="row">
            <th>#</th>
            <th class="sorting" rowspan="1" colspan="1">DNI</th>
            <th class="sorting" rowspan="1" colspan="1">A. Paterno</th>
            <th class="sorting" rowspan="1" colspan="1">A. Materno</th>
            <th class="sorting" rowspan="1" colspan="1">Nombres</th>
            <th class="sorting" rowspan="1" colspan="1">Empresa</th>
            <th class="sorting" rowspan="1" colspan="1">Unidad Minera</th>
            <th class="sorting" rowspan="1" colspan="1">Cargo</th>
            <th class="sorting" rowspan="1" colspan="1">Id Evento</th>
            <th class="sorting" rowspan="1" colspan="1">Curso</th>
            <th class="sorting" rowspan="1" colspan="1">Fecha Inicio</th>
            <th class="sorting" rowspan="1" colspan="1">Mes</th>
            <th class="sorting" rowspan="1" colspan="1">Año</th>
            <th class="sorting" rowspan="1" colspan="1">Capacitador</th>
            <th class="sorting" rowspan="1" colspan="1">Cod. Empresa</th>
            <th class="sorting" rowspan="1" colspan="1">Cliente</th>
            <th class="sorting" rowspan="1" colspan="1">Lugar</th>
            <th class="sorting" rowspan="1" colspan="1">Nota de Prueba</th>
            <th class="sorting" rowspan="1" colspan="1">Nota</th>
            <th class="sorting" rowspan="1" colspan="1">Condición</th>
            <th class="sorting" rowspan="1" colspan="1">Fecha Recuperación</th>
            <th class="sorting" rowspan="1" colspan="1">Horas de Curso</th>
        </tr>
    </thead>

    <tbody>
        @php $i = 0; @endphp
        @foreach ($certifications as $key => $certification)
            <tr role="row" class="odd">
                <td class="sorting_1">{{ ++$i }}</td>
                <td class="sorting_1">{{ $certification->user->dni }}</td>
                <td class="sorting_1">{{ $certification->user->paternal }}</td>
                <td class="sorting_1">{{ $certification->user->maternal }}</td>
                <td class="sorting_1">{{ $certification->user->name }}</td>
                <td class="sorting_1">HAMA PERU</td>
                <td class="sorting_1">
                @if ($certification->miningUnits->count() > 0)
                                @foreach ($certification->miningUnits as $key => $mining_unit)
                                    <li>{{ $mining_unit->description }} ;</li>
                                @endforeach
                            @else
                                -
                            @endif</td>
                <td class="sorting_1">{{ $certification->position }}</td>
                <td class="sorting_1">{{ $certification->event->id }}</td>
                <td class="sorting_1">{{ $certification->event->exam->course->description }}</td>
                <td class="sorting_1">{{ $certification->event->date }}</td>
                <td class="sorting_1">{{ $certification->event->date_carbon_instance->isoFormat('MM') }}</td>
                <td class="sorting_1">{{ $certification->event->date_carbon_instance->isoFormat('YYYY') }}</td>
                <td class="sorting_1">{{ $certification->event->instructor->full_name }}</td>
                <td class="sorting_1">{{ $certification->company->id }}</td>
                <td class="sorting_1">{{ $certification->company->description }}</td>
                <td class="sorting_1">Pasco</td>
                <td class="sorting_1">{{ $certification->testCertification->score ?? '0' }}</td>
                <td class="sorting_1">{{ $certification->score ?? '0' }}</td>
                <td class="sorting_1">{{ $certification->score == null || $certification->score == 0 ? 'Inasistencia' : ($certification->score >= 14 ? 'Aprobado' : 'Desaprobado') }}</td>
                <td class="sorting_1">{{ $certification->recovered_at }}</td>
                <td class="sorting_1">{{ $certification->event->exam->course->hours }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
