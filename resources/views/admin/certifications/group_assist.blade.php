@extends('adminlte::page')

@section('title', 'Asistencia')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Asistencia'])
    <h1 class="card-title">ASISTENCIAS POR EVENTO</h1> <br>
@stop

@section('content')
    @include('components.filters',[
        'route' => route('certifications.group_assist'),
        'instructor_filter' => true,
        'course_filter' => true,
        'date_from_filter' => true,
        'date_to_filter' => true,
    ])

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Eventos</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        @php $i = 0;@endphp
                        <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 30px;">#</th>
                                    <th class="sorting" rowspan="1" colspan="1">EVENTO</th>
                                    <th class="sorting" rowspan="1" colspan="1">EXAMEN</th>
                                    <th class="sorting" rowspan="1" colspan="1">CURSO</th>
                                    <th class="sorting" rowspan="1" colspan="1">FECHA</th>
                                    <th class="sorting" rowspan="1" colspan="1">INSTRUCTOR</th>
                                    <th class="sorting" rowspan="1" colspan="1">RESPONSABLE</th>
                                    <th class="sorting" rowspan="1" colspan="1">SALA</th>
                                    <th class="sorting" rowspan="1" colspan="1" style="width: 30px;">ASISTENCIAS</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($events as $key => $event)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ ++$i }}</td>
                                        <td class="sorting_1">{{ $event->description ?? '-' }}</td>
                                        <td class="sorting_1">{{ $event->exam->title ?? '-' }}</td>
                                        <td class="sorting_1">{{ $event->exam->course->description ?? '-' }}</td>
                                        <td class="sorting_1">{{ $event->date ?? '-' }}</td>
                                        <td class="sorting_1">{{ $event->instructor->full_name_complete ?? '-' }}</td>
                                        <td class="sorting_1">{{ $event->responsable->full_name_complete ?? '-' }}</td>
                                        <td class="sorting_1">{{ $event->room->description ?? '-' }}</td>
                                        <td style="text-align:center;"><a href="{{ route('certifications.group_assist_pdf',$event->id) }}"><img src="{{ asset('images/pdf.png') }}" title="Asistencia Grupal" width='25' height='25'/></a>
<a href="{{ route('certifications.group_assist_atacocha_pdf',$event->id) }}"><img src="{{ asset('images/pdf.png') }}" title="Asistencia Atacocha" width='25' height='25'/></a>
<a href="{{ route('certifications.group_assist_porvenir_pdf',$event->id) }}"><img src="{{ asset('images/pdf.png') }}" title="Asistencia El Porvenir" width='25' height='25'/></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(function () {
            $("#table_index").DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se encontró registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros)",
                    "search":"Buscar",
                    "searchPlaceholder":"Ingrese texto",
                    "paginate": {
                        "first": "Primero",
                        "previous": "Anterior",
                        "next": "Siguiente",
                        "last": "Último",
                    }
                },
                "order": []
            });
        });

    </script>
@stop
