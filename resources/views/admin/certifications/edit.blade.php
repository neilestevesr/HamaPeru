@extends('adminlte::page')

@section('title', 'Certificado')

@section('content_header')
    <h1>EDITAR INSCRIPCIÓN</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('certifications.update',$certification->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.certifications.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
