@extends('adminlte::page')

@section('title', 'Certificado')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => session('object') ? session('object') : 'Certificado'])
    <div class="card-header">
        {{-- <a href="{{ route('certifications.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Certificados</a> --}}
        <a href="{{ route('events.show',$certification->event->id) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Evento</a>
        <a href="{{ route('certifications.edit',$certification->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>CODIGO DE INSCRIPCIÓN: {{ $certification->id }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>DNI</td>
                        <td>{{ $certification->user->dni ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Participante</td>
                        <td>{{  $certification->user->full_name_complete ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Unidades Mineras</td>
                        <td>
                            @if ($certification->miningUnits->count() > 0)
                                @foreach ($certification->miningUnits as $key => $mining_unit)
                                    <li>{{ $mining_unit->description }}</li>
                                @endforeach
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>E-mail</td>
                        <td>{{  $certification->user->email ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Telefono</td>
                        <td>{{  $certification->user->telephone ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Usuario Activo</td>
                        <td>{{ $certification->user->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Empresa</td>
                        <td>{{ $certification->company->description ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Empresa Ruc</td>
                        <td>{{ $certification->company->ruc ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Cargo</td>
                        <td>{{ $certification->user->position ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Evento</td>
                        <td>{{ $certification->event->description ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de Evento</td>
                        <td>{{ $certification->event->date ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Evento Activo</td>
                        <td>{{ $certification->event->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Examen</td>
                        <td>{{ $certification->event->exam->title ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Examen Activo</td>
                        <td>{{ $certification->event->exam->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Curso</td>
                        <td>{{ $certification->event->exam->course->description ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Hora Inicio de Evaluación</td>
                        <td>{{ $certification->start_time ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Hora Fin de Evaluación</td>
                        <td>{{ $certification->end_time ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Tiempo Total de Evaluación(minutos)</td>
                        <td>{{ $certification->total_time ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Nota</td>
                        <td>{{ $certification->score ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Recuperación</td>
                        <td>{{ $certification->recovered_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Estado de la Evaluación</td>
                        <td>{{ config('parameters.certification_status')[$certification->status] ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Asistencia</td>
                        <td>{{ $certification->assist_user == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Firma</td>
                        <td>{{ $certification->user->signature == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $certification->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $certification->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
