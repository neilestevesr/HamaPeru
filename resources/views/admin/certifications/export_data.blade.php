@extends('adminlte::page')

@section('title', 'Exportar datos')

@section('plugins.Datatables',true)

@section('content_header')
    <h1 class="card-title">EXPORTAR DATOS</h1> <br>
@stop

@section('content')

    @include('components.filters',['route' => route('certifications.export_data'), 'date_from_filter' => true, 'date_to_filter' => true ])

    <div class="card">
        <div class="card-header">
            <div class="row">
                <h3 class="card-title" style="padding-top: 10px;">Lista de Certificados</h3>
                <a href="{{ $download_route }}" class="btn btn-success" style="margin-left:10px;">Descargar <i class="fas fa-download"></i></a>
            </div>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        @include('admin.certifications.partials.table_excel')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')

    <script>
        $(function () {
            $("#table_index").DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se encontró registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros)",
                    "search":"Buscar",
                    "searchPlaceholder":"Ingrese texto",
                    "paginate": {
                        "first": "Primero",
                        "previous": "Anterior",
                        "next": "Siguiente",
                        "last": "Último",
                    }
                },
                "order": []
            });
        });

    </script>
@stop
