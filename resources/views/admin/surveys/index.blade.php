@extends('adminlte::page')

@section('title', 'Encuestas')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => 'Encuesta'])
    <h1 class="card-title">ENCUESTAS</h1> <a href="{{ route('surveys.create') }}" class="btn btn-primary" style="margin-left:10px;"><i class="far fa-plus-square" aria-hidden="true"></i>Crear</a>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lista de Encuestas</h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Id</th>
                                    <th class="sorting" rowspan="1" colspan="1">Nombre</th>
                                    <th class="sorting" rowspan="1" colspan="1">Destinado para</th>
                                    <th class="sorting" rowspan="1" colspan="1">Activo</th>
                                    <th class="sorting" rowspan="1" colspan="1" style="width: 50px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($surveys as $key => $survey)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $survey->id }}</td>
                                        <td><a href="{{ route('surveys.show',$survey->id) }}">{{ $survey->name ?? '-' }}</a></td>
                                        <td>{{ $survey->destined_to ? config('parameters.survey_destined')[$survey->destined_to] : '-' }}</td>
                                        <td class="sorting_1">{{ $survey->active == 'S' ? 'Si' : 'No' }}</td>
                                        <td style="text-align:center;">
                                            <a href="{{ route('surveys.edit',$survey->id) }}" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            @can ('canDelete', $survey)
                                            <a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-{{$survey->id}}"
                                                href="javascript:;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                @include('components.deleteModal',[
                                                    'url_delete' => route('surveys.destroy',$survey->id),
                                                    'object' => 'Encuesta',
                                                    'id' => $survey->id,
                                                    'description' => $survey->name
                                                ])
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    <script>
    $(function () {
        $("#table_index").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No se encontró registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "search":"Buscar",
                "searchPlaceholder":"Ingrese texto",
                "paginate": {
                    "first": "Primero",
                    "previous": "Anterior",
                    "next": "Siguiente",
                    "last": "Último",
                }
            },
            "order": []
        });
    });
    </script>
@stop
