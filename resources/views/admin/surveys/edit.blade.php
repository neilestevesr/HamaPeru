@extends('adminlte::page')

@section('title', 'Encuesta')

@section('content_header')
    <h1>EDITAR ENCUESTA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('surveys.update',$survey->id) }}" method="post">
            @csrf
            @method('PUT')
            @include('admin.surveys.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@stop
