@extends('adminlte::page')

@section('title', 'Encuesta')

@section('content_header')
    <h1>CREAR ENCUESTA</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('surveys.store') }}" method="post">
            @csrf
            @include('admin.surveys.partials.form')
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
@stop
