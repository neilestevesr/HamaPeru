<div class="card-body">
    <div class="form-group">
        <label for="name">Nombre</label>
        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
        value="{{ old('name',$survey->name) }}" placeholder="Ingresa Nombre">
        @if($errors->has('name'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label>Destinado a</label>
        <select id="destined_to" name="destined_to" class="form-control select2 select2-hidden-accessible {{ $errors->has('destined_to') ? 'is-invalid' : '' }}" style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Destino">
            @foreach (config('parameters.survey_destined') as $key => $destined_to)
                <option value="{{ $key }}" {{ $survey->destined_to  == $key ? 'selected' :'' }}>{{ $destined_to }}</option>
            @endforeach
        </select>
        @if($errors->has('destined_to'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('destined_to') }}</strong>
            </div>
        @endif
    </div>

    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" name="active" class="custom-control-input" {{ $survey->active == 'S' ? 'checked':'' }} id="customSwitch1">
            <label class="custom-control-label" for="customSwitch1">Activo</label>
        </div>
    </div>
</div>


@section('js')
    <script>
        $(function () {
            $('#destined_to').select2()
        })
    </script>
@stop
