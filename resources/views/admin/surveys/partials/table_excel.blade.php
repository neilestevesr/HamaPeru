<table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
    <thead>
        <tr role="row">
            <th>#</th>
            <th class="sorting" rowspan="1" colspan="1">DNI</th>
            <th class="sorting" rowspan="1" colspan="1">A. Paterno</th>
            <th class="sorting" rowspan="1" colspan="1">A. Materno</th>
            <th class="sorting" rowspan="1" colspan="1">Nombres</th>
            <th class="sorting" rowspan="1" colspan="1">Empresa</th>
            <th class="sorting" rowspan="1" colspan="1">Encuesta</th>
            <th class="sorting" rowspan="1" colspan="1">Fecha</th>
            <th class="sorting" rowspan="1" colspan="1">Instructor</th>
            <th class="sorting" rowspan="1" colspan="1">Curso</th>
            @for ($i=0; $i < $max; $i++)
                <th class="sorting" rowspan="1" colspan="1">Pregunta #{{ $i+1 }}</th>
                <th class="sorting" rowspan="1" colspan="1">Respuesta #{{ $i+1 }}</th>
            @endfor
        </tr>
    </thead>
    <tbody>
        @php $i = 0; @endphp
        @foreach ($userSurveys as $key => $userSurvey)
            <tr role="row" class="odd">
                <td class="sorting_1">{{ ++$i }}</td>
                <td class="sorting_1">{{ $userSurvey->user->dni }}</td>
                <td class="sorting_1">{{ $userSurvey->user->paternal }}</td>
                <td class="sorting_1">{{ $userSurvey->user->maternal }}</td>
                <td class="sorting_1">{{ $userSurvey->user->name }}</td>
                <td class="sorting_1">{{ $userSurvey->company->description }}</td>
                {{-- <td class="sorting_1">HAMA PERU</td> --}}
                <td class="sorting_1">{{ $userSurvey->survey->name }}</td>
                <td class="sorting_1">{{ $userSurvey->date }}</td>
              
                 <td class="sorting_1">
              
                    @php
                      if (isset($userSurvey->event->instructor->full_name_complete)) {
                       echo $userSurvey->event->instructor->full_name_complete;
                       }
                       else
                       {
                          //echo $userSurvey->event->instructor->full_name_complete;
                           echo 'No Hay Registro';
                       } 
                    @endphp  
                
                  
                 </td>
                
                <td class="sorting_1">
                
                @php
                      if (isset($userSurvey->event->exam->course->description)) {
                       echo $userSurvey->event->exam->course->description;
                       }
                       else
                       {
                          //echo $userSurvey->event->instructor->full_name_complete;
                           echo 'No Hay Registro';
                       } 
                    @endphp  
                
              
                </td>
                @foreach ($userSurvey->surveyAnswers as $key => $surveyAnswer)
                    <td class="sorting_1">{{ $surveyAnswer->statement }}</td>
                    <td class="sorting_1">{{ $surveyAnswer->answer }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
