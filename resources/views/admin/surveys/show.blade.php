@extends('adminlte::page')

@section('title', 'Examen')

@section('plugins.Datatables',true)

@section('content_header')
    @include('components.alerts',['object' => session('group_message') ? 'Grupo de pregunta' : 'Encuesta'])
    <div class="card-header">
        <a href="{{ route('surveys.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Encuestas</a>
        <a href="{{ route('surveys.edit',$survey->id) }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>{{ $survey->name }}</strong></h3>
        </div>
        <div class="card-body p-0">
            @php $i = 0 @endphp
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Atributos</th>
                        <th>Valores</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Activo</td>
                        <td>{{ $survey->active == 'S' ? 'Si' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de creación</td>
                        <td>{{ $survey->created_at ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td>{{ ++$i }}.</td>
                        <td>Fecha de actualización</td>
                        <td>{{ $survey->updated_at ?? '-' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @include('admin.groups.index')

@stop
