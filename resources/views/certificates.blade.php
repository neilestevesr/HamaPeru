<html lang="es"><head>
    <title>HAMA PERU - DOC</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Free-Template.co">
    <link rel="shortcut icon" href="ftco-32x32.png">

    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/certificates/custom-bs.css') }}">
    <link rel="stylesheet" href="{{ asset('css/certificates/jquery.fancybox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/certificates/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/certificates/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/certificates/animate.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }} "> --}}

    <link rel="stylesheet" href="{{ asset('css/style_2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style_3.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style_4.css') }}">
    <style media="screen">
    .invalid-feedback {
        display: block;
        display: none;
        width: 100%;
        margin-top: 0.25rem;
        font-size: 80%;
        color: #dc3545;
    }
    .is-invalid
    {
        border-color: #dc3545;
        padding-right: calc(1.5em + 0.75rem);
        background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23dc3545' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23dc3545' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E);
        background-repeat: no-repeat;
        background-position: center right calc(0.375em + 0.1875rem);
        background-size: calc(0.75em + 0.375rem) calc(0.75em + 0.375rem);
    }
    .data-users{
        font-size: 28px;
        font-weight: 900;
        color: black;
    }
    .site-section-2{
        margin-top: 70px;
        margin-bottom: 70px;
    }
    .text-center{
        text-align: center;
    }
    </style>

</head>
<body id="top">

    <div id="overlayer" style="display: none;"></div>

    <div class="site-wrap">
        <div class="site-mobile-menu site-navbar-target">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
        </div>

        <header class="site-navbar mt-3">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="site-logo col-6"><a href="index.html">HAMA | PERU</a></div>

                    <nav class="mx-auto site-navigation">
                        <ul class="site-menu js-clone-nav d-none d-xl-block ml-0 pl-0"></ul>
                    </nav>

                    <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
                        <div class="ml-auto">
                            <a href="https://hamaperu.com" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Sito Web Oficial de Hama</a>
                            <a href="https://capacitacioneshamaperu.com/aulavirtual/Login.php" class="btn btn-danger border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Aula Virtual</a>
                        </div>
                        <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
                    </div>
                </div>
            </div>
        </header>

        <!-- HOME -->
        <section class="home-section section-hero overlay bg-image" style="background-image: url('/images/hero_1.png');" id="home-section">

            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-12">
                        <div class="mb-5 text-center">
                            <h1 class="text-white font-weight-bold">BIENVENIDO AL PORTAL DE EMISION DE DOCUMENTOS</h1>
                            <p>CONSULTA POR NUMERO DE DNI Y DESCARGA TU CARTAS DE COMPROMISO ,ANEXO 4 Y CERTIFICADOS.</p>
                        </div>
                        <form action="{{ route('certificates.index') }}#next" method="get" class="search-jobs-form">
                            <div class="row mb-5">
                                <div class="col-sm-3 col-md-3 col-lg-3 mb-3 mb-lg-0"></div>
                                <div class="col-sm-3 col-md-3 col-lg-3 mb-3 mb-lg-0">
                                    <input type="text" name="dni" class="form-control form-control-lg {{ $errors->has('dni') ? 'is-invalid' : '' }}"
                                    placeholder="ESCRIBE TU NUMERO DE DNI" value="{{ old('dni') }}">
                                    @if($errors->has('dni'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('dni') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                {{-- <div class="col-sm-4 col-md-4 col-lg-4 mb-4 mb-lg-0" style="select:invalid: red;">
                                    <select name="company_id" class="form-control form-control-lg {{ $errors->has('company_id') ? 'is-invalid' : '' }}" invalid="red" >
                                        <option value="" disabled selected>SELECCIONE EMPRESA</option>
                                        @foreach ($companies as $key => $company)
                                            <option value="{{ $company->id }}" {{ Request::get('company_id') == $company->id ? 'selected' : '' }}>{{ $company->description }}</option>
                                        @endforeach
                                    </select>
                                </div> --}}

                                <div class="col-sm-3 col-md-3 col-lg-3 mb-3 mb-lg-0">
                                    <button type="submit" class="btn btn-danger btn-lg btn-block text-white btn-search"><span class="icon-search icon mr-2"></span>BUSCA TU DOCUMENTO</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 popular-keywords">
                                    <h3>CONTENIDO DE DOCUMENTOS:</h3>
                                    <ul class="keywords list-unstyled m-0 p-0">
                                        <li><a href="#" class="">Cartas de Compromiso</a></li>
                                        <li><a href="#" class="">Certificados</a></li>
                                        <li><a href="#" class="">Anexo4</a></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <a href="#next" class="scroll-button smoothscroll">
                <span class=" icon-keyboard_arrow_down"></span>
            </a>

        </section>

        <section class="py-5 bg-image overlay-primary fixed overlay" id="next" style="background-image: url('/images/hero_1.png');">
            <div class="container">
                <div class="row mb-5 justify-content-center">
                    <div class="col-md-7 text-center">
                        <h2 class="section-title mb-2 text-white">RESULTADOS</h2>
                        <p class="lead text-white">Descarga tus Documentos.</p>
                    </div>
                </div>
            </div>
        </section>


        <section class="site-section-2">
            <div class="container" style="max-width: 1500px;">
                <div class="data-users">
                    <div class="">
                        <label for="">PARTICIPANTE: </label>
                        <span>{{ strtoupper($user->full_name_complete_reverse ?? '-' )}}</span>
                    </div>
                    <div class="">
                        <label for="">COD. IDENTIDAD: </label>
                        <span>{{ $user->dni  ?? '-'}}</span>
                    </div>
                    
                    <br>
                    <div class="">
                        <label for="">RIESGOS CRITICOS</label>
                    </div>
                    {{-- <div class="">
                        <label for="">UNIDADES MIENRAS: </label>
                        <span>{{ $user->mining_units_by_commas  ?? '-'}}</span>
                    </div> --}}
                </div>

                @php $i = 0 @endphp
                <table class="table table-striped table-bordered table-hover text-center" id="example-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col"><center>EMPRESA</center></th>
                            <th scope="col"><center>CURSO</center></th>
                            <th scope="col"><center>FECHA</center></th>
                            <th scope="col"><center>DOCUMENTOS</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $certificates_without_anexos = $regular_certifications->filter(function ($value, $key){
                                return !in_array($value->event->exam->course->id, ['1','10','15','74','82','84','88','92','95']);
                            })
                            // $certificates = $certifications->filter(function ($value, $key){
                            //     return $value->files->count() > 0 && $value->event->date >= '2021-05-01';
                            // });
                        @endphp
                        @foreach ($certificates_without_anexos as $key => $certification)
                        
                         @if($certification->score_fin == '0')
                         
                         
                         @elseif (strtoupper($certification->event->exam->course->description) == 'MANEJO DEFENSIVO' || strtoupper($certification->event->exam->course->id) == '98')
                            @if($certification->score_fin == null || $certification->score_fin == '0')
                            @else
                             <tr role="row" class="odd">
                                <td >{{ $certification->company->description }}</td>
                                <td>{{ $certification->event->exam->course->description }}
                                @if ($certification->event->exam->course->subtitle)
                                / {{ strtoupper($certification->event->exam->course->subtitle) }}
                                @endif
                                
                                </td>
                                <td>{{ $certification->event->date ?? '-' }}</td>
                                <td>
                                 @if($certification->event->date >= '2022-05-27')
                                   @if($certification->score_fin == null || $certification->score_fin == '0')
                                      
                                    @else
                                     @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                                    @endcan
                                    @endif
                                 @else
                                 @if($certification->score_fin == null || $certification->score_fin == '0' )
                                      
                                     @else
                                        @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf_old',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADOS" width='25' height='25'/></a>
                                       @endcan    
                                     @endif
                                 @endif
                                 
                                </td>
                            </tr>
                            
                            @endif
                            
                         
                           
                            
                            @else
                             <tr role="row" class="odd">
                                <td >{{ $certification->company->description }}</td>
                                <td>{{ $certification->event->exam->course->description }}
                                @if ($certification->event->exam->course->subtitle)
                                / {{ strtoupper($certification->event->exam->course->subtitle) }}
                                @endif
                                
                                </td>
                                <td>{{ $certification->event->date ?? '-' }}</td>
                                <td>
                                 @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf_old',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADOSSS" width='25' height='25'/></a>
                                 @endcan   
                                </td>
                            </tr>
                            
                            
                            
                            
                            @endif
                            
                            
                            
                            
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>

        <section class="site-section-2">
            <div class="container" style="max-width: 1500px;">
                <div class="data-users">
                    <span>INDUCCION ANEXO 4
                    </span>
                </div>

                <table class="table table-striped table-bordered table-hover text-center" id="example-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col"><center>EMPRESA</center></th>
                            <th scope="col"><center>CURSO</center></th>
                            <th scope="col"><center>FECHA</center></th>
                            <th scope="col"><center>DOCUMENTOS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $certificates = $regular_certifications->filter(function ($value, $key){
                                return in_array($value->event->exam->course->id, ['1','10','15','74']);

                            })
                            // $certificates = $certifications->filter(function ($value, $key){
                            //     return $value->files->count() > 0 && $value->event->date >= '2021-05-01';
                            // });
                        @endphp
                        @if ($certificates->count() > 0)
                            @foreach ($certificates as $key => $certification)
                                <tr role="row" class="odd">
                                    <td>{{ $certification->company->description }}</td>
                                    <td>{{ $certification->event->exam->course->description }}</td>
                                    <td style="text-align: center;">{{ $certification->event->date ?? '-' }}</td>
                                    <td style="text-align: center;">
                                        @can ('canDownloadCertificate', $certification)
                                         @if($certification->event->date >= '2023-02-22')
                                         <a href="{{ route('certificates.certificate_pdf',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                             
                                         @else
                                            <a href="{{ route('certificates.certificate_pdf_old',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                                        @endif
                                        @endcan
                                        
                                        @can ('canDownloadAnexo', [$certification,'atacocha'])
                                        
                                            @if( ( $certification->company->id) == '6')
                                            
                                                @if($certification->event->date >= '2022-05-09')
                                                <a href="{{ route('certificates.compromiso',[$certification->id,'atacocha']) }}"><img src="{{ asset('images/carta_compromiso.png') }}" title="CARTA COMPROMISO ATACOCHA" width='25' height='25'/></a>
                                                @else
                                                <a href="{{ route('certificates.compromisoOld',[$certification->id,'atacocha']) }}"><img src="{{ asset('images/carta_compromiso.png') }}" title="CARTA COMPROMISO ATACOCHA" width='25' height='25'/></a>
                                                @endif
                                                
                                            @else
                                                <a href="{{ route('certificates.compromisoOld',[$certification->id,'atacocha']) }}"><img src="{{ asset('images/carta_compromiso.png') }}" title="CARTA COMPROMISO ATACOCHA" width='25' height='25'/></a>
                                            @endif
                                        
                                            
                                        @endcan
                                        
                                        @can ('canDownloadAnexo', [$certification,'porvenir'])
                                           
                                              @if( ( $certification->company->id) == '6') 
                                                 @if($certification->event->date >= '2022-05-09')
                                                <a href="{{ route('certificates.compromiso',[$certification->id,'porvenir']) }}"><img src="{{ asset('images/carta_compromiso.png') }}" title="CARTA COMPROMISO EL PORVENIR" width='25' height='25'/></a>
                                                @else
                                                <a href="{{ route('certificates.compromisoOld',[$certification->id,'porvenir']) }}"><img src="{{ asset('images/carta_compromiso.png') }}" title="CARTA COMPROMISO EL PORVENIR" width='25' height='25'/></a>
                                                @endif
                                              @else
                                              <a href="{{ route('certificates.compromisoOld',[$certification->id,'porvenir']) }}"><img src="{{ asset('images/carta_compromiso.png') }}" title="CARTA COMPROMISO EL PORVENIR" width='25' height='25'/></a>
                                              @endif
                                    
                                        @endcan
                                        
                                        @can ('canDownloadAnexo', [$certification,'sinaycocha'])
                                            <a href="{{ route('certificates.compromisoOld',[$certification->id,'sinaycocha']) }}"><img src="{{ asset('images/carta_compromiso.png') }}" title="CARTA COMPROMISO SINAYCOCHA" width='25' height='25'/></a>
                                        @endcan
                                        @can ('canDownloadAnexo', [$certification,'cerro'])
                                            <a href="{{ route('certificates.compromisoOld',[$certification->id,'cerro']) }}"><img src="{{ asset('images/carta_compromiso.png') }}" title="CARTA COMPROMISO CERRO LINDO" width='25' height='25'/></a>
                                        @endcan
                                        @foreach ($certification->files as $key => $file)
                                        @if(substr($file->name, -5, 1)== 'P')
                                        <a href="{{ route('certificates.download',$file->id) }}" class="rgba-white-sligh"><img src="{{ asset('images/pdf.png') }}" title="Anexo 4 El Porvenir" width='25' height='25'/></a>
                                        @endif
                                        @if(substr($file->name, -5, 1)== 'A')
                                         <a href="{{ route('certificates.download',$file->id) }}" class="rgba-white-sligh"><img src="{{ asset('images/pdf.png') }}" title="Anexo 4 Atacocha" width='25' height='25'/></a>
                                        @endif
                                         @if(substr($file->name, -5, 1)== 'S')
                                         <a href="{{ route('certificates.download',$file->id) }}" class="rgba-white-sligh"><img src="{{ asset('images/pdf.png') }}" title="Anexo 4 Sinaycocha" width='25' height='25'/></a>
                                        @endif  
                                        @if(substr($file->name, -5, 1)== 'C')
                                         <a href="{{ route('certificates.download',$file->id) }}" class="rgba-white-sligh"><img src="{{ asset('images/pdf.png') }}" title="Anexo 4 Cerro Lindo" width='25' height='25'/></a>
                                        @endif   
                                            
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">
                                    
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </section>
        
        
           <section class="site-section-2">
            <div class="container" style="max-width: 1500px;">
                <div class="data-users">
                    <span>CURSOS EXTERNOS
                    </span>
                </div>
              <table class="table table-striped table-bordered table-hover text-center" id="example-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col"><center>EMPRESA</center></th>
                            <th scope="col"><center>CURSO</center></th>
                            <th scope="col"><center>FECHA</center></th>
                            <th scope="col"><center>DOCUMENTOS</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $certificates_without_anexos = $regular_certifications->filter(function ($value, $key){
                                return in_array($value->event->exam->course->id, ['57','82','84','88']);
                            })
                            //id 57 curso externo en desarrollo
                            // $certificates = $certifications->filter(function ($value, $key){
                            //     return $value->files->count() > 0 && $value->event->date >= '2021-05-01';
                            // });
                        @endphp
                     
                     @foreach ($certificates_without_anexos as $key => $certification)
                        

                @if($certification->score_fin == '0')
                

                @elseif (strtoupper($certification->event->exam->course->description) == 'GESTION DE EQUIPOS MOVILES' )
                           
                            @if($certification->score_fin == null || $certification->score_fin == '0')
                            @else
                             <tr role="row" class="odd">
                                <td >{{ $certification->company->description }}</td>
                                <td>{{ $certification->event->exam->course->description }}
                                @if ($certification->event->exam->course->subtitle)
                                / {{ strtoupper($certification->event->exam->course->subtitle) }}
                                @endif
                                
                                </td>
                                <td>{{ $certification->event->date ?? '-' }}</td>
                                <td>
                                 @if($certification->event->date >= '2022-05-27' && $certification->event->date < '2023-05-29')
                                 @if($certification->score_fin == null || $certification->score_fin == '0')
                                      
                                   @else
                                     @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                                    @endcan
                                    @endif
                                 @else
                                 @if($certification->score_fin == null || $certification->score_fin == '0' )
                                      
                                     @else
                                        @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf_old',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                                       @endcan    
                                     @endif
                                 @endif
                                 
                                </td>
                            </tr>
                            @endif
                            
                        
                           
                @elseif($certification->event->date >= '2023-05-29')            
                     <tr role="row" class="odd">
                                <td >{{ $certification->company->description }}</td>
                                <td>{{ $certification->event->exam->course->description }}
                                @if ($certification->event->exam->course->subtitle)
                                / {{ strtoupper($certification->event->exam->course->subtitle) }}
                                @endif
                                
                                </td>
                                <td>{{ $certification->event->date ?? '-' }}</td>
                                <td>
                                 @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf_externos',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                                 @endcan   
                                </td>
                            </tr>
                      
                @else
                             <tr role="row" class="odd">
                                <td >{{ $certification->company->description }}</td>
                                <td>{{ $certification->event->exam->course->description }}
                                @if ($certification->event->exam->course->subtitle)
                                / {{ strtoupper($certification->event->exam->course->subtitle) }}
                                @endif
                                
                                </td>
                                <td>{{ $certification->event->date ?? '-' }}</td>
                                <td>
                                 @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf_old',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                                 @endcan   
                                </td>
                            </tr>
                            
                            
               
                            
                 @endif
 @endforeach
              </tbody>
                </table>
                
            </div>
        </section>


       <section class="site-section-2">
            <div class="container" style="max-width: 1500px;">
                <div class="data-users">
                    <span>HAMA WEBINAR
                    </span>
                </div>
              <table class="table table-striped table-bordered table-hover text-center" id="example-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col"><center>EMPRESA</center></th>
                            <th scope="col"><center>CURSO</center></th>
                            <th scope="col"><center>FECHA</center></th>
                            <th scope="col"><center>DOCUMENTOS</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $certificates_without_anexos = $regular_certifications->filter(function ($value, $key){
                                return in_array($value->event->owner_companies_id, ['6']) ||
                                        Str::is('*WEBINAR*', strtoupper($value->event->EmpresaTitular->name ?? '-'));
                            })
                            //id 57 curso externo en desarrollo
                            // $certificates = $certifications->filter(function ($value, $key){
                            //     return $value->files->count() > 0 && $value->event->date >= '2021-05-01';
                            // });
                        @endphp
                     
                     @foreach ($certificates_without_anexos as $key => $certification)
                        

                @if($certification->score_fin == '0')
                           
                           
                @elseif($certification->event->date >= '2023-05-29')            
                     <tr role="row" class="odd">
                                <td >{{ $certification->company->description }}</td>
                                <td>{{ $certification->event->exam->course->description }}
                                @if ($certification->event->exam->course->subtitle)
                                / {{ strtoupper($certification->event->exam->course->subtitle) }}
                                @endif
                                
                                </td>
                                <td>{{ $certification->event->date ?? '-' }}</td>
                                <td>
                                 @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf_externos_2',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                                 @endcan   
                                </td>
                            </tr>
                      
                @else
                             <tr role="row" class="odd">
                                <td >{{ $certification->company->description }}</td>
                                <td>{{ $certification->event->exam->course->description }}
                                @if ($certification->event->exam->course->subtitle)
                                / {{ strtoupper($certification->event->exam->course->subtitle) }}
                                @endif
                                
                                </td>
                                <td>{{ $certification->event->date ?? '-' }}</td>
                                <td>
                                 @can ('canDownloadCertificate', $certification)
                                        <a href="{{ route('certificates.certificate_pdf_externos_2',$certification->id) }}"><img src="{{ asset('images/certificado.png') }}" title="CERTIFICADO" width='25' height='25'/></a>
                                 @endcan   
                                </td>
                            </tr>
                            
                            
               
                            
                 @endif
 @endforeach
              </tbody>
                </table>
                
            </div>
        </section>      
        
        
        <section class="site-section-2">
            <div class="container" style="max-width: 1500px;">
                <div class="data-users">
                    <span>CURSOS DE ESPECIALIZACIÓN
                    </span>
                </div>

                <table class="table table-striped table-bordered table-hover text-center" id="example-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col"><center>EMPRESA</center></th>
                            <th scope="col"><center>CURSO</center></th>
                            <th scope="col"><center>FECHA</center></th>
                            <th scope="col"><center>DOCUMENTOS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($spec_certifications->count() > 0)
                            @foreach ($spec_certifications as $key => $certification)
                                <tr role="row" class="odd">
                                    <td>{{ $certification->company->description }}</td>
                                    <td>{{ $certification->event->exam->course->description }}</td>
                                    <td style="text-align: center;">{{ $certification->event->date ?? '-' }}</td>
                                    <td style="text-align: center;">

                                        @foreach ($certification->files as $key => $file)

                                        <a href="{{ route('certificates.download',$file->id) }}" class="rgba-white-sligh"><img src="{{ asset('images/pdf.png') }}" title="Certificado de especialización" width='25' height='25'/></a>

                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">

                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </section>
        
        

        <section class="py-5 bg-image overlay-primary fixed overlay" style="background-image: url('/images/hero_1.png');">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h2 class="text-white">Encontraste tus Certificados?</h2>
                        <p class="mb-0 text-white lead">Si no encontraste tus certificados comunicate con los supervisores responsables.</p>
                    </div>
                    <div class="col-md-3 ml-auto">
                        <a href="#" class="btn btn-warning btn-block btn-lg">Ir al Inicio</a>
                    </div>
                </div>
            </div>
        </section>

        <footer class="site-footer">

            <a href="#top" class="smoothscroll scroll-top">
                <span class="icon-keyboard_arrow_up"></span>
            </a>

            <div class="container">
                <div class="row mb-5">
                    <div class="col-6 col-md-3 mb-4 mb-md-0">
                        <h3>HAMA | PERU</h3>
                        <ul class="list-unstyled">
                            <li><a href="#">Descarga tus certificados</a></li>
                            <li><a href="{{ route('classroom_login') }}">Portal de Capacitaciones Virtuales</a></li>
                            <li><a href="https://hamaperu.com">Pagina Web Oficial de Hama Peru</a></li>

                        </ul>
                    </div>
                </div>

                <div class="row text-center">
                    <div class="col-12">
                        <p class="copyright">
                            <small>
                                HAMA | PERU �<script>document.write(new Date().getFullYear());</script>Derechos Reservados | Plataforma Oficial de Hama Peru <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://hamaperu.com" target="_blank">HAMAPERU.COM</a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </footer>

    </div>

    <!-- SCRIPTS -->
    <script src="{{ asset('js/certificates/jquery.min.js') }}"></script>
    <script src="{{ asset('js/certificates/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/certificates/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/certificates/stickyfill.min.js') }}"></script>
    <script src="{{ asset('js/certificates/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('js/certificates/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('js/certificates/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('js/certificates/jquery.animateNumber.min.js') }}"></script>
    <script src="{{ asset('js/certificates/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/certificates/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/certificates/custom.js') }}"></script>
    {{-- <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script> --}}

    <script>
    $(function() {
        // $('#company_id').select({ allowClear: true });
    });
    </script>

</body>
</html>
