@extends('layouts.page')

@section('header_title')
	{{ 'Bienvenido,' }}
@stop

@section('header_body')
	{{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content_header')
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
					<video class="embed-responsive-item" width="560" height="315" controls>
						<source src="{{ asset('videos/firma_digital.mp4') }}" type="video/mp4">
						</video>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="authorizationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Autorizacion de Terminos y Condiciones</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						Autorizo el almacenamiento de mi "Firma Digital" en los documentos: <br><br>

						I) Anexo 4<br>
						II) Carta de Compromiso<br>
						III) Asistencias<br><br>

						Estos documentos con mi firma se mantendrán en poder de HAMA para sustentarlas capacitaciones y absolver los requerimientos de los clientes o de las autoridades administrativas.<br><br>

						Asimismo, HAMA y los terceros que tengan acceso deberán adoptar las medidas de seguridad, técnicas, legales y organizativas necesarias para resguardar mis datos personales.

					</div>
					<div class="modal-footer">
						<a href='#' id="startSignature"><button type='button' class='btn btn-primary'>Aceptar términos y condiciones</button></a>
					</div>
				</div>
			</div>
		</div>
	@stop



	@section('content')
		<div class="page-inner mt--5">
			<div class="row mt--2"></div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="card-head-row">
								<div class="col-md-12 ml-auto mr-auto"><center><div class="card-title">Digitaliza tu firma para poder acceder a las capacitaciones y evaluaciones</div></center></div>
							</div>
						</div>

						<div class="card-body">
							<center>
								@if (\Auth::user()->signature == 'S')
									<a href="{{ route('classroom.digital_signatures.create') }}" class="btn btn-danger btn-lg">
										<span class="btn-label">
											FIRMA AQUI
										</span>
									</a>
								@else
									<input class='btn btn-danger btn-lg' onclick="openModal('{{ route('classroom.digital_signatures.create') }}')" type='button' value='FIRMA AQUI'>
								@endif
							</center>
							<br>
							<br>
							<center> Como realizar tu firma digital</center>
							<br>
							<center><a href="#" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-danger btn-sm">
								<span class="btn-label">
									<i class="fas fa fa-play"></i>
								</span>
								Ver Tutorial
							</a></center>
						</div>
					</div>
				</div>
			</div>
		</div>
	@stop

	@section('css')
		<style media="screen">
			.card-title {
				margin: 0;
				color: #575962;
				font-size: 20px;
				font-weight: 400;
				line-height: 1.6;
			}
		</style>
	@stop

	@section('js')

		<script>
			function openModal(route) {
				$("#startSignature").attr("href", route);
				$("#authorizationModal").modal();
			}
		</script>
	@stop
