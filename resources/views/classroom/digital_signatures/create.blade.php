@extends('layouts.page')

@section('')
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
@stop

@section('title')
    {{ 'Hama Peru | Firmas' }}
@stop

@section('content_main')
    <div class="main-panel">
        <div class="content">
            <div id="signature-pad" class="signature-pad">
                <div class="signature-pad--body">
                    <canvas></canvas>
                </div>
                <div class="signature-pad--footer">
                    <div class="description">
                        Firmas Digitales - HAMA PERU <br>  (Estimado usuario firmar dentro del recuadro)
                    </div>
                        <div class="signature-pad--actions">
                            <div class="button-section-1">
                                <button type="button" class="button clear btn btn-danger" data-action="clear">Volver a Firmar </button>
                                <button type="button" class="button clear btn btn-warning" data-action="undo">Corregir Firma</button>
                            </div>
                            <div class="button-section-2">
                                <button type="button" class="button save btn btn-success" data-action="save-png">Guardar Firma</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('css')
        <link href="{{ asset('css/signature-pad.css') }}" rel="stylesheet">
        <style media="screen">
        .content{
            background-image: url('{{ asset('images/fondo_firma.png') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        </style>
@stop

@section('js')
    <script src="{{ asset('js/signature_pad.umd.js') }}"></script>
    <script type="text/javascript">
        var wrapper = document.getElementById("signature-pad");
        var clearButton = wrapper.querySelector("[data-action=clear]");
        var undoButton = wrapper.querySelector("[data-action=undo]");
        var savePNGButton = wrapper.querySelector("[data-action=save-png]");
        var canvas = wrapper.querySelector("canvas");
        var signaturePad = new SignaturePad(canvas, {
            backgroundColor: 'rgb(255, 255, 255)'
        });

        signaturePad.penColor = "#000000";

        function resizeCanvas() {
            var ratio =  Math.max(window.devicePixelRatio || 1, 1);
            canvas.width = canvas.offsetWidth * ratio;
            canvas.height = canvas.offsetHeight * ratio;
            canvas.getContext("2d").scale(ratio, ratio);
            signaturePad.clear();
        }

        window.onresize = resizeCanvas;
        resizeCanvas();

        clearButton.addEventListener("click", function (event) {
            signaturePad.clear();
        });

        undoButton.addEventListener("click", function (event) {
            var data = signaturePad.toData();

            if (data) {
                data.pop(); // remove the last dot or line
                signaturePad.fromData(data);
            }
        });

        savePNGButton.addEventListener("click", function (event) {
            if (signaturePad.isEmpty()) {
                alert("Por favor, provea una firma.");
                console.log("{{ route('classroom.digital_signatures.index') }}");

            } else {
                var dataURL = signaturePad.toDataURL("image/png");
                let _token   = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "POST",
                    url: "{{ route('classroom.digital_signatures.store') }}",
                    data: {
                        imgBase64: dataURL,
                        _token: _token
                    },
                    success: function(response){
                        console.log(response);
                        alert(response.success)
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("Hubo un error");
                    }
                }).done(function(o) {
                    console.log('saved');
                    window.top.location.href = "{{ route('classroom.digital_signatures.index') }}";
                });

            }
        });
    </script>
@stop
