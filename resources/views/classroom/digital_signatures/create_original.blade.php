<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>FIRMAS HAMA</title>
    <meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">

    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/signature-pad.css') }}" rel="stylesheet">
</head>
<body onselectstart="return false" >
    <div id="signature-pad" class="signature-pad">
        <div class="signature-pad--body">
            <canvas></canvas>
        </div>
        <div class="signature-pad--footer">
            <div class="description">Firmas Digitales - HAMA PERU <BR>  (Estimado usuario firmar dentro del recuadro) </div>
                <div class="signature-pad--actions">
                    <div>
                        <button type="button" class="button clear btn btn-danger" data-action="clear">Volver a Firmar </button>
                        <br>
                        <center><button type="button" class="button btn btn-warning" data-action="undo">Corregir Firma</button></center>

                    </div>
                    <div>
                        <button type="button" class="button save btn btn-success" data-action="save-png">Guardar Firma</button>
                    </div>
                </div>

            </div>
        </div>
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/signature_pad.umd.js') }}"></script>

        <script type="text/javascript">
            var wrapper = document.getElementById("signature-pad");
            var clearButton = wrapper.querySelector("[data-action=clear]");
            var undoButton = wrapper.querySelector("[data-action=undo]");
            var savePNGButton = wrapper.querySelector("[data-action=save-png]");
            var canvas = wrapper.querySelector("canvas");
            var signaturePad = new SignaturePad(canvas, {
                backgroundColor: 'rgb(255, 255, 255)'
            });

            signaturePad.penColor = "#000000";

            function resizeCanvas() {
                var ratio =  Math.max(window.devicePixelRatio || 1, 1);
                canvas.width = canvas.offsetWidth * ratio;
                canvas.height = canvas.offsetHeight * ratio;
                canvas.getContext("2d").scale(ratio, ratio);
                signaturePad.clear();
            }

            window.onresize = resizeCanvas;
            resizeCanvas();

            clearButton.addEventListener("click", function (event) {
                signaturePad.clear();
            });

            undoButton.addEventListener("click", function (event) {
                var data = signaturePad.toData();

                if (data) {
                    data.pop(); // remove the last dot or line
                    signaturePad.fromData(data);
                }
            });

            savePNGButton.addEventListener("click", function (event) {
                if (signaturePad.isEmpty()) {
                    alert("Por favor, provea una firma.");
                    console.log("{{ route('classroom.digital_signatures.index') }}");

                } else {
                    var dataURL = signaturePad.toDataURL("image/png");
                    let _token   = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: "POST",
                        url: "{{ route('classroom.digital_signatures.store') }}",
                        data: {
                            imgBase64: dataURL,
                            _token: _token
                        },
                        success: function(response){
                            console.log(response);
                            alert(response.success)
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            alert("Hubo un error");
                        }
                    }).done(function(o) {
                        console.log('saved');
                        window.top.location.href = "{{ route('classroom.digital_signatures.index') }}";
                    });

                }
            });
        </script>
    </body>
    <?php
    ob_start ();
    session_start();
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    header("Cache-Control: no-cache, must-revalidate");
    //header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");



    //$dni = $_SESSION['dni'];
    // comprovamos si se envió la imagen
    if (isset($_POST['imgBase64'])) {

        echo $_POST['imgBase64'];
        // mostrar la imagen
        echo '<img src="'.$_POST['imgBase64'].'" border="1">';

        // funcion para gusrfdar la imagen base64 en el servidor
        // el nombre debe tener la extension
        function uploadImgBase64 ($base64, $name){
            // decodificamos el base64
            $datosBase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64));
            // definimos la ruta donde se guardara en el server
            $path= $_SERVER['DOCUMENT_ROOT'].'/Evaluacion/admin/imagenes/'.$name;
            // guardamos la imagen en el server
            if(!file_put_contents($path, $datosBase64)){
                // retorno si falla
                return false;
            }
            else{
                // retorno si todo fue bien
                return true;
            }
        }

        // llamamos a la funcion uploadImgBase64( img_base64, nombre_fina.png)
        uploadImgBase64($_POST['imgBase64'], $dni.'.png' );

        $modificar="UPDATE usuario SET firma='S' WHERE dni='$dni' Limit 1";
        if ($resultado=$con->query($modificar)) {

        }else{
            echo "error en la MODIFICACION";
        }

    }
    ?>
    </html>
