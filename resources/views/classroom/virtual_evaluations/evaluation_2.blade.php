@extends('layouts.page')

@section('header_title')
    {{ 'EVALUACION VIRTUAL' }}
@stop

@section('header_body')
    {{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content_main')

    <div class="main-panel">
        <div class="content">
            <nav class="navbar navbar-light bg-danger"><br></nav>
            <div class="album py-3 bg-light">
                <div class="container">
                    <main role="main" class="container">
                        <h1 class="mt-5" style="text-align: center;">Evaluación</h1>
                        <p class="lead">Selecciona el inciso correcto de las siguientes preguntas de seleccion Múltiple
                            <font class="cuadros" id="countdown">

                            </font>
                            {{-- <div id="countdown"></div> --}}
                        </p>
                    </main>
                </div>

                <hr>

                <div class="container">
                    <form class="" action="{{ route('classroom.virtual_evaluations.anwser_question',[$certification->id,$evaluation->question_order]) }}" method="post">
                        <table class="table table-bordered">
                            <tr class="cuadro">
                                <td style="font-size:17px;">Categoria: {{ $evaluation->dynamicQuestion->exam->ownerCompany->name }}</td>
                                <td style="font-size:17px;">Titulo : "{{ $evaluation->dynamicQuestion->exam->course->description }}" </td>
                            </tr>
                            <tr>
                                <td colspan='2'>
                                    <div class="{{ $errors->has('alternative_id') ? 'is-invalid' : '' }}">
                                        <label >
                                            <strong class="question-number">PREGUNTA # {{ $evaluation->question_order . ': ' }}</strong>
                                        </label>
                                        <font class='lead text-primary'>
                                            <span>{{ $evaluation->dynamicQuestion->statement }}</span>
                                        </font>
                                    </div>
                                    <div class="row">
                                        @foreach ($evaluation->dynamicQuestion->files as $key => $file)
                                            <div class="col-4 multimedia" >
                                                @if ($file->file_type == 'imagenes')
                                                    <img class="myImg" src="{{ $file->file_url }}" alt="" onclick="openImgModal('{{ $file->file_url }}')" width="90%" height="90%">
                                                @elseif ($file->file_type == 'videos')
                                                    <video src="{{ $file->file_url }}" controls width="90%" height="90%"></video>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                    @if($errors->has('alternative_id'))
                                        <div class="invalid-feedback">
                                            <strong style="font-size:15px;">{{ $errors->first('alternative_id') }}</strong>
                                        </div>
                                    @endif
                                    <br>

                                    @csrf
                                    <div class="alternatives">
                                        @foreach ($evaluation->dynamicQuestion->dynamicAlternatives as $key => $dynamic_alternative)
                                            <div class="alternative">
                                                <label><input type="radio" name="alternative_id" value="{{ $dynamic_alternative->id }}" {{ $evaluation->selected_alternative == $dynamic_alternative->description ? 'checked' : ''  }}> {{ $dynamic_alternative->description }}</label>
                                            </div>
                                        @endforeach

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2'>
                                    <div class="">
                                        <div class="" style="float: left;">
                                            @if ($evaluation->question_order > 1)
                                                <a href="{{ route('classroom.virtual_evaluations.show_question',[$certification->id,$evaluation->question_order-1]) }}" class="btn btn-lg btn-success-2">Anterior</a>
                                            @endif
                                        </div>
                                        <div class="" style="float: right;">
                                            <input class='btn btn-lg btn-success-2' type='submit' value='Guardar'>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div id="myModal" class=" modal">
        {{-- <span class="close">&times;</span> --}}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <style media="screen">
        h1, .h1 {
            font-size: 2.5rem;
            margin-bottom: .5rem;
            font-family: inherit;
            font-weight: 650;
            line-height: 1.2;
            color: inherit;
        }
        .cuadro{
            background-color: #F80943;
            color:#ffffff;
        }
        .cuadros{
            background-color: #71C126;
            color:#FFFFFF;
            font-size:25px;
            padding: 0.5em;
            border-radius: 0.2em;
        }
        .lead {
            font-size: 1.25rem;
            font-weight: 300;
        }
        .question-number{
            font-size: 0.95rem;
            font-weight: 850;
        }
        .btn-success-2 {
            color: #fff;
            background-color: #28a745;
            border-color: #28a745;
        }
        .multimedia{
            --border: solid;
            --border-color: black;--
            --background-color: red;--
            margin: auto;
            text-align: center;
        }
        .myImg {
          border-radius: 5px;
          cursor: pointer;
          transition: 0.3s;
        }

        .myImg:hover {opacity: 0.7;}

        .modal {
          display: none; /* Hidden by default */
          position: fixed; /* Stay in place */
          z-index: 1; /* Sit on top */
          padding-top: 100px; /* Location of the box */
          left: 0;
          top: 0;
          width: 100%; /* Full width */
          height: 100%; /* Full height */
          overflow: auto; /* Enable scroll if needed */
          background-color: rgb(0,0,0); /* Fallback color */
          background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
        }

        .modal-content {
          margin: auto;
          display: block;
          width: 80%;
          max-width: 700px;
        }

        #caption {
          margin: auto;
          display: block;
          width: 80%;
          max-width: 700px;
          text-align: center;
          color: #ccc;
          padding: 10px 0;
          height: 150px;
        }

        /* Add Animation */
        .modal-content, #caption {
          -webkit-animation-name: zoom;
          -webkit-animation-duration: 0.6s;
          animation-name: zoom;
          animation-duration: 0.6s;
        }

        @-webkit-keyframes zoom {
          from {-webkit-transform:scale(0)}
          to {-webkit-transform:scale(1)}
        }

        @keyframes zoom {
          from {transform:scale(0)}
          to {transform:scale(1)}
        }

        /* The Close Button */
        .close {
          position: relative;
          top: 15px;
          right: 35px;
          color: #f1f1f1;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
        }

        .close:hover,
        .close:focus {
          color: #bbb;
          text-decoration: none;
          cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
          .modal-content {
            width: 100%;
          }
        }
    </style>
@stop

@section('js')
    <script defer src="{{ asset('js/fontawesome-all.js') }}"></script>

    <script>
        var timer;
        var time_left_segs = {{ $time_left }};

        function showRemaining() {
            time_left_segs = time_left_segs - 1;
            if (time_left_segs < 0) {

                clearInterval(timer);
                document.getElementById('countdown').innerHTML = 'EXPIRED!';
                return;
            }

            var minutes = Math.floor( time_left_segs / 60);
            var seconds = Math.floor( time_left_segs % 60 );

            document.getElementById('countdown').innerHTML = minutes + ' minutos, ';
            document.getElementById('countdown').innerHTML += seconds + ' segundos.';
        }

        timer = setInterval(showRemaining, 1000);
    </script>

    <script>
        function openImgModal(src) {
            $("#img01").attr("src", src);
            $("#myModal").modal();
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            $("#myModal").modal('hide');
        }
    </script>
@stop
