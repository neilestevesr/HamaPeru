@extends('layouts.page')

@section('header_title')
    {{ 'EVALUACION VIRTUALL' }}
@stop

@section('header_body')
    {{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content_header')
    <div class="modal fade" id="instructionsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Caracter&iacute;sticas de la Evaluaci&oacute;n</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Lea las siguientes instrucciones antes de comenzar la evaluaci&oacute;n:<br><br>
                    * Tiempo de examen 20 minutos, 2 minutos por pregunta<br><br>
                    * Una vez comenzado el examen debe permanecer en la pagina.<br><br>
                    * No abrir o visualizar otras p&aacute;ginas mientras est&aacute; desarrollando la evaluaci&oacute;n, porque sino la plataforma lo detectar&aacute; como inactividad y cerrar&aacute; la sesi&oacute;n autom&aacute;ticamente.<br><br>
                    <!--* Tienes un intento de responder cada pregunta, as&iacute; que lea y responda cada una con atenci&oacute;n.<br><br>-->
                    * Trabaje individualmente y no use su tel&eacute;fono celular o p&aacute;ginas web para responder esta evaluaci&oacute;n.<br><br>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <a href='#' id="startEvaluation"><button type='button' class='btn btn-primary'>Comenzar Examen</button></a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="album py-3 bg-light">
        <div class="container">
            <div class="row">
                @foreach ($certifications as $key => $certification)
                    <div class='col-md-4 exam'>
                        <div class='card mb-4 box-shadow'>
                            <img class='card-img-top' src=" {{ $certification->event->exam->course->files->where('file_type', 'imagenes')->last()->file_url ?? '#' }}" alt='Card image cap'>
                        
                            <div class='card-body'>
                                <p class='card-text'>
                                    <strong>TITULAR: </strong>
                                    <span style="color: green;">
                                        {{ $certification->event->flg_test_exam == 'N' ? $certification->event->exam->ownerCompany->name : $certification->event->testExam->ownerCompany->name }}
                                    </span>
                                    <br>
                                    <strong>CURSO: </strong>
                                    <span style="color: green;">
                                        {{ $certification->event->flg_test_exam == 'N' ? $certification->event->exam->course->description : $certification->event->testExam->course->description }}
                                    </span>
                                    <br>
                                    <strong>ESTADO:</strong>
                                    <span>
                                        {{ config('parameters.certification_status')[$certification->status] ?? '-' }}
                                    </span>
                                    {{-- <font class='text-white bg-danger p-1'>Finalizado</font> --}}
                                    <br>
                                    <strong>FECHA:</strong>
                                    <span>
                                        {{ $certification->event->date }}
                                    </span>
                                    <br>
                                    @if ($certification->score != NULL)
                                        <strong>ESTADO:</strong>
                                        @if ($certification->score >= 14)
                                            <font class='text-white bg-success p-1'>APROBADO</font>
                                        @else
                                            <font class='text-white bg-danger p-1'>DESAPROBADO</font>
                                        @endif
                                    @else
                                        <br>
                                    @endif
                                </p>
                                <div class='d-flex justify-content-between align-items-center'><hr>
                                @if ( $certification->event->flg_test_exam == 'N' )
                                    @can ('canStartEvaluation', $certification)
                                        <input class='btn btn-warning' id='testModalButton' onclick="openModal('{{ route('classroom.virtual_evaluations.start',$certification->id) }}')" type='button' value="Ir Examen">
                                    @endcan
                                @else
                                    @can ('canStartTestEvaluation', $certification)
                                        <input class='btn btn-warning' id='testModalButton' onclick="openModal('{{ route('classroom.virtual_evaluations.start',$certification->id) }}')" type='button' value="Test de Entrada">
                                    @endcan
                                @endif
                                    
                                </div>
                            </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}" type="text/css" media="all">
@stop

@section('js')

    <script>
        function openModal(route) {
            $("#startEvaluation").attr("href", route);
            $("#instructionsModal").modal();
        }
    </script>
@stop
