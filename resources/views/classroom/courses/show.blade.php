@extends('layouts.page')

@section('header_title')
{{ 'Evento' }}
@stop

@section('header_body')
{{ strtoupper( $event->description ?? '-' ) }}
@stop


@section('content_header')

@stop


@section('content')
	<div class="page-inner mt--5">
		<div class="row mt--2"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Lista de participantes - HAMA 	</h4>
         
								<a href="{{ route('classroom.courses.live_security_list') }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Regresar</a>                               
						
						</div>
						<form class="" action="{{ route('classroom.courses.assist',$event->id) }}" method="post">
							@csrf
						<div class="card-body">
							<div class="table-responsive">
								<table id="basic-datatables" class="display table table-striped table-hover {{ $errors->has('assists') ? 'is-invalid' : '' }}" >
									<thead>
										<tr>
											<th><center>DNI</center></th>
											<th><center>Nombre</center></th>
											<th><center>Apellido Paterno</center></th>
											<th><center>Apellido Materno</center></th>
									    <th><center>Empresa</center></th>
                       <th><center>Unidad Minera</center></th>                                  
										</tr>
									</thead>
									<tbody>
                       @if ($user->miningUnits->count() > 0)
                                @foreach ($user->miningUnits as $key => $mining_unit)
                                    @if($mining_unit->description == 'El Porvenir')
                                        	
                                          @foreach ($certifications as $key => $certification)
                                             @if ($certification->miningUnits->count() > 0)
                                             
                                                @foreach ($certification->miningUnits as $key => $mining_units)
                                                 
                                                  @if($mining_units->description == 'El Porvenir')
                                                    <tr>
                            												<td><center>{{ $certification->user->dni }}</center></td>
                            												<td><center>{{ $certification->user->name }}</center></td>
                            												<td><center>{{ $certification->user->paternal }}</center></td>
                            												<td><center>{{ $certification->user->maternal }}</center></td>
                                                    <td><center>{{ $certification->company->description }}</center></td>		
                                                 		<td><center>El Porvenir</center></td>								
                                                  </tr>
                                                    
                                                    
                                                    
                                                  @endif     
                                                      
                                                      
                                                @endforeach
                                             
                                             @else
                                               -
                                             @endif
                                             
                                             
                                          
                                          @endforeach
                                    
                                    
                                    
                                    
                                    @elseif( $mining_unit->description == 'Atacocha')
                                    @foreach ($certifications as $key => $certification)
                                             @if ($certification->miningUnits->count() > 0)
                                             
                                                @foreach ($certification->miningUnits as $key => $mining_units)
                                                 
                                                  @if($mining_units->description == 'Atacocha')
                                                    <tr>
                            												<td><center>{{ $certification->user->dni }}</center></td>
                            												<td><center>{{ $certification->user->name }}</center></td>
                            												<td><center>{{ $certification->user->paternal }}</center></td>
                            												<td><center>{{ $certification->user->maternal }}</center></td>
                                                    <td><center>{{ $certification->company->description }}</center></td>		
                                                 		<td><center>Atacocha</center></td>								
                                                  </tr>
                                                    
                                                    
                                                    
                                                  @endif     
                                                      
                                                      
                                                @endforeach
                                             
                                             @else
                                               -
                                             @endif
                                             
                                             
                                          
                                          @endforeach
                                    
                                    @endif
                                
                                @endforeach
                       
                       
                       @else
                        -
                       @endif
                      
                      
                      
                                                              
                                                                
										
									</tbody>
								</table>
								
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('css')
	<style media="screen">
		h2, .h2 {
			font-size: 1.35rem;
		}

		.h5, h5 {
			font-size: .9125rem;
		}

		h1,h2,h3,h4,h5,h6,
		.h1,.h2,.h3,.h4,.h5,.h6 {
			margin-bottom: 0.5rem;
			font-weight: 500;
			line-height: 1.4;
		}

		body {
			font-family: Lato,sans-serif;
		}
		.card{
			border: 0;
		}
		.contenedor{
        position:absolute;
    }
    .pdf{
        position:relative;
    }
    .bloqueo{
        position:relative;
        background-color:rgba(255,255,255,0.00);
        width:1250px;
        height:1250px;
    }

	
     
	</style>

@stop



