@extends('layouts.page')

@section('header_title')
{{ 'Bienvenido,' }}
@stop

@section('header_body')
{{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content')
	<div class="page-inner mt--5">
		<div class="row mt--2"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Eventos para Firmar</h4>
                            <span>
                                <i>
                                    Estimado usuario, si no visualiza el evento deseado, asegurate de haber realizado tu firma digital.
                                </i>
                            </span>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="basic-datatables" class="display table table-striped table-hover" >
									<thead>
										<tr>
											<th style="width: 150px;"><center>Codigo Evento</center></th>
                                            <th><center>Titulo de Curso</center></th>
                                            <!--<th><center>Sala</center></th>-->
											<th><center>Fecha</center></th>
                      <th><center>Participantes</center></th>
                        @foreach ($user->miningUnits as $key => $mining_unit)
                                           @if($mining_unit->description == 'El Porvenir')
                                                 <th><center>Firmar EP</center></th> 
                                           
                                           @endif
                                           @if($mining_unit->description == 'Atacocha')
                                             
                                                	<th><center>Firmar AT</center></th>
                                                
                                           @endif
                                         
                                          
                                         
                       @endforeach                                                                             
							                                                                                                                          
										</tr>
									</thead>
									<tbody>
                                    @php
									
                                     $event_lists = $event_list->filter(function ($value, $key){
                                         return in_array($value->exam->course->id, ['1','10','15','74']);
                                     })
                                    @endphp
                        
                   
                      
                            
                               
                                        @foreach ($event_lists as $key => $event)
                                    
                                        <tr>
                												<td><center>{{ $event->id }}</center></td>
                                        <td><center>{{ strtoupper($event->exam->course->description) }}</center></td>
                											  <td><center>{{ $event->date }}</center></td>
                                      	<td><center><a href="{{ route('classroom.courses.show',$event->id) }}">Lista</a></center></td>
                                         @foreach ($user->miningUnits as $key => $mining_unit)
                                           @if($mining_unit->description == 'El Porvenir')
                                                 @if( $event->flg_security_por == 'N')
                                        
                                        <td><center>  <a href="{{ route('classroom.digital_signatures.index_security_por',$event->id) }}">
                                                                         Pendiente </a></center></td>
                                        
                                         @elseif( $event->flg_security_por == 'S')
                                        
                                         <td><center><img src='{{ asset('images/BUENA.png') }}'  height='30' width='30'></center></td>
                                         
                                        
                                       @endif
                                           @endif
                                           @if($mining_unit->description == 'Atacocha')
                                            @if( $event->flg_security == 'N')
                                        
                                        <td><center>  <a href="{{ route('classroom.digital_signatures.index_security',$event->id) }}">
                                                                        Pendiente </a>   </center></td>
                                        
                                         @elseif( $event->flg_security == 'S')
                                        
                                         <td><center><img src='{{ asset('images/BUENA.png') }}'  height='30' width='30'></center></td>
                                        
                                        
                                       @endif    
                                                
                                                
                                                
                                           @endif
                                         
                                          
                                         
                                         @endforeach
                                        </tr>
                                       
                                        @endforeach
                           
     
                  
                                                                               
                                                                                 
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('css')
	<style media="screen">
		h2, .h2 {
			font-size: 1.35rem;
		}

		.h5, h5 {
			font-size: .9125rem;
		}

		h1,h2,h3,h4,h5,h6,
		.h1,.h2,.h3,.h4,.h5,.h6 {
			margin-bottom: 0.5rem;
			font-weight: 500;
			line-height: 1.4;
		}

		body {
			font-family: Lato,sans-serif;
		}
		.card{
			border: 0;
		}
	</style>
@stop