@extends('layouts.page')

@section('header_title')
{{ 'Bienvenido,' }}
@stop

@section('header_body')
{{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content')
	<div class="page-inner mt--5">
		<div class="row mt--2"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Salas Asignadas</h4>
                            <span>
                                <i>
                                    Estimado usuario, si no visualiza el curso deseado, asegurate de tener sala asignada y haber realizado tu firma digital.
                                </i>
                            </span>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="basic-datatables" class="display table table-striped table-hover" >
									<thead>
										<tr>
											<th style="width: 150px;"><center>Codigo Curso</center></th>
                                            <th><center>Titulo de Curso</center></th>
                                            <th><center>Sala</center></th>
											<th><center>Fecha</center></th>
											<th><center>Instructor</center></th>
										</tr>
									</thead>
									<tbody>
										@foreach ($certifications as $key => $certification)
											<tr>
												<td><center>{{ $certification->event->exam->course->id }}</center></td>
                                                <td><center>{{ strtoupper($certification->event->exam->course->description) }}</center></td>
												<td><center>
                                                    <a href="{{ route('classroom.courses.live',$certification->id) }}">
                                                        {{ strtoupper($certification->event->room->description) }}</center></td>
                                                    </a>
												<td><center>{{ $certification->event->date }}</center></td>
												<td><center>{{ strtoupper($certification->event->instructor->full_name) }}</center></td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('css')
	<style media="screen">
		h2, .h2 {
			font-size: 1.35rem;
		}

		.h5, h5 {
			font-size: .9125rem;
		}

		h1,h2,h3,h4,h5,h6,
		.h1,.h2,.h3,.h4,.h5,.h6 {
			margin-bottom: 0.5rem;
			font-weight: 500;
			line-height: 1.4;
		}

		body {
			font-family: Lato,sans-serif;
		}
		.card{
			border: 0;
		}
	</style>
@stop
