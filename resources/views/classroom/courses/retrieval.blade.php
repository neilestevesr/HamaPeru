@extends('layouts.page')

@section('header_title')
	{{ 'Bienvenido,' }}
@stop

@section('header_body')
	{{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content')
	<div class="page-inner mt--5">
		<div class="row mt--2"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="card-head-row">
							<div class="card-title">
								<center>
									<strong>
										SALA DE RECUPERACIONES
									</strong>
								</center>
							</div>
							<div class="card-tools">
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="iframe-16-9">
							<iframe src="https://us02web.zoom.us/j/81935252632" allow='livestreaming,sharedvideo,chat,raisehand,settings,microphone,camera,desktop,fullscreen,shortcuts,tileview,mute-everyone'  frameborder='0' marginwidth='0' marginheight='0' scrolling='no'></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<div class="container-fluid">
		<nav class="pull-left">
		</nav>
		<div class="copyright ml-auto">
			2020, plataforma hecha por <a href="https://hamaperu.com/" style="color: #495057;">HamaPeru</a>
		</div>
	</div>
@stop

@section('css')
	<style type="text/css">
		.card-header {
			height: 65px;
		}

		.iframe-16-9 {
			position: relative;
			padding-bottom: 56.25%;
			padding-top: 35px;
			height: 0;
			overflow: hidden;
		}
		.iframe-16-9  iframe {
			position: absolute;
			top:0;
			left: 0;
			width: 100%;
			height: 100%;
		}
	</style>
@stop
