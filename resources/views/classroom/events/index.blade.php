@extends('layouts.page')

@section('header_title')
{{ 'Bienvenido,' }}
@stop

@section('header_body')
{{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop


@section('content')
	<div class="page-inner mt--5">
		<div class="row mt--2"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Eventos</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="basic-datatables" class="display table table-striped table-hover" >
									<thead>
										<tr>
											<th><center>Codigo</center></th>
											<th><center>Descripción</center></th>
											<th><center>Tipo</center></th>
											<th><center>Fecha</center></th>
											<th><center>Activo</center></th>
											<th><center>Sala</center></th>
											<th><center>Curso</center></th>
										</tr>
									</thead>
									<tbody>
										@foreach ($events as $key => $event)
											<tr>
												<td><center>{{ $event->id }}</center></td>
												<td><center><a href="{{ route('classroom.events.show',$event->id) }}">{{ $event->description ?? '-' }}</a></center></td>
												<td><center>{{ $event->type ?? '-' }}</center></td>
												<td><center>{{ $event->date ?? '-' }}</center></td>
												<td><center>{{ $event->active == 'S' ? 'Si' : 'No' }}</center></td>
										    <td><center><a href="{{ $event->room->url_zoom ?? '-' }}" target="_blank">{{ $event->room->description ?? '-' }}</a></center></td> 
												<td><center>{{ $event->exam->course->description ?? '-' }}</center></td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('css')
	<style media="screen">
		h2, .h2 {
			font-size: 1.35rem;
		}

		.h5, h5 {
			font-size: .9125rem;
		}

		h1,h2,h3,h4,h5,h6,
		.h1,.h2,.h3,.h4,.h5,.h6 {
			margin-bottom: 0.5rem;
			font-weight: 500;
			line-height: 1.4;
		}

		body {
			font-family: Lato,sans-serif;
		}
		.card{
			border: 0;
		}
	</style>
@stop
