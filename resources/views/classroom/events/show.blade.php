@extends('layouts.page')

@section('header_title')
{{ 'Evento' }}
@stop

@section('header_body')
{{ strtoupper( $event->description ?? '-' ) }}
@stop


@section('content_header')
<!--  MODAL CONTENIDO-->
<div class="modal fade" id="culminacion-evento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">CONTENIDO DEL CURSO: <strong>{{ $event->description ?? '' }}</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <div class="contenedor">
                              <div class="pdf">
                             <!--  <object data="{{ $event->exam->course->files->where('file_type', 'archivos')->last()->file_url ?? '#faltacon' }}" type="application/PDF" width="1300px" height="1300px" align="right"></object>-->
							   <iframe src="https://view.officeapps.live.com/op/view.aspx?src={{ $event->exam->course->files->where('file_type', 'archivos')->last()->file_url ?? '#faltacon' }}&embedded=true" style="width:1300px; height:1000px;" frameborder="0" align="right"></iframe>
							  </div>
                            <div class="bloqueo">           
                             </div>
                             </div>
      </div>
      
    </div>
  </div>
</div>



<!--  MODAL PERFIL-->
<div class="modal fade" id="culminacion-perfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">PERFIL DEL EVENTO : <strong>{{ $event->description ?? '' }}</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                            <div class="contenedor">
                        
                                  <div class="bloqueo">   
                                  TOTAL DE PARTICIPANTES :
                                   <?php $total = 0; ?>
                                    <?php $DIV = 0; ?>
                                     <?php $ACO = 0; ?>
                                      <?php $ASI = 0; ?>
                                       <?php $CON = 0; ?>
                                       <?php $RESTANTES = 0; ?>
                                  @foreach ($certifications as $key => $certification)
				
                                    <?php $total++ ?>
                                     @if(Str::contains( $certification,'DIVERGENTE')== 'DIVERGENTE')
                                     <?php $DIV++ ?>   
                                     @endif 
                                     @if(Str::contains( $certification,'ACOMODADOR')== 'ACOMODADOR')
                                     <?php $ACO++ ?>   
                                     @endif
                                     @if(Str::contains( $certification,'ASIMILADOR')== 'ASIMILADOR')
                                     <?php $ASI++ ?>   
                                     @endif
                                     @if(Str::contains( $certification,'CONVERGENTE')== 'CONVERGENTE')
                                     <?php $CON++ ?>   
                                     @endif 
                                    
                                        
					              					@endforeach
                                         <?php echo $total; 
                                                $RESTANTES = ($total) - ($DIV) - ($ACO) - ($ASI) - ($CON);
                                         ?><br>
                                         
                                         
                                          <?php echo "DIVERGENTES : ".$DIV ; ?><br>
                                           <?php echo "ACOMODADOR : ".$ACO ; ?><br>
                                            <?php echo "ASIMILADOR : ".$ASI; ?><br>
                                             <?php echo "CONVERGENTE : ".$CON; ?><br>
                                             <?php echo "NO DIERON ENCUESTA : ".$RESTANTES; ?><br>                                         
                                         
                                         
                                   </div>
                                   <br>
                                   <br>
                                   
                                 
                                   
                                   
                                   
                             </div>
                                
                                   
                             <br>
                             <br>
                             <br>
                            <br>
                             <br>
                            
                             
                                  <center><iframe src="{{ route('classroom.events.highcharts',$event->id) }}" style="width:1300px; height:1000px;" frameborder="0" ></iframe></center>        
                             
      </div>
      
    </div>
  </div>
</div>

<!--  MODAL VIDEO-->

<div class="modal fade" id="culminacion-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">CONTENIDO DEL VIDEO: <strong>{{ $event->description ?? '' }}</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  @php $i = 0 @endphp
	  @foreach ($event->exam->course->files->where('file_type','link') as $key => $file)
							
								
	  <a class="btn waves-effect btn-link" data-toggle="modal" data-target="#link-video-{{$file->id}}"
                                                    href="javascript:;"> VIDEO:  {{ $file->file_path  }} </a>
                                                    @include('components.linkModal',[
                                                        'url_delete' => route('files.destroy',$file->id),
                                                        'object' => $file->file_path  ,
                                                        'id' => $file->id,
                                                        'description' => $file->file_url
                                                    ])
								<br>
	  @endforeach
      </div>
      
    </div>
  </div>
</div>




<!--<div class="modal fade modal-casos-2-video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Explicación del Caso</h5>

			</div>
			<div class="modal-body">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe  width="560" height="315" src="{{ $file->file_url  ?? '' }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="modal-footer">
			
				<a href="#" role="button"  class="btn btn-secondary">Cerrar</a>
			</div>
		</div>
	</div>
</div>-->

@stop


@section('content')
	<div class="page-inner mt--5">
		<div class="row mt--2"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Lista de participantes  
								{{-- @can ('canConcludeEvaluation', $event)
									<button class="btn btn-danger" style="float:right;" type="button" name="button" data-toggle="modal" data-target="#culminacion-evento">Culminar Evento</button>
								@endcan --}}
                  
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							
								<button class="btn btn-danger" style="float:1200px;" type="button" name="button" data-toggle="modal" data-target="#culminacion-video">VER VIDEOS</button>
                <button class="btn btn-danger" style="float:1200px;" type="button" name="button" data-toggle="modal" data-target="#culminacion-perfil">VER PERFIL</button>								
								<button class="btn btn-danger" style="float:right;" type="button" name="button" data-toggle="modal" data-target="#culminacion-evento">VER CONTENIDO</button>

							
							</h4>
						
						</div>
						<form class="" action="{{ route('classroom.events.assist',$event->id) }}" method="post">
							@csrf
						<div class="card-body">
							<div class="table-responsive">
								<table id="basic-datatables" class="display table table-striped table-hover {{ $errors->has('assists') ? 'is-invalid' : '' }}" >
									<thead>
										<tr>
											<th><center>DNI</center></th>
											<th><center>Nombre</center></th>
											<th><center>Apellido Paterno</center></th>
											<th><center>Apellido Materno</center></th>
										
											<th><center>Estado</center></th>
                      <th><center>Asistencia</center></th>
											<th><center>Perfil</center></th>
										</tr>
									</thead>
									<tbody>
                                                              
                                                                
										@foreach ($certifications as $key => $certification)
											<tr>
												<td><center>{{ $certification->user->dni }}</center></td>
												<td><center>{{ $certification->user->name }}</center></td>
												<td><center>{{ $certification->user->paternal }}</center></td>
												<td><center>{{ $certification->user->maternal }}</center></td>
												
                       	<td><center>{{ config('parameters.certification_status')[$certification->status] ?? '-' }}</center></td>
                        				 	
                      	<td><center><input class="" type="checkbox" name="assists[]" value="{{ $certification->id }}" {{ $certification->assist_user == 'S' ? 'checked' : '' }}></center></td>

                    
                       	<td><center>{{ $certification->user->profile_user ?? 'Pendiente' }}</center></td>
                        
                       

											</tr>
                              
                                        
										@endforeach
									</tbody>
								</table>
								@if($errors->has('assists'))
						            <div class="invalid-feedback">
						                <strong>{{ $errors->first('assists') }}</strong>
						            </div>
						        @endif
								@if($errors->has('assists.*'))
						            <div class="invalid-feedback">
						                <strong>{{ $errors->first('assists.*') }}</strong>
						            </div>
						        @endif
								@can ('canAssistsSave', $event)
									<button class="btn btn-success" style="float: right;" type="submit" name="button">Guardar Asistencia</button>
								@endcan
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('css')
	<style media="screen">
		h2, .h2 {
			font-size: 1.35rem;
		}

		.h5, h5 {
			font-size: .9125rem;
		}

		h1,h2,h3,h4,h5,h6,
		.h1,.h2,.h3,.h4,.h5,.h6 {
			margin-bottom: 0.5rem;
			font-weight: 500;
			line-height: 1.4;
		}

		body {
			font-family: Lato,sans-serif;
		}
		.card{
			border: 0;
		}
		.contenedor{
        position:absolute;
    }
    .pdf{
        position:relative;
    }
    .bloqueo{
        position:relative;
        background-color:rgba(255,255,255,0.00);
        width:1250px;
        height:1250px;
    }

	
     
	</style>

@stop



