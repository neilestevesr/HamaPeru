<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style type="text/css">
    
    .highcharts-figure,
.highcharts-data-table table {
    min-width: 320px;
    max-width: 660px;
    margin: 1em auto;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}

.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}

.highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
    padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}

.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>


                                    <?php $total2 = 0; ?>
                                    <?php $DIV2 = 0; ?>
                                     <?php $ACO2 = 0; ?>
                                      <?php $ASI2 = 0; ?>
                                       <?php $CON2 = 0; ?>
                                       <?php $RESTANTES2 = 0; ?>
                                       <?php $puntos2 = []; ?>
                                  @foreach ($certifications as $key => $certification)
				
                                    <?php $total2++ ?>
                                     @if(Str::contains( $certification,'DIVERGENTE')== 'DIVERGENTE')
                                     <?php $DIV2++ ?>   
                                     @endif 
                                     @if(Str::contains( $certification,'ACOMODADOR')== 'ACOMODADOR')
                                     <?php $ACO2++ ?>   
                                     @endif
                                     @if(Str::contains( $certification,'ASIMILADOR')== 'ASIMILADOR')
                                     <?php $ASI2++ ?>   
                                     @endif
                                     @if(Str::contains( $certification,'CONVERGENTE')== 'CONVERGENTE')
                                     <?php $CON2++ ?>   
                                     @endif 
                                    
                                        
					              					@endforeach
                                  
                                   <?php  
                                                $RESTANTES2 = ($total2) - ($DIV2) - ($ACO2) - ($ASI2) - ($CON2);
                                                $DIVPOR2 =  ($DIV2*100)/$total2;
                                                $ACOPOR2 =  ($ACO2*100)/$total2;
                                                $ASIPOR2 =  ($ASI2*100)/$total2;
                                                $CONPOR2 =  ($CON2*100)/$total2;
                                                $RESTANTESPOR =  ($RESTANTES2*100)/$total2;                                                
                                                                                  
                                        
                                                
                                         ?>



<figure class="highcharts-figure">
    <div id="container"></div>
    
</figure>

 
<script>  
// Radialize the colors
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    })
});

// Build the chart
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'GRAFICA DE RESULTADOS DE PERFIL POR EVENTO :'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                connectorColor: 'silver'
            }
        }
    },
    series: [{
        name: 'Share',
      
        data: [
            { name: 'DIVERGENTE', y: <?php echo ($DIVPOR2); ?> },
            { name: 'CONVERGENTE', y: <?php echo ($CONPOR2); ?> },
            { name: 'ASIMILADOR', y: <?php echo ($ASIPOR2) ?> },
            { name: 'ACOMODADOR', y: <?php echo ($ACOPOR2); ?>},
            { name: 'AUN NO DAN ENCUESTA', y: <?php echo ($RESTANTESPOR); ?> }
            
        ]
    }]
});
</script>

