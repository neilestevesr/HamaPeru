@extends('layouts.page')

@section('header_title')
    {{ 'Bienvenido,' }}
@stop

@section('header_body')
    {{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop


@section('content')
    <div class="page-inner mt--5">
        <div class="row mt--2"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Información General</h4>
                    </div>
                    
                    
                    <div class="card-body">
                    
                      <form class="form" action="{{ route('classroom.profile.user_profile') }}" name="myform" method="post">
                      @csrf
                        <div class="col-md-12">
                        <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Nombres</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" name= "name" id="nombres" placeholder="Nombres" Value="{{ $user->name ?? '-' }}" readonly>
                                </div>
                            </div>
                            
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Apellidos</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" id="apellidos" placeholder="Apellidos" Value="{{ $user->paternal . ' ' . $user->maternal  }}" disabled>
                                </div>
                            </div>

                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">DNI</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" id="dni" placeholder="DNI"
                                    Value="{{ $user->dni }}" disabled>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">ECM</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" id="empresa" placeholder="Empresa" Value="{{ $user->company->description ?? '-' }}" disabled>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Unidad</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" id="unidad" placeholder="Unidad" Value="{{ $user->mining_units_line ?? '-' }}" disabled>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Cargo</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" id="Cargo" placeholder="Cargo" Value="{{ $user->position ?? '-' }}" disabled>
                                </div>
                            </div>
                            @if ($user->cip)
                                <div class="form-group form-inline">
                                    <label for="inlineinput" class="col-md-3 col-form-label">CIP</label>
                                    <div class="col-md-9 p-0">
                                        <input type="text" class="form-control input-full" id="cip" placeholder="cip" Value="{{ $user->cip ?? '-' }}" disabled>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Correo</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" id="correo" placeholder="correo" Value="{{ $user->email ?? '-' }}" disabled>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Telefono/Celular</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" id="telf" placeholder="telf"  Value="{{ $user->telephone ?? '-' }}" disabled>
                                </div>
                            </div>
                            
                             <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Perfil</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="{{ $user->profile_user ?? '-' }}" readonly>
                                </div>
                                </div>
                       
                       
                        </div>
                        
                     </form>
                   
                   
                    </div>
                    
                    
                </div>
            </div>

        </div>
    </div>
@stop

@section('css')
    <style media="screen">
        .card-title {
            margin: 0;
            color: #575962;
            font-size: 20px;
            font-weight: 400;
            line-height: 1.6;
        }
        .form-group {
            margin-bottom: 0;
            padding: 10px;
        }

        .form-control {
            font-size: 14px;
            padding: .6rem 1rem;
            height: inherit!important;
        }
    </style>

    
@stop


@section('js')
    
@stop
