<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="google" content="notranslate"/>
	<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1' />
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<title>Hama|Peru</title>

	<link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
	<header class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">

				<a href="index.html" class="logo">
					<img src="{{ asset('images/logo_blanco.png') }}"  height="30" width="160" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="more" onclick="showAndroidToast('Desea salir de aula virtual ?')">	<img src="{{ asset('images/ICONO-SALIR.png') }}" alt="..."  width="30" height="30"></button>

			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="red2">

				<div class="container-fluid">

					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret"></li>

						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="{{ asset('images/profile.jpg') }}" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="{{ asset('images/profile.jpg') }}" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4><?php echo 'NEPTALI GUILLERMO' .' '. 'CASAS CASAS'?></h4>
												<p class="text-muted"><?php echo 'elmer.alcantara1@uguil.com' ?></p><a href="profile.html" class="btn btn-xs btn-secondary btn-sm">View Profile</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">My Profile</a>
										<a class="dropdown-item" href="#">My Balance</a>
										<a class="dropdown-item" href="#">Inbox</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Account Setting</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->

	</header>

	<!-- Sidebar -->
	<aside class="main-sidebar">
		<div class="sidebar sidebar-style-2">
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="{{ asset('images/profile.jpg') }}" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<?php echo 'NEPTALI GUILLERMO' ?>
									<span class="user-level"><?php echo 'elmer.alcantara1@uguil.com' ?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="perfil.php">
											<span class="link-collapse"><strong>Mi Perfil</strong></span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-primary">

						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section"><strong>MENU</strong></h4>
						</li>

						<li class="nav-item">
							<a href="documentos.php">
								<img src="{{ asset('images/icon_doc.png') }}" alt="..."  width="30" height="30">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<p><strong>E-Learning</strong></p>

							</a>
						</li>

						<li class="nav-item">
							<a href="cursos.php">
								<img src="{{ asset('images/noticias.png') }}" alt="..."  width="30" height="30">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<p><strong>Cursos Inscritos</strong></p>

							</a>
						</li>
						<li class="nav-item">
							<a href="firma.php">
								<img src="{{ asset('images/icono_firma.png') }}" alt="..."  width="30" height="30">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<p><strong>Firma Digital</strong></p>

							</a>
						</li>
						<li class="nav-item">
							<a href="index.php">
								<img src="{{ asset('images/aulavirtual.png') }}" alt="..."  width="30" height="30">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<p><strong>Curso en Vivo</strong></p>

							</a>
						</li>
						<li class="nav-item">
							<a href="index_recupera.php">
								<img src="{{ asset('images/alerta.png') }}" alt="..."  width="30" height="30">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<p><strong>Sala de Recuperaci&#243;n</strong></p>

							</a>
						</li>

						<li class="nav-item">
							<a href="https://hamaperu.net/Evaluacion/login.php?usuario=<?php echo '41018360'; ?>">
								<img src="{{ asset('images/hamanet.png') }}" alt="..."  width="30" height="30">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<p><strong>Evaluacion Virtual</strong></p>

							</a>
						</li>

						<li class="nav-item">
							<a href="javascript:showAndroidToast('Desea salir de aula virtual ?');">
								<img src="{{ asset('images/icono_salir2.png') }}" alt="..."  width="30" height="30">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<p><strong>Salir</strong></p>

							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</aside>

	<script src="{{ asset('js/app.js') }}"></script>

	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
</body>
</html>
