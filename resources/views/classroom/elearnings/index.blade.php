@extends('layouts.page')

@section('header_title')
{{ 'AULA VIRTUAL' }}
@stop

@section('header_body')
{{ 'E-LEARNING' }}
@stop

@section('plugins.Datatables',true)
@section('plugins.Bs-custom-file-input',true)

@section('content_header')
	
@stop

@section('content')
	<div class="page-inner mt--5">
		<div class="row mt--2">
		</div>
		<div class="row">
			@foreach ($elearnings as $key => $elearning)
			@php
				$modal_videos_id = 'elearning-videos-' . $elearning->id;
				$modal_contenidos_id = 'elearning-contenidos-' . $elearning->id;
				$modal_casos_id = 'elearning-casos-' . $elearning->id;
				$modal_rvi_id = 'elearning-rvi-' . $elearning->id;
				$modal_juegos_id = 'elearning-juegos-' . $elearning->id;
				$modal_archivos_id = 'elearning-archivos-' . $elearning->id;

				$videos_contents = $elearning->contents->filter(function($item) {return $item->section == 'Videos';});
				$contenidos_contents = $elearning->contents->filter(function($item) {return $item->section == 'Contenidos';});
				$casoos_contents = $elearning->contents->filter(function($item) {return $item->section == 'Casos';});
				$rvi_contents = $elearning->contents->filter(function($item) {return $item->section == 'RVI';});
				$juegos_contents = $elearning->contents->filter(function($item) {return $item->section == 'Juegos';});
				$archivos_contents = $elearning->contents->filter(function($item) {return $item->section == 'Archivos';});


			@endphp
			<div class="col-md-3">
				<div class="card card-post card-round">
					<h3 class="card-title">
						<br>
					</h3>
					<img class="card-img-top" src="{{ $elearning->files->where('file_type', 'imagenes')->last()->file_url ?? '#' }}" alt="Card image cap">
						<div class="card-body">
							<br>
							@if($videos_contents->count() > 0)
								@include('classroom.elearnings.partials.modals',['contents' => $videos_contents, 'section' => 'Videos', 'modal_class_id' => $modal_videos_id, 'src_img_icon' => asset('images/ver_video.jpeg')] )
								<a href="#" role="button"  data-toggle="modal" data-target=".{{ $modal_videos_id }}" title='Ver Video'><img src="{{ asset('images/ver_video.jpeg') }}" width="30px" height="30px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ver Video</a>
								<br>
							@endif

							@if($contenidos_contents->count() > 0)
								@include('classroom.elearnings.partials.modals',['contents' => $contenidos_contents, 'section' => 'Contenidos', 'modal_class_id' => $modal_contenidos_id, 'src_img_icon' =>  asset('images/ver_contenido.jpeg')] )
								<a href="#" role="button" data-toggle="modal" data-target=".{{ $modal_contenidos_id }}" title='Ver Contenido'><img src="{{ asset('images/ver_contenido.jpeg') }}" width="30px" height="30px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ver Contenido</a>
								<br>
							@endif

							@if($casoos_contents->count() > 0)
								@include('classroom.elearnings.partials.modals',['contents' => $casoos_contents, 'section' => 'Casos', 'modal_class_id' => $modal_casos_id, 'src_img_icon' => asset('images/ver_casos.jpeg')] )
								<a href="#" role="button"  data-toggle="modal" data-target=".{{ $modal_casos_id }}" title='Ver Casos'><img src="{{ asset('images/ver_casos.jpeg') }}" width="30px" height="30px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ver Casos</a>
								<br>
							@endif

							@if($rvi_contents->count() > 0)
								@include('classroom.elearnings.partials.modals',['contents' => $rvi_contents, 'section' => 'RVI', 'modal_class_id' => $modal_rvi_id, 'src_img_icon' => asset('images/ver_rvi.jpeg')] )
								<a href="#" role="button"  data-toggle="modal" data-target=".{{ $modal_rvi_id }}" title='Ver RVI'><img src="{{ asset('images/ver_rvi.jpeg') }}" width="30px" height="30px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ver RVI</a>
								<br>
							@endif

							@if($juegos_contents->count() > 0)
								@include('classroom.elearnings.partials.modals',['contents' => $juegos_contents, 'section' => 'Juegos', 'modal_class_id' => $modal_juegos_id, 'src_img_icon' => asset('images/icono-juegos.png')] )
								<a href="#" data-toggle="modal" data-target=".{{ $modal_juegos_id }}" title='Ver Cazador de Riesgos'><img src="{{ asset('images/icono-juegos.png') }}" width="30px" height="30px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ver Cazador de Riesgos</a>
								<br>
							@endif

							@if($archivos_contents->count() > 0)
								@include('classroom.elearnings.partials.modals',['contents' => $archivos_contents, 'section' => 'Archivos', 'modal_class_id' => $modal_archivos_id, 'src_img_icon' => asset('images/icon_doc.png')] )
								<a href="#" data-toggle="modal" data-target=".{{ $modal_archivos_id }}" title='Ver Archivos'><img src="{{ asset('images/icon_doc.png') }}" width="30px" height="30px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ver Archivos</a>
								<div class="separator-solid"></div>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@stop

@section('footer')
	<div class="container-fluid">
		<nav class="pull-left">
		</nav>
		<div class="copyright ml-auto">
			2020, plataforma hecha por <a href="https://hamaperu.com/">HamaPeru</a>
		</div>
	</div>
@stop

@section('css')
<style media="screen">
	.btn {
	    padding: .65rem 1.4rem;
	    font-size: 14px;
	    opacity: 1;
	    border-radius: 3px;
	}
	a {
	    color: #495057;
	}

	.card-title {
		margin: 0;
	}

	.card, .card-light {
	    border-radius: 5px;
	    background-color: #fff;
	    margin-bottom: 30px;
	    box-shadow: 2px 6px 15px 0 rgba(69,65,78,.1);
	    border: 0;
	    border-top-color: initial;
	    border-top-style: initial;
	    border-top-width: 0px;
	    border-right-color: initial;
	    border-right-style: initial;
	    border-right-width: 0px;
	    border-bottom-color: initial;
	    border-bottom-style: initial;
	    border-bottom-width: 0px;
	    border-left-color: initial;
	    border-left-style: initial;
	    border-left-width: 0px;
	    border-image-source: initial;
	    border-image-slice: initial;
	    border-image-width: initial;
	    border-image-outset: initial;
	    border-image-repeat: initial;
	}
</style>
@stop
