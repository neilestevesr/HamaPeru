<!DOCTYPE html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Unity WebGL Player | Cazador de Riesgos Hama</title>
	<link rel="shortcut icon" href="{{ asset('unity/TemplateData2/favicon.ico') }}">
	<link rel="stylesheet" href="{{ asset('unity/TemplateData2/style.css') }}">
	<script src="{{ asset('unity/TemplateData2/UnityProgress.js') }}"></script>
	<script src="{{ asset('unity/Build2/UnityLoader.js') }}"></script>
	<script>
	var unityInstance = UnityLoader.instantiate("unityContainer", "{{ asset('unity/Build2/Cazador prot de maquinas.json') }}", {onProgress: UnityProgress});
	</script>
</head>
<body>
	<div class="webgl-content">
		<div id="unityContainer" style="width: 960px; height: 600px"></div>
		<div class="footer">
			<div class="webgl-logo"></div>
			<div class="fullscreen" onclick="unityInstance.SetFullscreen(1)"></div>
			<div class="title">Cazador de Riesgos Hama</div>
		</div>
	</div>
</body>
</html>
