<div class="modal fade {{ $modal_class_id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">{{$section}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			@foreach ($contents as $key => $content)
				@if ( $content->type != 'URL' )
					@php $submodal_class_id = 'content-' . $content->id @endphp
					@include('classroom.elearnings.partials.submodals_test',['content' => $content, 'submodal_class_id' => $submodal_class_id])
					<a href="#" data-toggle="modal" data-target=".{{ $submodal_class_id }}" role="button" data-placement='bottom' title="{{ $content->title }}">
						<img src="{{ $src_img_icon }}" width="30px" height="30px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $content->title }}
					</a>
				@else
					<a href="{{ $content->url }}" role="button"  data-placement='bottom' title="{{ $content->title }}"><img src="{{ $src_img_icon }}" width="30px" height="30px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $content->title }}</a>
				@endif
				<br>
			@endforeach
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>


