@if ( $content->type == 'Video' )
<div class="modal fade {{ $submodal_class_id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">{{ $content->title }}</h5>
			</div>
			<div class="modal-body">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe  width="560" height="315" src="{{ $content->files->last()->file_url }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="modal-footer">
				<a href="{{ route('classroom.elearnings.index') }}" role="button"  class="btn btn-secondary">Cerrar</a>
			</div>
		</div>
	</div>
</div>

@elseif ( $content->type == 'Imagen' )
<div class="modal fade {{ $submodal_class_id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div id="{{ 'carousel-' . $content->id }}" class="carousel slide carousel-fade" data-interval="false"  data-ride="carousel">
				<div class="carousel-inner">
					@foreach ($content->files as $key => $file)
						<div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
							<img class="d-block w-100" src="{{ $file->file_url }}" alt="file_url">
						</div>
					@endforeach
				</div>
				<a class="carousel-control-prev" href="#{{ 'carousel-' . $content->id }}" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#{{ 'carousel-' . $content->id }}" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
</div>

@elseif ( $content->type == 'Archivo' )

<div class="modal fade {{ $submodal_class_id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">{{ $content->title }}</h5>
			</div>
			<div class="modal-body">
				@php $i = 0 @endphp
				<table id="table_index" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="table_index_info">
					<thead>
						<tr role="row">
							<th style="width: 30px;">#</th>
							<th class="sorting" rowspan="1" colspan="1">ARCHIVO</th>
							<!-- <th class="sorting" rowspan="1" colspan="1">CATEGORÍA</th> -->
							<th class="sorting" rowspan="1" colspan="1">TIPO</th>
							<th class="sorting" rowspan="1" colspan="1" style="width: 50px;">ACCIONES</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($content->files as $key => $file)
							<tr role="row" class="odd">
								<td class="sorting_1">{{ ++$i }}</td>
								<td class="sorting_1">{{ $file->name }}</td>
								<!-- <td class="sorting_1">{{ $file->category }}</td> -->
								<td class="sorting_1">{{ $file->file_type }}</td>
								<td style="text-align:center;">
									<a href="{{ route('classroom.elearnings.contents.files.download',$file->id) }}" class="rgba-white-sligh"><i class="fas fa-download"></i></a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<a href="{{ route('classroom.elearnings.index') }}" role="button"  class="btn btn-secondary">Cerrar</a>
			</div>
		</div>
	</div>
</div>




@endif

@push('css')
<style media="screen">
.carousel-control-next-icon, .carousel-control-prev-icon {
display: inline-block;
width: 20px;
height: 20px;
background: #dc3545;
background-size: 100% 100%;
}

.carousel-control-next-icon {
  background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23fff' width='8' height='8' viewBox='0 0 8 8'%3e%3cpath d='M2.75 0l-1.5 1.5L3.75 4l-2.5 2.5L2.75 8l4-4-4-4z'/%3e%3c/svg%3e");
}

.carousel-control-prev-icon {
  background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23fff' width='8' height='8' viewBox='0 0 8 8'%3e%3cpath d='M5.25 0l-4 4 4 4 1.5-1.5L4.25 4l2.5-2.5L5.25 0z'/%3e%3c/svg%3e");
}
</style>
@endpush
