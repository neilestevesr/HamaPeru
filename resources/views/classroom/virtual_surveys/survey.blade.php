@extends('layouts.page')

@section('header_title')
    {{ 'EVALUACION VIRTUAL' }}
@stop

@section('header_body')
    {{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content_main')
    <div class="main-panel">
        <div class="content">
            <nav class="navbar navbar-light bg-danger"><br></nav>
            <div class="album py-3 bg-light">
                <div class="container">
                    <main role="main" class="container">
                        <h1 class="mt-5" style="text-align: center;">Encuesta: {{ $userSurvey->survey->name }} </h1>
                        <p class="lead">Responda la encuesta
                            {{-- <font class="cuadros" id="countdown">

                            </font> --}}
                        </p>
                    </main>
                </div>

                <hr>

                <div class="container">
                    <form class="" action="{{ route('classroom.virtual_surveys.anwser_question',[$userSurvey->id,$surveyAnswer->question_order]) }}" method="post">
                        <table class="table table-bordered">
                            <tr class="cuadro">
                                <td style="font-size:17px;">Grupo Pregunta: {{ $surveyAnswer->question->group->name }}</td>
                                <td style="font-size:17px;">{{ $surveyAnswer->question->desc ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td colspan='2'>
                                    <div class="{{ $errors->has('answer') ? 'is-invalid' : '' }}">
                                        <label >
                                            <strong class="question-number">PREGUNTA # {{ $surveyAnswer->question_order . ': ' }}</strong>
                                        </label>
                                        <font class='lead text-primary'>
                                            <span>{{ $surveyAnswer->question->description ?? '-' }}</span>
                                        </font>
                                    </div>
                                    @if($errors->has('answer'))
                                        <div class="invalid-feedback">
                                            <strong style="font-size:15px;">{{ $errors->first('answer') }}</strong>
                                        </div>
                                    @endif
                                    <br>

                                    @csrf
                                    @if ($surveyAnswer->question->type == 'commentary')
                                        <div class="alternatives">
                                            <div class="answer">
                                                <label for="">
                                                    <input name="answer" value="{{ $surveyAnswer->answer ?? '' }}" >
                                                </label>
                                                {{-- <textarea name="answer" cols="90  ">{{ $surveyAnswer->answer ?? '' }}</textarea> --}}
                                            </div>
                                        </div>
                                    @else
                                        <div class="alternatives">
                                        @foreach ($surveyAnswer->question->options as $key => $option)
                                            <div class="answer">
                                                <label><input type="radio" name="answer" value="{{ $option->description }}" {{ $surveyAnswer->answer == $option->description ? 'checked' : ''  }}> {{ $option->description }}</label>
                                            </div>
                                        @endforeach
                                        </div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2'>
                                    <div class="">
                                        <div class="" style="float: left;">
                                            @if ($surveyAnswer->question_order > 1)
                                                <a href="{{ route('classroom.virtual_surveys.show_question',[$userSurvey->id,$surveyAnswer->question_order-1]) }}" class="btn btn-lg btn-success-2">Anterior</a>
                                            @endif
                                        </div>
                                        
                                     <?php $EC = 0; ?>
                                     <?php $OR = 0; ?>
                                     <?php $CA = 0; ?>
                                     <?php $EA = 0; ?>   
                        
                          
                         @foreach ($userSurvey->surveyAnswers as $key => $surveyAnswer)
                        
                          @if(Str::contains( $surveyAnswer->answer,'(EC)')== '(EC)')
                               <?php $EC++ ?>   
                          @endif  
                          @if(Str::contains( $surveyAnswer->answer,'(OR)')== '(OR)')
                               <?php $OR++ ?>   
                          @endif
                          @if(Str::contains( $surveyAnswer->answer,'(CA)')== '(CA)')
                               <?php $CA++ ?>   
                          @endif
                          @if(Str::contains( $surveyAnswer->answer,'(EA)')== '(EA)')
                               <?php $EA++ ?>   
                          @endif 
                         @endforeach
                           <?php 
                                 $div = (($OR * $EC * 4)/2);
                                 $aco = (($EA * $EC * 4)/2);
                                 $asi = (($OR * $CA * 4)/2);
                                 $con = (($EA * $CA * 4)/2);
                                
                               ?>  
                             
                               
                               @if($div>$aco && $div>$asi && $div>$con)
                               
                                
                                    <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="DIVERGENTE" >
                                   
                               
                               @endif
                                
                                @if($aco>$div && $aco>$asi && $aco>$con)
                            
                                    <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ACOMODADOR" >
                                
                               @endif
                               
                                @if($asi>$div && $asi>$aco && $asi>$con)
                               
                                    <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ASIMILADOR" >
                               
                               @endif
                               
                                @if($con>$div && $con>$aco && $con>$asi)
                                
                                    <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="CONVERGENTE" >
                              
                               @endif
                                
                             <!--    @if($con=$div && $con=$aco && $con=$asi)
                               
                                    <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="CONTRADICCION EN RESPUESTAS" >
                                
                               @endif  -->
                               
                                          @if($EC==0 && $OR==0)
                                        
                                                 <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="CONVERGENTE"> 
                                        
                                           @endif
                                           
                                           @if($CA==0 && $OR==0)
                                          
                                                <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ACOMODADOR">
                                                
                                        
                                           @endif
                                        
                                           @if($CA==0 && $EA==0)
                                          
                                                <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="DIVERGENTE">
                                                
                                        
                                           @endif
                                        
                                           @if($EC==0 && $EA==0)
                                          
                                                <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ASIMILADOR">
                                               
                                        
                                           @endif
                                           
                                           @if($EC==0 && $CA==0)
                                          
                                        
                                                 @if($EA>$OR)
                                                 
                                                  <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ACOMODADOR">
                                                    
                                                 @endif
                                                 
                                                 @if($EA<$OR)
                                                   <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="DIVERGENTE">
                                                  
                                                 @endif
                                        
                                        
                                           @endif
                                           
                                           @if($OR==0 && $EA==0)
                                          
                                        
                                                @if($EC>$CA)
                                                
                                                   <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="DIVERGENTE">
                                                 @endif
                                                
                                                @if($EC<$CA)
                                                
                                                    <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ASIMILADOR">
                                                @endif
                                        
                                        
                                           @endif
                                           
                                           
                                            @if($EC==0)
                                          
                                             <?php
                                             $asi = ($OR*$CA)/2;
                                             $con = ($CA*$EA)/2;
                                             ?>
                                             
                                             @if($asi>$con)
                                             
                                               <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ASIMILADOR">
                                              @endif
                                              @if($asi<$con)
                                             
                                                <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="CONVERGENTE">
                                              @endif
                                              
                                            @endif
                                            
                                            
                                          @if($OR==0)
                                          
                                             <?php
                                             $aco = ($EC*$EA)/2;
                                             $con = ($CA*$EA)/2;
                                             ?>
                                              @if($aco>$con)
                                             
                                                 <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ACOMODADOR">
                                              @endif
                                              
                                              @if($aco<$con)
                                             
                                                <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="CONVERGENTE">
                                             
                                              @endif
                                          @endif
                                          
                                          
                                          @if($CA==0)
                                          
                                             <?php
                                             $div = ($OR*$EC)/2;
                                             $aco = ($EC*$EA)/2;
                                             ?>
                                             
                                             @if($aco>$div)
                                             
                                                 <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ACOMODADOR">
                                             @endif
                                              @if($aco<$div)
                                             
                                               <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="DIVERGENTE">
                                             @endif
                                           @endif
                                        
                                        @if($EA==0)
                                          
                                           <?php
                                             $div = ($OR*$EC)/2;
                                             $asi = ($OR*$CA)/2;
                                             ?>
                                              @if($asi>$div)
                                             
                                                <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="ASIMILADOR">
                                              @endif
                                              
                                              @if($asi<$div)
                                             
                                               <input type="hidden" name="profile" class="form-control input-full" id="profile" placeholder="profile"  Value="DIVERGENTE">
                                              @endif
                                              
                                           @endif

                                        
                                        
                                       
                                        
                                      
                                        
                                        <div class="" style="float: right;">
                                            <input class='btn btn-lg btn-success-2' type='submit' value='Guardar'>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <style media="screen">
        h1, .h1 {
            font-size: 2.5rem;
            margin-bottom: .5rem;
            font-family: inherit;
            font-weight: 650;
            line-height: 1.2;
            color: inherit;
        }
        .cuadro{
            background-color: #F80943;
            color:#ffffff;
        }
        .cuadros{
            background-color: #71C126;
            color:#FFFFFF;
            font-size:25px;
            padding: 0.5em;
            border-radius: 0.2em;
        }
        .lead {
            font-size: 1.25rem;
            font-weight: 300;
        }
        .question-number{
            font-size: 0.95rem;
            font-weight: 850;
        }
        .btn-success-2 {
            color: #fff;
            background-color: #28a745;
            border-color: #28a745;
        }
        .dcenter-filters > .radio {
  display: inline-block;
}
    </style>
@stop

@section('js')
    <script defer src="{{ asset('js/fontawesome-all.js') }}"></script>

    {{-- <script>
        var timer;
        var time_left_segs = {{ $time_left }};

        function showRemaining() {
            time_left_segs = time_left_segs - 1;
            if (time_left_segs < 0) {

                clearInterval(timer);
                document.getElementById('countdown').innerHTML = 'EXPIRED!';
                return;
            }

            var minutes = Math.floor( time_left_segs / 60);
            var seconds = Math.floor( time_left_segs % 60 );

            document.getElementById('countdown').innerHTML = minutes + ' minutos, ';
            document.getElementById('countdown').innerHTML += seconds + ' segundos.';
        }

        timer = setInterval(showRemaining, 1000);
    </script> --}}
@stop
