@extends('layouts.page')

@section('header_title')
    {{ 'EVALUACION VIRTUAL' }}
@stop

@section('header_body')
    {{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content_main')
    <div class="main-panel">
        <div class="content">
            <nav class="navbar navbar-light bg-danger"><br></nav>
            <div class="album py-3 bg-light">
                <div class="container">
                    <main role="main" class="container">
                        <h1 class="mt-5" style="text-align: center;">Encuesta: {{ $userSurvey->survey->name }}, Finalizado </h1>
                        
                    </main>
                </div>

            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <style media="screen">
        h1, .h1 {
            font-size: 2.5rem;
            margin-bottom: .5rem;
            font-family: inherit;
            font-weight: 650;
            line-height: 1.2;
            color: inherit;
        }
        .cuadro{
            background-color: #F80943;
            color:#ffffff;
        }
        .cuadros{
            background-color: #71C126;
            color:#FFFFFF;
            font-size:25px;
            padding: 0.5em;
            border-radius: 0.2em;
        }
        .lead {
            font-size: 1.25rem;
            font-weight: 300;
        }
        .question-number{
            font-size: 0.95rem;
            font-weight: 850;
        }
        .btn-success-2 {
            color: #fff;
            background-color: #28a745;
            border-color: #28a745;
        }
    </style>
@stop

@section('js')
    <script defer src="{{ asset('js/fontawesome-all.js') }}"></script>

    {{-- <script>
        var timer;
        var time_left_segs = {{ $time_left }};

        function showRemaining() {
            time_left_segs = time_left_segs - 1;
            if (time_left_segs < 0) {

                clearInterval(timer);
                document.getElementById('countdown').innerHTML = 'EXPIRED!';
                return;
            }

            var minutes = Math.floor( time_left_segs / 60);
            var seconds = Math.floor( time_left_segs % 60 );

            document.getElementById('countdown').innerHTML = minutes + ' minutos, ';
            document.getElementById('countdown').innerHTML += seconds + ' segundos.';
        }

        timer = setInterval(showRemaining, 1000);
    </script> --}}
@stop

