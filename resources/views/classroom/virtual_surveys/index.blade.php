@extends('layouts.page')

@section('header_title')
    {{ 'ENCUESTA VIRTUAL' }}
@stop

@section('header_body')
    {{ strtoupper(\Auth::user()->name . ', ' . \Auth::user()->paternal . ' ' . \Auth::user()->maternal) }}
@stop

@section('content')
    <div class="album py-3 bg-light">
        <div class="container">
            <div class="row">
                @foreach ($surveys->where('destino_to','!=','user_profile') as $key => $survey)
                    <div class='col-md-4 exam'>
                        <div class='card mb-4 box-shadow'>
                            <div class='card-body'>
                                <p class='card-text'>
                                    <strong>ENCUESTA: </strong>
                                    <span style="color: green;">
                                        {{ $survey->name }}
                                    </span>
                                    <br>
                                    <strong>Activo: </strong>
                                    <span style="color: green;">
                                        {{ $survey->active ? 'Si' : 'No' }}
                                    </span>
                                    <br>
                                    <strong>DESTINADO:</strong>
                                    <span style="color: green;">
                                        {{ config('parameters.survey_destined')[$survey->destined_to] ?? '-' }}
                                    </span>
                                    <br>
                                    <strong>FECHA:</strong>
                                    <span style="color: green;">
                                        {{  \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD') }}
                                    </span>

                                </p>
                                <div class='d-flex justify-content-between align-items-center'><hr>
                                    {{-- @can ('canStartEvaluation', $certification) --}}
                                        <a class='btn btn-warning' href="{{ route('classroom.virtual_surveys.start',$survey->id) }}">Ir a Ecuesta</a>
                                    {{-- @endcan --}}
                                </div>
                            </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}" type="text/css" media="all">
@stop
