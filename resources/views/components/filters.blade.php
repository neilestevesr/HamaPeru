<div class="card">
    <form action="{{ $route }}" method="get">
        <div class="card-body">
            @if ( $dni_filter ?? false )
                <div class="form-group">
                    <label for="dni">DNI</label>
                    <input id="dni" type="text" name="dni" class="form-control {{ $errors->has('dni') ? 'is-invalid' : '' }}"
                    value="{{ old('dni') }}" placeholder="Ingresa dni">
                    @if($errors->has('dni'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('dni') }}</strong>
                        </div>
                    @endif
                </div>
            @endif
            @if ( $company_filter ?? false )
                <div class="form-group">
                    <label>Empresa</label>
                    <select id="company_id" name="company_id" class="form-control select2  {{ $errors->has('company_id') ? 'is-invalid' : '' }} " style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Empresa">
                        <option></option>
                        @foreach ($companies as $key => $company)
                            <option value="{{ $company->id }}" {{ Request::get('company_id') == $company->id ? 'selected' : '' }}>{{ $company->description }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('company_id'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('company_id') }}</strong>
                        </div>
                    @endif
                </div>
            @endif
            @if ( $instructor_filter ?? false )
                <div class="form-group">
                    <label>Instructor</label>
                    <select id="instructor_id" name="instructor_id" class="form-control select2  {{ $errors->has('instructor_id') ? 'is-invalid' : '' }} " style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Instructor">
                        <option></option>
                        @foreach ($instructors as $key => $instructor)
                            <option value="{{ $instructor->id }}" {{ Request::get('instructor_id') == $instructor->id ? 'selected' : '' }}>{{ $instructor->full_name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('instructor_id'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('instructor_id') }}</strong>
                        </div>
                    @endif
                </div>
            @endif
            @if ( $course_filter ?? false )
                <div class="form-group">
                    <label>Curso</label>
                    <select id="course_id" name="course_id" class="form-control select2 {{ $errors->has('course_id') ? 'is-invalid' : '' }} " style="width: 100%;" tabindex="-1" aria-hidden="true" data-placeholder="Seleccione Curso">
                        <option></option>
                        @foreach ($courses as $key => $course)
                            <option value="{{ $course->id }}" {{ Request::get('course_id') == $course->id ? 'selected' : '' }}>{{ $course->description }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('course_id'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('course_id') }}</strong>
                        </div>
                    @endif
                </div>
            @endif
            @if ( $date_from_filter ?? false )
                <div class="form-group">
                    <label>Desde</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                        <input id="date_from" type="text" name="date_from" autocomplete="off" class="form-control float-right {{ $errors->has('date_from') ? 'is-invalid' : '' }}"
                        value="{{ old('date_from') }}">
                        @if($errors->has('date_from'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('date_from') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
            @if ( $date_to_filter ?? false )
                <div class="form-group">
                    <label>Hasta</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                        <input id="date_to" type="text" name="date_to" autocomplete="off" class="form-control float-right {{ $errors->has('date_to') ? 'is-invalid' : '' }}"
                        value="{{ old('date_to') }}">
                        @if($errors->has('date_to'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('date_to') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>

@push('js')
    <script>
    $(function() {
        $('#instructor_id').select2({ allowClear: true });
        $('#course_id').select2({ allowClear: true });
        $('#company_id').select2({ allowClear: true });

        $('#date_from').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 2010,
            maxYear: parseInt(moment().format('YYYY'),10),
            singleClasses: "",
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        $('#date_to').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 2010,
            maxYear: parseInt(moment().format('YYYY'),10),
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        $('#date_from').attr("placeholder","Fecha Inicio");
        $('#date_to').attr("placeholder","Fecha Fin");

        $('#date_from').val("{{ Request::get('date_from') ?? ''}}");
        $('#date_to').val("{{ Request::get('date_to') ?? ''}}");

        $('#dni').val("{{ Request::get('dni') ?? ''}}");
    });
</script>
@endpush
