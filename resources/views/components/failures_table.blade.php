@if (session()->has('failures'))
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>Errores de Validación</strong></h3>
        </div>
        <div class="card-body">
            <div id="table_index_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="table_index" class="table table-danger table-bordered dataTable" role="grid" aria-describedby="table_index_info">
                            <thead>
                                <tr role="row">
                                    <th style="width: 50px;">Fila</th>
                                    <th rowspan="1" colspan="1" style="width: 50px;">Campo</th>
                                    <th class="sorting" rowspan="1" colspan="1" >Error</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (session()->get('failures') as $key => $validation)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $validation->row() }}</td>
                                        <td>{{ explode(".", $validation->attribute())[0] }}</td>
                                        <td>
                                            <ul>
                                                @foreach ($validation->errors() as $key => $e)
                                                    <li>{{ $e }}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
