@php
$title_alert = '';
$message_alert = '';
if( session('action') == 'created' ){
    $title_alert = 'Registrado!';
    $message_default = 'Se registró correctamente.';
}elseif ( session('action') == 'updated' ) {
    $title_alert = 'Actualizado!';
    $message_default = 'Se actualizó correctamente.';
} elseif ( session('action') == 'deleted' ) {
    $title_alert = 'Eliminado!';
    $message_default = 'Se eliminó correctamente.';
}else {
    $message_default = '';
}

$title_alert = session('title_alert') ?? $title_alert;
$message_alert = session('message') ?? $message_default;
@endphp

@if (session('status') == 'success')
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> {{ $object }} {{ $title_alert }}</h4>
        {{ $message_alert }}
    </div>
@endif

@if (session('status') == 'warning')
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-exclamation-triangle"></i> {{ $object }} {{ $title_alert }}</h5>
    @if (session('message_title'))
        <strong>{{ session('message_title') }}</strong>
    @endif
    @if (session('message_body'))
        <ul>
            @foreach (session('message_body') as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    @endif
</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
