<div class="modal fade" id="link-video-{{ $id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content timeout-modal">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                 
                <div class="text-center h4 mb-3">Video:  {{ $object ?? '' }}</div>
               <!-- <p class="text-center mb-4">¿Estás seguro que deseas eliminar {{ $object ?? '' }}: <strong>{{ $description ?? '' }}</strong> ? </p>-->
                <div id="timeout-activate-box">
                    <div class="row text-center">
                    <div class="modal-body">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe  width="560" height="315" src="{{ $description ?? '' }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
