<div class="modal fade" id="eliminacion-registro-{{ $id }}">
    <div class="modal-dialog" style="width:400px;" role="document">
        <div class="modal-content timeout-modal">
            <div class="modal-body">
                <button class="close" data-dismiss="modal" aria-label="Close"></button>

                <div class="text-center h4 mb-3">Eliminación de {{ $object ?? '' }}</div>
                <p class="text-center mb-4">¿Estás seguro que deseas eliminar {{ $object ?? '' }}: <strong>{{ $description ?? '' }}</strong> ? </p>
                <div id="timeout-activate-box">
                    <div class="row text-center">
                        <form action="{{ $url_delete }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary btn-fix btn-air" name="delete_modal">SI, deseo eliminar</button>
                            <button class="btn btn-danger btn-fix btn-air" data-dismiss="modal" >CANCELAR</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
