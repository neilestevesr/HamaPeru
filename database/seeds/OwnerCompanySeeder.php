<?php

use Illuminate\Database\Seeder;
use App\Models\OwnerCompany;

class OwnerCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner_company = OwnerCompany::create([
            'id' => 1,
            'name' => 'NEXA',
        ]);
    }
}
