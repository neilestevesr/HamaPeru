<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class HamaCuestionariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cuestionarios = DB::connection('mundoto1_capa')->table('cuestionarios')
        ->select('id','ci','id_datos','id_examen',
        'act1','act2','act3','act4','act5','act6','act7','act8','act9','act10',
        'resp1','resp2','resp3','resp4','resp5','resp6','resp7','resp8','resp9','resp10',
        'ran1','ran2','ran3','ran4','ran5','ran6','ran7','ran8','ran9','ran10',
        'cod_sala','cod_evento','fch_reinicio','flg_recupera','flg_asistencia',
        'codigo_empresa','fch_inicio','fch_fin')
        // ->where('id','>=',30000)
        //->where('cod_evento','111')
        ->get();

        $rejects = [];

        foreach ($cuestionarios as $key => $cuestionario) {
            try {
                // if($key > 100) break;
                $user = App\Models\User::where('dni',$cuestionario->ci)->first();

                $codigo_empresa = $cuestionario->codigo_empresa ?? 10;
                if ( in_array($cuestionario->codigo_empresa,[0,'']) ) $codigo_empresa = 10;

                $recovered_at = NULL;
                if($cuestionario->fch_reinicio != NULL && $cuestionario->fch_reinicio != '') $recovered_at = \Carbon\Carbon::createFromFormat('d-m-Y H:i:s', $cuestionario->fch_reinicio,'America/Lima')->isoFormat('YYYY-MM-DD HH:mm:ss');


                $total = $cuestionario->act1 == 10 ? 10 : 0;
                $total += $cuestionario->act2 == 10 ? 10 : 0;
                $total += $cuestionario->act3 == 10 ? 10 : 0;
                $total += $cuestionario->act4 == 10 ? 10 : 0;
                $total += $cuestionario->act5 == 10 ? 10 : 0;
                $total += $cuestionario->act6 == 10 ? 10 : 0;
                $total += $cuestionario->act7 == 10 ? 10 : 0;
                $total += $cuestionario->act8 == 10 ? 10 : 0;
                $total += $cuestionario->act9 == 10 ? 10 : 0;
                $total += $cuestionario->act10 == 10 ? 10 : 0;
                $score = $total*0.2;

                $status = 'pending';
                $time = NULL;

                if($score != 0){
                    $time = time();
                    $status = 'finished';
                }

                $certification = App\Models\Certification::create([
                    'id' => $cuestionario->id,
                    'assist_user' => $cuestionario->flg_asistencia == 'S' ? 'S' : 'N',
                    'recovered_at' => $recovered_at,
                    'status' => $status,
                    'user_id' => $user->id,
                    'event_id' => $cuestionario->cod_evento,
                    'company_id' => $codigo_empresa,
                    'position' => $user->position,
                    'evaluation_time' => $time,
                    'start_time' => NULL,
                    'end_time' => NULL,
                    'total_time' => NULL,
                    'score' => $score,
                ]);

                if($certification){

                     $preguntas = DB::connection('mundoto1_capa')->table('preguntas')
                     ->select('id_preguntas','id_examen','A','B','C','D','resp','preg','numero')
                     ->where('id_examen',$cuestionario->id_examen)
                     ->get();

                     if($preguntas->isNotEmpty()){

                         $result1 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act1,$cuestionario->resp1,$cuestionario->ran1);
                         $result2 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act2,$cuestionario->resp2,$cuestionario->ran2);
                         $result3 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act3,$cuestionario->resp3,$cuestionario->ran3);
                         $result4 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act4,$cuestionario->resp4,$cuestionario->ran4);
                         $result5 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act5,$cuestionario->resp5,$cuestionario->ran5);
                         $result6 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act6,$cuestionario->resp6,$cuestionario->ran6);
                         $result7 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act7,$cuestionario->resp7,$cuestionario->ran7);
                         $result8 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act8,$cuestionario->resp8,$cuestionario->ran8);
                         $result9 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act9,$cuestionario->resp9,$cuestionario->ran9);
                         $result10 = App\Http\Entities\Utils::saveEvaluation($certification,$preguntas,$cuestionario->act10,$cuestionario->resp10,$cuestionario->ran10);
                     }
                }

            } catch (\Exception $e) {
                echo $e->getMessage() . "-----------";
                array_push($rejects, ['id_cuestionario' => $cuestionario->id, 'ci' => $cuestionario->ci, 'message' => $e->getMessage()]);
            }
        }

        if(count($rejects) > 0){
            $file = fopen("storage\app\HamaCuestionariosSeeder.txt",'a') or die ("Error al crear");
            foreach ($rejects as $key => $reject) {
                fwrite($file,'id_cuestionario : ');
                fwrite($file,$reject['id_cuestionario']);
                fwrite($file,'  -- ci : ');
                fwrite($file,$reject['ci']);
                fwrite($file,'  -- message : ');
                fwrite($file,$reject['message']);
                fwrite($file,"\n");
            }
            fclose($file);
        }

        // foreach ($rejects as $key => $reject) {
        //     echo 'id_cuestionario: '. $reject['id_cuestionario'] . ' -- ci:' . $reject['ci'];
        //     echo '<br>';
        // }


    }
}
