<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class HamaUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = DB::connection('mundoto1_capa')->table('usuario')
        ->select('cod_usuario','dni','nombres','apellidos','materno','correo','codigo_empresa','unidad','ecm',
        'cargo','clave','telefono','rol','firma','flg_log','flg_firma','flg_activo','fch_registro','fch_acetp'
        )
        // ->where('cod_usuario','>=','2124')
        ->get();

        $roles = [
            'PAR' => 'participants',
            'INS' => 'instructor',
            'MASTER' => 'super_admin',
            'ADMIN' => 'admin',
            'SUP' => 'supervisor',
        ];

        $rejects = [];

        foreach ($usuarios as $key => $usuario) {
            $dni = $usuario->dni;
            $user = NULL;
            try {
                if ( in_array($usuario->cod_usuario,[2000,2001,2042]) ) continue;

                $unidad = [];

                if($usuario->cod_usuario <= 1999 || $usuario->cod_usuario >= 2083){

                    if ($usuario->unidad == 3) $unidad = [1,2];
                    elseif ( in_array($usuario->unidad,[1,2,4,5]) ) $unidad = [$usuario->unidad];

                    $user = App\Models\User::create([
                        'id' => $usuario->cod_usuario,
                        'dni' => $usuario->dni,
                        'name' => $usuario->nombres,
                        'paternal' => $usuario->apellidos,
                        'maternal' => $usuario->materno,
                        'email' => $usuario->correo,
                        'password' => Hash::make($usuario->clave),
                        'telephone' => $usuario->telefono,
                        'role' => $roles[$usuario->rol],
                        'signature' => $usuario->firma,
                        'active' => $usuario->flg_activo,
                        'position' => $usuario->cargo,
                        'company_id' => $usuario->codigo_empresa,
                    ]);

                    $user->miningUnits()->sync($unidad);
                }

                if($usuario->cod_usuario >= 2002 && $usuario->cod_usuario <= 2082){
                    if ($usuario->telefono == 'Si' || $usuario->telefono == 'si' || $usuario->telefono == 'SI') $unidad = [1];
                    else $unidad = [2];

                    $dni = $usuario->nombres;

                    $user = App\Models\User::create([
                        'id' => $usuario->cod_usuario,
                        'dni' => $usuario->nombres,
                        'name' => $usuario->apellidos,
                        'paternal' => $usuario->materno,
                        'maternal' => $usuario->codigo_empresa,
                        'email' => $usuario->unidad,
                        'password' => Hash::make($usuario->nombres),
                        'telephone' => $usuario->correo,
                        'role' => $roles[$usuario->rol],
                        'signature' => $usuario->firma,
                        'active' => $usuario->flg_activo,
                        'position' => $usuario->cargo,
                        'company_id' => NULL,
                    ]);

                    $user->miningUnits()->sync($unidad);
                }

                if($user && $user->signature == 'S'){

                    $full_path = "imagenes/firmas/$user->dni.png";
                    $file_url = \Storage::disk('s3')->url($full_path);

                    $file = new App\Models\File([
                    'file_path' => $full_path,
                    'file_url' => $file_url,
                    'file_type' => 'imagenes',
                    'category' => 'firmas'
                    ]);

                    $user->file()->save($file);
                }

            } catch (\Exception $e) {
                echo $e->getMessage() . "\n";
                array_push($rejects, ['cod_usuario' => $usuario->cod_usuario, 'dni' => $dni]);
            }
        }

        if(count($rejects) > 0){
            $file = fopen("storage\app\HamaUsuarioSeeder.txt",'a') or die ("Error al crear");
            foreach ($rejects as $key => $reject) {
                fwrite($file,'cod_usuario : ');
                fwrite($file,$reject['cod_usuario']);
                fwrite($file,'  -- dni : ');
                fwrite($file,$reject['dni']);
                fwrite($file,"\n");
            }
            fclose($file);
        }

        // foreach ($rejects as $key => $reject) {
        //     echo 'cod_usuario: '. $reject['cod_usuario'] . ' -- dni:' . $reject['dni'];
        //     echo '<br>';
        // }

    }
}
