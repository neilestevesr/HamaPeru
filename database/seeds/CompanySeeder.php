<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Company::class, 3)->create();

        /*factory(Company::class, 3)->create()->each(function ($company) {
            $company->users()->save(factory(User::class)->make());
        });*/

    }
}
