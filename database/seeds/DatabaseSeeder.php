<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        //$this->call(HamaEmpresaSeeder::class);
        //$this->call(HamaUsuarioSeeder::class);
        //$this->call(HamaPreguntasSeeder::class);
        //$this->call(HamaEventosSeeder::class);
        //$this->call(HamaCuestionariosSeeder::class);
        //$this->call(HamaAsistenciasFileSeeder::class);
        $this->call(HamaCertificadosUnidadMineraSeeder::class);



        // $this->call(OwnerCompanySeeder::class);
        // $this->call(CourseSeeder::class);
        // $this->call(CompanySeeder::class);
        // $this->call(UserSeeder::class);
        // $this->call(RoomSeeder::class);
        // $this->call(EventSeeder::class);
        // $this->call(MiningUnitSeeder::class);
        // $this->call(CertificationSeeder::class);
    }
}
