<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Certification;
use App\Models\User;

class HamaCertificadosUnidadMineraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $certifications = Certification::with('user.miningUnits')->get();

        $rejects = [];
        $count = 1;
        foreach ($certifications as $key => $certification) {
            try {

                $certification->miningUnits()->sync( $certification->user->miningUnits->pluck('id') );
                echo "Registro: " . $count++ . " - Certificado_id: " . $certification->id . "\n";

            } catch (\Exception $e) {
                echo $e->getMessage() . "\n";
                array_push($rejects, ['certification_id' => $certification->id]);
            }
        }

        if(count($rejects) > 0){
            $file = fopen("storage\app\HamaCertificadosUnidadMineraSeeder.txt",'a') or die ("Error al actualizar");
            foreach ($rejects as $key => $reject) {
                fwrite($file,'certification_id : ');
                fwrite($file,$reject['certification_id']);
                fwrite($file,"\n");
            }
            fclose($file);
        }

    }
}
