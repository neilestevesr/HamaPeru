<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Room;
use App\Models\Company;
use App\Models\OwnerCompany;
use App\Models\Course;


class HamaEmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ROOM
        $salas = DB::connection('mundoto1_capa')->table('sala')
        ->select('cod_sala','dsc_sala','aforo','ruta_sala','estado')
        ->get();

        foreach ($salas as $key => $sala) {
            Room::create([
                'id' => $sala->cod_sala,
                'description' => $sala->dsc_sala,
                'capacity' => $sala->aforo,
                'url_zoom' => $sala->ruta_sala,
                'active' => $sala->estado,
            ]);
        }

        //OWNER COMPANY
        $categorias = DB::connection('mundoto1_capa')->table('categoria')
        ->select('id','categoria')
        ->get();

        foreach ($categorias as $key => $categoria) {
            OwnerCompany::create([
                'id' => $categoria->id,
                'name' => $categoria->categoria,
            ]);
        }

        //COMPANY
        $empresas = DB::connection('mundoto1_capa')->table('empresa')
        ->select('codigo_empresa','dsc_empresa','abr_empresa','ruc','direccion','telefono','estado','nom_ref','telf_ref','correo_ref')
        ->get();

        foreach ($empresas as $key => $empresa) {
            App\Models\Company::create([
                'id' => $empresa->codigo_empresa,
                'description' => $empresa->dsc_empresa,
                'abbreviation' => $empresa->abr_empresa,
                'ruc' => $empresa->ruc,
                'address' => $empresa->direccion,
                'telephone' => $empresa->telefono,
                'name_ref' => $empresa->nom_ref,
                'telephone_ref' => $empresa->telf_ref,
                'email_ref' => $empresa->correo_ref,
                'active' => $empresa->estado,
            ]);

        }


        //COURSES
        $cursos = DB::connection('mundoto1_capa')->table('curso')
        ->select('codigo_curso','dsc_curso','fecha','horas','flg_activo','hr_inicio','hr_fin')
        ->get();

        foreach ($cursos as $key => $curso) {
                $string_inicio = Str::of($curso->hr_inicio != '' ? $curso->hr_inicio : '00:00 am')->replace(': ',':')->replace('pm ','pm')->replace('am ','am')->replace('.', ':')->replace('p:m:','pm')->replace('p:m','pm')->replace('a:m:','am')->replace('a:m','am');
                $hr_inicio = Str::contains($string_inicio, ['pm','am','p.m','a.m','p.m.','a.m.']) ? $string_inicio : $string_inicio.' am' ;

                $string_fin = Str::of($curso->hr_fin != '' ? $curso->hr_fin : '00:00 am')->replace(': ',':')->replace('pm ','pm')->replace('am ','am')->replace('.', ':')->replace('p:m:','pm')->replace('p:m','pm')->replace('a:m:','am')->replace('a:m','am');
                $hr_fin = Str::contains($string_fin, ['pm','am','p.m','a.m','p.m.','a.m.']) ? $string_fin : $string_fin.' pm' ;

                if($curso->codigo_curso <= 9){
                    App\Models\Course::create([
                        'id' => $curso->codigo_curso,
                        'description' => $curso->dsc_curso,
                        'date' => \Carbon\Carbon::createFromFormat('d/m/Y', $curso->fecha,'America/Lima')->isoFormat('YYYY-MM-DD'),
                        'hours' => $curso->horas != '' ? $curso->horas : 0,
                        'time_start' => \Carbon\Carbon::createFromFormat('H:i a', $hr_inicio,'America/Lima')->isoFormat('HH:mm:ss'),
                        'time_end' => \Carbon\Carbon::createFromFormat('H:i a', $hr_fin,'America/Lima')->isoFormat('HH:mm:ss'),
                        'active' => $curso->flg_activo,
                    ]);
                }
                else {
                    App\Models\Course::create([
                        'id' => $curso->codigo_curso,
                        'description' => $curso->dsc_curso,
                        'date' => \Carbon\Carbon::createFromFormat('d-m-Y H:i:s', $curso->fecha,'America/Lima')->isoFormat('YYYY-MM-DD'),
                        'hours' => $curso->horas,
                        'time_start' => \Carbon\Carbon::createFromFormat('H:i a', $hr_inicio,'America/Lima')->isoFormat('HH:mm:ss'),
                        'time_end' => \Carbon\Carbon::createFromFormat('H:i a', $hr_fin,'America/Lima')->isoFormat('HH:mm:ss'),
                        'active' => $curso->flg_activo,
                    ]);
                }
        }

        //MININ_UNIT
        $unidades = DB::connection('mundoto1_capa')->table('unidad_minera')
        ->select('cod_unidad','dsc_unidad')
        ->get();

        foreach ($unidades as $key => $unidad) {
            App\Models\MiningUnit::create([
                'id' => $unidad->cod_unidad,
                'description' => $unidad->dsc_unidad,
                'owner' => '',
                'district' => '',
                'Province' => '',
            ]);
        }

        //EXAM
        $examenes = DB::connection('mundoto1_capa')->table('examen')
        ->select('id','categoria','titulo','codigo_curso','estado','fecha_final','tiempo')
        ->get();

        foreach ($examenes as $key => $examen) {
            App\Models\Exam::create([
                'id' => $examen->id,
                'title' => $examen->titulo,
                'exam_time' => 20,
                'active' => $examen->estado == 'Publicado' ? 'S' : 'N',
                'owner_company_id' => 2,
                'course_id' => $examen->codigo_curso,
                'created_at' => \Carbon\Carbon::createFromFormat('Y-m-d', $examen->fecha_final,'America/Lima')->isoFormat('YYYY-MM-DD HH:mm:ss'),
            ]);
        }


    }
}
