<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\Exam;
use App\Models\Question;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Course::class, 4)->create()->each(function ($course) {
            $exam = factory(Exam::class)->make();
            $course->exams()->save($exam);
            $exam->questions()->saveMany(factory(Question::class,9)->make());
        });
    }
}
