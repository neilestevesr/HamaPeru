<?php

use Illuminate\Database\Seeder;

class MiningUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\MiningUnit::class, 3)->create();
    }
}
