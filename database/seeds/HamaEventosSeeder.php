<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class HamaEventosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eventos = DB::connection('mundoto1_capa')->table('eventos')
        ->select('cod_evento','tip_evento','fch_evento','id_examen','dni_capa','dsc_evento','estado_evento','cod_sala','flg_asist')
        //->where('cod_evento','111')
        ->get();

        $rejects = [];

        foreach ($eventos as $key => $evento) {
            try {
                $user = App\Models\User::where('dni','43734884')->first();
                $room_id = 1;

                if ( in_array($evento->cod_sala,[1,2,3,4,5,6]) ) $room_id = $evento->cod_sala;

                App\Models\Event::create([
                    'id' => $evento->cod_evento,
                    'description' => $evento->dsc_evento,
                    'type' => $evento->tip_evento,
                    'date' => \Carbon\Carbon::createFromFormat('Y-m-d', $evento->fch_evento,'America/Lima')->isoFormat('YYYY-MM-DD'),
                    'active' => $evento->estado_evento,
                    'flg_asist' => $evento->flg_asist == 'S' ? 'S' : 'N',
                    'exam_id' => $evento->id_examen,
                    'user_id' => $user->id,
                    'room_id' => $room_id,
                ]);

            } catch (\Exception $e) {
                echo $e->getMessage() . "\n";
                array_push($rejects, ['cod_evento' => $evento->cod_evento, 'id_examen' => $evento->id_examen, 'message' => $e->getMessage()]);
            }
        }

        if(count($rejects) > 0){
            $file = fopen("storage\app\HamaEventosSeeder.txt",'a') or die ("Error al crear");
            foreach ($rejects as $key => $reject) {
                fwrite($file,'cod_evento : ');
                fwrite($file,$reject['cod_evento']);
                fwrite($file,'  -- id_examen : ');
                fwrite($file,$reject['id_examen']);
                fwrite($file,'  -- message : ');
                fwrite($file,$reject['message']);
                fwrite($file,"\n");
            }
            fclose($file);
        }

        // foreach ($rejects as $key => $reject) {
        //     echo 'cod_evento: '. $reject['cod_evento'] . ' -- id_examen:' . $reject['id_examen'];
        //     echo '<br>';
        // }

    }
}
