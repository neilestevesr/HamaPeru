<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class HamaPreguntasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $preguntas = DB::connection('mundoto1_capa')->table('preguntas')
        ->select('id_preguntas','id_examen','A','B','C','D','resp','preg','numero')
        ->get();

        $rejects = [];

        foreach ($preguntas as $key => $pregunta) {
            try {
                $alternative_correct = '-';
                if($pregunta->A == $pregunta->resp) $alternative_correct = 'alternative_a';
                elseif ($pregunta->B == $pregunta->resp) $alternative_correct = 'alternative_b';
                elseif ($pregunta->C == $pregunta->resp) $alternative_correct = 'alternative_c';
                elseif ($pregunta->D == $pregunta->resp) $alternative_correct = 'alternative_d';

                App\Models\Question::create([
                    'id' => $pregunta->id_preguntas,
                    'statement' => $pregunta->preg,
                    'alternative_a' => $pregunta->A,
                    'alternative_b' => $pregunta->B,
                    'alternative_c' => $pregunta->C,
                    'alternative_d' => $pregunta->D,
                    'alternative_correct' => $alternative_correct,
                    'points' => 2,
                    'exam_id' => $pregunta->id_examen,
                ]);

            } catch (\Exception $e) {
                echo $e->getMessage() . "\n";
                array_push($rejects, ['id_preguntas' => $pregunta->id_preguntas, 'id_examen' => $pregunta->id_examen]);
            }
        }

        if(count($rejects) > 0){
            $file = fopen("storage\app\HamaPreguntasSeeder.txt",'a') or die ("Error al crear");
            foreach ($rejects as $key => $reject) {
                fwrite($file,'id_preguntas : ');
                fwrite($file,$reject['id_preguntas']);
                fwrite($file,'  -- id_examen : ');
                fwrite($file,$reject['id_examen']);
                fwrite($file,"\n");
            }
            fclose($file);
        }

        // foreach ($rejects as $key => $reject) {
        //     echo 'id_preguntas: '. $reject['id_preguntas'] . ' -- id_examen:' . $reject['id_examen'];
        //     echo '<br>';
        // }

    }
}
