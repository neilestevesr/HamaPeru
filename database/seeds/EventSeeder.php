<?php

use Illuminate\Database\Seeder;
use App\Models\Event;
use App\Models\Certification;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, 4)->create();
/*
        factory(Event::class, 4)->create()->each(function ($event) {
            $certification = factory(Certification::class)->make();
            $event->certifications()->save($certification);
        });
*/
    }
}
