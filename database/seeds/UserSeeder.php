<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'dni' => '47253577',
            'name' => 'Neil',
            'paternal' => 'Esteves',
            'maternal' => 'Rosales',
            'email' => 'neil.esteves@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('123456789'),
            'remember_token' => Str::random(10),
            'telephone' => '922462556',
            'role' => 'admin',
            'signature' => 'S',
            'active' => 'S',
        ]);

        $user = User::create([
            'dni' => '11111111',
            'name' => 'jorge',
            'paternal' => 'quispe',
            'maternal' => 'huaman',
            'email' => '11.1111@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('11111111'),
            'remember_token' => Str::random(10),
            'telephone' => '11111111',
            'role' => 'instructor',
            'signature' => 'S',
            'active' => 'S',
            'company_id' => '1',
        ]);

        $user = User::create([
            'dni' => '12345678',
            'name' => 'Luis',
            'paternal' => 'Salazar',
            'maternal' => 'Leon',
            'email' => 'luis_salazar@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('12345678'),
            'remember_token' => Str::random(10),
            'telephone' => '987654321',
            'role' => 'participants',
            'position' => 'obrero',
            'company_id' => '1',
            'signature' => 'S',
            'active' => 'S',
        ]);

        factory(User::class, 3)->create();
        /*factory(App\Models\User::class, 50)->create()->each(function ($user) {
            $user->posts()->save(factory(App\Models\Post::class)->make());
        });*/
    }
}
