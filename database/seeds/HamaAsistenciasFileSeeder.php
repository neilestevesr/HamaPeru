<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class HamaAsistenciasFileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivos = scandir("C:\\xampp\htdocs\capacitaciones-hama\AdminHAMA\archivos");
        unset($archivos[0],$archivos[1]); //elimina '.' , '..'

        $file_type = 'archivos';
        $category = 'asistencias';
        $rejects = [];

        foreach ($archivos as $key => $archivo) {
            try {
                $event_id = Str::of($archivo)->explode('.')[0];
                $event = App\Models\Event::find($event_id);

                if($event){
                    $full_path = "$file_type/$category/$archivo";
                    $file_url = \Storage::disk('s3')->url($full_path);

                    $file = new App\Models\File([
                        'file_path' => $full_path,
                        'file_url' => $file_url,
                        'file_type' => $file_type,
                        'category' => $category
                    ]);

                    $event->files()->save($file);
                }

            } catch (\Exception $e) {
                echo $e->getMessage() . "\n";
                array_push($rejects, ['archivo' => $archivo]);
            }
        }

        if(count($rejects) > 0){
            $file = fopen("storage\app\HamaAsistenciasFile.txt",'a') or die ("Error al crear");
            foreach ($rejects as $key => $reject) {
                fwrite($file,'archivo : ');
                fwrite($file,$reject['archivo']);
                fwrite($file,"\n");
            }
            fclose($file);
        }
    }
}
