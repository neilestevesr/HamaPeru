<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    static $number = 1;
    return [
        'description' => 'Empresa '.$number++,
        'abbreviation' => $faker->realText(20),
        'ruc' => $faker->randomNumber(8,false),
        'address' => $faker->address,
        'telephone' => $faker->randomNumber(9,false),
        'name_ref' => $faker->firstName,
        'telephone_ref' => $faker->randomNumber(9,false),
        'email_ref' => $faker->unique()->safeEmail,
        'active' => 'S',
    ];
});
