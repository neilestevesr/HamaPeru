<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OwnerCompany;
use Faker\Generator as Faker;

$factory->define(OwnerCompany::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(40),
    ];
});
