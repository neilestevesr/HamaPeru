<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\MiningUnit;
use Faker\Generator as Faker;

$factory->define(MiningUnit::class, function (Faker $faker) {
    static $number = 1;
    return [
        'description' => 'Unidad Minera '.$number,
        'owner' => 'Unidad Minera '.$number . ' S.A.C',
        'district' => 'Unidad Minera '.$number.$number++,
        'Province' => 'Pasco',
    ];
});
