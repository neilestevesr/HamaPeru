<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Faker\Provider\es_PE\Person as Person;
use Illuminate\Support\Facades\Hash;

$factory->define(User::class, function (Faker $faker) {
    $faker->addProvider(new Person($faker));
    $dni = $faker->unique()->dni;
    return [
        'dni' => $dni,
        'name' => $faker->firstName,
        'paternal' => $faker->lastName,
        'maternal' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make($dni),
        'email_verified_at' => now(),
        'remember_token' => Str::random(10),
        'telephone' => $faker->tollFreePhoneNumber,
        'role' => $faker->randomElement(array('participants')),
        'signature' => 'S',
        'active' => 'S',
        'company_id' => '1',
        //'mining_unit_id' => 'S',
    ];
});
