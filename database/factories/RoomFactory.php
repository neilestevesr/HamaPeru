<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Room;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    static $number = 1;
    return [
        'description' => 'Sala '.$number++,
        'capacity' => $faker->numberBetween(5,15),
        'url_zoom' => 'https://us02web.zoom.us/j/81116827986',
    ];
});
