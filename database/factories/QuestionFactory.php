<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'statement' => $faker->realText(100),
        'alternative_a' => $faker->realText(30),
        'alternative_b' => $faker->realText(30),
        'alternative_c' => $faker->realText(30),
        'alternative_d' => $faker->realText(30),
        'alternative_correct' => $faker->randomElement(array ('alternative_a','alternative_b','alternative_c','alternative_d')),
        //'exam_id' =>'1',
    ];
});
