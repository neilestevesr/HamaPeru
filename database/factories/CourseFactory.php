<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    static $number = 1;
    return [
        'description' => 'Curso '.$number++,
        //'date' => $faker->date('Y-m-d','now'),
        'date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+30 days', $timezone = null),
        'hours' => $faker->randomFloat(1,1,5),
        'time_start' => \Carbon\Carbon::now('America/Lima')->isoFormat('hh:mm:ss'),
        'time_end' => \Carbon\Carbon::now('America/Lima')->addHour()->isoFormat('hh:mm:ss'),
        'active' => 'S',

    ];
});
