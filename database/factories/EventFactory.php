<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    static $number = 1;
    return [
        'description' => 'Evento '.$number,
        'type' => $faker->randomElement(array ('virtual','present')),
        'date' => date("Y-m-d"),
        'active' => 'S',
        'flg_asist' => 'S',
        'active' => 'S',
        'exam_id' => $number++,
        'user_id' => '2',
        'room_id' => '1',
    ];
});
