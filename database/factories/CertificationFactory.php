<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Certification;
use Faker\Generator as Faker;

$factory->define(Certification::class, function (Faker $faker) {
    static $number = 1;
    return [
        // 'company_description' => $faker->company,
        // 'company_ruc' => '10' . $faker->unique()->dni . '9',
        'assist_user' => 'N',
        'recovered_at' => NULL,
        'status' => 'pending',
        'user_id' => 3,
        'event_id' => $number++,
        'company_id' => 1,
        'position' => 'AGENTE DE SEGURIDAD',
        'evaluation_time' => NULL,
    ];
});
