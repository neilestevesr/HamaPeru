<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Exam;
use Faker\Generator as Faker;

$factory->define(Exam::class, function (Faker $faker) {
    static $number = 1;
    return [
        'title' => 'Examen '.$number++,
        'exam_time' => $faker->randomNumber(1, false),
        'active' => 'S',
        'owner_company_id' => 1,
    ];
});
