<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('dni',11)->unique();
            $table->string('name');
            $table->string('paternal');
            $table->string('maternal');
            $table->string('email');
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('telephone',20)->nullable();
            $table->string('role')->nullable();//temporal
            $table->char('signature', 1)->default('N');
            $table->char('active', 1)->default('S');

            $table->unsignedBigInteger('company_id')->nullable();
            $table->string('position')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
