<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeyDynamicAlternatives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dynamic_alternatives', function (Blueprint $table) {
            $table->foreign('dynamic_question_id', 'dynamic_question_id_fk')->references('id')->on('dynamic_questions')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamic_alternatives', function (Blueprint $table) {
            $table->dropForeign('dynamic_question_id_fk');
        });
    }
}
