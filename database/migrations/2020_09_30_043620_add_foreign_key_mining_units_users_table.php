<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyMiningUnitsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mining_units_users', function (Blueprint $table) {
            $table->foreign('mining_unit_id', 'mining_units_users_mining_unit_fk')->references('id')->on('mining_units')->onUpdate('NO ACTION');
            $table->foreign('user_id', 'mining_units_users_users_fk')->references('id')->on('users')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mining_units_users', function (Blueprint $table) {
            $table->dropForeign('mining_units_users_mining_unit_fk');
            $table->dropForeign('mining_units_users_users_fk');
        });
    }
}
