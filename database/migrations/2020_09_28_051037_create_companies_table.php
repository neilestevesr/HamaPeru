<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->string('abbreviation',100)->nullable();
            $table->string('ruc',15)->unique();
            $table->string('address');
            $table->string('telephone',20)->nullable();
            $table->string('name_ref')->nullable();
            $table->string('telephone_ref',20)->nullable();
            $table->string('email_ref',50)->nullable();
            $table->char('active', 1)->default('S');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
