<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElearningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_learnings', function (Blueprint $table) {
            $table->id();
            $table->string('title',500);
            $table->string('description',500)->nullable();
            $table->char('active', 1)->default('S');
            $table->string('videos_section',500)->default('video');
            $table->string('contents_section',500)->default('contenido');
            $table->string('cases_section',500)->default('caso');
            $table->string('rvis_section',500)->default('rvi');
            $table->string('games_section',500)->default('juego');
            $table->string('files_section',500)->default('archivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_learnings');
    }
}
