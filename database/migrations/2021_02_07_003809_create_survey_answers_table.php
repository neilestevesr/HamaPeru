<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_answers', function (Blueprint $table) {
            $table->id();
            $table->string('statement')->nullable()->default(NULL);
            $table->string('answer')->nullable()->default(NULL);
            $table->Integer('question_order')->nullable();

            $table->unsignedBigInteger('user_survey_id');
            $table->unsignedBigInteger('statement_id');
            $table->timestamps();

            $table->foreign('user_survey_id', 'survey_answers_users_surveys_fk')->references('id')->on('users_surveys')->onUpdate('NO ACTION');
            $table->foreign('statement_id', 'users_surveys_statements_fk')->references('id')->on('statements')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_answers');
    }
}
