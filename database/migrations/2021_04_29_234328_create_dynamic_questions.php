<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDynamicQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_questions', function (Blueprint $table) {
            $table->id();
            $table->string('statement',1000);
            $table->float('points',5,2)->nullable()->default(2.0);
            $table->unsignedBigInteger('correct_alternative_id')->nullable();
            $table->unsignedBigInteger('exam_id');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_questions');
    }
}
