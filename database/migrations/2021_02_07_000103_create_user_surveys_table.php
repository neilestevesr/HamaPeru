<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_surveys', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('survey_id');
            $table->unsignedBigInteger('company_id');
            $table->date('date');
            $table->string('status')->default('pending');
            $table->dateTime('start_time')->nullable()->default(NULL);
            $table->dateTime('end_time')->nullable()->default(NULL);
            $table->integer('total_time')->nullable()->default(NULL);
            $table->timestamps();

            $table->foreign('user_id', 'users_surveys_users_fk')->references('id')->on('users')->onUpdate('NO ACTION');
            $table->foreign('survey_id', 'users_surveys_surveys_fk')->references('id')->on('surveys')->onUpdate('NO ACTION');
            $table->foreign('company_id', 'users_surveys_companies_fk')->references('id')->on('companies')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_surveys');
    }
}
