<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->string('statement',1000)->nullable()->default(NULL)->change();
            $table->string('correct_alternative',500)->nullable()->default(NULL)->change();
            $table->string('selected_alternative',500)->nullable()->default(NULL)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->string('statement')->nullable()->default(NULL)->change();
            $table->string('correct_alternative')->nullable()->default(NULL)->change();
            $table->string('selected_alternative')->nullable()->default(NULL)->change();
        });
    }
}
