<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->foreign('exam_id', 'events_exams_fk')->references('id')->on('exams')->onUpdate('NO ACTION');
            $table->foreign('user_id', 'events_users_fk')->references('id')->on('users')->onUpdate('NO ACTION');
            $table->foreign('room_id', 'events_rooms_fk')->references('id')->on('rooms')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign('events_exams_fk');
            $table->dropForeign('events_users_fk');
            $table->dropForeign('events_rooms_fk');
        });
    }
}
