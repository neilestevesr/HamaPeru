<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exams', function (Blueprint $table) {
            $table->foreign('owner_company_id', 'exams_owner_companies_fk')->references('id')->on('owner_companies')->onUpdate('NO ACTION');
            $table->foreign('course_id', 'exams_courses_fk')->references('id')->on('courses')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exams', function (Blueprint $table) {
            $table->dropForeign('exams_owner_companies_fk');
            $table->dropForeign('exams_courses_fk');
        });
    }
}
