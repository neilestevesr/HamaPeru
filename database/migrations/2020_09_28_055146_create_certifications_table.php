<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certifications', function (Blueprint $table) {
            $table->id();
            $table->char('assist_user', 1)->default('N');
            $table->dateTime('recovered_at')->nullable()->default(NULL);
            $table->string('status')->default('pending');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('event_id');
            $table->unsignedBigInteger('company_id');
            $table->string('position')->nullable()->default('');
            $table->unsignedBigInteger('evaluation_time')->nullable()->default(NULL);
            $table->dateTime('start_time')->nullable()->default(NULL);
            $table->dateTime('end_time')->nullable()->default(NULL);
            $table->integer('total_time')->nullable()->default(NULL);
            $table->float('score',5,2)->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certifications');
    }
}
