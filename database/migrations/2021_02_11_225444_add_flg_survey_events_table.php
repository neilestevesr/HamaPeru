<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlgSurveyEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->char('flg_survey_course', 1)->after('flg_asist')->default('N');
            $table->char('flg_survey_evaluation', 1)->after('flg_survey_course')->default('N');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('flg_survey_course');
            $table->dropColumn('flg_survey_evaluation');
        });
    }
}
