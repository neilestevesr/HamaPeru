<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('evaluation_time')->nullable()->default(NULL);
            $table->string('statement')->nullable()->default(NULL);
            $table->string('correct_alternative')->nullable()->default(NULL);
            $table->string('selected_alternative')->nullable()->default(NULL);
            $table->boolean('is_correct')->nullable()->default(NULL);
            $table->Integer('question_order');
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('certification_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
