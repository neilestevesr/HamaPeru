<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('certifications', function (Blueprint $table) {
            $table->foreign('user_id', 'certifications_users_fk')->references('id')->on('users')->onUpdate('NO ACTION');
            //$table->foreign('exam_id', 'certifications_exams_fk')->references('id')->on('exams')->onUpdate('NO ACTION');
            $table->foreign('event_id', 'certifications_events_fk')->references('id')->on('events')->onUpdate('NO ACTION');
            $table->foreign('company_id', 'certifications_rooms_fk')->references('id')->on('companies')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('certifications', function (Blueprint $table) {
            $table->dropForeign('certifications_users_fk');
            //$table->dropForeign('certifications_exams_fk');
            $table->dropForeign('certifications_events_fk');
            $table->dropForeign('certifications_rooms_fk');
        });
    }
}
