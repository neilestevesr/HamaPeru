<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyCertificationsMiningUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('certifications_mining_units', function (Blueprint $table) {
            $table->foreign('mining_unit_id', 'certifications_mining_units_mining_units_fk')->references('id')->on('mining_units')->onUpdate('NO ACTION');
            $table->foreign('certification_id', 'certifications_mining_units_certifications_fk')->references('id')->on('certifications')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('certifications_mining_units', function (Blueprint $table) {
            $table->dropForeign('certifications_mining_units_mining_units_fk');
            $table->dropForeign('certifications_mining_units_certifications_fk');
        });
    }
}
