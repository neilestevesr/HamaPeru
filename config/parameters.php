<?php

return [
    'alternatives' =>[
        'alternative_a' => 'Alternativa A',
        'alternative_b' => 'Alternativa B',
        'alternative_c' => 'Alternativa C',
        'alternative_d' => 'Alternativa D',
    ],

    'roles' =>[
        'super_admin' => 'Super Administrador',
        'admin' => 'Administrador',
        'instructor' => 'Instructor',
        'supervisor' => 'Supervisor',
        'participants' => 'Participante',
        'security_manager' => 'Ingeniero de Seguridad Nexa',
        'security_manager_admin' => 'Gerente de Seguridad Nexa',
        'technical_support' => 'Soporte Tecnico',
    ],

    'event_types' =>[
        'virtual' => 'Virtual',
        'present' => 'Presencial',
    ],

    'certification_status' =>[
        'pending' => 'Pendiente',
        'in_progress' => 'En curso',
        'finished' => 'Finalizado',
    ],

    'month_spanish' =>[
        '01' => 'Enero',
        '02' => 'Febrero',
        '03' => 'Marzo',
        '04' => 'Abril',
        '05' => 'Mayo',
        '06' => 'Junio',
        '07' => 'Julio',
        '08' => 'Agosto',
        '09' => 'Septiembre',
        '10' => 'Octubre',
        '11' => 'Noviembre',
        '12' => 'Diciembre',
    ],

    'file_categories' =>[
        'certificados' => 'certificados',
        'asistencias' => 'asistencias',
        'anexos' => 'anexos',
    ],

    'statement_types' =>[
        'commentary' => 'Comentario',
        'select_multi' => 'Selección Multiple',
    ],
    'survey_destined' =>[
        'course_live' => 'Ficha Sintomatológica',
        'evaluation' => 'Satisfacción',
        'user_profile' => 'Perfil de Usuario',
    ],

    'content_sections' =>[
        'videos_section' => 'Videos',
        'contents_section' => 'Contenidos',
        'cases_section' => 'Casos',
        'rvis_section' => 'RVI',
        'games_section' => 'Juegos',
        'files_section' => 'Archivos',
    ],

    'content_types' =>[
        'video' => 'Video',
        'image' => 'Imagen',
        'file' => 'Archivo',
        'url' => 'URL',
    ],
];
