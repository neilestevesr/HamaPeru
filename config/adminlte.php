<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => '',
    'title_prefix' => 'Hama Peru |',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-favicon
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-logo
    |
    */

    'logo' => '',
    //'logo' => '<b>Hama</b>PERU',
    'logo_img' => 'images/logo.png',
    //'logo_img_class' => 'brand-image img-circle elevation-3',
    'logo_img_class' => 'brand-image',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'AdminLTE',

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-user-menu
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => false,
    'usermenu_header_class' => 'bg-primary',
    'usermenu_image' => false,
    'usermenu_desc' => false,
    'usermenu_profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#71-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => true,
    'layout_fixed_navbar' => true,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#721-authentication-views-classes
    |
    */

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#722-admin-panel-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => 'bg-white',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-dark-primary elevation-4',
    'classes_sidebar_nav' => 'nav-child-indent',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#73-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#74-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#92-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,
    'laravel_mix_css_path' => 'css/app.css',
    'laravel_mix_js_path' => 'js/app.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#8-menu-configuration
    |
    */

    'menu' => [
        [
            'text' => 'search',
            'search' => false,
            'topnav' => false,
        ],
        [
            'text' => 'Principal',
            'icon'    => 'fas fa-th-large',
            'url'  => 'admin',
            //'can'  => 'admin',
        ],
        ['header' => 'MODULOS HAMA'],
        [
            'text'    => 'USUARIOS',
            'icon'    => 'fas fa-users',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/usuarios',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/usuarios/crear',
                ],
                [
                    'text' => 'Registro Masivo',
                    'url' => 'admin/usuarios/registro-masivo',
                ],

            ],
        ],
        [
            'text'    => 'EMPRESAS',
            'icon'    => 'fas fa-building',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/companias',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/companias/crear',
                ],

            ],
        ],
        [
            'text'    => 'EMPRESA TITULAR',
            'icon'    => 'far fa-building',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/owner-companies',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/owner-companies/crear',
                ],

            ],
        ],
        [
            'text'    => 'UNIDADES MINERAS',
            'icon'    => 'fas fa-mountain',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/unidades-mineras',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/unidades-mineras/crear',
                ],

            ],
        ],
        [
            'text'    => 'SALAS',
            'icon'    => 'fas fa-film',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/salas',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/salas/crear',
                ],

            ],
        ],
        [
            'text'    => 'E-LEARNING',
            'icon'    => 'fas fa-book',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/e-learnings',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/e-learnings/crear',
                ],

            ],
        ],
        [
            'text'    => 'CURSOS',
            'icon'    => 'fas fa-book',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/cursos',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/cursos/crear',
                ],

            ],
        ],
        [
            'text'    => 'EXAMENES',
            'icon'    => 'fas fa-laptop',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/examenes',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/examenes/crear',
                ],

            ],
        ],

        [
            'text'    => 'EXAMENES DINAMICOS',
            'icon'    => 'fas fa-laptop',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/examenes-dinamicos',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/examenes-dinamicos/crear',
                ],

            ],
        ],

        [
            'text'    => 'EXAMENES DE PRUEBA',
            'icon'    => 'fas fa-laptop',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/examenes-prueba-dinamicos',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/examenes-prueba-dinamicos/crear',
                ],

            ],
        ],

        [
            'text'    => 'EVENTOS',
            'icon'    => 'far fa-calendar',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/eventos',
                ],
                [
                    'text' => 'Recuperacion',
                    'url' => 'admin/eventos/recovery',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/eventos/crear',
                ],
            ],
        ],

        [
            'text'    => 'EVENTOS',
            'icon'    => 'far fa-calendar',
            'can'  => 'onlySoporte',
            'submenu' => [
                [
                    'text' => 'Todos',
                    'url' => 'admin/eventos',
                ],
                [
                    'text' => 'Recuperacion',
                    'url' => 'admin/eventos/recovery',
                ],
               
            ],
        ],
        
         [
            'text'    => 'EXPORTAR DATOS',
            'icon'    => 'far fa-calendar',
            'can'  => 'onlySoporte',
            'submenu' => [
                
                [
                    'text' => 'Reporte Desaprobados',
                    'url' => 'admin/exportar-desaprobados',
                ],
            ],
        ],
        
         [
            'text'    => 'DOCUMENTOS',
            'icon'    => 'fas fa-file',
            'can'  => 'onlySoporte',
            'submenu' => [
                [
                    'text' => 'Periodo 2023',
                    'url' => 'admin/certificados',
                ],
                
                [
                    'text' => 'Asistencia grupal',
                    'url' => 'admin/asistencia/grupal',
                ],
                [
                    'text' => 'Asistencia Individual',
                    'url' => 'admin/asistencia/individual',
                ],
               
            ],
        ],

        [
            'text'    => 'ENCUESTAS',
            'icon'    => 'fas fa-laptop',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Migrar a Excel',
                    'url' => 'admin/encuestas/exportar',
                ],
                [
                    'text' => 'Reporte Perfil',
                    'url' => 'admin/encuestas/exportar-perfil',
                ],
                [
                    'text' => 'Todos',
                    'url' => 'admin/encuestas',
                ],
                [
                    'text' => 'Crear',
                    'url' => 'admin/encuestas/crear',
                ],
            ],
        ],
        
         [
            'text'    => 'ENCUESTAS',
            'icon'    => 'fas fa-laptop',
            'can'  => 'onlySupervisor',
            'submenu' => [
                [
                    'text' => 'Reporte Satisfaccion',
                    'url' => 'https://app.powerbi.com/reportEmbed?reportId=323273e7-1fd7-43ad-9d37-37ef429722cc&autoAuth=true&ctid=ba3ab148-b925-4b77-ac85-a09bba519da1&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXBhYXMtMS1zY3VzLXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9',
                ],

                [
                    'text' => 'Gestion de Capacitacion',
                    'url' => 'https://app.powerbi.com/reportEmbed?reportId=271bd3ff-16a1-44b0-b150-dd6bf8f09076&autoAuth=true&ctid=ba3ab148-b925-4b77-ac85-a09bba519da1&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXBhYXMtMS1zY3VzLXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9',
                ],               
               
            ],
         ],


        [
            'text'    => 'EXPORTAR DATOS',
            'icon'    => 'far fa-calendar',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Migrar a Excel',
                    'url' => 'admin/exportar',
                ],
                [
                    'text' => 'Reporte Desaprobados',
                    'url' => 'admin/exportar-desaprobados',
                ],
            ],
        ],
        [
            'text'    => 'DOCUMENTOS',
            'icon'    => 'fas fa-file',
            'can'  => 'onlyAdmin',
            'submenu' => [
                [
                    'text' => 'Periodo 2023',
                    'url' => 'admin/certificados',
                ],
                [
                    'text' => 'Periodo 2018 - 2020-1',
                    'url' => 'admin/certificados-antiguos',
                ],
                [
                    'text' => 'Asistencia grupal',
                    'url' => 'admin/asistencia/grupal',
                ],
                [
                    'text' => 'Asistencia Individual',
                    'url' => 'admin/asistencia/individual',
                ],
                [
                    'text' => 'Gestion de Archivos',
                    'url' => 'admin/archivos',
                ],
                [
                    'text' => 'Consulta gestion Arch.',
                    'url' => 'admin/archivos-hist',
                ],
            ],
        ],
        [
            'text'    => 'BUSQUEDA RAPIDA',
            'icon'    => 'fas fa-search',
            'can'  => 'onlySupervisor',
            'submenu' => [
                [
                    'text' => 'Asistencia Individual',
                    'url' => 'admin/asistencia/individual',
                ],
            ],
        ],
        
        [
            'text'    => 'BUSQUEDA RAPIDA',
            'icon'    => 'fas fa-search',
            'can'  => 'onlySupervisor',
            'submenu' => [
                
                [
                    'text' => 'Asistencia Individual',
                    'url' => 'admin/asistencia/individual',
                ],
                
            ],
        ],
        [
            'text'    => 'DOCUMENTOS CERTIFICADOS',
            'icon'    => 'fas fa-file',
            'can'  => 'adminAndSupervisor',
            'submenu' => [
                /*[
                    'text' => 'Ver Anexos 4',
                    'url' => 'admin/doc-certificados/anexos',
                ],*/
                [
                    'text' => 'Ver Asistencias',
                    'url' => 'admin/doc-certificados/asistencias',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#83-custom-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#91-plugins
    |
    */

    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'vendor/datatables/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'vendor/datatables/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => 'vendor/datatables/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    //'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                    'location' => 'vendor/select2/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => 'vendor/select2/css/select2.min.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
        'Fontawesome' => [
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => 'vendor/fontawesome-free/css/v4-shims.min.css',
                ]
            ],
        ],
        'Moment' => [
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'vendor/moment/moment.min.js',
                ],
            ],
        ],
        'Daterangepicker' => [
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => 'vendor/daterangepicker/daterangepicker.css',
                ],
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'vendor/daterangepicker/daterangepicker.js',
                ],
            ],
        ],
        'Tempusdominus' => [
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => 'vendor\tempusdominus-bootstrap-4\css\tempusdominus-bootstrap-4.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'vendor\tempusdominus-bootstrap-4\js\tempusdominus-bootstrap-4.min.js',
                ],
            ],
        ],
        'Bs-custom-file-input' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'vendor\bs-custom-file-input\bs-custom-file-input.min.js',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Livewire
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Livewire support.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#93-livewire
    */

    'livewire' => false,
];
