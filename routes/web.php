<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Room;
use App\Mail\ConfirmationMailable;


//Auth::routes();
Route::get('admin/login', 'Auth\LoginController@showAdminLoginForm')->name('admin_login');
Route::get('aula/login', 'Auth\LoginController@showClassroomLoginForm')->name('classroom_login');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/certificados','CertificateController@certificates')->name('certificates.index');
// Route::get('/certificados/{certification}/exam', 'CertificateController@exportExamPDF')->name('certificates.exam_pdf');
Route::get('/certificados/{certification}/pdf', 'CertificateController@exportCertificatePDF')->name('certificates.certificate_pdf');
Route::get('/certificados/{certification}/pdf-externos', 'CertificateController@exportCertificatePDFexternos')->name('certificates.certificate_pdf_externos');
Route::get('/certificados/{certification}/pdf-externos2', 'CertificateController@exportCertificatePDFexternos2')->name('certificates.certificate_pdf_externos_2');
Route::get('/certificados/{certification}/pdf-old', 'CertificateController@exportCertificatePDFold')->name('certificates.certificate_pdf_old');
Route::get('/certificados/{certification}/anexo/{mining_unit}', 'CertificateController@exportAnexoPDF')->name('certificates.anexo4');
Route::get('/certificados/{certification}/compromiso/{mining_unit}', 'CertificateController@exportCompromisoPDF')->name('certificates.compromiso');
Route::get('/certificados/{certification}/compromiso-old/{mining_unit}', 'CertificateController@exportCompromisoPDFold')->name('certificates.compromisoOld');
Route::get('/certificados/{file}/descarga', 'CertificateController@downloadFile')->name('certificates.download');

Route::get('/certificados/2020-1','CertificateController@certificatesOld')->name('certificates_old.index');
Route::get('/certificados/2020-1/descarga', 'CertificateController@downloadCertificatesOld')->name('certificates_old.download');


Route::middleware(['auth'])->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');

    Route::prefix('admin')->namespace('Admin')->middleware('only.admin')->group(function(){
        Route::get('/','AdminController@index')->name('admin_home');

        Route::get('/usuarios/registro-masivo', 'UserController@registerMassive')->name('users.register_massive');
        Route::post('/usuarios/registro-masivo', 'UserController@storeMassive')->name('users.store_massive');
        
        Route::get('/usuarios/confirmar-registro-mail', function() {
            
            $correo = new ConfirmationMailable;
            Mail::to('rickard140893@gmail.com')->send($correo);
            
            return "Mensaje Enviado";
        });
        
        

        Route::get('/eventos/recovery', 'EventController@indexRecovery')->name('events.index_recovery');
        Route::get('/eventos/recovery/{event}', 'EventController@showRecovery')->name('events.show_recovery');        
                 
        Route::get('/eventos/{event}/registro-participantes', 'EventController@registerParticipants')->name('events.register_participants');
        Route::post('/eventos/{event}/registro-participantes', 'EventController@storeParticipants')->name('events.store_participants');
        Route::get('/eventos/{event}/registro-masivo-participantes', 'EventController@registerMassiveParticipants')->name('events.register_massive_participants');
        Route::get('/eventos/{event}/registro-masivo-participantes-scoress', 'EventController@registerMassiveParticipantsScore')->name('events.register_massive_participants_scores');
        
        Route::get('/eventos/{event}/registro-masivo-participantes-scoress-practice', 'EventController@registerMassiveParticipantsScorePractice')->name('events.register_massive_participants_scores_practice');
        
        Route::get('/eventos/{event}/registro-masivo-participantes-area', 'EventController@registerMassiveParticipantsArea')->name('events.register_massive_participants_area');
        
        
        Route::post('/eventos/{event}/registro-masivo-participantes', 'EventController@storeMassiveParticipants')->name('events.store_massive_participants');
        Route::post('/eventos/{event}/registro-masivo-participantes-scoress', 'EventController@storeMassiveParticipantsScore')->name('events.store_massive_participants_scores');
        Route::post('/eventos/{event}/registro-masivo-participantes-area', 'EventController@storeMassiveParticipantsArea')->name('events.store_massive_participants_area');
        
        Route::post('/eventos/{event}/registro-masivo-participantes-scoress-practico', 'EventController@storeMassiveParticipantsScorePractice')->name('events.store_massive_participants_scores_practice');

        Route::post('/certificados/{certification}/reset', 'CertificationController@reset')->name('certifications.reset');
        Route::get('/certificados/{certification}/pdf', 'CertificationController@exportCertificatePDF')->name('certifications.certificate_pdf');
        Route::get('/certificados/{certification}/pdf-externos', 'CertificationController@exportCertificatePDFexternos')->name('certifications.certificate_pdf_externos');
        Route::get('/certificados/{certification}/pdf-externos-2', 'CertificationController@exportCertificatePDFexternos2')->name('certification.certificate_pdf_externos_2');
        Route::get('/certificados/{certification}/exam', 'CertificationController@exportExamPDF')->name('certifications.exam_pdf');
        Route::get('/certificados/{certification}/anexo/{mining_unit}', 'CertificationController@exportAnexoPDF')->name('certifications.anexo4');
        Route::get('/certificados/{certification}/anexo-atacocha/{mining_unit}', 'CertificationController@exportAnexoPDFAtacocha')->name('certifications.anexo4Atacocha');
        Route::get('/certificados/{certification}/anexo-porvenir/{mining_unit}', 'CertificationController@exportAnexoPDFPorvenir')->name('certifications.anexo4Porvenir');
        Route::get('/certificados/{certification}/anexo-old/{mining_unit}', 'CertificationController@exportAnexoPDFold')->name('certifications.anexo4old');
        Route::get('/certificados/{certification}/compromiso/{mining_unit}', 'CertificationController@exportCompromisoPDF')->name('certifications.compromiso');
        Route::get('/certificados/{certification}/compromiso-old/{mining_unit}', 'CertificationController@exportCompromisoPDFold')->name('certifications.compromisoOld');

        Route::get('/exportar', 'CertificationController@exportData')->name('certifications.export_data');
        Route::get('/exportar-desaprobados', 'CertificationController@exportDataRecovery')->name('certifications.export_data_recovery');
        Route::get('/exportar/descarga', 'CertificationController@downloadExcel')->name('certifications.export_download_excel');
        Route::get('/exportar/descarga-desaprobados', 'CertificationController@downloadExcelRecovery')->name('certifications.export_download_excel_recovery');

        Route::get('/encuestas/exportar', 'SurveyController@exportData')->name('surveys.export_data');
        Route::get('/encuestas/exportar-perfil', 'SurveyController@exportDataProfile')->name('surveys.export_data_profile');
        Route::get('/encuestas/exportar/descarga', 'SurveyController@downloadExcel')->name('surveys.export_download_excel');
        Route::get('/encuestas/descarga-perfil', 'SurveyController@downloadExcelprofile')->name('surveys.export_download_excel_profile');

        Route::get('/archivos', 'FileController@managementFile')->name('files.management');
        Route::get('/archivos/{file}/descarga', 'FileController@downloadFile')->name('files.download');
        Route::post('/archivos', 'FileController@storeFile')->name('files.store');
        Route::delete('/archivos/{file}', 'FileController@destroy')->name('files.destroy');
        
        Route::get('/archivos-hist', 'FileController@managementHistFile')->name('files.managementhist');
        Route::get('/archivos-hist', 'FileController@filterFile')->name('files.filter');
        Route::get('/archivos-hist/{file}/descarga', 'FileController@downloadFile')->name('files.download');
        Route::post('/archivos-hist', 'FileController@storeFile')->name('files.store');
        Route::delete('/archivos-hist/{file}', 'FileController@destroy')->name('files.destroy');

        Route::get('/asistencia/grupal', 'CertificationController@groupAssist')->name('certifications.group_assist');
        Route::get('/asistencia/individual', 'CertificationController@individualAssist')->name('certifications.individual_assist');
        Route::get('/asistencia/grupal/{event}', 'CertificationController@exportGroupAssistPDF')->name('certifications.group_assist_pdf');
        Route::get('/asistencia/grupal-atacocha/{event}', 'CertificationController@exportGroupAssistAtacochaPDF')->name('certifications.group_assist_atacocha_pdf');
        Route::get('/asistencia/grupal-porvenir/{event}', 'CertificationController@exportGroupAssistPorvenirPDF')->name('certifications.group_assist_porvenir_pdf');
        Route::get('/asistencia/individual/{certification}', 'CertificationController@exportIndividualAssistPDF')->name('certifications.individual_assist_pdf');

        Route::get('/certificados-antiguos', 'CertificationOldController@index')->name('certification_old.index');
        Route::get('/certificados-antiguos/descarga', 'CertificationOldController@download')->name('certification_old.download');

        Route::get('/doc-certificados/anexos', 'CertifiedDocumentsController@anexos')->name('certified_documents.anexos');
        Route::get('/doc-certificados/asistencias', 'CertifiedDocumentsController@assists')->name('certified_documents.assists');

        Route::get('/usuarios/import/template', 'UserController@downloadTemplate')->name('users.import.download_template');

        Route::resource('/unidades-mineras', 'MiningUnitController', ['names' => 'mining_units', 'parameters' => ['unidades-mineras' => 'mining_unit'], 'except' => ['show']]);
        Route::resource('/companias', 'CompanyController', ['names' => 'companies', 'parameters' => ['companias' => 'company']]);
        Route::resource('/owner-companies', 'OwnerCompanyController', ['names' => 'owner_companies', 'parameters' => ['owner-companies' => 'owner_company'], 'except' => ['show']]);
        Route::resource('/salas', 'RoomController', ['names' => 'rooms', 'parameters' => ['salas' => 'room']]);
        Route::resource('/cursos', 'CourseController', ['names' => 'courses', 'parameters' => ['cursos' => 'course']]);
        #Route::post('/contenidos/{content}', 'ELearningContentController@update')->name('elearnings.contents.update');
        Route::resource('/e-learnings', 'ELearningController', ['names' => 'elearnings', 'parameters' => ['e-learnings' => 'elearning']]);
        Route::resource('e-learnings.contenidos', 'ELearningContentController', ['names' => 'elearnings.contents', 'parameters' => ['e-learnings' => 'elearning','contenidos' => 'content'], 'except' => ['index']])->shallow();
        Route::resource('/usuarios', 'UserController', ['names' => 'users', 'parameters' => ['usuarios' => 'user']]);
        Route::resource('/examenes', 'ExamController', ['names' => 'exams', 'parameters' => ['examenes' => 'exam']]);
        Route::resource('examenes.preguntas', 'ExamQuestionController', ['names' => 'exams.questions', 'parameters' => ['examenes' => 'exam','preguntas' => 'question'], 'except' => ['index']])->shallow();

        Route::post('/alternativas-dinamicas/{dynamic_alternative}', 'DynamicQuestionAlternativeController@selectCorrectAlternative')->name('dynamic_questions.dynamic_alternatives.select_correct');
        Route::post('/preguntas-dinamicas/{dynamic_question}', 'DynamicExamQuestionController@storeMultimedia')->name('dynamic_exams.dynamic_questions.store_file');
        Route::resource('/examenes-dinamicos', 'DynamicExamController', ['names' => 'dynamic_exams', 'parameters' => ['examenes-dinamicos' => 'dynamic_exam']]);
        Route::resource('/examenes-prueba-dinamicos', 'DynamicTestExamController', ['names' => 'dynamic_test_exams', 'parameters' => ['examenes-prueba-dinamicos' => 'dynamic_test_exam']]);
        Route::resource('examenes-dinamicos.preguntas-dinamicas', 'DynamicExamQuestionController', ['names' => 'dynamic_exams.dynamic_questions', 'parameters' => ['examenes-dinamicos' => 'dynamic_exam','preguntas-dinamicas' => 'dynamic_question'], 'except' => ['index']])->shallow();
        Route::resource('preguntas-dinamicas.alternativas-dinamicas', 'DynamicQuestionAlternativeController', ['names' => 'dynamic_questions.dynamic_alternatives', 'parameters' => ['preguntas-dinamicas' => 'dynamic_question','alternativas-dinamicas' => 'dynamic_alternative'], 'except' => ['index']])->shallow();


        Route::resource('/eventos', 'EventController', ['names' => 'events', 'parameters' => ['eventos' => 'event']]);
        Route::resource('/certificados', 'CertificationController', ['names' => 'certifications', 'parameters' => ['certificados' => 'certification']]);
        Route::resource('/encuestas', 'SurveyController', ['names' => 'surveys', 'parameters' => ['encuestas' => 'survey']]);
        Route::resource('encuestas.grupos', 'GroupController', ['names' => 'groups', 'parameters' => ['encuestas' => 'survey','grupos' => 'group'], 'except' => ['index']])->shallow();
        Route::resource('grupos.enunciados', 'StatementController', ['names' => 'statements', 'parameters' => ['grupos' => 'group','enunciados' => 'statement'], 'except' => ['index']])->shallow();
        Route::resource('enunciados.opciones', 'OptionController', ['names' => 'options', 'parameters' => ['enunciados' => 'statement','opciones' => 'option'], 'except' => ['index','show']])->shallow();

    });

    Route::prefix('aula')->namespace('Classroom')->middleware('only.classroom')->group(function(){
        Route::get('/','ClassroomController@home')->name('classroom.home');
        Route::get('/perfil_encuesta','ClassroomController@profile_survey')->name('classroom.profile_survey');
        Route::get('/perfil','ProfileController@index')->name('classroom.profile.index');
        Route::post('perfil-usuario','ProfileController@userProfile')->name('classroom.profile.user_profile');

        Route::get('/e-learnings/contenidos/archivos/{file}/descarga', 'ELearningController@downloadFile')->name('classroom.elearnings.contents.files.download');
        Route::get('/e-learnings','ELearningController@index')->name('classroom.elearnings.index');
        Route::get('/e-learnings/cazador','ELearningController@cazador')->name('classroom.elearnings.cazador');
        Route::get('/e-learnings/cazador-prot-maquinas','ELearningController@cazadorProtMaquinas')->name('classroom.elearnings.cazador_prot_maquinas');
        Route::get('/e-learnings/cazador-prev-caidas','ELearningController@cazadorPrevCaidas')->name('classroom.elearnings.cazador_prev_caidas');

        Route::get('/cursos','CourseController@index')->name('classroom.courses.index')->middleware('not.instructor');
        Route::get('/cursos/vivo/list','CourseController@liveList')->name('classroom.courses.live_list')->middleware('not.instructor');
        Route::get('/cursos/vivo/list-security','CourseController@liveSecurityList')->name('classroom.courses.live_security_list')->middleware('not.instructor');
        Route::get('/cursos/vivo/{certification}','CourseController@live')->name('classroom.courses.live')->middleware('not.instructor');
        Route::get('/cursos/recuperacion','CourseController@retrieval')->name('classroom.courses.retrieval')->middleware('not.instructor');

        Route::get('/firma-digital','DigitalSignatureController@index')->name('classroom.digital_signatures.index');
        Route::get('/firma-digital/crear','DigitalSignatureController@create')->name('classroom.digital_signatures.create');
        Route::post('/firma-digital/crear','DigitalSignatureController@store')->name('classroom.digital_signatures.store');
        
        Route::get('/firma-digital-security/{event}','DigitalSignatureController@indexSecurity')->name('classroom.digital_signatures.index_security');
        Route::get('/firma-digital-security/crear-security/{event}','DigitalSignatureController@createSecurity')->name('classroom.digital_signatures.create_security');
        Route::post('/firma-digital-security/crear-security/{event}','DigitalSignatureController@storeSecurity')->name('classroom.digital_signatures.store_security');
        
        Route::get('/firma-digital-security_por/{event}','DigitalSignatureController@indexSecurityPor')->name('classroom.digital_signatures.index_security_por');
        Route::get('/firma-digital-security_por/crear-security/{event}','DigitalSignatureController@createSecurityPor')->name('classroom.digital_signatures.create_security_por');
        Route::post('/firma-digital-security_por/crear-security/{event}','DigitalSignatureController@storeSecurityPor')->name('classroom.digital_signatures.store_security_por');


        Route::get('/evaluacion-virtual','VirtualEvaluationController@index')->name('classroom.virtual_evaluations.index')->middleware('not.instructor');
        Route::get('/evaluacion-virtual/{certification}','VirtualEvaluationController@start')->name('classroom.virtual_evaluations.start')->middleware('not.instructor');
        Route::get('/evaluacion-virtual/{certification}/pregunta/{question_order}','VirtualEvaluationController@showQuestion')->name('classroom.virtual_evaluations.show_question')->middleware('not.instructor');
        Route::post('/evaluacion-virtual/{certification}/pregunta/{question_order}','VirtualEvaluationController@answerQuestion')->name('classroom.virtual_evaluations.anwser_question')->middleware('not.instructor');

        Route::get('/encuesta-virtual-evaluacion','VirtualSurveyController@evaluationSurveysIndex')->name('classroom.virtual_surveys.evaluations_index')->middleware('not.instructor');
        Route::get('/encuesta-virtual-curso','VirtualSurveyController@courseSurveysIndex')->name('classroom.virtual_surveys.courses_index')->middleware('not.instructor');
        Route::get('/encuesta-virtual-perfil','VirtualSurveyController@profileSurveysIndex')->name('classroom.virtual_surveys.profile_survey_index')->middleware('not.instructor');
        Route::get('/encuesta-virtual/{survey}/evento/{event}','VirtualSurveyController@start')->name('classroom.virtual_surveys.start')->middleware('not.instructor');
        Route::get('/encuesta-virtual/{survey}','VirtualSurveyController@startProfile')->name('classroom.virtual_surveys.start_profile')->middleware('not.instructor');
        Route::get('/encuesta-virtual/{user_survey}/pregunta/{question_order}','VirtualSurveyController@showQuestion')->name('classroom.virtual_surveys.show_question')->middleware('not.instructor');
        Route::post('/encuesta-virtual/{user_survey}/pregunta/{question_order}','VirtualSurveyController@answerQuestion')->name('classroom.virtual_surveys.anwser_question')->middleware('not.instructor');

        
        Route::get('/eventos-asistencia/{event}','EventController@showSecurity')->name('classroom.courses.show');
        Route::post('/eventos-asistencia/{event}','EventController@assistSecurity')->name('classroom.courses.assist');
        Route::middleware(['only.instructor'])->group(function () {
        
            Route::get('/graficas/{event}','EventController@highcharts')->name('classroom.events.highcharts');
            Route::get('/eventos','EventController@index')->name('classroom.events.index');
            Route::get('/eventos/{event}','EventController@show')->name('classroom.events.show');
            Route::post('/eventos/{event}','EventController@assist')->name('classroom.events.assist');
            Route::post('/eventos/{event}/conclude','EventController@conclude')->name('classroom.events.conclude');
        });
        
        Route::middleware(['only.instructor'])->group(function () {
        
            Route::get('/graficas/{event}','EventController@highcharts')->name('classroom.events.highcharts');
            Route::get('/eventos','EventController@index')->name('classroom.events.index');
            Route::get('/eventos/{event}','EventController@show')->name('classroom.events.show');
            Route::post('/eventos/{event}','EventController@assist')->name('classroom.events.assist');
            Route::post('/eventos/{event}/conclude','EventController@conclude')->name('classroom.events.conclude');
        });
        

    });
});


Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('classroom_login');
});

// Route::get('/test', function () {
// });


Route::fallback(function () {
    //return view('404');
});
