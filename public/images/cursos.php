
<!DOCTYPE html>
<html lang="en">
<head>
	<style type="text/css">
  @media screen and (max-width: 320px) {
     table {
       display: block;
       overflow-x: auto;
     }
}
  </style>
</head>
<body>


	<div class="wrapper">
	 <?php

include("masterpage.php");

?>


		<!-- End Sidebar -->

			<div class="main-panel">
			<div class="content">
				<div class="panel-header bg-danger-gradient">
					<div class="page-inner py-5">
						<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row ">
							<div class="col-md-6 ml-auto mr-auto">
								<center><h2 class="text-white pb-2 fw-bold "><center>Bienvenido,</center><br>  <?php echo $_SESSION['nombres'] .',  '. $_SESSION['apellidos'].' '.$_SESSION['materno'] ?></h2></center>
							<hr align="center" noshade="noshade" size="2" color="white" width="80%" />
              	<h5 class="text-white op-7 mb-2"><center><i>"Pensar en tu seguridad es pensar <br> en el bien mayor, tu familia"<center></i></h5>
							</div>

						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">

					</div>
					<div class="row">
							<div class="col-md-12">
							<div class="card">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Cursos Inscritos</h4>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table id="basic-datatables" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th><center>Codigo</center></th>
													<th><center>Titulo de Curso</center></th>
													<th><center>Fecha</center></th>
													<th><center>Instructor</center></th>
													<th><center>Condicion</center></th>

												</tr>
											</thead>

											   <tbody>
                          <?php
               $ci = $_SESSION['dni'];
               $consulta_resultados = mysqli_query($con,
             "SELECT * FROM examen inner join cuestionarios on examen.id=cuestionarios.id_examen inner join eventos on cuestionarios.cod_evento=eventos.cod_evento inner join usuario on usuario.dni = eventos.dni_capa WHERE ci='$ci' AND estado_evento='S'  order by cuestionarios.cod_evento desc   ");
              $i = 1;
              while($row = mysqli_fetch_array($consulta_resultados)) { ?>
              <tr>

                 <td><center><?php  echo $row['cod_evento']; ?></center></td>
                  <td><center><?php  echo utf8_encode($row['titulo']); ?></center></td>
                  <td><center><?php echo $row['fch_evento']; ?></center></td>
                 <td><center><?php echo $row['nombres'].' '.$row['apellidos'].' '.$row['materno']; ?></center></td>
                 <td><center><?php if(($row['act1'] + $row['act2'] + $row['act3'] + $row['act4'] + $row['act5'] + $row['act6'] + $row['act7'] + $row['act8'] + $row['act9'] + $row['act10'])*0.2 >= 14)
                               {echo "&nbsp;&nbsp;&nbsp;<img src='assets/img/BUENA.png'  height='30' width='30'>";} else {echo "&nbsp;&nbsp;&nbsp;<img src='assets/img/MALA.png'  height='30' width='30'>"; } ?></center></td>
              </tr>
                <?php } ?>
                      </tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div>


					<div class="row">


					</div>
				</div>
			</div>

		</div>

		<!-- Custom template | don't include it in your project! -->

		<!-- End Custom template -->
	</div>
	<!--   Core JS Files   -->
	<script src="assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="assets/js/core/popper.min.js"></script>
	<script src="assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>


	<!-- Chart JS -->
	<script src="assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->


	<!-- jQuery Vector Maps -->
	<script src="assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Sweet Alert -->
	<script src="assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Atlantis JS -->
	<script src="assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="assets/js/setting-demo.js"></script>

</body>
</html>
