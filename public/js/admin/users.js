import DataTableEs from "../common/datatable_es.js";

$(() => {

    if($('#table_index_user').length) {

        var usersTableEle = $("#table_index_user");
        var getDataTable = usersTableEle.data("url");
        var usersTable = usersTableEle.DataTable({
            responsive: true,
            language: DataTableEs,
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataTable
            },
            columns: [
                { data: "id", name: "id" },
                { data: "dni", name: "dni" },
                { data: "name", name: "name" },
                { data: "email", name: "email" },
                { data: "role", name: "role" },
                { data: "company.description", name: "company.description", orderable: false, searchable: false },
                {
                    data: "action",
                    name: "action",
                    orderable: false,
                    searchable: false,
                },
            ],
            order: [[0, "desc"]],
        })
    }
})
