import DataTableEs from "../common/datatable_es.js";

$(() => {
    if ($("#table_index_company").length) {
        var companiesTableEle = $("#table_index_company");
        var getDataTable = companiesTableEle.data("url");
        var compani8esTable = companiesTableEle.DataTable({
            responsive: true,
            language: DataTableEs,
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataTable,
            },
            columns: [
                { data: "id", name: "id" },
                { data: "description", name: "description" },
                { data: "ruc", name: "ruc" },
                {
                    data: "action",
                    name: "action",
                    orderable: false,
                    searchable: false,
                },
            ],
            order: [[0, "desc"]],
        });
    }
});
