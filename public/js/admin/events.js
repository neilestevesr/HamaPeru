import DataTableEs from "../common/datatable_es.js";

$(() => {

    if($('#table_index_event').length) {

        var eventsTable;

        var filter_form = $('#filter_box_container').find('form')

        filter_form.on('submit', function (e) {
            e.preventDefault()
            eventsTable.draw()
        })

        var eventsTableEle = $("#table_index_event");
        var getDataTable = eventsTableEle.data("url");
        eventsTable = eventsTableEle.DataTable({
            responsive: true,
            language: DataTableEs,
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataTable,
                data: function (data) {
                    data.instructor_id = filter_form.find('#instructor_id').val()
                    data.course_id = filter_form.find('#course_id').val()
                    data.date_from = filter_form.find('#date_from').val()
                    data.date_to = filter_form.find('#date_to').val()
                    data.recovery = $("input[name=is_recovery]").val()
                }
            },
            columns: [
                { data: "id", name: "id" },
                { data: "description", name: "description" },
                { data: "type", name: "type" },
                { data: "date", name: "date" },
                { data: "exam.course.description", name: "exam.course.description", orderable: false, searchable: false },
                { data: "instructor.name", name: "instructor.name"},
                { data: "responsable.name", name: "responsable.name"},
                { data: "active", name: "active"},
                { data: "flg_asist", name: "flg_asist"},
                {
                    data: "action",
                    name: "action",
                    orderable: false,
                    searchable: false,
                },
            ],
            order: [[3, "desc"]],
        })
    }

})
