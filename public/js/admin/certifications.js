import DataTableEs from "../common/datatable_es.js";

$(() => {
    if ($("#table_index_certification").length) {
        var certificationsTable;

        var filter_form = $("#filter_box_container").find("form");

        filter_form.on("submit", function (e) {
            e.preventDefault();
            certificationsTable.draw();
        });

        var certificationsTableEle = $("#table_index_certification");
        var getDataTable = certificationsTableEle.data("url");
        certificationsTable = certificationsTableEle.DataTable({
            responsive: true,
            language: DataTableEs,
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataTable,
                data: function (data) {
                    data.dni = filter_form.find("#dni").val();
                    data.company_id = filter_form.find("#company_id").val();
                    data.course_id = filter_form.find("#course_id").val();
                    data.date_from = filter_form.find("#date_from").val();
                    data.date_to = filter_form.find("#date_to").val();
                },
            },
            columns: [
                { data: "id", name: "id" },
                { data: "dni", name: "dni" },
                { data: "full_name", name: "full_name" },
                { data: "company", name: "company" },
                { data: "course", name: "course" },
                { data: "date", name: "date" },
                { data: "note", name: "note" },
                { data: "exam", name: "exam" },
                { data: "certification", name: "certification" },
                { data: "annexed_atocha", name: "annexed_atocha" },
                { data: "annexed_porvenir", name: "annexed_porvenir" },
                { data: "annexed_sinaycocha", name: "annexed_sinaycocha" },
                { data: "annexed_cerrolindo", name: "annexed_cerrolindo" },
                { data: "cart_1", name: "cart_1" },
                { data: "cart_2", name: "cart_2" },
                { data: "cart_3", name: "cart_3" },
                { data: "cart_4", name: "cart_4" },
            ],
            order: [[0, "desc"]],
        });
    }
});
