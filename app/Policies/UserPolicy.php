<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canUpdate(User $me, User $user)
    {
        if($user->role == 'super_admin' || $me->id == $user->id) return False;

        if($me->role == 'super_admin') return True;

        if($me->role == 'admin' && !in_array($user->role, ['super_admin','admin']) ) return True;

        return False;
    }

    public function canDelete(User $me, User $user)
    {
        $canUpdate = self::canUpdate($me,$user);

        if($canUpdate){
            if($user->events->isEmpty() && $user->certifications->isEmpty()){
                return true;
            }
        }

        return false;

    }
}
