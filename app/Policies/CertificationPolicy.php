<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Http\Entities\VirtualEvaluationEntity;
use Illuminate\Support\Str;
use App\Models\Certification;
use App\Models\User;
use App\Models\MiningUnit;

class CertificationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canStartEvaluation(User $user, Certification $certification)
    {
        $can =  $user->signature == 'S'
        && $user->active == 'S'
        //&& $user->profile_survey == 'S'
        && $user->id == $certification->user->id
        && $certification->assist_user == 'S'
        && $certification->event->active == 'S'
        && $certification->event->exam->is_complete
        //&& $certification->event->date == \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD')
        && ($certification->status == 'pending' || $certification->status == 'in_progress');


        if($can && $certification->start_time != NULL){
            return self::canAnswerEvaluation($user,$certification);
        }

        return $can;
    }

    public function canStartTestEvaluation(User $user, Certification $certification)
    {
        $can =  $user->signature == 'S'
        && $user->active == 'S'
        //&& $user->profile_survey == 'S'
        && $user->id == $certification->user->id
        && $certification->assist_user == 'S'
        && $certification->event->active == 'S'
        && $certification->event->testExam->is_complete
        //&& $certification->event->date == \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD')
        && ($certification->status == 'pending' || $certification->status == 'in_progress');
        
        if($can && $certification->start_time != NULL){
            return self::canAnswerTestEvaluation($user,$certification);
        }

        return $can;
    }

    public function canAnswerEvaluation(User $user, Certification $certification)
    {
        $exam_time = $certification->event->exam->exam_time;
        $start_time = $certification->start_time;

        if($start_time != NULL && $exam_time != NULL && $exam_time > 0)
        {
            $now = \Carbon\Carbon::now('America/Lima');
            $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $start_time,'America/Lima');

            if( !($start_time->diffInMinutes($now) < $exam_time) ){
                VirtualEvaluationEntity::concludeEvaluation($certification);
            }

            return $start_time < $now
            && $start_time->diffInMinutes($now) < $exam_time;
        }
        else {
            return false;
        }
    }

    public function canAnswerTestEvaluation(User $user, Certification $certification)
    {
        $exam_time = $certification->event->testExam->exam_time;
        $start_time = $certification->start_time;

        if($start_time != NULL && $exam_time != NULL && $exam_time > 0)
        {
            $now = \Carbon\Carbon::now('America/Lima');
            $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $start_time,'America/Lima');

            if( !($start_time->diffInMinutes($now) < $exam_time) ){
                VirtualEvaluationEntity::concludeEvaluation($certification);
            }

            return $start_time < $now
            && $start_time->diffInMinutes($now) < $exam_time;
        }
        else {
            return false;
        }
    }

    public function canDownloadExam(User $user, Certification $certification)
    {
        if( $certification->evaluation_time == NULL || $certification->score == NULL ) return false;

        if( in_array($user->role, ['super_admin','admin']) ){
            return true;
        }
        else{
            if( $certification->user->active == 'S' && $certification->user->id == $user->id) return true;
            else return false;
        }
    }

    public function canDownloadCertificate(?User $user, Certification $certification)
    {
        if( $certification->score == NULL || $certification->score < 14) return false;

        if($user) {
            return in_array($user->role, ['super_admin','admin']);
        }
        else {
            return $certification->company->active == 'S' ; // && $certification->user->active == 'S'
        }

        return false;
    }

    public function canDownloadAnexo(?User $user, Certification $certification, $miningUnit)
    {
        $mining_unit = MiningUnit::where('description', 'like', "%$miningUnit%")->first();


        if($certification->score < 14 || $mining_unit == NULL) return false; // $certification->evaluation_time == NULL

        if( !Str::is( '*INDUCCI*', strtoupper($certification->event->exam->course->description)) ) return false;

        if( in_array($mining_unit->id, [1,2]) ){
            if( $certification->miningUnits->whereIn('id',[$mining_unit->id,3])->first() == NULL ) return false;
        }
        else {
            if( $certification->miningUnits->whereIn('id',[$mining_unit->id])->first() == NULL ) return false;
        }


        if($user) {
            return in_array($user->role, ['super_admin','admin']);
        }
        else {
            return $certification->company->active == 'S' ; // && $certification->user->active == 'S' &&  $certification->event->exam->course->flg_public == 'S'
        }

        return false;
    }
}
