<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Exam;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createQuestion(User $user, Exam $exam)
    {
        return $exam->questions->count() < 100;
    }

    public function canDelete(User $user, Exam $exam)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $exam->questions->isEmpty() && $exam->events->isEmpty() ){
            return true;
        }
        return false;
    }
}
