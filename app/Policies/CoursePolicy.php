<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Course;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canDelete(User $user, Course $course)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $course->exams->isEmpty() ){
            return true;
        }
        return false;
    }

}
