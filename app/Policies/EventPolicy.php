<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Str;
use App\Models\Event;
use App\Models\User;

class EventPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canConcludeEvaluation(User $user, Event $event)
    {
        if( !in_array($user->role, ['super_admin','admin','instructor','technical_support']) ) return false;

        if($event->active != 'S') return false;

        foreach ($event->certifications as $key => $certification) {
            if($certification->status != 'finished') return true;
        }

        return false;
    }

    public function canAssistsSave(User $user, Event $event)
    {
        if( !in_array($user->role, ['super_admin','admin','instructor','technical_support']) ) return false;

        if($event->active != 'S') return false;

        if($event->flg_asist != 'S') return false;

        return true;
    }

    public function canEdit(User $user, Event $event)
    {
        if (in_array($user->role, ['super_admin','admin','technical_support']) ) return true;

        return false;
    }

    public function canDelete(User $user, Event $event)
    {
        if (!in_array($user->role, ['super_admin','admin','technical_support']) ) return false;

        if( $event->certifications->isEmpty() ){
            return true;
        }
        return false;
    }

}
