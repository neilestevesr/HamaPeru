<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Survey;
use Illuminate\Auth\Access\HandlesAuthorization;

class SurveyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canDelete(User $user, Survey $survey)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $survey->groups->isEmpty() ){
            return true;
        }

        return false;
    }
}
