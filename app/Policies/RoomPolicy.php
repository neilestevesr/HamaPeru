<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Room;
use Illuminate\Auth\Access\HandlesAuthorization;

class RoomPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canDelete(User $user, Room $room)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $room->events->isEmpty() ){
            return true;
        }
        return false;
    }

}
