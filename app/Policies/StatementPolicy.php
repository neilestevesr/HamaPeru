<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Statement;
use Illuminate\Auth\Access\HandlesAuthorization;

class StatementPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createOption(User $user, Statement $statement)
    {
        return $statement->type != 'commentary';
    }

    public function canDelete(User $user, Statement $statement)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $statement->options->isEmpty() || $statement->type == 'commentary' ){
            return true;
        }

        return false;
    }
}
