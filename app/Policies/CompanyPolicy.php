<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canDelete(User $user, Company $company)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $company->users->isEmpty() && $company->certifications->isEmpty() ){
            return true;
        }
        return false;
    }

}
