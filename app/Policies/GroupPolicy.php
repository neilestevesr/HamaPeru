<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Group;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canDelete(User $user, Group $group)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $group->statements->isEmpty() ){
            return true;
        }

        return false;
    }
}
