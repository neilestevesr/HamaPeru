<?php

namespace App\Policies;

use App\Models\User;
use App\Models\MiningUnit;
use Illuminate\Auth\Access\HandlesAuthorization;

class MiningUnitPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canDelete(User $user, MiningUnit $miningUnit)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $miningUnit->users->isEmpty() ){
            return true;
        }
        return false;
    }

}
