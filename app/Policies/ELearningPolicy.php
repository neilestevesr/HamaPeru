<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ELearning;
use Illuminate\Auth\Access\HandlesAuthorization;

class ELearningPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canDelete(User $user, ELearning $elearning)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $elearning->events->isEmpty() ){
            return true;
        }
        return false;
    }

}
