<?php

namespace App\Policies;

use App\Models\User;
use App\Models\OwnerCompany;
use Illuminate\Auth\Access\HandlesAuthorization;

class OwnerCompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canDelete(User $user, OwnerCompany $ownerCompany)
    {
        if (!in_array($user->role, ['super_admin','admin']) ) return false;

        if( $ownerCompany->exams->isEmpty() ){
            return true;
        }

        return false;
    }

}
