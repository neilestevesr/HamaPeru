<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Validation\ValidationException as IlluminateValidationException;
use Throwable;

class UsersImport implements
ToModel,
WithHeadingRow,
SkipsOnError,
WithValidation,
SkipsOnFailure
{
    CONST PARTICIPANT_ROLE = 'participants';

    use Importable, SkipsErrors, SkipsFailures;
    private $dnis = [];
    private $dnis_duplicates = [];


    public function model(array $row)
    {
        try {
            if (in_array($row['dni'], $this->dnis_duplicates)) {
                return null;
            }

            $data = collect([
                'dni' => $row['dni'],
                'password' => bcrypt($row['dni']),
                'name' => $row['nombre'],
                'paternal' => $row['paterno'],
                'maternal' => $row['materno'],
                'company_id' => $row['cod_empresa'],
                'position' => $row['cargo'],
                'email' => $row['email'],
                'telephone' => $row['telefono'],
                'role' => self::PARTICIPANT_ROLE,
                ]);

                $user = User::where('dni', $data['dni'])->first();

                if ($user) {
                    $data = $data->except('dni')->all();
                    $user->update($data);
                }
                else {
                    $user = User::create($data->all());
                }

                $user->miningUnits()->sync($row['unidad_minera']);

                return $user;

            } catch (\Exception $e) {
                $this->onError($e);
            }

            return null;

        }


        public function rules(): array
        {
            return [
            '*.dni'             => ['required', 'digits_between:8,11', 'distinct'],
            '*.nombre'          => ['required', 'max:255'],
            '*.paterno'         => ['required', 'max:255'],
            '*.materno'         => ['required', 'max:255'],
            '*.email'           => ['required', 'email', 'max:255'],
            '*.telefono'        => ['nullable', 'max:20'],
            '*.cod_empresa'     => ['required', 'exists:App\Models\Company,id'],
            '*.cargo'           => ['nullable', 'max:255'],
            '*.unidad_minera'   => ['filled', 'array'],
            '*.unidad_minera.*' => ['exists:App\Models\MiningUnit,id'],
            ];
        }


        public function customValidationMessages()
        {
            return [
            'email.email' => 'No es un correo válido: ":input"',
            'cod_empresa.exists' => 'El valor :input no existe.',
            'unidad_minera.*.exists' => 'El valor :input no existe.',
            ];
        }

        public function customValidationAttributes()
        {
            return [
            'cod_empresa' => 'cod empresa',
            'unidad_minera.*' => 'unidad minera',
            ];
        }


        public function prepareForValidation($row, $index)
        {
            $this->validateDniDuplicate($row['dni']);

            $miningUnits = Str::of($row['unidad_minera'])->explode(',')->all();
            $row['unidad_minera'] = $miningUnits ?? $row['unidad_minera'];

            return $row;
        }

        public function validateDniDuplicate($dni)
        {
            if(in_array($dni, $this->dnis)){
                $this->dnis_duplicates[] = $dni;
            }
            $this->dnis[] = $dni;
        }

        public function getDnisDuplicates()
        {
            return collect(array_unique($this->dnis_duplicates));
        }

    }
