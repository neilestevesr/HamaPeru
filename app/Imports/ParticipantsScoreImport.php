<?php

namespace App\Imports;

use App\Models\User;
use App\Models\Event;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Validation\ValidationException as IlluminateValidationException;
use Throwable;

class ParticipantsScoreImport implements
ToCollection,
WithHeadingRow,
SkipsOnError,
WithValidation,
SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;
    private $dnis;
    private $dnis_duplicates = [];

    public function collection(Collection $rows)
    {
        
        $this->dnis = $rows->collect()->all();
        
        //$this->dnis_duplicates = array_diff_assoc($this->dnis, array_unique($this->dnis));
    }

    public function rules(): array
    {
        return [
            '*.dni' => ['required','exists:App\Models\User,dni', 'digits_between:8,11'],
        ];
    }

    public function customValidationMessages()
    {
        return [
            'dni.exists' => 'No existe dni: :input',
        ];
    }

    public function getDnis()
    {
        return $this->dnis;
    }

    public function getDnisDuplicates()
    {
        return collect(array_unique($this->dnis_duplicates));
    }
}
