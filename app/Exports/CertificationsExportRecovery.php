<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;

class CertificationsExportRecovery implements FromView
{
    use Exportable;

    private $certifications = [];

    public function view(): View
    {
        return view('admin.certifications.partials.table_excel_recovery',['certifications' => $this->certifications ]);
    }

    public function setCertifications($certifications)
    {
        $this->certifications = $certifications;
    }
}
