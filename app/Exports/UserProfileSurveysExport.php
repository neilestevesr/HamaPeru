<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;

class UserProfileSurveysExport implements FromView
{
    use Exportable;

    private $userSurveys = [];
    private $max = 0;

    public function view(): View
    {
        return view('admin.surveys.partials.table_excel_profile',['userSurveys' => $this->userSurveys, 'max' => $this->max ]);
    }

    public function setUserSurveys($userSurveys)
    {
        $this->userSurveys = $userSurveys;

        $max = 0;
        foreach ($userSurveys as $key => $userSurvey) {
            $count = $userSurvey->surveyAnswers()->count();
            if($count > $max) $max = $count;
        }
        $this->max = $max;
    }

}
