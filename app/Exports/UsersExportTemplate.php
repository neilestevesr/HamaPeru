<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;

class UsersExportTemplate implements FromView
{
    use Exportable;

    public function view(): View
    {
        return view('admin.users.excel.users_import_template');
    }

}
