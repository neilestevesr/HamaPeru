<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = [
        'name', 'destined_to', 'active',
    ];

    /**
    * Relationships
    */
    public function groups()
    {
        return $this->hasMany('App\Models\Group','survey_id');
    }
}
