<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = [
        'title','section','type','url','elearning_id'
    ];

    /**
    * Relationships
    */
    public function elearning()
    {
        return $this->belongsTo('App\Models\ELearning','elearning_id');
    }

    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }
}
