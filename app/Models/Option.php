<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'description', 'statement_id',
    ];

    /**
    * Relationships
    */
    public function statement()
    {
        return $this->belongsTo('App\Models\Statement','statement_id');
    }
}
