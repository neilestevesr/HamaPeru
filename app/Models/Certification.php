<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'assist_user','recovered_at',
        'status', 'user_id','event_id','company_id', 'position', 'evaluation_type', 'evaluation_time', 'test_certification_id',
        'start_time', 'end_time', 'total_time', 'score', 'score_fin','area','observation',
    ];

    /**
    * Relationships
    */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event','event_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Models\Evaluation','certification_id');
    }

    public function miningUnits()
    {
        return $this->belongsToMany('App\Models\MiningUnit','certifications_mining_units','certification_id','mining_unit_id')->withTimestamps();
    }

    public function testCertification()
    {
        return $this->belongsTo('App\Models\Certification','test_certification_id');
    }

    /**
    * Accessor y Mutators
    */
    public function getCurrentEvaluationsAttribute()
    {
        if($this->evaluation_time != NULL)
        {
            $evaluations = $this->evaluations()->where('evaluation_time', $this->evaluation_time)->orderBy('question_order', 'asc')->get();
            return $evaluations;
        }
        else {
            return NULL;
        }
    }

    public function getIsEnableEvaluationAttribute()
    {
        $messages = [];
        $now = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD');

        if($this->user->active != 'S') array_push($messages, "No está activo.");
        if($this->user->signature != 'S') array_push($messages, "No tiene firma.");
        if($this->assist_user != 'S') array_push($messages, "No tiene asistencia.");
        if($this->event->date != $now) array_push($messages, "Fuera de fecha.");
        if($this->status == 'finished' || $this->end_time != NULL) array_push($messages, "Finalizó evaluación.");

        if(count($messages) > 0){
            return $messages;
        }else {
            return ["Habilitado"];
        }

    }

    public function getTimeLeftAttribute()
    {
        $now = \Carbon\Carbon::now('America/Lima');
        $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->start_time,'America/Lima');
        if($this->evaluation_type == 'test')
        {
          $end_time = $start_time->addMinutes($this->event->testExam->exam_time);
        }
        else
        {
           $end_time = $start_time->addMinutes($this->event->exam->exam_time);
        }
        return $end_time->diffInSeconds($now);
    }

    public function getMiningUnitsByCommasAttribute()
    {
        $text = '';
        foreach ($this->miningUnits as $key => $mining_unit) {
            if($key != 0){
                $text .= ', ';
            }
            $text .= $mining_unit->description;
        }

        return $text;
    }

    public function getMiningUnitsLineAttribute()
    {
        $minings_units = '';
        $flag = false;
        $porvenir = $this->miningUnits->firstWhere('id',1);
        $atacocha = $this->miningUnits->firstWhere('id',2);
        $complejo_pasco = $this->miningUnits->firstWhere('id',3);

        $mining_units = $this->miningUnits->whereNotIn('id', [1,2,3]);

        if( $complejo_pasco != NULL || ($porvenir != NULL && $atacocha != NULL) )
        {
            $minings_units .= 'Complejo Pasco';
            $flag = true;
        }
        elseif ($porvenir != NULL || $atacocha != NULL) {
            $minings_units .= ucwords(strtolower($porvenir->description ?? $atacocha->description));
            $flag = true;
        }


        foreach ($mining_units as $key => $miningUnit) {
            if($flag) $minings_units .= ' - ';
            $flag = True;
            $minings_units .= ucwords(strtolower($miningUnit->description));
        }

        return $minings_units;
    }

    public function getUmSedeAttribute()
    {

        $count = $this->miningUnits->count();
        // return $this->miningUnits[0]->description ?? '-';
        if($count == 1){
            return ucwords($this->miningUnits[0]->description);
        }
        else if ($count > 1) {
            $atacocha = False;
            $porvenir = False;
            foreach ($this->miningUnits as $key => $miningUnit) {
                if( Str::is( '*ATACOCHA*', strtoupper($miningUnit->description) ) ) $atacocha = True;
                if( Str::is( '*PORVENIR*', strtoupper($miningUnit->description) ) ) $porvenir = True;
            }

            if( $atacocha && $porvenir) return 'Complejo Pasco';
            if( $atacocha ) return 'Atacocha';
            if( $porvenir ) return 'El Porvenir';

            return ucwords($this->miningUnits[0]->description);
        }
        else {
            return '-';
        }
    }
}
