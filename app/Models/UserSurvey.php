<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSurvey extends Model
{
    protected $table = 'users_surveys';

    protected $fillable = [
        'user_id','survey_id','company_id',
        'date','status','start_time', 'end_time', 'total_time','event_id',
    ];

    /**
    * Relationships
    */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function survey()
    {
        return $this->belongsTo('App\Models\Survey','survey_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id');
    }
    
    public function event()
    {
        return $this->belongsTo('App\Models\Event','event_id');
    }

    public function surveyAnswers()
    {
        return $this->hasMany('App\Models\SurveyAnswer','user_survey_id');
    }
}
