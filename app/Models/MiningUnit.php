<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MiningUnit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'owner', 'district', 'Province',
    ];

    /**
    * Relationships
    */
    public function users()
    {
        return $this->belongsToMany('App\Models\User','mining_units_users','mining_unit_id','user_id')->withTimestamps();
    }

    public function certifications()
    {
        return $this->belongsToMany('App\Models\Certification','certifications_mining_units','mining_unit_id','certification_id')->withTimestamps();
    }
}
