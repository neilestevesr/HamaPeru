<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicAlternative extends Model
{
    protected $fillable = [
        'description','dynamic_question_id', 'is_correct'
    ];

    /**
    * Relationships
    */
    public function dynamicQuestion()
    {
        return $this->belongsTo('App\Models\DynamicQuestion','dynamic_question_id');
    }
}
