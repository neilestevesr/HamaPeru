<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyAnswer extends Model
{
    protected $table = 'survey_answers';

    protected $fillable = [
        'statement','answer','question_order',
        'user_survey_id','statement_id',
    ];

    /**
    * Relationships
    */
    public function userSurvey()
    {
        return $this->belongsTo('App\Models\UserSurvey','user_survey_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Models\Statement','statement_id');
    }
}
