<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description','capacity','url_zoom','active',
    ];

    public function events()
    {
        return $this->hasMany('App\Models\Event','room_id');
    }

}
