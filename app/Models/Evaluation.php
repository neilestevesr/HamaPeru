<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $fillable = [
        'evaluation_time', 'statement', 'selected_alternative','correct_alternative','is_correct',
        'question_order','question_id','certification_id',
    ];

    /**
    * Relationships
    */
    public function question()
    {
        return $this->belongsTo('App\Models\Question','question_id');
    }

    public function dynamicQuestion()
    {
        return $this->belongsTo('App\Models\DynamicQuestion','question_id');
    }

    public function certification()
    {
        return $this->belongsTo('App\Models\Certification','certification_id');
    }

}
