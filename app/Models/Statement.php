<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
    protected $fillable = [
        'description', 'desc', 'type', 'group_id',
    ];

    /**
    * Relationships
    */
    public function group()
    {
        return $this->belongsTo('App\Models\Group','group_id');
    }

    public function options()
    {
        return $this->hasMany('App\Models\Option','statement_id');
    }
}
