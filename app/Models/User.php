<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dni', 'name', 'paternal', 'maternal', 'email','password',
        'telephone', 'role', 'cip', 'signature','active', 'position', 'company_id','profile_survey','profile_user',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
    * Relationships
    */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function file()
    {
        return $this->morphOne('App\Models\File', 'fileable');
    }

    public function miningUnits()
    {
        return $this->belongsToMany('App\Models\MiningUnit','mining_units_users','user_id','mining_unit_id')->withTimestamps();
    }
/*
    public function certificationEvents()
    {
        return $this->belongsToMany('App\Models\Event','certifications','user_id','event_id')->withTimestamps();
    }
*/
    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id');
    }

    public function events()
    {
        return $this->hasMany('App\Models\Event','user_id');
    }

    public function certifications()
    {
        return $this->hasMany('App\Models\Certification','user_id');
    }

    public function userSurveys()
    {
        return $this->hasMany('App\Models\UserSurvey','user_id');
    }

    /**
    * Accessor y Mutators
    */
    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->paternal;
    }

    public function getFullNameCompleteAttribute()
    {
        return $this->name . ' ' . $this->paternal . ' ' . $this->maternal;
    }

    public function getFullNameCompleteReverseAttribute()
    {
        return $this->paternal . ' ' . $this->maternal  . ', ' . $this->name;
    }

    public function getMiningUnitsByCommasAttribute()
    {
        $text = '';
        foreach ($this->miningUnits as $key => $mining_unit) {
            if($key != 0){
                $text .= ', ';
            }
            $text .= $mining_unit->description;
        }

        return $text;
    }

    public function getMiningUnitsLineAttribute()
    {
        $minings_units = '';
        $flag = false;
        $porvenir = $this->miningUnits->firstWhere('id',1);
        $atacocha = $this->miningUnits->firstWhere('id',2);
        $complejo_pasco = $this->miningUnits->firstWhere('id',3);

        $mining_units = $this->miningUnits->whereNotIn('id', [1,2,3]);

        if( $complejo_pasco != NULL || ($porvenir != NULL && $atacocha != NULL) )
        {
            $minings_units .= 'Complejo Pasco';
            $flag = true;
        }
        elseif ($porvenir != NULL || $atacocha != NULL) {
            $minings_units .= ucwords(strtolower($porvenir->description ?? $atacocha->description));
            $flag = true;
        }


        foreach ($mining_units as $key => $miningUnit) {
            if($flag) $minings_units .= ' - ';
            $flag = True;
            $minings_units .= ucwords(strtolower($miningUnit->description));
        }

        return $minings_units;
    }

    public function getUmSedeAttribute()
    {

        $count = $this->miningUnits->count();
        // return $this->miningUnits[0]->description ?? '-';
        if($count == 1){
            return ucwords($this->miningUnits[0]->description);
        }
        else if ($count > 1) {
            $atacocha = False;
            $porvenir = False;
            foreach ($this->miningUnits as $key => $miningUnit) {
                if( Str::is( '*ATACOCHA*', strtoupper($miningUnit->description) ) ) $atacocha = True;
                if( Str::is( '*PORVENIR*', strtoupper($miningUnit->description) ) ) $porvenir = True;
            }

            if( $atacocha && $porvenir) return 'Complejo Pasco';
            if( $atacocha ) return 'Atacocha';
            if( $porvenir ) return 'El Porvenir';

            return ucwords($this->miningUnits[0]->description);
        }
        else {
            return '-';
        }
    }

    public function getSignatureUrlAttribute()
    {
        return $this->file()->where('category','firmas')->first()->file_url ?? '#';
    }
}
