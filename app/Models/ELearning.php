<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ELearning extends Model
{
    protected $fillable = [
        'title','description','active','videos_section','contents_section','cases_section','rvis_section','games_section',
        'files_section'
    ];

    /**
    * Relationships
    */
    public function contents()
    {
        return $this->hasMany('App\Models\Content','elearning_id');
    }

    public function events()
    {
        return $this->hasMany('App\Models\Event','elearning_id');
    }

    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function file()
    {
        return $this->morphOne('App\Models\File', 'fileable');
    }
}
