<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description','subtitle','date','hours','time_start','time_end','active','flg_public',
    ];

    /**
    * Relationships
    */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function file()
    {
        return $this->morphOne('App\Models\File', 'fileable');
    }

    public function exams()
    {
        return $this->hasMany('App\Models\Exam','course_id');
    }

    /**
    * Accessor y Mutators
    */
    public function getTimeStartFormatAttribute()
    {
        return \Carbon\Carbon::createFromFormat('H:i:s', $this->time_start,'America/Lima')->isoFormat('hh:mm A');
    }

    public function getTimeEndFormatAttribute()
    {
        return \Carbon\Carbon::createFromFormat('H:i:s', $this->time_end,'America/Lima')->isoFormat('hh:mm A');
    }
}
