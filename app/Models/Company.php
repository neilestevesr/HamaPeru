<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Gate;


class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'abbreviation',
        'ruc',
        'address',
        'telephone',
        'name_ref',
        'telephone_ref',
        'email_ref',
        'active',
    ];

    /**
     * Relationships
     */
    public function users()
    {
        return $this->hasMany('App\Models\User', 'company_id');
    }

    public function certifications()
    {
        return $this->hasMany('App\Models\Certification', 'company_id');
    }

    static function getDatatable()
    {
        $query = Company::with(['users']);

        $allCompanies = DataTables::of($query)
            ->addColumn('description', function ($company) {
                return $company->description ?? '-';
            })
            ->editColumn('ruc', function ($company) {
                return $company->ruc ?? '-';
            })
            ->editColumn('action', function ($company) {
                $btn = "";

                $btn .= '<a href="' . route('companies.edit', $company) . '" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';

                if (Gate::allows('canDelete', $company)) {

                    $modal = view('components.deleteModal', [
                        'url_delete' => route('companies.destroy', $company->id),
                        'object' => 'Usuario',
                        'id' => $company->id,
                        'description' => $company->description
                    ])->render();

                    $btn .= '<a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-' . $company->id . '"
                                    href="javascript:;">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>' . $modal;
                }

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);

        return $allCompanies;
    }
}
