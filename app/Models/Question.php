<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'statement','alternative_a','alternative_b','alternative_c','alternative_d',
        'alternative_correct','points', 'exam_id'
    ];

    /**
    * Relationships
    */
    public function exam()
    {
        return $this->belongsTo('App\Models\Exam','exam_id');
    }
}
