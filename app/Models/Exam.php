<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'exam_time', 'active', 'exam_type', 'owner_company_id', 'course_id',
    ];

    /**
    * Relationships
    */
    public function ownerCompany()
    {
        return $this->belongsTo('App\Models\OwnerCompany','owner_company_id');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course','course_id');
    }

    public function questions()
    {
        return $this->hasMany('App\Models\Question','exam_id');
    }

    public function dynamicQuestions()
    {
        return $this->hasMany('App\Models\DynamicQuestion','exam_id');
    }

    public function events()
    {
        return $this->hasMany('App\Models\Event','exam_id');
    }

    public function eventsTest()
    {
        return $this->hasMany('App\Models\Event','test_exam_id');
    }


    /**
    * Accessor y Mutators
    */
    public function getIsCompleteAttribute()
    {
        $complete_questions_count =  $this->dynamicQuestions()->where('correct_alternative_id','!=','NULL')->count();
        
        return $complete_questions_count >= 10;
    }
}
