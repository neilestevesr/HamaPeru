<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class File extends Model
{
    protected $fillable = [
        'file_path', 'file_url', 'file_type', 'category', 'fileable_id', 'fileable_type',
    ];

    public function fileable()
    {
        return $this->morphTo();
    }

    /**
    * Accessor y Mutators
    */
    public function getNameAttribute()
    {
        return Str::of($this->file_path)->explode('/')->last();
    }

}
