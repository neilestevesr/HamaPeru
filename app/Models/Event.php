<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description','type','date','active','flg_test_exam','flg_asist','flg_survey_course','flg_survey_evaluation',
        'exam_id','test_exam_id','elearning_id','user_id','responsable_id','room_id','security_id','flg_security','security_por_id','flg_security_por','owner_companies_id'
    ];

    /**
    * Relationships
    */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function exam()
    {
        return $this->belongsTo('App\Models\Exam','exam_id');
    }

    public function elearning()
    {
        return $this->belongsTo('App\Models\ELearning','elearning_id');
    }

    public function testExam()
    {
        return $this->belongsTo('App\Models\Exam','test_exam_id');
    }

    public function instructor()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function responsable()
    {
        return $this->belongsTo('App\Models\User','responsable_id');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Room','room_id');
    }

     public function seguridad()
    {
        return $this->belongsTo('App\Models\User','security_id');
    }

      public function seguridadPorvenir()
    {
        return $this->belongsTo('App\Models\User','security_por_id');
    }    
    
    public function EmpresaTitular()
    {
        return $this->belongsTo('App\Models\OwnerCompany','owner_companies_id');
    }   
    
    public function certifications()
    {
        return $this->hasMany('App\Models\Certification','event_id');
    }

    public function certificationUsers()
    {
        return $this->belongsToMany('App\Models\User','certifications','event_id','user_id')
        ->withPivot('assist_user','recovered_at','user_id','event_id','company_id')
        ->withTimestamps();
    }

    /**
    * Accessor y Mutators
    */
    public function getDateCarbonInstanceAttribute()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $this->date,'America/Lima');
    }

    public function getDateCarbonSpanishAttribute()
    {
        $date_carbon = \Carbon\Carbon::createFromFormat('Y-m-d', $this->date,'America/Lima');
        $mes_spanish = config('parameters.month_spanish')[$date_carbon->isoFormat('MM')];

        return $date_carbon->isoFormat('DD') . ' de ' . $mes_spanish . ' del ' . $date_carbon->isoFormat('YYYY');
    }
    
}
