<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'name', 'description', 'survey_id',
    ];

    /**
    * Relationships
    */
    public function survey()
    {
        return $this->belongsTo('App\Models\Survey','survey_id');
    }

    public function statements()
    {
        return $this->hasMany('App\Models\Statement','group_id');
    }

}
