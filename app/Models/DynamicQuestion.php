<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicQuestion extends Model
{
    protected $fillable = [
        'statement', 'points', 'correct_alternative_id','exam_id',
    ];

    /**
    * Relationships
    */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    public function exam()
    {
        return $this->belongsTo('App\Models\Exam','exam_id');
    }

    public function dynamicAlternatives()
    {
        return $this->hasMany('App\Models\DynamicAlternative','dynamic_question_id');
    }

    public function correctDynamicAlternative()
    {
        return $this->belongsTo('App\Models\DynamicAlternative','correct_alternative_id');
    }
}
