<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OwnerCompany extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
    * Relationships
    */
    public function exams()
    {
        return $this->hasMany('App\Models\Exam','owner_company_id');
    }
}
