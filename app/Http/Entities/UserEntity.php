<?php
namespace App\Http\Entities;

use App\Models\User;
use App\Http\Entities\Utils;
use Illuminate\Support\Str;
use App\Http\Entities\FileEntity;
use App\Mail\ConfirmationMailable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;

class UserEntity
{
    CONST PARTICIPANT_ROLE = 'participants';

    static function all()
    {
        return User::all();
    }

    static function allWithRelationships()
    {
        return User::with(['company','miningUnits','events','certifications'])->get();
    }

    static function getDatatable()
    {
        $query = User::with(['company']);

        $allUsers = DataTables::of($query)
                ->addColumn('company.description', function ($user) {
                    return $user->company->description ?? '-';
                })
                ->editColumn('role', function ($user) {
                    return config('parameters')['roles'][$user->role] ?? '-';
                })
                ->editColumn('action', function ($user) {
                    $btn = "";

                    if (Gate::allows('canUpdate', $user)) {
                        $btn .= '<a href="'. route('users.edit', $user) .'" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                    }

                    if (Gate::allows('canDelete', $user)) {

                        $modal = view('components.deleteModal', [
                            'url_delete' => route('users.destroy', ["user" => $user]),
                            'object' => 'Usuario',
                            'id' => $user->id,
                            'description' => $user->name
                        ])->render();

                        $btn .= '<a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-'. $user->id . '"
                                    href="javascript:;">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>'. $modal;
                    }

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);

        return $allUsers;
    }

    static function getInstructor()
    {
        return User::whereIn('role', ['instructor'])->get();
    }

      static function getSeguridad()
    {
        return User::whereIn('role', ['security_manager'])->get();
    }

    static function getResponsable()
    {
        return User::where('company_id', 10)->get();
    }

    static function store($request)
    {
        $data = $request->except('password');
        $data = collect($data)->all();

        $data = Utils::NormalizeInputData($data);

          //$email = $request->get('email');

          //$correo = new ConfirmationMailable;
          //Mail::to($email)->send($correo);

        $data['password'] = bcrypt($request->get('password'));
        $user = User::create($data);
        $user->miningUnits()->sync($request->get('mining_units'));
        return $user;
    }

    static function update($request,User $user)
    {
        if($request->get('password')){
            $data = $request->all();
            $data['password'] = bcrypt($request->get('password'));
        }else {
            $data = $request->except('password');
        }
        $data = Utils::NormalizeInputData($data);

         //$email = $request->get('email');

          //$correo = new ConfirmationMailable;
          //Mail::to($email)->send($correo);

        $user->update($data);
        $user->miningUnits()->sync($request->get('mining_units'));
        return $user;
    }

    static function destroy(User $user)
    {
        $user->miningUnits()->sync([]);
        $files = $user->files()->get();
        if($files->isNotEmpty()){
            foreach ($files as $key => $file) {
                FileEntity::delete($file);
            }
        }
        return $user->delete();
    }

    static function getAllParticipants()
    {
        $users = User::whereIn('role', ['supervisor','participants'])->get();

        return $users;
    }

    static function getRoles()
    {
        $roles = collect(config('parameters.roles'))->except('super_admin');

        if(Auth::user()->role == 'admin'){
            $roles = $roles->except('admin');
        }

        return $roles;
    }

}
