<?php
namespace App\Http\Entities;

use App\Models\Company;
use App\Http\Entities\Utils;

class CompanyEntity
{
    static function all()
    {
        return Company::all();
    }

    static function allWithRelationships()
    {
        return Company::with(['users','certifications'])->get();
    }

    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());
        return Company::create($data);
    }

    static function update($request,Company $company)
    {
        $data = Utils::NormalizeInputData($request->all());
        return $company->update($data);
    }

    static function destroy(Company $company)
    {
        return $company->delete();
    }

}
