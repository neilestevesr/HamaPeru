<?php
namespace App\Http\Entities;

use App\Models\Course;
use App\Http\Entities\Utils;
use App\Http\Entities\FileEntity;
use App\Models\File;

class CourseEntity
{
    static function all()
    {
        return Course::all();
    }

    static function allWithRelationships()
    {
        return Course::with(['exams'])->get();
    }

    static function store($request)
    {
        
        $data = Utils::NormalizeInputData($request->all());

        $course = Course::create($data);
        if($request->has('image')){
            $result = FileEntity::store($course,$request->file('image'),'imagenes','cursos');
        }

        if($request->has('file')){
            $result = FileEntity::store($course,$request->file('file'),'archivos','cursos');
        }

        return $course;
    }

    static function update($request,Course $course)
    {
        //dd($request->all());
        $data = Utils::NormalizeInputData($request->all());

        if($request->has('image')){
            $files = $course->files()->where('file_type','imagenes')->where('category','cursos')->get();

            if($files->isNotEmpty()){
                foreach ($files as $key => $file) {
                    FileEntity::delete($file);
                }
            }  

            $result = FileEntity::store($course,$request->file('image'),'imagenes','cursos');
        }

        if($request->has('file')){
            $files = $course->files()->where('file_type','archivos')->where('category','cursos')->get();

            if($files->isNotEmpty()){
                foreach ($files as $key => $file) {
                    FileEntity::delete($file);
                }
            } 

            $result = FileEntity::store($course,$request->file('file'),'archivos','cursos');
        }

        
            $file = $course->files()->where('file_type','link')->where('category','cursos')->first();
        
            
            if($request->has('url') && $request->get('url') != '' && $request->get('url') != NULL && $request->has('dsc_url') && $request->get('dsc_url') != '' && $request->get('dsc_url') != NULL ){
                if($file != NULL ){
                /*  $url = $request->get('url');
                    $file->file_path = $url;
                    $file->file_url = $url;
                    $file->save(); */
                    $file = new File([
                        'file_path' => $request->get('dsc_url'),
                        'file_url' => $request->get('url'),
                        'file_type' => 'link',
                        'category' => 'cursos'
                        ]);
                
                        $course->file()->save($file);

                }else{
                    
                        $file = new File([
                            'file_path' => $request->get('dsc_url'),
                            'file_url' => $request->get('url'),
                            'file_type' => 'link',
                            'category' => 'cursos'
                            ]);
                    
                            return $course->file()->save($file); 
                    
                    
                }
            }

        $result = $course->update($data);

        return $result;
    }

    static function destroy(Course $course)
    {
        $files = $course->files()->get();
        if($files->isNotEmpty()){
            foreach ($files as $key => $file) {
                FileEntity::delete($file);
            }
        }
        return $course->delete();
    }

}
