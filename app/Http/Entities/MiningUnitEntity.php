<?php
namespace App\Http\Entities;

use App\Models\MiningUnit;

class MiningUnitEntity
{
    static function all()
    {
        return MiningUnit::all();
    }

    static function allWithRelationships()
    {
        return MiningUnit::with(['users'])->get();
    }

    static function store($request)
    {
        return MiningUnit::create($request->all());
    }

    static function update($request,MiningUnit $miningUnit)
    {
        return $miningUnit->update($request->all());
    }

    static function destroy(MiningUnit $miningUnit)
    {
        return $miningUnit->delete();
    }

}
