<?php
namespace App\Http\Entities;

use App\Models\Certification;
use Illuminate\Support\Facades\DB;

class CertificationOldEntity
{
    CONST CERTIFICATE_OLD_DIRECTORY_S3 = 'archivos/certificados_antiguos/';

    static function getCertificationsOldFilter($request)
    {
        /*
        SELECT alumno.nombre as nombres,apellidopaterno,apellidomaterno,dni,cursos.nombre as curso,fechainicio,nombreempresa,url
        FROM alumno
        inner join certificado on alumno.idalumno = certificado.idalumno
        inner join matricula on matricula.idmatricula = certificado.idmatricula
        inner join programacioncurso on matricula.idprogramacioncurso = programacioncurso.idprogramacioncurso
        inner join cursos on cursos.idcursos = programacioncurso.idcursos
        inner join empresa on empresa.idempresa = alumno.idempresa
        where dni='71021883' and estadoaprobado='1'
        */
        $query = DB::connection('hamaperu_ant')->table('alumno')
        ->join('certificado', 'certificado.idalumno', '=', 'alumno.idalumno')
        ->join('matricula', 'matricula.idmatricula', '=', 'certificado.idmatricula')
        ->join('programacioncurso', 'programacioncurso.idprogramacioncurso', '=', 'matricula.idprogramacioncurso')
        ->join('cursos', 'cursos.idcursos', '=', 'programacioncurso.idcursos')
        ->join('empresa', 'empresa.idempresa', '=', 'alumno.idempresa')
        ->select('alumno.nombre as nombres','apellidopaterno','apellidomaterno','dni','cursos.nombre as curso','fechainicio','nombreempresa','url', DB::raw("CONCAT('" . self::CERTIFICATE_OLD_DIRECTORY_S3 . "',url) AS file_path") )
        ->where('estadoaprobado', '1');

        if( $request->has('dni') && $request->get('dni') != '' ){
            $query = $query->where('dni', $request->get('dni'));
        }

        $certifications = $query->limit(100)->get();

        return $certifications;
    }


}
