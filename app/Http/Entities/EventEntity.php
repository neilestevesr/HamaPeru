<?php

namespace App\Http\Entities;

use App\Models\User;
use App\Models\Certification;
use App\Models\Event;
use App\Http\Entities\Utils;
use Illuminate\Support\Facades\Gate;
use Yajra\DataTables\Facades\DataTables;

class EventEntity
{
    static function all()
    {
        return Event::all();
    }

    static function getDatatable($request)
    {
        $query = Event::with(['exam.course', 'instructor', 'responsable', 'certifications']);

        $is_recovery = $request->get('recovery') == 'true' ? true : false;

        if ($request->has('instructor_id') && $request->get('instructor_id') != '') {
            $instructor_id = $request->get('instructor_id');
            $query->whereHas('instructor', function ($q) use ($instructor_id) {
                $q->where('id', $instructor_id);
            });
        }

        if ($request->has('course_id') && $request->get('course_id') != '') {
            $course_id = $request->get('course_id');
            $query->whereHas('exam.course', function ($q) use ($course_id) {
                $q->where('id', $course_id);
            });
        }

        if ($request->has('date_from') && $request->get('date_from') != '') {
            $query->where('date', '>=', $request->get('date_from'));
        }

        if ($request->has('date_to')  && $request->get('date_to') != '') {
            $query->where('date', '<=', $request->get('date_to'));
        }


        $allEvents = DataTables::of($query)
            ->editColumn('description', function ($event) use ($is_recovery) {

                $link = $is_recovery ? '<a href="'. route('events.show_recovery', $event) .'">'. $event->description ?? '-' .'</a>'
                                    : '<a href="'. route('events.show', $event) .'">'. $event->description ?? '-' .'</a>';

                return $link;
            })
            ->editColumn('type', function ($event) {
                return $event->type ?? '-';
            })
            ->editColumn('date', function ($event) {
                return $event->date ?? '-';
            })
            ->addColumn('exam.course.description', function ($event) {
                return $event->exam->course->description ?? '-';
            })
            ->editColumn('instructor.name', function ($event) {
                return $event->instructor->full_name_complete ?? '-';
            })
            ->editColumn('responsable.name', function ($event) {
                return $event->responsable->full_name_complete ?? '-';
            })
            ->editColumn('active', function ($event) {
                return $event->active == 'S' ? 'Si' : 'No';
            })
            ->editColumn('flg_asist', function ($event) {
                return $event->flg_asist == 'S' ? 'Habilitado' : 'Deshabilitado';
            })
            ->addColumn('action', function ($event) {
                $btn = "";

                if (Gate::allows('canEdit', $event)) {
                    $btn .= '<a href="' . route('events.edit', $event) . '" class="rgba-white-sligh"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                }

                if (Gate::allows('canDelete', $event)) {
                    $modal = view('components.deleteModal', [
                        'url_delete' => route('events.destroy', $event),
                        'object' => 'Evento',
                        'id' => $event->id,
                        'description' => $event->description
                    ])->render();

                    $btn .= '<a class="btn waves-effect" data-toggle="modal" data-target="#eliminacion-registro-' . $event->id . '"
                                href="javascript:;">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>' . $modal;
                }

                return $btn;
            })
            ->rawColumns(['description', 'action'])
            ->make(true);

        return $allEvents;
    }

    static function getByFilters($request, $file_type = NULL, $category = NULL, $conclude = False)
    {
        $query = Event::with(['exam.course', 'instructor', 'certifications']);

        if ($file_type || $category) {
            $query->with(['files' => function ($q) use ($file_type, $category) {
                if ($file_type) $q->where('file_type', $file_type);
                if ($category) $q->where('category', $category);
            }]);

            $query->whereHas('files', function ($q) use ($file_type, $category) {
                if ($file_type) $q->where('file_type', $file_type);
                if ($category) $q->where('category', $category);
            });
        }

        if ($request->has('instructor_id') && $request->get('instructor_id') != '') {
            $instructor_id = $request->get('instructor_id');
            $query->whereHas('instructor', function ($q) use ($instructor_id) {
                $q->where('id', $instructor_id);
            });
        }

        if ($request->has('course_id') && $request->get('course_id') != '') {
            $course_id = $request->get('course_id');
            $query->whereHas('exam.course', function ($q) use ($course_id) {
                $q->where('id', $course_id);
            });
        }

        if ($request->has('date_from') && $request->get('date_from') != '') {
            $query->where('date', '>=', $request->get('date_from'));
        }

        if ($request->has('date_to')  && $request->get('date_to') != '') {
            $query->where('date', '<=', $request->get('date_to'));
        }

        if ($conclude) {
            $now = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD');
            $query->where('date', '<', $now)->whereHas('certifications', function ($q) {
                $q->where('score', '!=', NULL)
                    ->where('assist_user', 'S');
            });
        }

        if (!$request->hasAny(['instructor_id', 'course_id', 'date_from', 'date_to'])) {
            $query->orderBy('date', 'desc')->limit(1000);
        }

        $events = $query->get();
        return $events;
    }

    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());
        $event = Event::create($data);

        return $event;
    }

    static function update($request, Event $event)
    {
        $data = Utils::NormalizeInputData($request->all());
        $event->update($data);
        return $event;
    }

    static function destroy(Event $event)
    {
        return $event->delete();
    }

    static function getTypes()
    {
        $types = collect(config('parameters.event_types'));
        return $types;
    }


    static function getParticipants(Event $event)
    {
        $participants = $event->certificationUsers->pluck('dni');
        $users = User::whereIn('role', ['participants'])
            ->whereNotIn('dni', $participants)->get();

        return $users;
    }

    static function storeParticipants($dnis, Event $event)
    {
        $data = [];
        $certification = false;
        $dnis = collect(array_unique($dnis));

        $participants = $event->certificationUsers->pluck('dni');
        $register_dnis = $dnis->diff($participants);

        foreach ($register_dnis as $key => $dni) {
            if ($event->certifications()->count() >= ($event->room->capacity) * 2) {
                return [$certification, $dnis];
            }

            $user = User::where('dni', $dni)->with('company')->has('company')->first();

            if ($user) {
                $data = [
                    // 'company_description' => $user->company->description,
                    // 'company_ruc' => $user->company->ruc,
                    'user_id' => $user->id,
                    //'exam_id' => $event->exam->id,
                    'assist_user' => 'N',
                    'recovered_at' => NULL,
                    'status' => 'pending',
                    'position' => $user->position,
                    'evaluation_type' => 'certification',
                    //'event_id' => $event->id,
                    'company_id' => $user->company->id,
                ];
                $certification = $event->certifications()->create($data);
                $certification->miningUnits()->sync($user->miningUnits->pluck('id'));
                $data['evaluation_type'] = 'test';
                $data['assist_user'] = 'S';
                $test_certification = $event->certifications()->create($data);
                $test_certification->miningUnits()->sync($user->miningUnits->pluck('id'));

                $certification->testCertification()->associate($test_certification);
                $certification->save();

                $dnis = $dnis->diff([$dni]);
            }
        }

        return [$certification, $dnis];
    }


    static function storeParticipantsArea($dnis, Event $event)
    {


        foreach ($dnis as $key => $dni) {


            $user = User::where('dni', $dni)->with('company')->has('company')->first();
            //dd($user->id);
            if ($user) {


                $certificate = $event->certifications->where('event_id', $event->id)->where('user_id', $user->id);

                foreach ($certificate as $key => $score) {
                    //dd($file->score);
                    $score->area = $dni['area'];
                    $score->observation = $dni['observation'];
                    $score->save();
                }




                //$certificate->score = $dni['nota'];
                //$certificate->save();

                //$data = [
                // 'company_description' => $user->company->description,
                // 'company_ruc' => $user->company->ruc,
                //'user_id' => $user->id,
                //'exam_id' => $event->exam->id,
                //'assist_user' => 'N',
                //'recovered_at' => NULL,
                //'status' => 'pending',
                //'position' => $user->position,
                //'event_id' => $event->id,
                //'company_id' => $user->company->id,
                //];
                //$certification = $event->certifications()->create($data);
                //$dnis = $dnis->diff([$dni]);

            }
        }

        return [$certificate, $dnis];
    }



    static function storeParticipantsScore($dnis, Event $event)
    {


        foreach ($dnis as $key => $dni) {


            $user = User::where('dni', $dni)->with('company')->has('company')->first();
            //dd($user->id);
            if ($user) {


                $certificate = $event->certifications->where('event_id', $event->id)->where('user_id', $user->id);

                foreach ($certificate as $key => $score) {
                    //dd($file->score);
                    $score->score = $dni['nota'];
                    $score->save();
                }




                //$certificate->score = $dni['nota'];
                //$certificate->save();

                //$data = [
                // 'company_description' => $user->company->description,
                // 'company_ruc' => $user->company->ruc,
                //'user_id' => $user->id,
                //'exam_id' => $event->exam->id,
                //'assist_user' => 'N',
                //'recovered_at' => NULL,
                //'status' => 'pending',
                //'position' => $user->position,
                //'event_id' => $event->id,
                //'company_id' => $user->company->id,
                //];
                //$certification = $event->certifications()->create($data);
                //$dnis = $dnis->diff([$dni]);

            }
        }

        return [$certificate, $dnis];
    }


    static function storeParticipantsScorePractice($dnis, Event $event)
    {


        foreach ($dnis as $key => $dni) {


            $user = User::where('dni', $dni)->with('company')->has('company')->first();
            //dd($user->id);
            if ($user) {


                $certificate = $event->certifications->where('event_id', $event->id)->where('user_id', $user->id);

                foreach ($certificate as $key => $score) {
                    //dd($file->score);
                    $score->score_fin = $dni['nota_fin'];
                    $score->save();
                }




                //$certificate->score = $dni['nota'];
                //$certificate->save();

                //$data = [
                // 'company_description' => $user->company->description,
                // 'company_ruc' => $user->company->ruc,
                //'user_id' => $user->id,
                //'exam_id' => $event->exam->id,
                //'assist_user' => 'N',
                //'recovered_at' => NULL,
                //'status' => 'pending',
                //'position' => $user->position,
                //'event_id' => $event->id,
                //'company_id' => $user->company->id,
                //];
                //$certification = $event->certifications()->create($data);
                //$dnis = $dnis->diff([$dni]);

            }
        }

        return [$certificate, $dnis];
    }


    static function updateAssists(Event $event, $assists = [])
    {
        if ($assists == NULL) $assists = [];

        foreach ($event->certifications as $key => $certification) {
            if (in_array($certification->id, $assists)) $certification->assist_user = 'S';
            else $certification->assist_user = 'N';

            $certification->save();
        }
    }
}
