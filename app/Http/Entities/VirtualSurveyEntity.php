<?php
namespace App\Http\Entities;

use App\Models\Certification;
use App\Models\Survey;
use App\Models\Event;
use App\Models\UserSurvey;

class VirtualSurveyEntity
{
    static function redirectToSurvey(Certification $certification)
    {
        if($certification->event->flg_survey_course == 'N' && $certification->event->flg_survey_evaluation == 'N') return NULL;

        $user = \Auth::user();
        $now = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD');

        $surveys = collect([]);
        

        if($certification->event->flg_survey_course == 'S')
            $surveys = $surveys->concat(self::getCourseSurvey()) ;

        if($certification->event->flg_survey_evaluation == 'S')
            $surveys = $surveys->concat(self::getEvaluationSurvey()) ;


        $surveys_finished = $user->userSurveys()
        ->where('status','finished')
        ->where('date',$now)
        ->with('survey')
        ->get();

        $surveys_finished_arr = $surveys_finished->pluck('survey_id');
        $surveys_pending = $surveys->whereNotIn('id', $surveys_finished_arr);

        if($surveys_pending->isNotEmpty()) return $surveys_pending->first();

        return NULL;
    }

    static function redirectToSurveyEvaluation(Certification $certification)
    {
        if($certification->event->flg_survey_evaluation == 'N') return NULL;

        $user = \Auth::user();
        $now = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD');
        $surveys = self::getEvaluationSurvey();

        $surveys_finished = $user->userSurveys()
        ->where('status','finished')
        ->where('date',$now)
        ->with('survey')
        ->get();

        $surveys_finished_arr = $surveys_finished->pluck('survey_id');
        $surveys_pending = $surveys->whereNotIn('id', $surveys_finished_arr);


        if($surveys_pending->isNotEmpty()) return $surveys_pending->first();

        return NULL;
    }

    static function redirectToSurveyCourse(Certification $certification)
    {
        if($certification->event->flg_survey_course == 'N') return NULL;

        $user = \Auth::user();
        $now = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD');
        $surveys = self::getCourseSurvey();

        $surveys_finished = $user->userSurveys()
        ->where('status','finished')
        ->where('date',$now)
        ->with('survey')
        ->get();

        $surveys_finished_arr = $surveys_finished->pluck('survey_id');
        $surveys_pending = $surveys->whereNotIn('id', $surveys_finished_arr);

        if($surveys_pending->isNotEmpty()) return $surveys_pending->first();

        return NULL;
    }

    static function getEvaluationSurvey()
    {
        $surveys = Survey::where('destined_to','evaluation')->where('active','S')->get();
        
        return $surveys;
    }

    static function getCourseSurvey()
    {
        $surveys = Survey::where('destined_to','course_live')->where('active','S')->get();
        return $surveys;
    }

     static function getUserProfile()
    {
        $surveys = Survey::where('destined_to','user_profile')->where('active','S')->get();
        return $surveys;
    }
    

    static function generateUserSurvey(Survey $survey ,Event $event)
    {
        $user = \Auth::user();
        $survey->load(['groups.statements.options']);



        $userSurvey = $user->userSurveys()->create([
            'survey_id' => $survey->id,
            'company_id' => $user->company_id,
            'date' => \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD'),
            'status' => 'pending',
            'start_time' => \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD HH:mm:ss'),
            'end_time' => NULL,
            'total_time' => NULL,
            'event_id'=> $event->id,
        ]);

        $i = 1;
        foreach ($survey->groups as $key => $group) {

            foreach ($group->statements as $key => $statement) {
                $userSurvey->surveyAnswers()->create([
                    'statement' => $statement->description,
                    'answer' => NULL,
                    'question_order' => $i++,
                    'statement_id' => $statement->id,
                ]);
            }
        }

        return $userSurvey;
    }



      static function generateUserSurveyProfile(Survey $survey)
      {
      
        $user = \Auth::user();
        $survey->load(['groups.statements.options']);



        $userSurvey = $user->userSurveys()->create([
            'survey_id' => $survey->id,
            'company_id' => $user->company_id,
            'date' => \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD'),
            'status' => 'pending',
            'start_time' => \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD HH:mm:ss'),
            'end_time' => NULL,
            'total_time' => NULL,
            'event_id'=> NULL,
        ]);

        $i = 1;
        foreach ($survey->groups as $key => $group) {

            foreach ($group->statements as $key => $statement) {
                $userSurvey->surveyAnswers()->create([
                    'statement' => $statement->description,
                    'answer' => NULL,
                    'question_order' => $i++,
                    'statement_id' => $statement->id,
                ]);
            }
        }

        return $userSurvey;
    }






    static function countQuestions(UserSurvey $userSurvey)
    {
        $count = $userSurvey->surveyAnswers->count();
        return $count;
    }

    static function getSurveyAnswer(UserSurvey $userSurvey, $question_order)
    {
        $surveyAnswer = $userSurvey->surveyAnswers()
        ->where('question_order',$question_order)
        ->with(['question.options'])
        ->first();

        return $surveyAnswer;
    }

    static function answerQuestion($request, UserSurvey $userSurvey, $question_order)
    {
        $surveyAnswer = self::getSurveyAnswer($userSurvey,$question_order);
        $question = $surveyAnswer->question;

        $surveyAnswer->statement = $question->description;
        $surveyAnswer->answer = $request->get('answer');

        return $surveyAnswer->save();
    }

    static function concludeSurvey(UserSurvey $userSurvey)
    {  
        $user = \Auth::user();
        $user->profile_survey = 'S';
        $user->save();
        $now = \Carbon\Carbon::now('America/Lima');
        $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $userSurvey->start_time,'America/Lima');

        $userSurvey->status = 'finished';
        $userSurvey->end_time = $now->isoFormat('YYYY-MM-DD HH:mm:ss');
        $userSurvey->total_time = $start_time->diffInMinutes($now);

        return $userSurvey->save();
    }






    static function getFirstEvaluationNoAnswer(Certification $certification)
    {

        $evaluations = $certification->evaluations()->where('evaluation_time', $certification->evaluation_time)->orderBy('question_order', 'asc')->get();

        foreach ($evaluations as $key => $evaluation) {
            if($evaluation->selected_alternative == NULL) break;
        }

        $question_order = $evaluation->question_order ?? 1;

        return $question_order;
    }



    static function concludeEvaluation(Certification $certification)
    {
        $evaluations = $certification->evaluations()
        ->where('evaluation_time', $certification->evaluation_time)
        ->with(['question'])
        ->get();

        $score = 0;
        foreach ($evaluations as $key => $evaluation) {
            if($evaluation->is_correct){
                $score += $evaluation->question->points ?? 0;
            }
        }

        $exam_time = $certification->event->exam->exam_time ?? 0;
        $now = \Carbon\Carbon::now('America/Lima');

        if($certification->start_time == NULL)  $certification->start_time = $now->isoFormat('YYYY-MM-DD HH:mm:ss');

        $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $certification->start_time,'America/Lima');

        $certification->status = 'finished';
        $certification->end_time = $now->isoFormat('YYYY-MM-DD HH:mm:ss');
        $certification->total_time = ($start_time->diffInMinutes($now) <= $exam_time ? $start_time->diffInMinutes($now) : $exam_time);
        $certification->score = $score;

        return $certification->save();
    }

    static function concludeEvaluations(Event $event)
    {
        $certifications = $event->certifications()
        ->where('status','!=','finished')
        ->get();

        foreach ($certifications as $key => $certification) {
            self::concludeEvaluation($certification);
        }

        return True;
    }


}
