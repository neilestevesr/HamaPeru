<?php
namespace App\Http\Entities;

use App\Models\Certification;
use App\Models\Event;
use App\Http\Entities\Utils;

class VirtualEvaluationEntity
{
    static function generateEvaluation(Certification $certification)
    {
        $time = time();
        if($certification->event->flg_test_exam == 'S' && $certification->event->testExam != NULL ){
            $dynamic_questions = $certification->event->testExam->dynamicQuestions()->where('correct_alternative_id','!=','NULL')->get();
        }
        else{
            $dynamic_questions = $certification->event->exam->dynamicQuestions()->where('correct_alternative_id','!=','NULL')->get();
        }
        $random = $dynamic_questions->random(10)->shuffle();

        $data = [];
        $flag = NULL;

        foreach ($random as $key => $dynamic_question) {

            $data = [
                'evaluation_time' => $time,
                'statement' => $dynamic_question->statement,
                'correct_alternative' => $dynamic_question->correctDynamicAlternative->description,
                'selected_alternative' => NULL,
                'is_correct' => False,
                'question_order' => $key+1,
                'question_id' => $dynamic_question->id,
            ];
            $flag = $certification->evaluations()->create($data);
        }

        if($flag != NULL){
            $certification->status = 'in_progress';
            $certification->evaluation_time = $time;
            $certification->start_time = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD HH:mm:ss');
            $certification->save();
        }

        return $flag != NULL;
    }

    static function getFirstEvaluationNoAnswer(Certification $certification)
    {
        $evaluations = $certification->evaluations()->where('evaluation_time', $certification->evaluation_time)->orderBy('question_order', 'asc')->get();

        foreach ($evaluations as $key => $evaluation) {
            if($evaluation->selected_alternative == NULL) break;
        }

        $question_order = $evaluation->question_order ?? 1;

        return $question_order;
    }

    static function countQuestions(Certification $certification)
    {
        $count = $certification->evaluations()
        ->where('evaluation_time', $certification->evaluation_time)->count();

        return $count;
    }

    static function getQuestion(Certification $certification, $question_order)
    {
        $question = $certification->evaluations()
        ->where('evaluation_time', $certification->evaluation_time)
        ->where('question_order',$question_order)
        ->with(['dynamicQuestion.dynamicAlternatives','dynamicQuestion.files' => function($q){
            $q->whereIn('file_type',['imagenes','videos']);
        }])
        ->first();

        return $question;
    }

    static function answerQuestion($request, Certification $certification, $question_order)
    {
        $alternative_id = $request->get('alternative_id');
        $evaluation = self::getQuestion($certification,$question_order);
        $dynamic_question = $evaluation->dynamicQuestion;

        $evaluation->statement = $dynamic_question->statement;
        $evaluation->correct_alternative = $dynamic_question->correctDynamicAlternative->description;
        $evaluation->selected_alternative = $dynamic_question->dynamicAlternatives->where('id', $alternative_id)->first()->description;
        $evaluation->is_correct = ($evaluation->correct_alternative == $evaluation->selected_alternative);

        return $evaluation->save();
    }

    static function concludeEvaluation(Certification $certification)
    {
        $evaluations = $certification->evaluations()
        ->where('evaluation_time', $certification->evaluation_time)
        ->with(['dynamicQuestion'])
        ->get();

        $score = 0;
        foreach ($evaluations as $key => $evaluation) {
            if($evaluation->is_correct){
                $score += $evaluation->dynamicQuestion->points ?? 0;
            }
        }

        $exam_time = $certification->event->exam->exam_time ?? 0;
        $now = \Carbon\Carbon::now('America/Lima');

        if($certification->start_time == NULL)  $certification->start_time = $now->isoFormat('YYYY-MM-DD HH:mm:ss');

        $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $certification->start_time,'America/Lima');

        $certification->status = 'finished';
        $certification->end_time = $now->isoFormat('YYYY-MM-DD HH:mm:ss');
        $certification->total_time = ($start_time->diffInMinutes($now) <= $exam_time ? $start_time->diffInMinutes($now) : $exam_time);
        $certification->score = $score;

        return $certification->save();
    }

    static function concludeEvaluations(Event $event)
    {
        $certifications = $event->certifications()
        ->where('status','!=','finished')
        ->get();

        foreach ($certifications as $key => $certification) {
            self::concludeEvaluation($certification);
        }

        return True;
    }


}
