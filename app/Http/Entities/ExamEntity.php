<?php
namespace App\Http\Entities;

use App\Models\Exam;
use App\Http\Entities\Utils;

class ExamEntity
{
    static function all()
    {
        return Exam::all();
    }

    static function allWithRelationships()
    {
        return Exam::with(['ownerCompany','course','questions','events'])->get();
    }

    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());
        $exam = Exam::create($data);

        return $exam;
    }

    static function update($request,Exam $exam)
    {
        $data = Utils::NormalizeInputData($request->all());
        $exam->update($data);
        return $exam;
    }

    static function destroy(Exam $exam)
    {
        $exam->questions()->delete();
        return $exam->delete();
    }

}
