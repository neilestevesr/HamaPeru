<?php
namespace App\Http\Entities;

use App\Models\Survey;
use App\Models\Group;

class GroupEntity
{
    static function all()
    {
        return Group::all();
    }

    static function store($request,Survey $survey)
    {
        $group = $survey->groups()->create($request->all());
        return $group;
    }

    static function update($request,Group $group)
    {
        $group->update($request->all());
        return $group;
    }

    static function destroy(Group $group)
    {
        return $group->delete();
    }

}
