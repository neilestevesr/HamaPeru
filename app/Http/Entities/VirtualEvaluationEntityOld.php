<?php
namespace App\Http\Entities;

use App\Models\Certification;
use App\Models\Event;
use App\Http\Entities\Utils;

class VirtualEvaluationEntityOld
{
    static function generateEvaluation(Certification $certification)
    {
        $time = time();
        $questions = $certification->event->exam->questions;

        $random = $questions->random(10);

        //$max = $questions->count();
        $numbers = range(1, 10);
        shuffle($numbers);
        $data = [];
        $flag = NULL;

        foreach ($random as $key => $question) {
            $data = [
                'evaluation_time' => $time,
                'statement' => $question->statement,
                'correct_alternative' => $question->{$question->alternative_correct},
                'selected_alternative' => NULL,
                'is_correct' => False,
                'question_order' => $numbers[$key],
                'question_id' => $question->id,
            ];
            $flag = $certification->evaluations()->create($data);
        }

        if($flag != NULL){
            $certification->status = 'in_progress';
            $certification->evaluation_time = $time;
            $certification->start_time = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD HH:mm:ss');
            $certification->save();
        }

        return $flag != NULL;
    }

    static function getFirstEvaluationNoAnswer(Certification $certification)
    {
        $evaluations = $certification->evaluations()->where('evaluation_time', $certification->evaluation_time)->orderBy('question_order', 'asc')->get();

        foreach ($evaluations as $key => $evaluation) {
            if($evaluation->selected_alternative == NULL) break;
        }

        $question_order = $evaluation->question_order ?? 1;

        return $question_order;
    }

    static function countQuestions(Certification $certification)
    {
        $count = $certification->evaluations()
        ->where('evaluation_time', $certification->evaluation_time)
        ->get()->count();

        return $count;
    }

    static function getQuestion(Certification $certification, $question_order)
    {
        $question = $certification->evaluations()
        ->where('evaluation_time', $certification->evaluation_time)
        ->where('question_order',$question_order)
        ->with(['question'])
        ->first();

        return $question;
    }

    static function answerQuestion($request, Certification $certification, $question_order)
    {
        $evaluation = self::getQuestion($certification,$question_order);
        $question = $evaluation->question;

        $evaluation->statement = $question->statement;
        $evaluation->correct_alternative = $question->{$question->alternative_correct};
        $evaluation->selected_alternative = $question->{$request->get('alternative')};
        $evaluation->is_correct = ($evaluation->correct_alternative == $evaluation->selected_alternative);

        return $evaluation->save();
    }

    static function concludeEvaluation(Certification $certification)
    {
        $evaluations = $certification->evaluations()
        ->where('evaluation_time', $certification->evaluation_time)
        ->with(['question'])
        ->get();

        $score = 0;
        foreach ($evaluations as $key => $evaluation) {
            if($evaluation->is_correct){
                $score += $evaluation->question->points ?? 0;
            }
        }

        $exam_time = $certification->event->exam->exam_time ?? 0;
        $now = \Carbon\Carbon::now('America/Lima');

        if($certification->start_time == NULL)  $certification->start_time = $now->isoFormat('YYYY-MM-DD HH:mm:ss');

        $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $certification->start_time,'America/Lima');

        $certification->status = 'finished';
        $certification->end_time = $now->isoFormat('YYYY-MM-DD HH:mm:ss');
        $certification->total_time = ($start_time->diffInMinutes($now) <= $exam_time ? $start_time->diffInMinutes($now) : $exam_time);
        $certification->score = $score;

        return $certification->save();
    }

    static function concludeEvaluations(Event $event)
    {
        $certifications = $event->certifications()
        ->where('status','!=','finished')
        ->get();

        foreach ($certifications as $key => $certification) {
            self::concludeEvaluation($certification);
        }

        return True;
    }


}
