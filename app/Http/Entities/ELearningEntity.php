<?php
namespace App\Http\Entities;

use App\Models\ELearning;
use App\Http\Entities\Utils;
use App\Http\Entities\FileEntity;
use App\Models\File;

class ELearningEntity
{
    static function all()
    {
        return ELearning::all();
    }

    static function allWithRelationships()
    {
        return ELearning::with(['contents'])->get();
    }

    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());

        $elearning = ELearning::create($data);
        if($request->has('image')){
            $result = FileEntity::store($elearning,$request->file('image'),'imagenes','e-learnings');
        }
        return $elearning;
    }

    static function update($request,ELearning $elearning)
    {
        $data = Utils::NormalizeInputData($request->all());

        if($request->has('image')){
            $files = $elearning->files()->where('file_type','imagenes')->where('category','e-learnings')->get();

            if($files->isNotEmpty()){
                foreach ($files as $key => $file) {
                    FileEntity::delete($file);
                }
            }  

            $result = FileEntity::store($elearning,$request->file('image'),'imagenes','e-learnings');
        }

        $result = $elearning->update($data);
        return $result;
    }

    static function destroy(ELearning $elearning)
    {
        $files = $elearning->files()->get();
        if($files->isNotEmpty()){
            foreach ($files as $key => $file) {
                FileEntity::delete($file);
            }
        }
        return $elearning->delete();
    }

}
