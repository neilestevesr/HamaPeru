<?php
namespace App\Http\Entities;
use App\Models\Certification;
use App\Models\Evaluation;

class Utils
{
    static function NormalizeInputData($data)
    {
        if(isset($data['active'])){
            $data['active'] = 'S';
        }else {
            $data['active'] = 'N';
        }

        if(isset($data['flg_test_exam'])){
            $data['flg_test_exam'] = 'S';
        }else {
            $data['flg_test_exam'] = 'N';
        }

        if(isset($data['flg_public'])){
            $data['flg_public'] = 'S';
        }else {
            $data['flg_public'] = 'N';
        }
        
        if(isset($data['flg_asist'])){
            $data['flg_asist'] = 'S';
        }else {
            $data['flg_asist'] = 'N';
        }

        if(isset($data['flg_survey_course'])){
            $data['flg_survey_course'] = 'S';
        }else {
            $data['flg_survey_course'] = 'N';
        }

        if(isset($data['flg_survey_evaluation'])){
            $data['flg_survey_evaluation'] = 'S';
        }else {
            $data['flg_survey_evaluation'] = 'N';
        }

        if(isset($data['assist_user'])){
            $data['assist_user'] = 'S';
        }else {
            $data['assist_user'] = 'N';
        }

        return $data;
    }

    static function saveEvaluation(Certification $certification,$preguntas,$act,$resp,$nro_orden)
    {
        $pregunta = $preguntas->firstWhere('numero', $nro_orden);
        if($pregunta){
            $evaluation = new Evaluation([
                'evaluation_time' => $certification->evaluation_time,
                'statement' => $pregunta->preg,
                'selected_alternative' => $resp,
                'correct_alternative' => $pregunta->resp,
                'is_correct' => $act == '10' ? true : false,
                'question_order' => $nro_orden,
                'question_id' => $pregunta->id_preguntas
            ]);

            return $certification->evaluations()->save($evaluation);

        }


    }


}
