<?php
namespace App\Http\Entities;

use App\Models\Exam;
use App\Http\Entities\Utils;

class DynamicTestExamEntity
{
    static function all()
    {
        return Exam::where('exam_type','test');
    }

    static function allWithRelationships()
    {
        return Exam::with(['ownerCompany','course','dynamicQuestions','events'])->where('exam_type','test')->get();
    }

    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());
        $data['exam_type'] = 'test';
        $dynamic_test_exam = Exam::create($data);

        return $dynamic_test_exam;
    }

    static function update($request,Exam $dynamic_test_exam)
    {
        $data = Utils::NormalizeInputData($request->all());
        $dynamic_test_exam->update($data);
        return $dynamic_test_exam;
    }

    static function destroy(Exam $dynamic_test_exam)
    {
        $dynamic_test_exam->questions()->delete();
        return $dynamic_test_exam->delete();
    }

}
