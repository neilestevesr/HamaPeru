<?php
namespace App\Http\Entities;

use App\Models\DynamicQuestion;
use App\Models\DynamicAlternative;
use App\Http\Entities\Utils;

class DynamicAlternativeEntity
{
    static function store($request,DynamicQuestion $dynamic_question)
    {
        $dynamic_alternative = $dynamic_question->dynamicAlternatives()->create($request->all());
        return $dynamic_alternative;
    }

    static function update($request,DynamicAlternative $dynamic_alternative)
    {
        $dynamic_alternative->update($request->all());
        return $dynamic_alternative;
    }

    static function destroy(DynamicAlternative $dynamic_alternative)
    {
        $dynamic_question = $dynamic_alternative->dynamicQuestion;

        if($dynamic_question->correct_alternative_id == $dynamic_alternative->id ){
            $dynamic_question->correctDynamicAlternative()->dissociate();
            $dynamic_question->save();
        }

        return $dynamic_alternative->delete();
    }

    static function selectCorrectAlternative(DynamicAlternative $dynamic_alternative)
    {
        $dynamic_question = $dynamic_alternative->dynamicQuestion;
        $dynamic_question->correctDynamicAlternative()->associate($dynamic_alternative);
        return $dynamic_question->save();
    }

}
