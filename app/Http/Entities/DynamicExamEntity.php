<?php
namespace App\Http\Entities;

use App\Models\Exam;
use App\Http\Entities\Utils;

class DynamicExamEntity
{
    static function all()
    {
        return Exam::where('exam_type','dynamic');
    }

    static function allWithRelationships()
    {
        return Exam::with(['ownerCompany','course','dynamicQuestions','events'])->where('exam_type','dynamic')->get();
    }

    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());
        $data['exam_type'] = 'dynamic';
        $dynamic_exam = Exam::create($data);

        return $dynamic_exam;
    }

    static function update($request,Exam $dynamic_exam)
    {
        $data = Utils::NormalizeInputData($request->all());
        $dynamic_exam->update($data);
        return $dynamic_exam;
    }

    static function destroy(Exam $dynamic_exam)
    {
        $dynamic_exam->questions()->delete();
        return $dynamic_exam->delete();
    }

}
