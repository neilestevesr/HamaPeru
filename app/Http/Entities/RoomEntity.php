<?php
namespace App\Http\Entities;

use App\Models\Room;
use App\Http\Entities\Utils;

class RoomEntity
{
    static function all()
    {
        return Room::all();
    }

    static function allWithRelationships()
    {
        return Room::with(['events'])->get();
    }

    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());
        return Room::create($data);
    }

    static function update($request,Room $room)
    {
        $data = Utils::NormalizeInputData($request->all());
        return $room->update($data);
    }

    static function destroy(Room $room)
    {
        return $room->delete();
    }

}
