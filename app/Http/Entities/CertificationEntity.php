<?php

namespace App\Http\Entities;

use App\Models\Certification;
use App\Models\MiningUnit;
use App\Models\Event;
use App\Models\User;
use App\Models\File;
use App\Http\Entities\FileEntity;
use App\Http\Entities\Utils;
// use App\Http\Entities\CertificationEntity;
use Image;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Gate;


class CertificationEntity
{
    static function all()
    {
        return Certification::all();
    }

    static function getDatatable($request)
    {
        $query = Certification::with(['user.miningUnits', 'company', 'event.exam.course']);

        if ($request->has('dni') && $request->get('dni') != '') {
            $dni = $request->get('dni');
            $query->whereHas('user', function ($q) use ($dni) {
                $q->where('dni', $dni);
            });
        }

        if ($request->has('company_id') && $request->get('company_id') != '') {
            $company_id = $request->get('company_id');
            $query->whereHas('company', function ($q) use ($company_id) {
                $q->where('id', $company_id);
            });
        }

        if ($request->has('course_id') && $request->get('course_id') != '') {
            $course_id = $request->get('course_id');
            $query->whereHas('event.exam.course', function ($q) use ($course_id) {
                $q->where('id', $course_id);
            });
        }

        if ($request->has('date_from') && $request->get('date_from') != '') {
            $dateFrom = $request->get('date_from');
            $query->whereHas('event', function ($q) use ($dateFrom) {
                $q->where('date', '>=', $dateFrom);
            });
        }

        if ($request->has('date_to')  && $request->get('date_to') != '') {
            $dateTo = $request->get('date_to');
            $query->whereHas('event', function ($q) use ($dateTo) {
                $q->where('date', '<=', $dateTo);
            });
        }

        $query->where('evaluation_type', 'certification');

        // if (!$request->hasAny(['dni', 'company_id', 'course_id', 'date_from', 'date_to'])) {
        //     $query->orderBy('updated_at', 'desc')->limit(500);
        // }

        $allCertifications = DataTables::of($query)
            ->editColumn('dni', function ($certification) {
                return $certification->user->dni;
            })
            ->editColumn('full_name', function ($certification) {
                return $certification->user->full_name_complete_reverse;
            })
            ->addColumn('company', function ($certification) {
                return $certification->company->description;
            })
            ->editColumn('course', function ($certification) {
                return $certification->event->exam->course->description;
            })
            ->editColumn('date', function ($certification) {
                return $certification->event->date ?? '-';
            })
            ->editColumn('note', function ($certification) {
                return $certification->score ?? '0';
            })
            ->editColumn('exam', function ($certification) {
                if (Gate::allows('canDownloadExam', $certification)) {
                    return '<a href="' . route('certifications.exam_pdf', $certification->id) . '"> <img src="' . asset('images/examen.png')  . '" width="25" height="25" /> </a>';
                } else {
                    return '-';
                }
            })
            ->editColumn('certification', function ($certification) {
                if (Gate::allows('canDownloadCertificate', $certification)) {
                    if (empty($certification->event->EmpresaTitular)) {
                        return '<a href="' . route('certifications.certificate_pdf', $certification->id) . '"> <img src="' . asset('images/certificado.png')  . '" width="25" height="25" /> </a>';
                    } else {
                        if ($certification->event->EmpresaTitular->id == '2') {
                            return '<a href="' . route('certifications.certificate_pdf', $certification->id) . '"> <img src="' . asset('images/certificado.png')  . '" width="25" height="25" /> </a>';
                        } else {
                            return '<a href="' . route('certifications.certificate_pdf_externos', $certification->id) . '"> <img src="' . asset('images/certificado.png')  . '" width="25" height="25" /> </a>';
                        }
                    }
                } else {
                    return '-';
                }
            })
            ->editColumn('annexed_atocha', function ($certification) {
                if (Gate::allows('canDownloadAnexo', [$certification, 'atacocha'])) {
                    if ($certification->event->id < '3420') {
                        return '<a href="' . route('certifications.anexo4old', [$certification->id, 'atacocha']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                    } elseif ($certification->event->id >= '3420') {
                        return '<a href="' . route('certifications.anexo4Atacocha', [$certification->id, 'atacocha']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                    }
                } else {
                    return '-';
                }
            })
            ->editColumn('annexed_porvenir', function ($certification) {
                if (Gate::allows('canDownloadAnexo', [$certification, 'porvenir'])) {
                    if ($certification->event->id < '3420') {
                        return '<a href="' . route('certifications.anexo4Porvenir', [$certification->id, 'porvenir']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                    } elseif ($certification->event->id >= '3420') {
                        return '<a href="' . route('certifications.anexo4Atacocha', [$certification->id, 'porvenir']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                    }
                } else {
                    return '-';
                }
            })
            ->editColumn('annexed_sinaycocha', function ($certification) {
                if (Gate::allows('canDownloadAnexo', [$certification, 'sinaycocha'])) {
                    return '<a href="' . route('certifications.anexo4old', [$certification->id, 'sinaycocha']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                } else {
                    return '-';
                }
            })
            ->editColumn('annexed_cerrolindo', function ($certification) {
                if (Gate::allows('canDownloadAnexo', [$certification, 'cerro'])) {
                    return '<a href="' . route('certifications.anexo4old', [$certification->id, 'cerro']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                } else {
                    return '-';
                }
            })
            ->editColumn('cart_1', function ($certification) {
                if (Gate::allows('canDownloadAnexo', [$certification, 'atacocha'])) {
                    if ($certification->company->id == '6') {
                        return '<a href="' . route('certifications.compromiso', [$certification->id, 'atacocha']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                    } else {
                        return '<a href="' . route('certifications.compromisoOld', [$certification->id, 'atacocha']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                    }
                } else {
                    return '-';
                }
            })
            ->editColumn('cart_2', function ($certification) {
                if (Gate::allows('canDownloadAnexo', [$certification, 'porvenir'])) {
                    if ($certification->company->id == '6') {
                        return '<a href="' . route('certifications.compromiso', [$certification->id, 'porvenir']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                    } else {
                        return '<a href="' . route('certifications.compromisoOld', [$certification->id, 'porvenir']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                    }
                } else {
                    return '-';
                }
            })
            ->editColumn('cart_3', function ($certification) {
                if (Gate::allows('canDownloadAnexo', [$certification, 'sinaycocha'])) {
                    return '<a href="' . route('certifications.compromiso', [$certification->id, 'sinaycocha']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                } else {
                    return '-';
                }
            })
            ->editColumn('cart_4', function ($certification) {
                if (Gate::allows('canDownloadAnexo', [$certification, 'cerro'])) {
                    return '<a href="' . route('certifications.compromiso', [$certification->id, 'cerro']) . '"> <img src="' . asset('images/pdf.png')  . '" width="25" height="25" /> </a>';
                } else {
                    return '-';
                }
            })
            ->rawColumns(['date', 'note', 'exam', 'certification', 'annexed_atocha', 'annexed_porvenir', 'annexed_sinaycocha', 'annexed_cerrolindo', 'cart_1', 'cart_2', 'cart_3', 'cart_4'])
            ->make(true);

        return $allCertifications;
    }

    static function getByFilters($request, $file_type = NULL, $category = NULL, $conclude = False)
    {
        $query = Certification::with(['user.miningUnits', 'company', 'event.exam.course']); //->where('score','!=',NULL)

        if ($file_type || $category) {
            $query->with(['files' => function ($q) use ($file_type, $category) {
                if ($file_type) $q->where('file_type', $file_type);
                if ($category) $q->where('category', $category);
            }]);

            $query->whereHas('files', function ($q) use ($file_type, $category) {
                if ($file_type) $q->where('file_type', $file_type);
                if ($category) $q->where('category', $category);
            });
        }

        if ($request->has('dni') && $request->get('dni') != '') {
            $dni = $request->get('dni');
            $query->whereHas('user', function ($q) use ($dni) {
                $q->where('dni', $dni);
            });
        }

        if ($request->has('company_id') && $request->get('company_id') != '') {
            $company_id = $request->get('company_id');
            $query->whereHas('company', function ($q) use ($company_id) {
                $q->where('id', $company_id);
            });
        }

        if ($request->has('course_id') && $request->get('course_id') != '') {
            $course_id = $request->get('course_id');
            $query->whereHas('event.exam.course', function ($q) use ($course_id) {
                $q->where('id', $course_id);
            });
        }

        if ($request->has('date_from') && $request->get('date_from') != '') {
            $dateFrom = $request->get('date_from');
            $query->whereHas('event', function ($q) use ($dateFrom) {
                $q->where('date', '>=', $dateFrom);
            });
        }

        if ($request->has('date_to')  && $request->get('date_to') != '') {
            $dateTo = $request->get('date_to');
            $query->whereHas('event', function ($q) use ($dateTo) {
                $q->where('date', '<=', $dateTo);
            });
        }

        if ($conclude) {
            $query->where('status', 'finished')->where('assist_user', 'S');
        }

        $query->where('evaluation_type', 'certification');

        if (!$request->hasAny(['dni', 'company_id', 'course_id', 'date_from', 'date_to'])) {
            $query->orderBy('updated_at', 'desc')->limit(500);
        }

        $certifications = $query->get();
        return $certifications;
    }


    static function getCertificationsFilterDni($request)
    {
        $query = Certification::with(['user', 'company', 'event.EmpresaTitular', 'files' => function ($q) {
            $q->whereIn('category', ['anexos', 'esp']);
        }])
            ->where('score', '>=', 14)
            ->where('evaluation_type', 'certification')
            ->whereHas('company', function ($q) {
                $q->where('active', 'S');
            });


        // -------- FILTRAR CERTIFICADOS DE EDGAR KRISTHIAN VALDIVIA GAMARRA, ID: 2901

        // $query->whereHas('event', function ($q) {
        //     $q->where('events.user_id', "!=", "2901");
        // });

        // --------- FILTRAR CERTIFICADOS DE NEXA, ID: 6 ------------

        // $query->where('certifications.company_id', "!=", "6")
        $query->where(function ($q) {
            $q->where('certifications.company_id', '!=', '6')
                ->orWhere(function ($q) {
                    $q->where('certifications.company_id', '6')
                        ->whereHas('event', function ($q) {
                            $q->where('events.date', '>=', '2023-01-01');
                        });
                });
        });

        if ($request->has('dni') && $request->get('dni') != '') {
            $dni = $request->get('dni');
            $query->whereHas('user', function ($q) use ($dni) {
                $q->where('dni', $dni);
            });
        }

        if ($request->has('company_id') && $request->get('company_id') != '') {
            $company_id = $request->get('company_id');
            $query->where('company_id', $company_id);
        }

        $certifications = $query->get();
        return $certifications;
    }


    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());
        return Certification::create($data);
    }

    static function update($request, Certification $certification)
    {
        $data = Utils::NormalizeInputData($request->all());

        $certification->update($data);
        $certification->miningUnits()->sync($request->get('mining_units'));
        return $certification;
    }

    static function destroy(Certification $certification)
    {
        $certification->miningUnits()->sync([]);
        $files = $certification->files()->get();
        if ($files->isNotEmpty()) {
            foreach ($files as $key => $file) {
                FileEntity::delete($file);
            }
        }
        if ($certification->testCertification != null) {
            CertificationEntity::destroy($certification->testCertification);
        }

        return  $certification->delete();
    }

    static function reset(Certification $certification)
    {
        $certification->recovered_at = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD HH:mm:ss');
        $certification->status = 'pending';
        $certification->evaluation_time = NULL;
        $certification->start_time = NULL;
        $certification->end_time = NULL;
        $certification->total_time = NULL;
        $certification->score = NULL;
        return $certification->save();
    }

    static function exportCertificatePDF(Certification $certification)
    {

        $pdf = \PDF::loadView('admin.certifications.pdfs.certificado', compact('certification'));

        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'certificados_' . $certification->user->dni . '_' . $certification->event->id . '.pdf';
        // return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }
    static function exportCertificatePDFexternos(Certification $certification)
    {

        $pdf = \PDF::loadView('admin.certifications.pdfs.certificado_externos', compact('certification'));

        $pdf->setPaper(array(0, 0, 580.00, 800.00), 'landscape');

        $pdf_name = 'certificados_' . $certification->user->dni . '_' . $certification->event->id . '.pdf';
        //return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportCertificatePDFexternos2(Certification $certification)
    {


        $original_image = $certification->event->instructor->signature_url; // Url de la imagen

        $mask = Image::make($original_image)
            ->greyscale()
            ->contrast(100)
            ->trim('top-left', null, 40)
            ->invert();

        $new_image = Image::canvas($mask->width(), $mask->height(), '#000000')
            ->mask($mask)
            ->encode('png', 100);

        $pdf = \PDF::loadView('admin.certifications.pdfs.certificado_externos_control_energia', compact(['certification', 'new_image']));

        $pdf->setPaper('a4', 'landscape');



        $pdf_name = 'certificados_' . $certification->user->dni . '_' . $certification->event->id . '.pdf';
        //return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }


    static function exportCertificatePDFold(Certification $certification)
    {
        $pdf = \PDF::loadView('admin.certifications.pdfs.certificado_old', compact('certification'));
        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'certificados_' . $certification->user->dni . '_' . $certification->event->id . '.pdf';
        // return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportExamPDF(Certification $certification)
    {
        $pdf = \PDF::loadView('admin.certifications.pdfs.exam', compact('certification'));
        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'examen_' . $certification->id . '.pdf';
        //return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportAnexoPDF(Certification $certification, $miningUnit)
    {
        $mining_unit = MiningUnit::where('description', 'like', "%$miningUnit%")->first();

        $sufijo = self::getSufijo($miningUnit);

        $pdf = \PDF::loadView('admin.certifications.pdfs.anexo', compact(['certification', 'mining_unit']));
        $pdf->setPaper('A4', 'portrait');

        $pdf_name = 'anexos_' . $certification->user->dni . '_' . $certification->event->id . '_' . $sufijo . '.pdf';
        #return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportAnexoPDFAtacocha(Certification $certification, $miningUnit)
    {
        $mining_unit = MiningUnit::where('description', 'like', "%$miningUnit%")->first();

        $sufijo = self::getSufijo($miningUnit);

        $pdf = \PDF::loadView('admin.certifications.pdfs.anexoAtacocha', compact(['certification', 'mining_unit']));
        $pdf->setPaper('A4', 'portrait');

        $pdf_name = 'anexos_' . $certification->user->dni . '_' . $certification->event->id . '_' . $sufijo . '.pdf';
        #return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportAnexoPDFPorvenir(Certification $certification, $miningUnit)
    {
        $mining_unit = MiningUnit::where('description', 'like', "%$miningUnit%")->first();

        $sufijo = self::getSufijo($miningUnit);

        $pdf = \PDF::loadView('admin.certifications.pdfs.anexoPorvenir', compact(['certification', 'mining_unit']));
        $pdf->setPaper('A4', 'portrait');

        $pdf_name = 'anexos_' . $certification->user->dni . '_' . $certification->event->id . '_' . $sufijo . '.pdf';
        #return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportAnexoPDFold(Certification $certification, $miningUnit)
    {
        $mining_unit = MiningUnit::where('description', 'like', "%$miningUnit%")->first();

        $sufijo = self::getSufijo($miningUnit);

        $pdf = \PDF::loadView('admin.certifications.pdfs.anexoOld', compact(['certification', 'mining_unit']));
        $pdf->setPaper('A4', 'portrait');

        $pdf_name = 'anexos_' . $certification->user->dni . '_' . $certification->event->id . '_' . $sufijo . '.pdf';
        #return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportCompromisoPDF(Certification $certification, $miningUnit)
    {
        $mining_unit = MiningUnit::where('description', 'like', "%$miningUnit%")->first();
        $sufijo = self::getSufijo($miningUnit);

        $pdf = \PDF::loadView('admin.certifications.pdfs.compromiso', compact(['certification', 'mining_unit']));
        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'compromisos_' . $certification->user->dni . '_' . $certification->event->id . '_' . $sufijo . '.pdf';
        // return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }
    static function exportCompromisoPDFold(Certification $certification, $miningUnit)
    {
        $mining_unit = MiningUnit::where('description', 'like', "%$miningUnit%")->first();
        $sufijo = self::getSufijo($miningUnit);

        $pdf = \PDF::loadView('admin.certifications.pdfs.compromiso_old', compact(['certification', 'mining_unit']));
        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'compromisos_' . $certification->user->dni . '_' . $certification->event->id . '_' . $sufijo . '.pdf';
        // return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }


    static function exportGroupAssistPDF(Event $event)
    {
        $certifications = $event->certifications()->where('score', '>', 0)->where('evaluation_type', '=', 'certification')->where('assist_user', 'S')->with(['user.file' => function ($q) {
            $q->where('category', 'firmas');
        }])->get();

        $event->load(['instructor', 'responsable', 'exam.course']);

        $pdf = \PDF::loadView('admin.certifications.pdfs.assists', compact(['event', 'certifications']));
        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'asistencias_' . $event->id . '.pdf';
        // return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportGroupAssistAtacochaPDF(Event $event)
    {
        $certifications = $event->certifications()->where('score', '>', 0)->where('evaluation_type', '=', 'certification')->where('assist_user', 'S')->with(['user.file' => function ($q) {
            $q->where('category', 'firmas');
        }])->get();

        $event->load(['instructor', 'responsable', 'exam.course']);

        $pdf = \PDF::loadView('admin.certifications.pdfs.assists_atacocha', compact(['event', 'certifications']));
        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'asistencias_' . $event->id . '_Atacocha.pdf';
        // return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function exportGroupAssistPorvenirPDF(Event $event)
    {
        $certifications = $event->certifications()->where('score', '>', 0)->where('evaluation_type', '=', 'certification')->where('assist_user', 'S')->with(['user.file' => function ($q) {
            $q->where('category', 'firmas');
        }])->get();

        $event->load(['instructor', 'responsable', 'exam.course']);

        $pdf = \PDF::loadView('admin.certifications.pdfs.assists_porvenir', compact(['event', 'certifications']));
        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'asistencias_' . $event->id . '_El_Porvenir.pdf';
        // return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }



    static function exportIndividualAssistPDF(Certification $certification)
    {
        $event = $certification->event;
        $event->load(['instructor', 'responsable', 'exam.course']);
        $certification->load(['user.file' => function ($q) {
            $q->where('category', 'firmas');
        }]);

        $certifications = collect([$certification]);

        $pdf = \PDF::loadView('admin.certifications.pdfs.assists', compact(['event', 'certifications']));
        $pdf->setPaper('letter', 'portrait');

        $pdf_name = 'asistencias_' . $certification->user->dni . '_' . $certification->event->id . '.pdf';
        // return $pdf->stream($pdf_name);
        return $pdf->download($pdf_name);
    }

    static function getSufijo($miningUnit)
    {
        $sufijo = 'A';
        switch ($miningUnit) {
            case 'atacocha':
                $sufijo = 'A';
                break;
            case 'porvenir':
                $sufijo = 'P';
                break;
            case 'sinaycocha':
                $sufijo = 'S';
                break;
            case 'cerro':
                $sufijo = 'C';
                break;
        }
        return $sufijo;
    }
}
