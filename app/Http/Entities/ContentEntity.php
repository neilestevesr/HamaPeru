<?php
namespace App\Http\Entities;

use App\Models\ELearning;
use App\Models\Content;
use App\Http\Entities\FileEntity;

class ContentEntity
{
    static function store($request,ELearning $elearning)
    {
        $content = $elearning->contents()->create($request->all());
        if($request->has('files') || $request->has('video')){
            $result = self::storeMultimedia($request, $content);
        }
        return $content;
    }

    static function update($request,Content $content)
    {   
        $content->update($request->all());
        
        if($content->wasChanged('type') || ($request->has('video') && $content->type == 'Video'))
        {
            foreach ($content->files as $file) {
                FileEntity::delete($file);
            }
        }

        if( (in_array($content->type, ['Imagen','Archivo']) && $request->has('files')) || 
            ($content->type == 'Video' && $request->has('video')) ){
            $result = self::storeMultimedia($request, $content);
        }
        return $content;
    }

    static function destroy(Content $content)
    {
        $files = $content->files()->get();
        if($files->isNotEmpty()){
            foreach ($files as $key => $file) {
                FileEntity::delete($file);
            }
        }

        return $content->delete();
    }

    static function getSections()
    {
        $sections = collect(config('parameters.content_sections'));
        return $sections;
    }

    static function getTypes()
    {
        $types = collect(config('parameters.content_types'));
        return $types;
    }
    
    static function storeMultimedia($request, Content $content)
    {
        $image_mimes = ['image/png','image/jpeg'];
        $video_mimes = ['video/mp4'];
        $category = 'contenidos';
        $file_type = 'videos';
        $result = [];

        if($content->type == 'Video' && $request->has('video')){
            $file = $request->file('video');
            return FileEntity::store($content, $file, $file_type, $category);
        }

        if( in_array($content->type, ['Imagen','Archivo']) && $request->has('files') ){
            foreach ($request->file('files') as $key => $file) {
                if( in_array($file->getMimeType(), $image_mimes) ) $file_type = 'imagenes';
                else if( in_array($file->getMimeType(), $video_mimes) ) $file_type = 'videos';
                else $file_type = 'archivos';
                
                if ($content->type == 'Imagen'){
                    if ($file_type == 'imagenes'){
                        $result []= FileEntity::store($content, $file, $file_type, $category);
                    }
                    else{
                        continue;
                    }
                }
                else{
                    $result []= FileEntity::store($content, $file, $file_type, $category);
                }
            }
        }
        return $result;
    }
}
