<?php
namespace App\Http\Entities;

use App\Models\Exam;
use App\Models\DynamicQuestion;
use App\Http\Entities\FileEntity;

class DynamicQuestionEntity
{
    static function store($request,Exam $dynamic_exam)
    {
        $dynamic_question = $dynamic_exam->dynamicQuestions()->create($request->all());
        return $dynamic_question;
    }

    static function update($request,DynamicQuestion $dynamic_question)
    {
        $dynamic_question->update($request->all());
        return $dynamic_question;
    }

    static function destroy(DynamicQuestion $dynamic_question)
    {
        $dynamic_question->dynamicAlternatives()->delete();
        return $dynamic_question->delete();
    }

    static function storeMultimedia($request,DynamicQuestion $dynamic_question)
    {
        $imagemimes = ['image/png','image/jpeg'];
        $videomimes = ['video/mp4'];
        $category = 'preguntas_dinamicas';
        $file_type = '';
        $result = [];

        foreach ($request->file('files') as $key => $file) {
            if( in_array($file->getMimeType() ,$imagemimes) ){
                $file_type = 'imagenes';
            }else {
                $file_type = 'videos';
            }

            $result []= FileEntity::store($dynamic_question, $file, $file_type, $category);
        }

        return $result[0];
    }

}
