<?php
namespace App\Http\Entities;

use App\Models\Survey;
use App\Models\UserSurvey;
use App\Http\Entities\Utils;

class SurveyEntity
{
    static function all()
    {
        return Survey::all();
    }

    static function allWithRelationships()
    {
        return Survey::with(['groups.statements.options'])->get();
    }

    static function store($request)
    {
        $data = Utils::NormalizeInputData($request->all());
        $survey = Survey::create($data);

        return $survey;
    }

    static function update($request,Survey $survey)
    {
        $data = Utils::NormalizeInputData($request->all());
        $survey->update($data);
        return $survey;
    }

    static function destroy(Survey $survey)
    {
        return $survey->delete();
    }

    static function getByFilters($request)
    {
        $query = UserSurvey::with(['user','survey','company','surveyAnswers'])->where('status','finished');


        if( $request->has('dni') && $request->get('dni') != '' ){
            $dni = $request->get('dni');
            $query->whereHas('user',function($q) use ($dni){
                $q->where('dni',$dni);
            });
        }

        if( $request->has('company_id') && $request->get('company_id') != '' ){
            $company_id = $request->get('company_id');
            $query->whereHas('company',function($q) use ($company_id){
                $q->where('id',$company_id);
            });
        }

        if( $request->has('survey_id') && $request->get('survey_id') != '' ){
            $survey_id = $request->get('survey_id');
            $query->whereHas('survey',function($q) use ($survey_id){
                $q->where('id',$survey_id);
            });
        }

        if( $request->has('date_from') && $request->get('date_from') != '' ){
            $dateFrom = $request->get('date_from');
            $query->where('date','>=',$dateFrom);

        }

        if( $request->has('date_to')  && $request->get('date_to') != '' ){
            $dateTo = $request->get('date_to');
            $query->where('date','<=',$dateTo);

        }


        if( !$request->hasAny(['dni','company_id','survey_id','date_from','date_to']) ){
            $query->orderBy('updated_at','desc')->limit(500);
        }

        $userSurveys = $query->get();
        return $userSurveys;
    }

}
