<?php
namespace App\Http\Entities;

use App\Models\Statement;
use App\Models\Option;
use App\Http\Entities\Utils;

class OptionEntity
{
    static function all()
    {
        return Option::all();
    }

    static function store($request,Statement $statement)
    {
        $option = $statement->options()->create($request->all());
        return $option;
    }

    static function update($request,Option $option)
    {
        $option->update($request->all());
        return $option;
    }

    static function destroy(Option $option)
    {
        return $option->delete();
    }

}
