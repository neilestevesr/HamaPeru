<?php
namespace App\Http\Entities;

use Illuminate\Support\Str;
use Storage;
use App\Models\File;
use App\Models\User;
use App\Models\Event;
use App\Models\Certification;

class FileEntity
{
    CONST IMAGE_DIRECTORY_S3 = 'imagenes';
    CONST FILE_DIRECTORY_S3 = 'archivos';
    CONST SIGNATURE_SUBDIRECTORY_S3 = 'firmas';
    CONST COURSE_SUBDIRECTORY_S3 = 'cursos';
    CONST DOCUMENT_SUBDIRECTORY_S3 = 'documentos';

    static function getFiles()
    {
        $files = File::where('file_type','archivos')
        ->whereIn('category',['certificados','asistencias','anexos', 'esp'])
        ->orderBy('updated_at','desc')
        ->take(300)
        ->get();
        return $files;
    }

     static function getFilesSpecific($dateFrom, $dateTo)
    {
        //$files = File::where('file_type',$file_type)->where('category',$category)->get();
        //return $files;
        $files = File::whereBetween('created_at', [$dateFrom, $dateTo])->whereIn('category',['certificados','asistencias','anexos', 'esp'])->get();
        return $files;
    }

    static function getByCertificationFilters($request, $file_type = NULL, $category = NULL)
    {
        $query = File::with(['fileable.user','fileable.event']);

        $query->where('fileable_type','App\Models\Certification');

        if($file_type) $query->where('file_type',$file_type);

        if($category) $query->where('category',$category);

        $query->orderBy('updated_at','desc');

        $files = $query->get();


        if( $request->has('dni') && $request->get('dni') != '' ){
            $dni = $request->get('dni');
            $files = $files->filter(function ($value, $key) use ($dni) {
                return $value->fileable->user->dni == $dni;
            });
        }

        if( $request->has('date_from') && $request->get('date_from') != '' ){
            $dateFrom = $request->get('date_from');
            $files = $files->filter(function ($value, $key) use ($dateFrom) {
                return $value->fileable->event->date >= $dateFrom;
            });

        }

        if( $request->has('date_to')  && $request->get('date_to') != '' ){
            $dateTo = $request->get('date_to');
            $files = $files->filter(function ($value, $key) use ($dateTo) {
                return $value->fileable->event->date <= $dateTo;
            });
        }

        return $files;
    }

    static function storeFile($files, $file_type = 'archivos')
    {
        $reject = [];
        $success = [];

        foreach ($files as $key => $file) {
            $original_name = $file->getClientOriginalName();
            $original_filename = pathinfo( $original_name , PATHINFO_FILENAME);

            try {
                $explode = Str::of($original_filename)->explode('_');
                $category = $explode[0];

                $model = NULL;
                if($category == 'asistencias') //formato: asistencias_codevento.pdf
                {
                    $model = Event::where('id',$explode[1])->first();
                }
                elseif (in_array($category, ['anexos', 'esp'])) //anexos_dni_codevento_p.pdf / esp_dni_codevento.pdf 
                {
                    $dni = $explode[1];
                    $event_id = $explode[2];

                    $model = Certification::where('event_id',$event_id)->whereHas('user',function($q) use($dni){
                        $q->where('dni', $dni);
                    })->first();

                }

                if($model){
                    $directory_s3 = $file_type . '/' . $category;
                    //$filename = self::getFilename($file,$directory_s3);
                    $filename = $original_name;

                    if( Storage::disk('s3')->exists($directory_s3 . '/' . $original_name) ){
                        Storage::disk('s3')->delete($directory_s3 . '/' . $original_name);
                        Storage::disk('s3')->putFileAs($directory_s3,$file,$filename);
                    }else {
                        $full_path = Storage::disk('s3')->putFileAs($directory_s3,$file,$filename);
                        $file_url = Storage::disk('s3')->url($full_path);

                        $fileModel = new File([
                            'file_path' => $full_path,
                            'file_url' => $file_url,
                            'file_type' => $file_type,
                            'category' => $category
                        ]);
                        $model->files()->save($fileModel);
                    }

                    array_push($success, $filename);
                }
                else{
                    array_push($reject, $original_name);
                }
            } catch (\Exception $e) {
                array_push($reject, $original_name);
            }
        }

        return [$success, $reject];
    }

    static function getFilename($file,$path)
    {
        $original_name = $file->getClientOriginalName();

        $original_filename = pathinfo($original_name, PATHINFO_FILENAME);
        $extension = pathinfo($original_name, PATHINFO_EXTENSION);

        $increment = 1;

        $filename = $original_filename;
        while ( Storage::disk('s3')->exists($path . '/' . $filename . '.' . $extension) ) {
            $filename = $original_filename . '(' . $increment++ . ')';
        }

        return $filename . '.' . $extension;
    }

    static function getDirectoryS3($type = 'archivos', $category = 'documentos')
    {
        $directory_s3_final = '';
        if($type == 'imagenes' )
        {
            if($category == 'firmas'){
                $directory_s3_final = self::IMAGE_DIRECTORY_S3 . '/' . self::SIGNATURE_SUBDIRECTORY_S3;
            }
            else if($category == 'cursos'){
                $directory_s3_final = self::IMAGE_DIRECTORY_S3 . '/' . self::COURSE_SUBDIRECTORY_S3;
            }
            else {
                $directory_s3_final = self::IMAGE_DIRECTORY_S3 . '/' . $category;
            }
        }
        else {
            $directory_s3_final = self::FILE_DIRECTORY_S3 . '/' . self::DOCUMENT_SUBDIRECTORY_S3;
        }

        return $directory_s3_final;
    }

    static function storeOld($model, $file, $file_type = 'archivos', $category = 'documentos')
    {
        $directory_s3 = self::getDirectoryS3($file_type,$category);
        $filename = (new \ReflectionClass($model))->getShortName() . '_' . rand(1000, 9999) . '_' . time() . '.' . $file->extension();
        $full_path = Storage::disk('s3')->putFileAs($directory_s3,$file,$filename);
        $file_url = Storage::disk('s3')->url($full_path);

        $file = new File([
        'file_path' => $full_path,
        'file_url' => $file_url,
        'file_type' => $file_type,
        'category' => $category
        ]);

        return $model->file()->save($file);
    }

    static function store($model, $file, $file_type = 'archivos', $category = 'documentos')
    {
        //$directory_s3 = self::getDirectoryS3($file_type,$category);
        $directory_s3 = $file_type . '/' . $category;
        $filename = self::getFilename($file,$directory_s3);

        $full_path = Storage::disk('s3')->putFileAs($directory_s3,$file,$filename);
        $file_url = Storage::disk('s3')->url($full_path);

        $file_object = new File([
            'file_path' => $full_path,
            'file_url' => $file_url,
            'file_type' => $file_type,
            'category' => $category
        ]);

        return $model->files()->save($file_object);
    }


    static function storeSignature(User $user, $imgBase64)
    {
        $directory_s3 = self::IMAGE_DIRECTORY_S3 . '/' . self::SIGNATURE_SUBDIRECTORY_S3;
        $image = str_replace('data:image/png;base64,', '', $imgBase64);
        $image = str_replace(' ', '+', $image);
        $image_name = $user->dni . '_' . time() . '.png';
        $full_path = $directory_s3 . '/' . $image_name;

        $result = Storage::disk('s3')->put($full_path, base64_decode($image));

        if($result){
            $files = $user->files()->where('category','firmas')->get();
            if($files->isNotEmpty()){
                foreach ($files as $key => $file) {
                    self::delete($file);
                }
            }

            $file_url = Storage::disk('s3')->url($full_path);
            $file = new File([
            'file_path' => $full_path,
            'file_url' => $file_url,
            'file_type' => 'imagenes',
            'category' => 'firmas'
            ]);

            $user->file()->save($file);
            return $user->update(['signature' => 'S']);
        }

        return False;
    }
    
    
     static function storeSignatureSecurity(User $user, $imgBase64,$event)
    {
         
        
        $directory_s3 = self::IMAGE_DIRECTORY_S3 . '/' . self::SIGNATURE_SUBDIRECTORY_S3;
        $image = str_replace('data:image/png;base64,', '', $imgBase64);
        $image = str_replace(' ', '+', $image);
        $image_name = $event.'_'.$user->dni . '.png';
        $full_path = $directory_s3 . '/' . $image_name;

        $result = Storage::disk('s3')->put($full_path, base64_decode($image));

           if($result){
            $files = $user->files()->where('category','firmas')->get();
            if($files->isNotEmpty()){
                foreach ($files as $key => $file) {
                    //self::delete($file);
                }
            } 

            $file_url = Storage::disk('s3')->url($full_path);
            $file = new File([
            
            
            'file_path' => $full_path,
            'file_url' => $file_url,
            'file_type' => 'imagenes',
            'category' => 'firmas'
            ]);
            
           
            $user->file()->save($file);
            return $user->update(['signature' => 'S']);
        }

        return False;
    }
    
    

    static function deleteById($id)
    {
        $file = File::find($id);
        return self::delete($file);
    }

    static function delete(File $file)
    {
        if($file){
            if(Storage::disk('s3')->exists($file->file_path)){
                Storage::disk('s3')->delete($file->file_path);
            }
            return $file->delete();
        }
        else {
            return False;
        }
    }

}
