<?php
namespace App\Http\Entities;

use App\Models\Group;
use App\Models\Statement;

class StatementEntity
{
    static function all()
    {
        return Statement::all();
    }

    static function store($request,Group $group)
    {
        $statement = $group->statements()->create($request->all());
        return $statement;
    }

    static function update($request,Statement $statement)
    {
        $statement->update($request->all());
        return $statement;
    }

    static function destroy(Statement $statement)
    {
        $statement->options()->delete();
        return $statement->delete();
    }

}
