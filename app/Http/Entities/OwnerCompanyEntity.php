<?php
namespace App\Http\Entities;

use App\Models\OwnerCompany;

class OwnerCompanyEntity
{
    static function all()
    {
        return OwnerCompany::all();
    }

    static function allWithRelationships()
    {
        return OwnerCompany::with(['exams'])->get();
    }

    static function store($request)
    {
        return OwnerCompany::create($request->all());
    }

    static function update($request,OwnerCompany $ownerCompany)
    {
        return $ownerCompany->update($request->all());
    }

    static function destroy(OwnerCompany $ownerCompany)
    {
        return $ownerCompany->delete();
    }

}
