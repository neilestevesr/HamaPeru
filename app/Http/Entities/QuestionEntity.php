<?php
namespace App\Http\Entities;

use App\Models\Exam;
use App\Models\Question;
use App\Http\Entities\Utils;

class QuestionEntity
{
    static function all()
    {
        return Question::all();
    }

    static function store($request,Exam $exam)
    {
        $question = $exam->questions()->create($request->all());
        return $question;
    }

    static function update($request,Question $question)
    {
        $question->update($request->all());
        return $question;
    }

    static function destroy(Question $question)
    {
        return $question->delete();
    }

}
