<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\VirtualEvaluation\EvaluationAnswerRequest;
use App\Models\Certification;
use App\Models\Evaluation;
use App\Http\Entities\VirtualEvaluationEntity;
use App\Http\Entities\VirtualSurveyEntity;

class VirtualEvaluationControllerOld extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        $certifications = $user->certifications()->with(['user','event.exam.course.file','event.exam.ownerCompany'])->whereHas('event',function($q){
            $q->where('active', 'S');
        })->get()->sortByDesc('event.date');

        return view('classroom.virtual_evaluations.index',compact(['user','certifications']));
    }

    public function start(Certification $certification)
    {
        $certification->load(['event.exam.questions','user']);
        $this->authorize('canStartEvaluation', $certification);

        #valida si tiene que dar encuestas
        $survey = VirtualSurveyEntity::redirectToSurvey($certification);
        if($survey) return redirect()->route('classroom.virtual_surveys.start',$survey->id);

        if($certification->evaluation_time == NULL){
            $result = VirtualEvaluationEntity::generateEvaluation($certification);
            if(!$result) return redirect()->route('classroom.virtual_evaluations.index');
        }

        return self::redirectFirstEvaluationNoAnswer($certification);

    }

    public function redirectFirstEvaluationNoAnswer(Certification $certification)
    {
        $question_order = VirtualEvaluationEntity::getFirstEvaluationNoAnswer($certification);
        return redirect()->route('classroom.virtual_evaluations.show_question',[$certification->id,$question_order]);
    }

    public function showQuestion(Certification $certification, $question_order)
    {
        $certification->load(['event.exam.questions','user']);
        $this->authorize('canStartEvaluation', $certification);
        $this->authorize('canAnswerEvaluation', $certification);

        $count = VirtualEvaluationEntity::countQuestions($certification);
        if(1 <= $question_order && $question_order <= $count){
            $evaluation = VirtualEvaluationEntity::getQuestion($certification,$question_order);
            $certification->load(['evaluations']);

            $time_left = $certification->time_left;

            return view('classroom.virtual_evaluations.evaluation',compact(['certification','evaluation','time_left']) );
        }
        else {
            return self::redirectFirstEvaluationNoAnswer($certification);
        }
    }

    public function answerQuestion(EvaluationAnswerRequest $request, Certification $certification, $question_order)
    {
        $this->authorize('canStartEvaluation', $certification);
        $this->authorize('canAnswerEvaluation', $certification);

        $count = VirtualEvaluationEntity::countQuestions($certification);
        if(1 <= $question_order && $question_order <= $count){
            $result = VirtualEvaluationEntity::answerQuestion($request,$certification,$question_order);

            if($question_order == $count){
                $result = VirtualEvaluationEntity::concludeEvaluation($certification);
                return redirect()->route('classroom.virtual_evaluations.index');
            }
            else {
                $question_order++;
                return redirect()->route('classroom.virtual_evaluations.show_question',[$certification->id,$question_order]);
            }
        }
        else {
            return self::redirectFirstEvaluationNoAnswer($certification);
        }
    }

}
