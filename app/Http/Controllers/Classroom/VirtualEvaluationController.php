<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\VirtualEvaluation\EvaluationAnswerRequest;
use App\Models\Certification;
use App\Models\Evaluation;
use App\Http\Entities\VirtualEvaluationEntity;
use App\Http\Entities\VirtualSurveyEntity;

class VirtualEvaluationController extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        $certifications = $user->certifications()->with(['user','event.exam.course.file','event.exam.ownerCompany'])->where('evaluation_type','!=','test')
        ->whereHas('event',function($q){
            $q->where('active', 'S')->where('flg_test_exam','N');
        })->get();
        
        $certifications_test = $user->certifications()->with(['user','event.testExam.course.file','event.testExam.ownerCompany'])->where('evaluation_type','test')
        ->whereHas('event',function($q){
            $q->where('active', 'S')->where('flg_test_exam','S')->where('score',NULL);
        })->get();
        #dd($certifications_test);
        
        foreach ($certifications_test as $certification_test)
            $certifications->push($certification_test);
        
        $certifications = $certifications->sortByDesc('event.date');

        return view('classroom.virtual_evaluations.index',compact(['user','certifications']));
    }

    public function start(Certification $certification)
    {
        $certification->load(['event']);
        if($certification->event->flg_test_exam == 'S' && $certification->event->testExam != NULL ){
            $certification->load(['event.testExam.dynamicQuestions','user']);
            $this->authorize('canStartTestEvaluation', $certification);
            
        }
        else{
            $certification->load(['event.exam.dynamicQuestions','user']);
            $this->authorize('canStartEvaluation', $certification);
        }

        #valida si tiene que dar encuestas
        $survey = VirtualSurveyEntity::redirectToSurvey($certification);
        if($survey) return redirect()->route('classroom.virtual_surveys.start', [$survey->id,$certification->event_id]);

        if($certification->evaluation_time == NULL){
            $result = VirtualEvaluationEntity::generateEvaluation($certification);
            if(!$result) return redirect()->route('classroom.virtual_evaluations.index');
        }

        return self::redirectFirstEvaluationNoAnswer($certification);

    }

    public function redirectFirstEvaluationNoAnswer(Certification $certification)
    {
        $question_order = VirtualEvaluationEntity::getFirstEvaluationNoAnswer($certification);
        return redirect()->route('classroom.virtual_evaluations.show_question',[$certification->id,$question_order]);
    }

    public function showQuestion(Certification $certification, $question_order)
    {
        $certification->load(['event.exam.dynamicQuestions','user']);
        $this->authorize('canStartEvaluation', $certification);
        $this->authorize('canAnswerEvaluation', $certification);

        $count = VirtualEvaluationEntity::countQuestions($certification);
        if(1 <= $question_order && $question_order <= $count){
            $evaluation = VirtualEvaluationEntity::getQuestion($certification,$question_order);
            //$certification->load(['evaluations']);

            $time_left = $certification->time_left;
            //dd($certification);               
               
            return view('classroom.virtual_evaluations.evaluation',compact(['certification','evaluation','time_left']) );
        }
        else {
            return self::redirectFirstEvaluationNoAnswer($certification);
        }
    }

    public function answerQuestion(EvaluationAnswerRequest $request, Certification $certification, $question_order)
    {
        $this->authorize('canStartEvaluation', $certification);
        $this->authorize('canAnswerEvaluation', $certification);

        $count = VirtualEvaluationEntity::countQuestions($certification);
        if(1 <= $question_order && $question_order <= $count){
            $result = VirtualEvaluationEntity::answerQuestion($request,$certification,$question_order);

            if($question_order == $count){
                $result = VirtualEvaluationEntity::concludeEvaluation($certification);
                return redirect()->route('classroom.virtual_evaluations.index');
            }
            else {
                $question_order++;
                return redirect()->route('classroom.virtual_evaluations.show_question',[$certification->id,$question_order]);
            }
        }
        else {
            return self::redirectFirstEvaluationNoAnswer($certification);
        }
    }

}
