<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Certification;
use App\Models\Event;
use App\Http\Entities\VirtualSurveyEntity;
use App\Http\Entities\MiningUnitEntity;

class CourseController extends Controller
{

    public function index()
    {
        $user = \Auth::user();
        $certifications = $user->certifications()->with(['event.exam.course'])->get()->sortByDesc('event.date');

        return view('classroom.courses.index',compact(['user','certifications']));
    }

  
     public function liveSecurityList()
    {
        $user = \Auth::user();
        $events = Event::where('security_id','=',$user->id)->get();
        $events_por = Event::where('security_por_id','=',$user->id)->get();
        $event_list = Event::where('active','=','S')->where('date','>=','2023-12-01')->orderBy('id', 'DESC')->get();
        //$event_list = Event::where('active','=','S')->where('date','>=','2022-04-18')->whereYear('date', '=', date('Y'))->orderBy('id', 'DESC')->get();

        
        //$event_list = Event::where('active','=','S')->where('date','>=','2022-04-18')->whereMonth('date','=',date('m'))->whereYear('date', '=', date('Y'))->orderBy('id', 'DESC')->get();
        $mining_units = MiningUnitEntity::all();

        return view('classroom.courses.live_security_list',compact(['events','events_por','mining_units','user','event_list']));
    }
    
    public function liveList()
    {
        $user = \Auth::user();
        $certifications = $user->certifications()->with(['event.exam.course'])->whereHas('event' , function($q){
            $now = \Carbon\Carbon::now('America/Lima')->isoFormat('YYYY-MM-DD');
            $q->where('date','=',$now);
        })->get()->sortByDesc('event.date');

        return view('classroom.courses.live_list',compact(['certifications']));
    }

    public function live(Certification $certification)
    {
        return view('classroom.courses.live',compact('certification'));
    }

    public function retrieval()
    {
        return view('classroom.courses.retrieval');
    }
}
