<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SurveyAnswer;
use App\Models\UserSurvey;
use App\Models\Survey;
use App\Models\User;
use App\Http\Entities\SurveyEntity;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = \Auth::user();
        $id = $user->id;
        //dd($user->id);
        $query = UserSurvey::with(['user','survey','company','surveyAnswers'])->where('status','finished')->where('user_id',$id)->where('survey_id','4');
        
         $userSurveys = $query->get();
        //dd($userSurveys);
        $max = 0;
        foreach ($userSurveys as $key => $userSurvey) {
            $count = $userSurvey->surveyAnswers()->count();
            if($count > $max) $max = $count;
        }
       // dd($userSurvey);
        return view('classroom.profile.index',compact('user','userSurveys','max'));
    }
   
    public function userProfile(Request $request)
    {
         
         
         //return $request;
         //dd($request->all());
         
         $user = \Auth::user();
         
         
         $id = $user->id;
         //return view('classroom.profile.index',compact('user','userSurveys','max'));
         //dd($request->get('profile'));
         $user->profile_user = $request->get('profile');
         $user->save();
         $query = UserSurvey::with(['user','survey','company','surveyAnswers'])->where('status','finished')->where('user_id',$id)->where('survey_id','4');
        
         $userSurveys = $query->get();
        //dd($userSurveys);
        $max = 0;
        foreach ($userSurveys as $key => $userSurvey) {
            $count = $userSurvey->surveyAnswers()->count();
            if($count > $max) $max = $count;
        }
       // dd($userSurvey);
        return view('classroom.profile.index',compact('user','userSurveys','max'));
    }

}
