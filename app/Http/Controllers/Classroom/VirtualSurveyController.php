<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\VirtualSurvey\SurveyAnswerRequest;
use App\Models\SurveyAnswer;
use App\Models\UserSurvey;
use App\Models\Survey;
use App\Models\Event;
use App\Http\Entities\VirtualSurveyEntity;


class VirtualSurveyController extends Controller
{
    public function evaluationSurveysIndex()
    {
        $surveys = VirtualSurveyEntity::getEvaluationSurvey();

        return view('classroom.virtual_surveys.index',compact(['surveys']));
    }

    public function courseSurveysIndex()
    {
        $surveys = VirtualSurveyEntity::getCourseSurvey();
        return view('classroom.virtual_surveys.index',compact(['surveys']));
    }

      public function profileSurveysIndex()
    {
        $surveys = VirtualSurveyEntity::getUserProfile();
        $survey_id = $surveys->first()->id;
        //dd($surveys->first()->id);
        return redirect()->route('classroom.virtual_surveys.start_profile', $survey_id);
    } 
     
    public function start(Survey $survey ,Event $event)
    {
        //dd('el Gay no puede sin su macho',$survey,$event);
        $survey->load(['groups.statements.options']);
        //dd($survey->where('destino_to','<>','user_profile'));
        $userSurvey = VirtualSurveyEntity::generateUserSurvey($survey,$event);
        #dd($userSurvey);

        if($userSurvey){
            return redirect()->route('classroom.virtual_surveys.show_question',[$userSurvey->id,1]);
        }

        return redirect()->back();
    }
    
    public function startProfile(Survey $survey)
    {
       
        $survey->load(['groups.statements.options']);
        /*$survey_user = survey->user_survey()->where('user',$user_id)
        if ($survey_user){
        showQuestion
        }*/
        
        
        $userSurvey = VirtualSurveyEntity::generateUserSurveyProfile($survey);

        //dd($userSurvey);

        if($userSurvey){
            return redirect()->route('classroom.virtual_surveys.show_question',[$userSurvey->id,1]);            
        }
        
        return redirect()->back();
    }
    
    
    public function showQuestion(UserSurvey $userSurvey, $question_order)
    {
        $userSurvey->load(['surveyAnswers']);
        // $this->authorize('canStartEvaluation', $certification);
        // $this->authorize('canAnswerEvaluation', $certification);
        
         //$user = \Auth::user();
        // dd($user);
        //if($user->profile_survey == 'S')
        //{
         //  return view('classroom.virtual_surveys.finish',compact(['userSurvey']) );
        //}

        $count = VirtualSurveyEntity::countQuestions($userSurvey);
        
        //dd($userSurvey);

        if(1 <= $question_order && $question_order <= $count){
            $surveyAnswer = VirtualSurveyEntity::getSurveyAnswer($userSurvey,$question_order);
            // $userSurvey->load(['evaluations']);

            // $time_left = $certification->time_left;

            return view('classroom.virtual_surveys.survey',compact(['userSurvey','surveyAnswer']) );
        }
        else {
            return redirect()->back();
            // return self::redirectFirstEvaluationNoAnswer($certification);
        }
    }

    public function answerQuestion(SurveyAnswerRequest $request, UserSurvey $userSurvey, $question_order)
    {
        // $this->authorize('canStartEvaluation', $certification);
        // $this->authorize('canAnswerEvaluation', $certification);
         
        $user = \Auth::user();
        $count = VirtualSurveyEntity::countQuestions($userSurvey);
        if(1 <= $question_order && $question_order <= $count){
            $result = VirtualSurveyEntity::answerQuestion($request,$userSurvey,$question_order);

            if($question_order == $count){
                $result = VirtualSurveyEntity::concludeSurvey($userSurvey);
                
                //dd($request->get('profile'));
                $user->profile_user = $request->get('profile');
                $user->save();
                
                return view('classroom.virtual_surveys.finish',compact(['userSurvey']) );
            }
            else {
                $question_order++;
                return redirect()->route('classroom.virtual_surveys.show_question',[$userSurvey->id,$question_order]);
            }
        }
        else {
            return redirect()->back();
            // return self::redirectFirstEvaluationNoAnswer($certification);
        }
    }
}

