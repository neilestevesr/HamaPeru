<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Entities\FileEntity;
use App\Models\Event;
use App\Http\Entities\MiningUnitEntity;

class DigitalSignatureController extends Controller
{
    public function index()
    {
        return view('classroom.digital_signatures.index');
    }


      public function indexSecurity(Event $event)
    {
          
        $events = $event->id;   
        return view('classroom.digital_signatures.index_security',compact('events'));
    }

     public function indexSecurityPor(Event $event)
    {
          
        $events = $event->id;   
        return view('classroom.digital_signatures.index_security_por',compact('events'));
    }
    
    public function create()
    {
        return view('classroom.digital_signatures.create');
    }

     
     public function createSecurity(Event $event)
    {
        $events = $event->id;
        return view('classroom.digital_signatures.create_security',compact('events'));
    }
    
     public function createSecurityPor(Event $event)
    {
        $events = $event->id;
        return view('classroom.digital_signatures.create_security_por',compact('events'));
    }
    
    

    public function store(Request $request)
    {
         $user = \Auth::user();
         $result = FileEntity::storeSignature($user,$request->get('imgBase64'));

         if($result){
             return response()->json([
                 'status' => 200,
                 'success' => 'Se generó firma digital.',
             ],200);
         }
         else {
             return response()->json([
                 'status' => 400,
                 'success' => 'Hubo un error!',
             ],400);
         }
    }
    
    
        public function storeSecurity(Request $request, Event $event)
     {
          
         $evento = $request->get('event');
         $user = \Auth::user();
         $result = FileEntity::storeSignatureSecurity($user,$request->get('imgBase64'),$evento);
         
         $mining_units = MiningUnitEntity::all();
  
         if($result){
         
         
             if($user->miningUnits->count() > 0)
          {
            foreach($user->miningUnits as $key => $mining_unit)
            {
               
                if($mining_unit->description == 'Atacocha')
                {
                    $event->update(['flg_security' => 'S']);
                    $event->update(['security_id' => $user->id]); 
                }
                
            }
          }
         
             //$event->update(['flg_security' => 'S']);
             return response()->json([
                 'status' => 200,
                 'success' => 'Se generó firma digital.',
             ],200);
         }
         else {
             return response()->json([
                 'status' => 400,
                 'success' => 'Hubo un error!',
             ],400);
         }
    }

    public function storeSecurityPor(Request $request, Event $event)
     {
          
         $evento = $request->get('event');
         $user = \Auth::user();
         $result = FileEntity::storeSignatureSecurity($user,$request->get('imgBase64'),$evento);
         
         $mining_units = MiningUnitEntity::all();
  
         if($result){
         
         
             if($user->miningUnits->count() > 0)
          {
            foreach($user->miningUnits as $key => $mining_unit)
            {
               
                if($mining_unit->description == 'El Porvenir')
                {
                    $event->update(['flg_security_por' => 'S']);
                    $event->update(['security_por_id' => $user->id]); 
                }
                
            }
          }
         
             //$event->update(['flg_security' => 'S']);
             return response()->json([
                 'status' => 200,
                 'success' => 'Se generó firma digital.',
             ],200);
         }
         else {
             return response()->json([
                 'status' => 400,
                 'success' => 'Hubo un error!',
             ],400);
         }
    }






    
}
