<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ELearning;
use App\Http\Entities\FileEntity;
use App\Http\Entities\ELearningEntity;
use App\Models\File;
use Storage;


class ELearningController extends Controller
{
    public function index()
    {
        $user = \Auth::user();

        $elearnings = ELearning::with(['contents','events.certifications.user'])->where('active','S')
        ->whereHas('events',function($q){
            $q->where('active', 'S')->where('flg_test_exam','N');
        })
        ->whereHas('events.certifications',function($q){
            $q->where('evaluation_type','!=','test');
        })
        ->whereHas('events.certifications.user',function($q) use ($user){
            $q->where('user_id',$user->id);
        })
        ->get();
    
        return view('classroom.elearnings.index',compact('elearnings'));
    }

    public function cazador()
    {
        return view('classroom.elearnings.unity.cazador');
    }

    public function cazadorProtMaquinas()
    {
        return view('classroom.elearnings.unity.cazador_prot_maquinas');
    }

    public function cazadorPrevCaidas()
    {
        return view('classroom.elearnings.unity.cazador_prev_caidas');
    }

    public function downloadFile(File $file)
    {
        if(Storage::disk('s3')->exists($file->file_path) ){
            return Storage::disk('s3')->download($file->file_path);
        }

        return redirect()->back()->withErrors(['message'=>'No se encontró documento.']);
    }
}
