<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        // return view('classroom.home');
        return redirect()->route('classroom.profile.index');
    }
    
    public function profile_survey()
    {
        return redirect()->route('classroom.virtual_surveys.profile_index');
      
    }
    
    
}
