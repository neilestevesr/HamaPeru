<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\Event\EventAssistRequest;
use App\Models\Certification;
use App\Models\Event;
use App\Models\UserSurvey;
use App\Http\Entities\VirtualEvaluationEntity;
use App\Http\Entities\EventEntity;

class EventController extends Controller
{
    public function index()
    {
        $events = \Auth::user()->events()->orderBy('date','desc')->get();
        return view('classroom.events.index',compact('events'));
    }

    public function show(Event $event)
    {
        $certifications = $event->certifications->load(['user']);
        
             //$id = $certification->user->id;
             $query = UserSurvey::with(['user','survey','company','surveyAnswers'])->where('status','finished')->where('survey_id','4');
                $userSurveys = $query->get();
            //dd($userSurveys);
        $max = 0;
          foreach ($userSurveys as $key => $userSurvey) 
            {
            $count = $userSurvey->surveyAnswers()->count();
            if($count > $max) $max = $count;
            }
          return view('classroom.events.show', compact(['event','certifications','userSurveys','max']));
         

       
    }
    
     public function showSecurity(Event $event)
    {
         $user = \Auth::user();
      $certifications = $event->certifications->load(['user'])->where('score','>=','14')->where('evaluation_type','=','certification');
          
             //$id = $certification->user->id;
             $query = UserSurvey::with(['user','survey','company','surveyAnswers'])->where('status','finished')->where('survey_id','4');
                $userSurveys = $query->get();
            //dd($userSurveys);
        $max = 0;
          foreach ($userSurveys as $key => $userSurvey) 
            {
            $count = $userSurvey->surveyAnswers()->count();
            if($count > $max) $max = $count;
            }
          return view('classroom.courses.show', compact(['event','certifications','userSurveys','user','max']));
         

       
    }

    public function assist(EventAssistRequest $request, Event $event)
    {
        $this->authorize('canAssistsSave', $event);

        $result = EventEntity::updateAssists($event, $request->get('assists') );

        return redirect()->route('classroom.events.show',$event->id);
    }
    
     public function assistSecurity(EventAssistRequest $request, Event $event)
    {
        $this->authorize('canAssistsSave', $event);

        $result = EventEntity::updateAssists($event, $request->get('assists') );

        return redirect()->route('classroom.events.show',$event->id);
    }

    public function conclude(Event $event)
    {
        $result = VirtualEvaluationEntity::concludeEvaluations($event);
        return redirect()->route('classroom.events.show',$event->id);
    }
    
    public function highcharts(Event $event)
    {
           //dd($event->id);
           $certifications = $event->certifications->load(['user']);
        
             //$id = $certification->user->id;
             $query = UserSurvey::with(['user','survey','company','surveyAnswers'])->where('status','finished')->where('survey_id','4');
                $userSurveys = $query->get();
            //dd($userSurveys);
        $max = 0;
          foreach ($userSurveys as $key => $userSurvey) 
            {
            $count = $userSurvey->surveyAnswers()->count();
            if($count > $max) $max = $count;
            }
        //$events = \Auth::user()->events()->orderBy('date','desc')->get();
        return view('classroom.events.highcharts', compact(['event','certifications','userSurveys','max']));
    }
    
    

}
