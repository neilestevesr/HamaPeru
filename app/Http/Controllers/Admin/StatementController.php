<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Statement\StatementStoreRequest;
use App\Http\Requests\Statement\StatementUpdateRequest;
use App\Http\Entities\StatementEntity;
use App\Models\Group;
use App\Models\Statement;

class StatementController extends Controller
{
    public function create(Group $group)
    {
        $statement = new Statement();
        return view('admin.statements.create',compact('group','statement'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StatementStoreRequest $request,Group $group)
    {
        $statement = StatementEntity::store($request,$group);
        if($statement){
            return redirect()->route('statements.show',$statement->id)
            ->with(['status' => 'success','action' => 'created','statement_message' => 'Se registró correctamente.']);
        }
        return redirect()->route('groups.show',$group->id)->withErrors(['statement_message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Statement  $statement
     * @return \Illuminate\Http\Response
     */
    public function show(Statement $statement)
    {
        return view('admin.statements.show',compact('statement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Statement  $statement
     * @return \Illuminate\Http\Response
     */
    public function edit(Statement $statement)
    {
        return view('admin.statements.edit',compact('statement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Statement  $statement
     * @return \Illuminate\Http\Response
     */
    public function update(StatementUpdateRequest $request, Statement $statement)
    {
        StatementEntity::update($request,$statement);

        return redirect()->route('statements.show',$statement->id)
        ->with(['status' => 'success','action' => 'updated','statement_message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Statement  $statement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Statement $statement)
    {
        $this->authorize('canDelete', $statement);

        $group_id = $statement->group_id;
        $result =  StatementEntity::destroy($statement);
        return redirect()->route('groups.show',$group_id)
        ->with(['status' => 'success','action' => 'deleted','statement_message' => 'Se eliminó el registro correctamente.']);
    }
}
