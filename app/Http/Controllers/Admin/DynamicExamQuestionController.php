<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Exam;
use App\Models\DynamicQuestion;
use App\Http\Requests\DynamicQuestion\DynamicQuestionStoreRequest;
use App\Http\Requests\DynamicQuestion\DynamicQuestionUpdateRequest;
use App\Http\Requests\DynamicQuestion\StoreMultimediaRequest;
use App\Http\Entities\DynamicQuestionEntity;

class DynamicExamQuestionController extends Controller
{
    public function create(Exam $dynamic_exam)
    {
        $dynamic_question = new DynamicQuestion();
        return view('admin.dynamic_questions.create',compact('dynamic_exam','dynamic_question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DynamicQuestionStoreRequest $request,Exam $dynamic_exam)
    {
        $dynamic_question = DynamicQuestionEntity::store($request,$dynamic_exam);

        if($dynamic_question){
            return redirect()->route('dynamic_exams.dynamic_questions.show',$dynamic_question->id)
            ->with(['status' => 'success','action' => 'created','statement_message' => 'Se registró correctamente.']);
        }
        return redirect()->route('dynamic_exams.show',$dynamic_exam->id)->withErrors(['statement_message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(DynamicQuestion $dynamic_question)
    {
       return view('admin.dynamic_questions.show',compact('dynamic_question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(DynamicQuestion $dynamic_question)
    {
        return view('admin.dynamic_questions.edit',compact('dynamic_question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DynamicQuestionUpdateRequest $request, DynamicQuestion $dynamic_question)
    {
        DynamicQuestionEntity::update($request,$dynamic_question);

        return redirect()->route('dynamic_exams.dynamic_questions.show',$dynamic_question->id)
        ->with(['status' => 'success','action' => 'updated','question_message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DynamicQuestion $dynamic_question)
    {
        $exam_id = $dynamic_question->exam_id;
        $result =  DynamicQuestionEntity::destroy($dynamic_question);
        return redirect()->route('dynamic_exams.show',$exam_id)
        ->with(['status' => 'success','action' => 'deleted','question_message' => 'Se eliminó el registro correctamente.']);
    }

    public function storeMultimedia(StoreMultimediaRequest $request,DynamicQuestion $dynamic_question)
    {
        $result = DynamicQuestionEntity::storeMultimedia($request,$dynamic_question);

        if($result){
            return redirect()->route('dynamic_exams.dynamic_questions.show',$dynamic_question->id)
            ->with(['status' => 'success','action' => 'created','statement_message' => 'Se registró correctamente.']);
        }
        return redirect()->back()->withErrors(['statement_message'=>'No se registró.']);
    }
}
