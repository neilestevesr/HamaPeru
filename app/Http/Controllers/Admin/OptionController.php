<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Option\OptionStoreRequest;
use App\Http\Requests\Option\OptionUpdateRequest;
use App\Http\Entities\OptionEntity;
use App\Models\Statement;
use App\Models\Option;

class OptionController extends Controller
{
    public function create(Statement $statement)
    {
        $this->authorize('createOption', $statement);

        $option = new Option();
        return view('admin.options.create',compact('statement','option'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OptionStoreRequest $request, Statement $statement)
    {
        $this->authorize('createOption', $statement);
        
        $option = OptionEntity::store($request,$statement);
        if($option){
            return redirect()->route('statements.show',$option->statement_id)
            ->with(['status' => 'success','action' => 'created','option_message' => 'Se registró correctamente.']);
        }
        return redirect()->route('statements.show',$statement->id)->withErrors(['option_message'=>'No se registró.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function edit(Option $option)
    {
        return view('admin.options.edit',compact('option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function update(OptionUpdateRequest $request, Option $option)
    {
        OptionEntity::update($request,$option);

        return redirect()->route('statements.show',$option->statement_id)
        ->with(['status' => 'success','action' => 'updated','option_message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function destroy(Option $option)
    {
        $statement_id = $option->statement_id;
        $result =  OptionEntity::destroy($option);
        return redirect()->route('statements.show',$statement_id)
        ->with(['status' => 'success','action' => 'deleted','option_message' => 'Se eliminó el registro correctamente.']);
    }
}
