<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Group\GroupStoreRequest;
use App\Http\Requests\Group\GroupUpdateRequest;
use App\Http\Entities\GroupEntity;
use App\Models\Survey;
use App\Models\Group;

class GroupController extends Controller
{

    public function create(Survey $survey)
    {
        $group = new Group();
        return view('admin.groups.create',compact('survey','group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupStoreRequest $request,Survey $survey)
    {
        $group = GroupEntity::store($request,$survey);
        if($group){
            return redirect()->route('groups.show',$group->id)
            ->with(['status' => 'success','action' => 'created','group_message' => 'Se registró correctamente.']);
        }
        return redirect()->route('surveys.show',$survey->id)->withErrors(['group_message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        return view('admin.groups.show',compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('admin.groups.edit',compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(GroupUpdateRequest $request, Group $group)
    {
        GroupEntity::update($request,$group);

        return redirect()->route('groups.show',$group->id)
        ->with(['status' => 'success','action' => 'updated','group_message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $this->authorize('canDelete', $group);

        $survey_id = $group->survey_id;
        $result =  GroupEntity::destroy($group);
        return redirect()->route('surveys.show',$survey_id)
        ->with(['status' => 'success','action' => 'deleted','group_message' => 'Se eliminó el registro correctamente.']);
    }
}
