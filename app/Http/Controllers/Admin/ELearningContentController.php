<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ELearning;
use App\Models\Content;
use App\Http\Requests\Content\ContentStoreRequest;
use App\Http\Requests\Content\ContentUpdateRequest;
use App\Http\Entities\ContentEntity;

class ELearningContentController extends Controller
{
    public function create(ELearning $elearning,Request $request)
    {
        $sections = ContentEntity::getSections();
        $section = $request->get('seccion');
        
        if( !in_array($section, $sections->toArray()) ){
            return redirect()->route('elearnings.show',$elearning->id)->withErrors(['statement_message'=>'No se puede registrar.']);
        }
    
        $content = new Content();
        $types = ContentEntity::getTypes();
        return view('admin.contents.create',compact('elearning','content','section','types'));
    }

    public function store(ContentStoreRequest $request,ELearning $elearning)
    {
        $content = ContentEntity::store($request,$elearning);

        if($content){
            return redirect()->route('elearnings.contents.show',$content->id)
            ->with(['status' => 'success','action' => 'created','statement_message' => 'Se registró correctamente.']);
        }
        return redirect()->route('elearnings.show',$elearning->id)->withErrors(['statement_message'=>'No se registró.']);
    }

    public function show(Content $content)
    {
        $content->load("elearning");
        return view('admin.contents.show',compact('content'));
    }

    public function edit(Content $content)
    {
        $types = ContentEntity::getTypes();
        return view('admin.contents.edit',compact('content','types'));
    }

    public function update(ContentUpdateRequest $request, Content $content)
    {
        ContentEntity::update($request,$content);

        return redirect()->route('elearnings.contents.show',$content->id)
        ->with(['status' => 'success','action' => 'updated','question_message' => 'Se actualizó correctamente.']);
    }

    public function destroy(Content $content)
    {
        $elearning_id = $content->elearning_id;
        $result =  ContentEntity::destroy($content);
        return redirect()->route('elearnings.show',$elearning_id)
        ->with(['status' => 'success','action' => 'deleted','question_message' => 'Se eliminó el registro correctamente.']);
    }

    public function storeMultimedia(StoreMultimediaRequest $request,DynamicQuestion $dynamic_question)
    {
        $result = DynamicQuestionEntity::storeMultimedia($request,$dynamic_question);

        if($result){
            return redirect()->route('dynamic_exams.dynamic_questions.show',$dynamic_question->id)
            ->with(['status' => 'success','action' => 'created','statement_message' => 'Se registró correctamente.']);
        }
        return redirect()->back()->withErrors(['statement_message'=>'No se registró.']);
    }
}
