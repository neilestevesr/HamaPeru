<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\UsersImport;
use App\Exports\UsersExportTemplate;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\File\FileExcelRequest;
use App\Http\Entities\UserEntity;
use App\Http\Entities\MiningUnitEntity;
use App\Http\Entities\CompanyEntity;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return UserEntity::getDatatable();
        }
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $mining_units = MiningUnitEntity::all();
        $companies = CompanyEntity::all();
        $roles = UserEntity::getRoles();
        return view('admin.users.create',compact('user','mining_units','companies','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user = UserEntity::store($request);
        if($user){
            return redirect()->route('users.show',$user->id)
            ->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('users.index')->withErrors(['message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('canUpdate', $user);

        $companies = CompanyEntity::all();
        $mining_units = MiningUnitEntity::all();
        $roles = UserEntity::getRoles();
        return view('admin.users.edit', compact('user','mining_units','companies','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $this->authorize('canUpdate', $user);

        UserEntity::update($request,$user);

        return redirect()->route('users.show',$user->id)
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('canDelete', $user);

        $result =  UserEntity::destroy($user);
        return redirect()->route('users.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }

    public function downloadTemplate()
    {
        $usersExportTemplate = new UsersExportTemplate;

        return $usersExportTemplate->download('usuarios_import_template.xlsx');
    }

    public function registerMassive()
    {
        return view('admin.users.register_massive');
    }

    public function storeMassive(FileExcelRequest $request)
    {
        $usersImport = new UsersImport;
        $usersImport->import( $request->file('file') );

        $back = back()->with([
            'status' => 'success',
            'action' => 'created',
            'message' => 'Se realizó el registro correctamente'
        ]);


        if($usersImport->failures()->isNotEmpty()){
            $back = $back->withFailures($usersImport->failures())->with([
                'status' => 'warning',
                'message_title' => 'Se encontró errores de validación'
            ]);
        }

        if($usersImport->getDnisDuplicates()->isNotEmpty()){
            $back = $back->with([
                'status' => 'warning',
                'action' => 'created',
                'message_title' => 'Se encontraron DNIs duplicados:',
                'message_body' => $usersImport->getDnisDuplicates()
            ]);
        }

        if($usersImport->errors()->isNotEmpty()){
            $errors = [];
            foreach ($usersImport->errors() as $key => $error) {
                $error_arr = ['message'=>$error->getMessage()];
                if( !in_array($error_arr, $errors) ){
                    $errors[] = $error_arr;
                }
            }

            $back = $back->withErrors($errors)->with([
                'status' => 'danger',
            ]);
        }

        return $back;
    }
}
