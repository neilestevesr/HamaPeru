<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Http\Requests\DynamicExam\DynamicExamStoreRequest;
use App\Http\Requests\DynamicExam\DynamicExamUpdateRequest;
use App\Http\Entities\DynamicTestExamEntity;
use App\Http\Entities\CourseEntity;
use App\Http\Entities\OwnerCompanyEntity;

class DynamicTestExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dynamic_test_exams = DynamicTestExamEntity::allWithRelationships();
        return view('admin.dynamic_test_exams.index',compact('dynamic_test_exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dynamic_test_exam = new Exam();
        $courses = CourseEntity::all();
        $owner_companies = OwnerCompanyEntity::all();
        return view('admin.dynamic_test_exams.create',compact('dynamic_test_exam','courses','owner_companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DynamicExamStoreRequest $request)
    {
        $dynamic_test_exam = DynamicTestExamEntity::store($request);
        if($dynamic_test_exam){
            //return "se agrego con exito dynamic exam";
            return redirect()->route('dynamic_test_exams.show',$dynamic_test_exam->id)
            ->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('dynamic_test_exams.index')->withErrors(['message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $dynamic_test_exam)
    {
        $dynamic_test_exam->load('dynamicQuestions');
        return view('admin.dynamic_test_exams.show',compact('dynamic_test_exam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $dynamicTestExam)
    {
        $dynamic_test_exam = $dynamicTestExam->load('dynamicQuestions');
        $courses = CourseEntity::all();
        $owner_companies = OwnerCompanyEntity::all();
        return view('admin.dynamic_test_exams.edit',compact('dynamic_test_exam','courses','owner_companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(DynamicExamUpdateRequest $request, Exam $dynamicTestExam)
    {
        DynamicTestExamEntity::update($request,$dynamicTestExam);

        return redirect()->route('dynamic_test_exams.show',$dynamicTestExam->id)
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $dynamicTestExam)
    {
        $this->authorize('canDelete', $dynamicTestExam);

        $result =  DynamicTestExamEntity::destroy($dynamicTestExam);
        return redirect()->route('dynamic_test_exams.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }
}
