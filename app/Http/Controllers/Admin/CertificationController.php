<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Certification\CertificationUpdateRequest;
use App\Http\Requests\Certification\CertificationFilterRequest;
use App\Exports\CertificationsExport;
use App\Exports\CertificationsExportRecovery;
use App\Http\Entities\CertificationEntity;
use App\Http\Entities\CompanyEntity;
use App\Http\Entities\MiningUnitEntity;
use App\Http\Entities\EventEntity;
use App\Http\Entities\UserEntity;
use App\Http\Entities\CourseEntity;
use App\Models\Certification;
use App\Models\Event;
use App\Models\MiningUnit;

class CertificationController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return CertificationEntity::getDatatable($request);
        }

        // $certifications = CertificationEntity::getByFilters($request);
        $courses = CourseEntity::all();
        $companies = CompanyEntity::all();

        return view('admin.certifications.index', compact('courses', 'companies'));
    }

    public function exportData(Request $request)
    {
        $certifications = CertificationEntity::getByFilters($request);
        $download_route = route('certifications.export_download_excel', ['date_from' => $request->get('date_from'), 'date_to' => $request->get('date_to')]);
        return view('admin.certifications.export_data', compact('certifications', 'download_route'));
    }

    public function exportDataRecovery(Request $request)
    {
        $certifications = CertificationEntity::getByFilters($request);
        $download_route = route('certifications.export_download_excel_recovery', ['date_from' => $request->get('date_from'), 'date_to' => $request->get('date_to')]);
        return view('admin.certifications.export_data_recovery', compact('certifications', 'download_route'));
    }


    public function downloadExcel(Request $request)
    {
        $certificationsExport = new CertificationsExport;
        $certificationsExport->setCertifications(CertificationEntity::getByFilters($request));

        return $certificationsExport->download('ReporteHAMA.xlsx');
    }

    public function downloadExcelRecovery(Request $request)
    {
        $certificationsExport = new CertificationsExportRecovery;
        $certificationsExport->setCertifications(CertificationEntity::getByFilters($request));

        return $certificationsExport->download('ReporteDesaprobados.xlsx');
    }

    public function show(Certification $certification)
    {
        return view('admin.certifications.show', compact('certification'));
    }

    public function edit(Certification $certification)
    {
        $users = UserEntity::getAllParticipants();
        $mining_units = MiningUnitEntity::all();
        $events = EventEntity::all();
        $companies = CompanyEntity::all();
        $position = UserEntity::getAllParticipants();
        return view('admin.certifications.edit', compact('certification', 'users', 'mining_units', 'events', 'companies', 'position'));
    }

    public function update(CertificationUpdateRequest $request, Certification $certification)
    {
        CertificationEntity::update($request, $certification);

        return redirect()->route('certifications.show', $certification->id)
            ->with(['status' => 'success', 'action' => 'updated', 'message' => 'Se actualizó correctamente.']);
    }

    public function destroy(Certification $certification)
    {
        $result =  CertificationEntity::destroy($certification);
        return redirect()->back()
            ->with(['object' => 'Participante', 'status' => 'success', 'action' => 'deleted', 'certification_message' => 'Se eliminó el participante correctamente.']);
    }

    public function reset(Certification $certification)
    {
        $result =  CertificationEntity::reset($certification);
        return redirect()->back()
            ->with(['status' => 'success', 'action' => 'updated', 'message' => 'Se reinició la evaluación.']);
    }

    public function groupAssist(Request $request)
    {
        $events = EventEntity::getByFilters($request, NULL, NULL, false);
        $instructors = UserEntity::getInstructor();
        $courses = CourseEntity::all();

        return view('admin.certifications.group_assist', compact('events', 'instructors', 'courses'));
    }

    public function individualAssist(Request $request)
    {
        $certifications = CertificationEntity::getByFilters($request, NULL, NULL, true);
        $courses = CourseEntity::all();
        $companies = CompanyEntity::all();

        return view('admin.certifications.individual_assist', compact('certifications', 'courses', 'companies'));
    }

    public function exportGroupAssistPDF(Event $event)
    {
        return CertificationEntity::exportGroupAssistPDF($event);
    }

    public function exportGroupAssistAtacochaPDF(Event $event)
    {
        return CertificationEntity::exportGroupAssistAtacochaPDF($event);
    }

    public function exportGroupAssistPorvenirPDF(Event $event)
    {
        return CertificationEntity::exportGroupAssistPorvenirPDF($event);
    }

    public function exportIndividualAssistPDF(Certification $certification)
    {
        return CertificationEntity::exportIndividualAssistPDF($certification);
    }

    public function exportExamPDF(Certification $certification)
    {
        $certification->load([
            'user',
            'event.exam.course',
            'company',
            'evaluations'  => function ($q) use ($certification) {
                $q->where('evaluation_time', $certification->evaluation_time);
            },
            'evaluations.dynamicQuestion.dynamicAlternatives',
            'evaluations.dynamicQuestion.files' => function ($q) {
                $q->where('file_type', 'imagenes');
            }
        ]);
        $this->authorize('canDownloadExam', $certification);

        return CertificationEntity::exportExamPDF($certification);
    }

    public function exportCertificatePDF(Certification $certification)
    {
        $certification->load(['event.exam.course', 'user', 'event.instructor']);
        $this->authorize('canDownloadCertificate', $certification);

        return CertificationEntity::exportCertificatePDF($certification);
    }

    public function exportCertificatePDFexternos(Certification $certification)
    {
        $certification->load(['event.exam.course', 'user', 'event.instructor']);
        $this->authorize('canDownloadCertificate', $certification);

        return CertificationEntity::exportCertificatePDFexternos($certification);
    }
    public function exportCertificatePDFexternos2(Certification $certification)
    {
        $certification->load(['event.exam.course', 'user', 'event.instructor']);
        $this->authorize('canDownloadCertificate', $certification);

        return CertificationEntity::exportCertificatePDFexternos2($certification);
    }

    public function exportAnexoPDF(Certification $certification, $miningUnit)
    {
        $certification->load(['user.miningUnits', 'event.exam.course', 'evaluations', 'company']);
        $this->authorize('canDownloadAnexo', [$certification, $miningUnit]);

        return CertificationEntity::exportAnexoPDF($certification, $miningUnit);
    }

    public function exportAnexoPDFAtacocha(Certification $certification, $miningUnit)
    {
        $certification->load(['user.miningUnits', 'event.exam.course', 'evaluations', 'company']);
        $this->authorize('canDownloadAnexo', [$certification, $miningUnit]);

        return CertificationEntity::exportAnexoPDFAtacocha($certification, $miningUnit);
    }

    public function exportAnexoPDFPorvenir(Certification $certification, $miningUnit)
    {
        $certification->load(['user.miningUnits', 'event.exam.course', 'evaluations', 'company']);
        $this->authorize('canDownloadAnexo', [$certification, $miningUnit]);

        return CertificationEntity::exportAnexoPDFPorvenir($certification, $miningUnit);
    }

    public function exportAnexoPDFold(Certification $certification, $miningUnit)
    {
        $certification->load(['user.miningUnits', 'event.exam.course', 'evaluations', 'company']);
        $this->authorize('canDownloadAnexo', [$certification, $miningUnit]);

        return CertificationEntity::exportAnexoPDFold($certification, $miningUnit);
    }

    public function exportCompromisoPDF(Certification $certification, $miningUnit)
    {
        $certification->load(['user', 'event.exam.course', 'evaluations', 'company']);
        $this->authorize('canDownloadAnexo', [$certification, $miningUnit]);

        return CertificationEntity::exportCompromisoPDF($certification, $miningUnit);
    }

    public function exportCompromisoPDFold(Certification $certification, $miningUnit)
    {
        $certification->load(['user', 'event.exam.course', 'evaluations', 'company']);
        $this->authorize('canDownloadAnexo', [$certification, $miningUnit]);

        return CertificationEntity::exportCompromisoPDFold($certification, $miningUnit);
    }
}
