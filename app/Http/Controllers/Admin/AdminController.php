<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Certification;
use App\Models\Event;
use App\Models\Course;

class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $events_total = Event::count();
        $courses_total = Course::count();
        $certifications_approved = Certification::where('score','>=','14')->whereHas('event', function($q){
            $now = \Carbon\Carbon::now('America/Lima');
            $q->where('date','<=',$now->isoFormat('YYYY-MM-DD') )->where('date','>=',$now->isoFormat('YYYY-MM-01'));
        })->count();

        $certifications_disapproved = Certification::where('score','<','14')->whereHas('event', function($q){
            $now = \Carbon\Carbon::now('America/Lima');
            $q->where('date','<=',$now->isoFormat('YYYY-MM-DD') )->where('date','>=',$now->isoFormat('YYYY-MM-01'));
        })->count();

        return view('admin.home',compact(['events_total','courses_total','certifications_approved','certifications_disapproved']));
    }
}
