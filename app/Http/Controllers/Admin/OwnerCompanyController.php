<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OwnerCompany;
use App\Http\Requests\OwnerCompany\OwnerCompanyStoreRequest;
use App\Http\Requests\OwnerCompany\OwnerCompanyUpdateRequest;
use App\Http\Entities\OwnerCompanyEntity;

class OwnerCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ownerCompanies = OwnerCompanyEntity::allWithRelationships();
        return view('admin.owner_companies.index',compact('ownerCompanies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ownerCompany = new OwnerCompany();
        return view('admin.owner_companies.create',compact('ownerCompany'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OwnerCompanyStoreRequest $request)
    {
        $ownerCompany = OwnerCompanyEntity::store($request);

        if($ownerCompany){
            return redirect()->route('owner_companies.index')->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('owner_companies.index')->withErrors(['message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OwnerCompany  $ownerCompany
     * @return \Illuminate\Http\Response
     */
    public function show(OwnerCompany $ownerCompany)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OwnerCompany  $ownerCompany
     * @return \Illuminate\Http\Response
     */
    public function edit(OwnerCompany $ownerCompany)
    {
        return view('admin.owner_companies.edit',compact('ownerCompany'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OwnerCompany  $ownerCompany
     * @return \Illuminate\Http\Response
     */
    public function update(OwnerCompanyUpdateRequest $request, OwnerCompany $ownerCompany)
    {
        $ownerCompany = OwnerCompanyEntity::update($request,$ownerCompany);

        return redirect()->route('owner_companies.index')
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OwnerCompany  $ownerCompany
     * @return \Illuminate\Http\Response
     */
    public function destroy(OwnerCompany $ownerCompany)
    {
        $this->authorize('canDelete', $ownerCompany);

        $result =  OwnerCompanyEntity::destroy($ownerCompany);
        return redirect()->route('owner_companies.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }
}
