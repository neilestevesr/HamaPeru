<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Survey\SurveyStoreRequest;
use App\Http\Requests\Survey\SurveyUpdateRequest;
use App\Exports\UserSurveysExport;
use App\Exports\UserProfileSurveysExport;
use App\Models\Survey;
use App\Http\Entities\SurveyEntity;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = SurveyEntity::allWithRelationships();
        return view('admin.surveys.index',compact('surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $survey = new Survey();
        return view('admin.surveys.create',compact('survey'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SurveyStoreRequest $request)
    {
        $survey = SurveyEntity::store($request);
        if($survey){
            return redirect()->route('surveys.show',$survey->id)
            ->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('surveys.index')->withErrors(['message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function show(Survey $survey)
    {
        $survey->load('groups.statements.options');
        return view('admin.surveys.show',compact('survey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        return view('admin.surveys.edit',compact('survey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(SurveyUpdateRequest $request, Survey $survey)
    {
        SurveyEntity::update($request,$survey);

        return redirect()->route('surveys.show',$survey->id)
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        $this->authorize('canDelete', $survey);

        $result =  SurveyEntity::destroy($survey);
        return redirect()->route('surveys.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }


    public function exportData(Request $request)
    {
        $userSurveys = SurveyEntity::getByFilters($request);
        $max = 0;
        foreach ($userSurveys as $key => $userSurvey) {
            $count = $userSurvey->surveyAnswers()->count();
            if($count > $max) $max = $count;
        }

        $download_route = route('surveys.export_download_excel',['date_from' => $request->get('date_from'),'date_to' => $request->get('date_to')]);
        return view('admin.surveys.export_data', compact('userSurveys','download_route','max'));
    }
    
    
    public function exportDataProfile(Request $request)
    {
        $userSurveys = SurveyEntity::getByFilters($request);
        $max = 0;
        foreach ($userSurveys as $key => $userSurvey) {
            $count = $userSurvey->surveyAnswers()->count();
            if($count > $max) $max = $count;
        }

        
        $download_route = route('surveys.export_download_excel_profile',['date_from' => $request->get('date_from'),'date_to' => $request->get('date_to')]);
        //dd($download_route);
        return view('admin.surveys.export_data_profile', compact('userSurveys','download_route','max'));
    }


    public function downloadExcel(Request $request)
    {
        $userSurveysExport = new UserSurveysExport;
        $userSurveysExport->setUserSurveys(SurveyEntity::getByFilters($request));

        return $userSurveysExport->download('EncuestasHAMA.xlsx');
    }
    
    public function downloadExcelprofile(Request $request)
    {
        $userSurveysExport = new UserProfileSurveysExport;
        $userSurveysExport->setUserSurveys(SurveyEntity::getByFilters($request));

        return $userSurveysExport->download('Perfil.xlsx');
    }

}
