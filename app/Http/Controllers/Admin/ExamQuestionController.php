<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Question;
use App\Http\Requests\Question\QuestionStoreRequest;
use App\Http\Requests\Question\QuestionUpdateRequest;
use App\Http\Entities\QuestionEntity;

class ExamQuestionController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Exam $exam)
    {
        $this->authorize('createQuestion', $exam);

        $question = new Question();
        return view('admin.questions.create',compact('exam','question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionStoreRequest $request,Exam $exam)
    {

        
        $this->authorize('createQuestion', $exam);

        $question = QuestionEntity::store($request,$exam);
        if($question){
            return redirect()->route('exams.questions.show',$question->id)
            ->with(['status' => 'success','action' => 'created','question_message' => 'Se registró correctamente.']);
        }
        return redirect()->route('exams.show',$exam->id)->withErrors(['question_message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        return view('admin.questions.show',compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        return view('admin.questions.edit',compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionUpdateRequest $request, Question $question)
    {
        QuestionEntity::update($request,$question);

        return redirect()->route('exams.questions.show',$question->id)
        ->with(['status' => 'success','action' => 'updated','question_message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $exam_id = $question->exam_id;
        $result =  QuestionEntity::destroy($question);
        return redirect()->route('exams.show',$exam_id)
        ->with(['status' => 'success','action' => 'deleted','question_message' => 'Se eliminó el registro correctamente.']);
    }
}
