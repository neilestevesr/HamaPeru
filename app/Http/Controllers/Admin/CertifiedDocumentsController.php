<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Entities\FileEntity;
use App\Http\Entities\EventEntity;
use App\Http\Entities\UserEntity;
use App\Http\Entities\CourseEntity;
use App\Http\Entities\CompanyEntity;
use App\Models\Certification;


class CertifiedDocumentsController extends Controller
{

    public function anexos(Request $request)
    {
        $files = FileEntity::getByCertificationFilters($request,'archivos','anexos');
        $courses = CourseEntity::all();
        $companies = CompanyEntity::all();

        return view('admin.certified_documents.anexos', compact('files','courses','companies'));
    }

    public function assists(Request $request)
    {
        $events = EventEntity::getByFilters($request,'archivos','asistencias')->where('date','>=','2023-01-01');
        $instructors = UserEntity::getInstructor();
        $courses = CourseEntity::all();

        return view('admin.certified_documents.assists', compact('events','instructors','courses'));
    }

}
