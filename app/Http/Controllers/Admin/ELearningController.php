<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ELearning;
use App\Http\Requests\ELearning\ELearningStoreRequest;
use App\Http\Requests\ELearning\ELearningUpdateRequest;
use App\Http\Entities\ELearningEntity;

class ELearningController extends Controller
{
    public function index()
    {
        $elearnings = ELearningEntity::allWithRelationships();
        return view('admin.elearnings.index',compact('elearnings'));
    }

    public function create()
    {
        $elearning = new ELearning();
        return view('admin.elearnings.create',compact('elearning'));
    }

    public function store(ELearningStoreRequest $request)
    {
        
        $elearning = ELearningEntity::store($request);

        if($elearning){
            return redirect()->route('elearnings.show',$elearning->id)
            ->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('elearnings.index')->withErrors(['message'=>'No se registró.']);
    }

    public function show(ELearning $elearning)
    {
        $elearning->load(['contents','files']);
        return view('admin.elearnings.show',compact('elearning'));
    }

    public function edit(ELearning $elearning)
    {
        return view('admin.elearnings.edit',compact('elearning'));
    }

    public function update(ELearningUpdateRequest $request, ELearning $elearning)
    {
       
        ELearningEntity::update($request,$elearning);

        return redirect()->route('elearnings.show',$elearning->id)
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    public function destroy(ELearning $elearning)
    {
        $this->authorize('canDelete', $elearning);

        $result =  ELearningEntity::destroy($elearning);
        return redirect()->route('elearnings.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }
}
