<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DynamicQuestion;
use App\Models\DynamicAlternative;
use App\Http\Requests\DynamicAlternative\DynamicAlternativeStoreRequest;
use App\Http\Requests\DynamicAlternative\DynamicAlternativeUpdateRequest;
use App\Http\Entities\DynamicAlternativeEntity;
use Illuminate\Http\Request;

class DynamicQuestionAlternativeController extends Controller
{
    public function create(DynamicQuestion $dynamic_question)
    {
        $dynamic_alternative = new DynamicAlternative();
        return view('admin.dynamic_alternatives.create',compact('dynamic_question','dynamic_alternative'));
    }

    public function store(DynamicAlternativeStoreRequest $request, DynamicQuestion $dynamic_question)
    {
        $dynamic_alternative = DynamicAlternativeEntity::store($request,$dynamic_question);

        if($dynamic_alternative){
            return redirect()->route('dynamic_exams.dynamic_questions.show',$dynamic_question->id)
            ->with(['status' => 'success','action' => 'created','statement_message' => 'Se registró correctamente.']);
        }
        return redirect()->route('dynamic_exams.dynamic_questions.show',$dynamic_question->id)->withErrors(['statement_message'=>'No se registró.']);
    }


    public function edit(DynamicAlternative $dynamic_alternative)
    {
        return view('admin.dynamic_alternatives.edit',compact('dynamic_alternative'));
    }


    public function update(DynamicAlternativeUpdateRequest $request, DynamicAlternative $dynamic_alternative)
    {
        DynamicAlternativeEntity::update($request,$dynamic_alternative);

        return redirect()->route('dynamic_exams.dynamic_questions.show',$dynamic_alternative->dynamic_question_id)
        ->with(['status' => 'success','action' => 'updated','question_message' => 'Se actualizó correctamente.']);
    }


    public function destroy(DynamicAlternative $dynamic_alternative)
    {
        $dynamic_question_id = $dynamic_alternative->dynamic_question_id;

        $result =  DynamicAlternativeEntity::destroy($dynamic_alternative);
        return redirect()->route('dynamic_exams.dynamic_questions.show',$dynamic_question_id)
        ->with(['status' => 'success','action' => 'deleted','question_message' => 'Se eliminó el registro correctamente.']);
    }

    public function selectCorrectAlternative(Request $request, DynamicAlternative $dynamic_alternative)
    {
        DynamicAlternativeEntity::selectCorrectAlternative($dynamic_alternative);

        return redirect()->back()->with(['status' => 'success','action' => 'updated','question_message' => 'Se actualizó correctamente.']);
    }


}
