<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Http\Requests\Exam\ExamStoreRequest;
use App\Http\Requests\Exam\ExamUpdateRequest;
use App\Http\Entities\ExamEntity;
use App\Http\Entities\CourseEntity;
use App\Http\Entities\OwnerCompanyEntity;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams = ExamEntity::allWithRelationships();
        return view('admin.exams.index',compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exam = new Exam();
        $courses = CourseEntity::all();
        $owner_companies = OwnerCompanyEntity::all();
        return view('admin.exams.create',compact('exam','courses','owner_companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExamStoreRequest $request)
    {
        $exam = ExamEntity::store($request);
        if($exam){
            return redirect()->route('exams.show',$exam->id)
            ->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('exams.index')->withErrors(['message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        $exam->load('questions');
        return view('admin.exams.show',compact('exam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        $courses = CourseEntity::all();
        $owner_companies = OwnerCompanyEntity::all();
        return view('admin.exams.edit',compact('exam','courses','owner_companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(ExamUpdateRequest $request, Exam $exam)
    {
        ExamEntity::update($request,$exam);

        return redirect()->route('exams.show',$exam->id)
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        $this->authorize('canDelete', $exam);

        $result =  ExamEntity::destroy($exam);
        return redirect()->route('exams.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }
}
