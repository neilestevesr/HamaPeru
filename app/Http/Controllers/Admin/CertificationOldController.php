<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CertificationOld\CertificationOldFilterRequest;
use App\Http\Entities\CertificationOldEntity;
use Storage;

class CertificationOldController extends Controller
{

    public function index(Request $request)
    {
        $certifications = CertificationOldEntity::getCertificationsOldFilter($request);

        return view('admin.certifications_old.index', compact('certifications'));
    }

    public function download(Request $request)
    {
        if($request->has('file_path')){
            if(Storage::disk('s3')->exists($request->get('file_path'))){
                return Storage::disk('s3')->download($request->get('file_path'));
            }
        }

        return redirect()->back()->withErrors(['message'=>'No se encontró documento.']);
    }

}
