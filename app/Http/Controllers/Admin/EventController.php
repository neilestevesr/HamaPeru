<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Imports\ParticipantsImport;
use App\Imports\ParticipantsScoreImport;
use Illuminate\Http\Request;
use App\Http\Requests\Event\EventStoreRequest;
use App\Http\Requests\Event\EventUpdateRequest;
use App\Http\Requests\Event\ParticipantsStoreRequest;
use App\Http\Requests\File\FileExcelRequest;
use App\Http\Entities\EventEntity;
use App\Http\Entities\UserEntity;
use App\Http\Entities\RoomEntity;
use App\Http\Entities\ExamEntity;
use App\Http\Entities\CourseEntity;
use App\Http\Entities\OwnerCompanyEntity;
use App\Http\Entities\ELearningEntity;
use App\Models\Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return EventEntity::getDatatable($request);
        }

        $instructors = UserEntity::getInstructor();
        $courses = CourseEntity::all();

        return view('admin.events.index', compact('instructors', 'courses'));
    }

     public function indexRecovery(Request $request)
    {
        if ($request->ajax()) {
            return EventEntity::getDatatable($request);
        }
        // $events = EventEntity::getByFilters($request);
        $instructors = UserEntity::getInstructor();
        $courses = CourseEntity::all();
        return view('admin.events.index_recovery',compact('instructors','courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('onlyAdmin')) {
             abort(403);
        }

        $event = new Event();
        $users = UserEntity::getInstructor();
        $responsables = UserEntity::getResponsable();
        $seguridades =  UserEntity::getSeguridad();
        $rooms = RoomEntity::all();
        $exams = ExamEntity::all();
        $companys = OwnerCompanyEntity::all();
        $types = EventEntity::getTypes();
        $elearnings = ELearningEntity::all();
        return view('admin.events.create',compact('event','users','responsables','rooms','exams','types','seguridades','companys','elearnings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventStoreRequest $request)
    {
        $event = EventEntity::store($request);
        if($event){
            return redirect()->route('events.show',$event->id)
            ->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('events.index')->withErrors(['message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $event->load(['exam.course','room','instructor','certifications.user','certifications.company']);
        return view('admin.events.show',compact('event'));
    }

     public function showRecovery(Event $event)
    {
        $event->load(['exam.course','room','instructor','certifications.user','certifications.company']);
        return view('admin.events.show_recovery',compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $this->authorize('canEdit', $event);

        $users = UserEntity::getInstructor();
        $responsables = UserEntity::getResponsable();
        $seguridades =  UserEntity::getSeguridad();
        $rooms = RoomEntity::all();
        $exams = ExamEntity::all();
        $companys = OwnerCompanyEntity::all();
        $types = EventEntity::getTypes();
        $elearnings = ELearningEntity::all();
        return view('admin.events.edit',compact('event','users','responsables','rooms','exams','types','seguridades','companys','elearnings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(EventUpdateRequest $request, Event $event)
    {
        $this->authorize('canEdit', $event);
        EventEntity::update($request,$event);

        return redirect()->route('events.show',$event->id)
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $this->authorize('canDelete', $event);

        $result =  EventEntity::destroy($event);
        return redirect()->route('events.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }

    public function registerParticipants(Event $event)
    {
        if (! Gate::allows('onlyAdmin')) {
             abort(403);
        }

        $users = EventEntity::getParticipants($event);
        return view('admin.events.register_participants',compact('event','users'));
    }


    public function storeParticipants(ParticipantsStoreRequest $request,Event $event)
    {
        $dnis = $request->get('participants');
        list($result, $not_register_dnis) = EventEntity::storeParticipants($dnis,$event);

        if($result){
            if($not_register_dnis->count() > 0){
                return redirect()->route('events.show',$event->id)
                ->with(['object' => 'Participante','status' => 'warning','action' => 'created','message_title' => 'No se registraron(dni):','message_body' => $not_register_dnis]);
            }
            return redirect()->route('events.show',$event->id)
            ->with(['object' => 'Participante','status' => 'success','action' => 'created','message' => 'Se registraron correctamente.']);
        }
        return redirect()->route('events.show',$event->id)->withErrors(['message'=>'No se registró.']);
    }

    public function registerMassiveParticipants(Event $event)
    {
        if (! Gate::allows('onlyAdmin')) {
             abort(403);
        }

        return view('admin.events.register_massive_participants',compact('event'));
    }


    public function registerMassiveParticipantsScorePractice(Event $event)
    {
        if (! Gate::allows('onlyAdmin')) {
             abort(403);
        }

        return view('admin.events.register_massive_participants_score_practice',compact('event'));
    }


   public function registerMassiveParticipantsArea(Event $event)
    {
        if (! Gate::allows('onlyAdmin')) {
             abort(403);
        }

        return view('admin.events.register_massive_participants_area',compact('event'));
    }



    public function registerMassiveParticipantsScore(Event $event)
    {
        if (! Gate::allows('onlyAdmin')) {
             abort(403);
        }

        return view('admin.events.register_massive_participants_score',compact('event'));
    }


    public function storeMassiveParticipantsScore(FileExcelRequest $request,Event $event)
    {
        $participantsScoreImport = new ParticipantsScoreImport;
        $participantsScoreImport->import( $request->file('file') );
        $dnis = $participantsScoreImport->getDnis();

        list($result, $not_register_dnis) = EventEntity::storeParticipantsScore($dnis,$event);

        $back = back()->with([
            'status' => 'success',
            'action' => 'created',
            'message' => 'Se actualizaron las notas correctamente'
        ]);

        if($participantsScoreImport->failures()->isNotEmpty()){
            $back = $back->withFailures($participantsScoreImport->failures())->with([
                'status' => 'warning',
                'message_title' => 'Se encontró errores de validación'
            ]);
        }

        if($participantsScoreImport->getDnisDuplicates()->isNotEmpty()){
            $back = $back->with([
                'status' => 'warning',
                'action' => 'created',
                'message_title' => 'Se encontraron DNIs duplicados:',
                'message_body' => $participantsScoreImport->getDnisDuplicates()
            ]);
        }

        if(!$result) {
            $back = $back->with([
                'status' => 'danger',
            ])->withErrors(['message'=>'No se Actualizaron Notas revise su archivo.']);
        }

        return $back;
    }

    public function storeMassiveParticipantsArea(FileExcelRequest $request,Event $event)
    {
        $participantsScoreImport = new ParticipantsScoreImport;
        $participantsScoreImport->import( $request->file('file') );
        $dnis = $participantsScoreImport->getDnis();

        list($result, $not_register_dnis) = EventEntity::storeParticipantsArea($dnis,$event);

        $back = back()->with([
            'status' => 'success',
            'action' => 'created',
            'message' => 'Se actualizaron las notas correctamente'
        ]);

        if($participantsScoreImport->failures()->isNotEmpty()){
            $back = $back->withFailures($participantsScoreImport->failures())->with([
                'status' => 'warning',
                'message_title' => 'Se encontró errores de validación'
            ]);
        }

        if($participantsScoreImport->getDnisDuplicates()->isNotEmpty()){
            $back = $back->with([
                'status' => 'warning',
                'action' => 'created',
                'message_title' => 'Se encontraron DNIs duplicados:',
                'message_body' => $participantsScoreImport->getDnisDuplicates()
            ]);
        }

        if(!$result) {
            $back = $back->with([
                'status' => 'danger',
            ])->withErrors(['message'=>'No se Actualizaron Notas revise su archivo.']);
        }

        return $back;
    }

    public function storeMassiveParticipantsScorePractice(FileExcelRequest $request,Event $event)
    {
        $participantsScoreImport = new ParticipantsScoreImport;
        $participantsScoreImport->import( $request->file('file') );
        $dnis = $participantsScoreImport->getDnis();

        list($result, $not_register_dnis) = EventEntity::storeParticipantsScorePractice($dnis,$event);

        $back = back()->with([
            'status' => 'success',
            'action' => 'created',
            'message' => 'Se actualizaron las notas correctamente'
        ]);

        if($participantsScoreImport->failures()->isNotEmpty()){
            $back = $back->withFailures($participantsScoreImport->failures())->with([
                'status' => 'warning',
                'message_title' => 'Se encontró errores de validación'
            ]);
        }

        if($participantsScoreImport->getDnisDuplicates()->isNotEmpty()){
            $back = $back->with([
                'status' => 'warning',
                'action' => 'created',
                'message_title' => 'Se encontraron DNIs duplicados:',
                'message_body' => $participantsScoreImport->getDnisDuplicates()
            ]);
        }

        if(!$result) {
            $back = $back->with([
                'status' => 'danger',
            ])->withErrors(['message'=>'No se Actualizaron Notas revise su archivo.']);
        }

        return $back;
    }






    public function storeMassiveParticipants(FileExcelRequest $request,Event $event)
    {
        $participantsImport = new ParticipantsImport;
        $participantsImport->import( $request->file('file') );
        $dnis = $participantsImport->getDnis();

        list($result, $not_register_dnis) = EventEntity::storeParticipants($dnis,$event);

        $back = back()->with([
            'status' => 'success',
            'action' => 'created',
            'message' => 'Se realizó el registro correctamente'
        ]);

        if($participantsImport->failures()->isNotEmpty()){
            $back = $back->withFailures($participantsImport->failures())->with([
                'status' => 'warning',
                'message_title' => 'Se encontró errores de validación'
            ]);
        }

        if($participantsImport->getDnisDuplicates()->isNotEmpty()){
            $back = $back->with([
                'status' => 'warning',
                'action' => 'created',
                'message_title' => 'Se encontraron DNIs duplicados:',
                'message_body' => $participantsImport->getDnisDuplicates()
            ]);
        }

        if(!$result) {
            $back = $back->with([
                'status' => 'danger',
            ])->withErrors(['message'=>'No se registró participantes.']);
        }

        return $back;
    }
}
