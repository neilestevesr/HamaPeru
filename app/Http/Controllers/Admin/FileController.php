<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\File\FileRequest;
use App\Models\File;
use App\Http\Entities\FileEntity;
use Storage;

class FileController extends Controller
{

    public function managementFile()
    {
        $files = FileEntity::getFiles();

        return view('admin.certifications.file_management',compact('files'));
    }


    public function managementHistFile()
    {
        $files = FileEntity::getFiles();

        return view('admin.certifications.file_management_hist',compact('files'));
    }

    public function filterFile(Request $request)
    
    {
    
        $date_from = $request->get('date_from');
        $date_to = $request->get('date_to'); 
        $files = FileEntity::getFilesSpecific($date_from,$date_to);

        return view('admin.certifications.file_management_hist',compact('files'));
    }
    

    public function storeFile(FileRequest $request)
    {
        list($success, $reject) = FileEntity::storeFile($request->file('files'));

        if(count($reject) == 0){
            return redirect()->route('files.management')
            ->with(['status' => 'success','title_alert' => 'Cargados','action' => 'created','message' => "Carga de archivos con éxito."]);
        }
        elseif (count($success) > 0) {
            return redirect()->route('files.management')
            ->with(['object' => 'Archivos',
            'status' => 'warning',
            'action' => 'created',
            'title_alert' => 'Cargados',
            'message_title' => 'No se cargaron los archivos:',
            'message_body' => $reject]);
        }

        return redirect()->route('files.management')->withErrors($reject);

    }

    public function downloadFile(File $file)
    {
        if(Storage::disk('s3')->exists($file->file_path) ){
            return Storage::disk('s3')->download($file->file_path);
        }

        return redirect()->back()->withErrors(['message'=>'No se encontró documento.']);
    }

    public function destroy(File $file)
    {
        $result =  FileEntity::delete($file);
        //return redirect()->route('files.management')
        //->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el archivo correctamente.']);

        return redirect()->back()->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el archivo correctamente.']);
    }

}
