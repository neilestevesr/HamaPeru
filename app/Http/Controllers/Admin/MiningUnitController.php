<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MiningUnit;
use App\Http\Requests\MiningUnit\MiningUnitStoreRequest;
use App\Http\Requests\MiningUnit\MiningUnitUpdateRequest;
use App\Http\Entities\MiningUnitEntity;

class MiningUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $miningUnits = MiningUnitEntity::allWithRelationships();
        return view('admin.mining_units.index',compact('miningUnits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $miningUnit = new MiningUnit();
        return view('admin.mining_units.create',compact('miningUnit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MiningUnitStoreRequest $request)
    {
        $miningUnit = MiningUnitEntity::store($request);

        if($miningUnit){
            return redirect()->route('mining_units.index')->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('mining_units.index')->withErrors(['message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MiningUnit  $miningUnit
     * @return \Illuminate\Http\Response
     */
    public function show(MiningUnit $miningUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MiningUnit  $miningUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(MiningUnit $miningUnit)
    {
        return view('admin.mining_units.edit',compact('miningUnit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MiningUnit  $miningUnit
     * @return \Illuminate\Http\Response
     */
    public function update(MiningUnitUpdateRequest $request, MiningUnit $miningUnit)
    {
        $miningUnit = MiningUnitEntity::update($request,$miningUnit);

        return redirect()->route('mining_units.index')
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MiningUnit  $miningUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy(MiningUnit $miningUnit)
    {
        $this->authorize('canDelete', $miningUnit);

        $result =  MiningUnitEntity::destroy($miningUnit);
        return redirect()->route('mining_units.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }
}
