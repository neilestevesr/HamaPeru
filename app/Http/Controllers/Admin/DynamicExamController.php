<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Http\Requests\DynamicExam\DynamicExamStoreRequest;
use App\Http\Requests\DynamicExam\DynamicExamUpdateRequest;
use App\Http\Entities\DynamicExamEntity;
use App\Http\Entities\CourseEntity;
use App\Http\Entities\OwnerCompanyEntity;

class DynamicExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dynamic_exams = DynamicExamEntity::allWithRelationships();
        return view('admin.dynamic_exams.index',compact('dynamic_exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dynamic_exam = new Exam();
        $courses = CourseEntity::all();
        $owner_companies = OwnerCompanyEntity::all();
        return view('admin.dynamic_exams.create',compact('dynamic_exam','courses','owner_companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DynamicExamStoreRequest $request)
    {
        $dynamic_exam = DynamicExamEntity::store($request);
        if($dynamic_exam){
            //return "se agrego con exito dynamic exam";
            return redirect()->route('dynamic_exams.show',$dynamic_exam->id)
            ->with(['status' => 'success','action' => 'created','message' => 'Se registró correctamente.']);
        }
        return redirect()->route('dynamic_exams.index')->withErrors(['message'=>'No se registró.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $dynamic_exam)
    {
        $dynamic_exam->load('dynamicQuestions');
        return view('admin.dynamic_exams.show',compact('dynamic_exam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $dynamicExam)
    {
        $dynamic_exam = $dynamicExam->load('dynamicQuestions');
        $courses = CourseEntity::all();
        $owner_companies = OwnerCompanyEntity::all();
        return view('admin.dynamic_exams.edit',compact('dynamic_exam','courses','owner_companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(DynamicExamUpdateRequest $request, Exam $dynamicExam)
    {
        DynamicExamEntity::update($request,$dynamicExam);

        return redirect()->route('dynamic_exams.show',$dynamicExam->id)
        ->with(['status' => 'success','action' => 'updated','message' => 'Se actualizó correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $dynamicExam)
    {
        $this->authorize('canDelete', $dynamicExam);

        $result =  DynamicExamEntity::destroy($dynamicExam);
        return redirect()->route('dynamic_exams.index')
        ->with(['status' => 'success','action' => 'deleted','message' => 'Se eliminó el registro correctamente.']);
    }
}
