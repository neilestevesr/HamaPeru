<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
    * Where to redirect users after login.
    *
    * @var string
    */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/home';

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'dni';
    }

    public function redirectTo()
    {
        if( in_array(\Auth::user()->role, ['admin','super_admin']) ){
            return route('admin_home');
        }
        else {
        
          
          if(in_array(\Auth::user()->role, ['participants']))
          {
              if (\Auth::user()->profile_survey == 'N')
                {
                  
                 
                 return route('classroom.virtual_surveys.profile_survey_index');
                }
                else
                {
                   return route('classroom.home');
                }
          }
          else
          {
             return route('classroom.home');
          }
          
          
            
        }
        return route('home');
    }

    public function showAdminLoginForm()
    {
        return view('auth.admin_login');
    }

    public function showClassroomLoginForm()
    {
        return view('auth.classroom_login');
    }

    protected function logout(Request $request)
    {
        $role = $this->guard()->user()->role;
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        if( in_array($role, ['admin','super_admin']) ){
            return redirect()->route('admin_login');
        }
        else {
            return redirect()->route('classroom_login');
        }

        return redirect()->route('classroom_login');
    }
     

}
