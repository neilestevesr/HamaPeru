<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CertificationOld\CertificationOldFilterRequest;
use App\Http\Entities\CertificationOldEntity;
use App\Http\Entities\CertificationEntity;
use App\Models\Certification;
use App\Models\MiningUnit;
use App\Models\User;
use App\Models\File;
use App\Http\Entities\CompanyEntity;
use Illuminate\Support\Str;
use Storage;

class CertificateController extends Controller
{
    public function certificates(Request $request)
    {
        $certifications = collect([]);
        $regular_certifications = collect([]);
        $spec_certifications = collect([]);

        $user = null;

        if( ($request->has('dni') && $request->get('dni') != '') || ($request->has('company_id') && $request->get('company_id') != '') ){
            
            $certifications = CertificationEntity::getCertificationsFilterDni($request);

            $regular_certifications = $certifications->filter(function ($certification) {
                return $certification->files->filter(function ($file) {
                    return strtolower((explode('_', $file->name))[0]) == 'esp';
                })->isEmpty() && !Str::is('*HAMA CERTIFICADOS', strtoupper($certification->event->EmpresaTitular->name ?? '-'));
            });

            $spec_certifications = $certifications->filter(function ($certification) {
                return $certification->files->filter(function ($file) {
                    return strtolower((explode('_', $file->name))[0]) == 'esp';
                })->isNotEmpty() && Str::is('*HAMA CERTIFICADOS', strtoupper($certification->event->EmpresaTitular->name ?? '-'));
            });

            $user = User::with('miningUnits')->where('dni',$request->get('dni'))->first();
        }
        // $companies = CompanyEntity::all()->sortBy('description');

        return view('certificates', compact(
            'regular_certifications',
            'spec_certifications',
            'user'
        ));
    }

    public function downloadCertificates(Request $request)
    {
        if($request->has('file_path')){
            if(Storage::disk('s3')->exists($request->get('file_path'))){
                return Storage::disk('s3')->download($request->get('file_path'));
            }
        }

        return redirect()->back()->withErrors(['message'=>'No se encontró documento.']);
    }

    public function certificatesOld(Request $request)
    {
        $certifications = [];
        if($request->has('dni')){
            $certifications = CertificationOldEntity::getCertificationsOldFilter($request);
            // dd($certifications);
        }

        return view('certificates_old', compact('certifications'));
    }

    public function downloadCertificatesOld(Request $request)
    {
        if($request->has('file_path')){
            if(Storage::disk('s3')->exists($request->get('file_path'))){
                return Storage::disk('s3')->download($request->get('file_path'));
            }
        }

        return redirect()->back()->withErrors(['message'=>'No se encontró documento.']);
    }

    // public function exportExamPDF(Certification $certification)
    // {
    //     $certification->load(['user','event.exam.course','evaluations','company']);
    //     return CertificationEntity::exportExamPDF($certification);
    // }

    public function exportCertificatePDF(Certification $certification)
    {
        $certification->load(['event.exam.course','user','event.instructor']);
        return CertificationEntity::exportCertificatePDF($certification);
    }

     public function exportCertificatePDFexternos(Certification $certification)
    {
        $certification->load(['event.exam.course','user','event.instructor']);
        return CertificationEntity::exportCertificatePDFexternos($certification);
    }
     public function exportCertificatePDFexternos2(Certification $certification)
    {
        $certification->load(['event.exam.course','user','event.instructor']);
        return CertificationEntity::exportCertificatePDFexternos2($certification);
    }
    public function exportCertificatePDFold(Certification $certification)
    {
        $certification->load(['event.exam.course','user','event.instructor']);
        return CertificationEntity::exportCertificatePDFold($certification);
    }

    public function exportAnexoPDF(Certification $certification, $miningUnit)
    {
        //dd($certificacion,$miningUnit);
        $certification->load(['user','event.exam.course','evaluations','company']);
        return CertificationEntity::exportAnexoPDF($certification,$miningUnit);
    }

    public function exportCompromisoPDF(Certification $certification, $miningUnit)
    {
        $certification->load(['user','event.exam.course','evaluations','company']);
        return CertificationEntity::exportCompromisoPDF($certification,$miningUnit);
    }

    public function exportCompromisoPDFold(Certification $certification, $miningUnit)
    {
        $certification->load(['user','event.exam.course','evaluations','company']);
        return CertificationEntity::exportCompromisoPDFold($certification,$miningUnit);
    }
    public function downloadFile(File $file)
    {
        if(Storage::disk('s3')->exists($file->file_path) ){
            return Storage::disk('s3')->download($file->file_path);
        }

        return redirect()->back()->withErrors(['message'=>'No se encontró documento.']);
    }

}
