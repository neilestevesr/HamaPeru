<?php

namespace App\Http\Requests\VirtualSurvey;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class SurveyAnswerRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'answer' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'answer.required'    => 'Debe Responder a la pregunta.',
        ];
    }
}
