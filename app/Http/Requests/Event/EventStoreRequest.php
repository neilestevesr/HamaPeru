<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class EventStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'           => ['required', 'max:255'],
            'type'                  => ['required', 'max:30'],
            'date'                  => ['required', 'date', 'date_format:Y-m-d'],
            'active'                => ['nullable', 'max:2'],
            'flg_test_exam'         => ['nullable', 'max:2'],
            'flg_asist'             => ['nullable', 'max:2'],
            'flg_survey_course'     => ['nullable', 'max:2'],
            'flg_survey_evaluation' => ['nullable', 'max:2'],
            'exam_id'               => ['required', 'exists:App\Models\Exam,id'],
            'user_id'               => ['required', 'exists:App\Models\User,id'],
            'responsable_id'        => ['required', 'exists:App\Models\User,id'],
            'room_id'               => ['required', 'exists:App\Models\Room,id'],
        ];
    }
}
