<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class EventAssistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'assists'      => ['filled', 'array'],
            'assists.*'    => ['filled', 'exists:App\Models\Certification,id'],
        ];
    }

    public function messages()
    {
        return [
            'assists.required' => "El campo Asistencia es obligatorio.",
        ];
    }
}
