<?php

namespace App\Http\Requests\DynamicQuestion;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class DynamicQuestionUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'statement'             => ['required', 'max:1000'],
        ];
    }
}
