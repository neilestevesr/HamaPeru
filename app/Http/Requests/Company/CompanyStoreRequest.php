<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CompanyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'   => ['required', 'max:255'],
            'abbreviation'  => ['nullable', 'max:100'],
            'ruc'           => ['required', 'unique:App\Models\Company,ruc', 'digits_between:11,15'],
            'address'       => ['required', 'max:255'],
            'telephone'     => ['nullable', 'max:20'],
            'name_ref'      => ['nullable', 'max:255'],
            'telephone_ref' => ['nullable', 'max:20'],
            'email_ref'     => ['nullable', 'email', 'max:50'],
            'active'        => ['nullable', 'max:2'],
        ];
    }
}
