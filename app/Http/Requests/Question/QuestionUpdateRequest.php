<?php

namespace App\Http\Requests\Question;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class QuestionUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'statement'             => ['required', 'max:255'],
            'alternative_a'         => ['required', 'max:255'],
            'alternative_b'         => ['required', 'max:255'],
            'alternative_c'         => ['required', 'max:255'],
            'alternative_d'         => ['required', 'max:255'],
            'alternative_correct'   => ['required', 'in:alternative_a,alternative_b,alternative_c,alternative_d'],
        ];
    }
}
