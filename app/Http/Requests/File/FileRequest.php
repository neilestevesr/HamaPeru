<?php

namespace App\Http\Requests\File;

use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'files' => ['required'],
            'files.*' => ['mimes:doc,pdf,docx,txt,xlsx,csv'],
        ];
    }

    public function messages()
    {
        return [
            'files.required'    => 'El campo Archivo es obligatorio.',
            'files.*.mimes'    => 'Los achivos debe tener tener el formato doc,pdf,docx,txt,xlsx,csv.',
        ];
    }
}
