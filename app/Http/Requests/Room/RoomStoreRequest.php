<?php

namespace App\Http\Requests\Room;

use Illuminate\Foundation\Http\FormRequest;

class RoomStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'   => ['required', 'unique:App\Models\Room,description', 'max:255'],
            'capacity'      => ['required', 'Integer', 'max:999999', 'min:1'],
            'url_zoom'      => ['nullable', 'url', 'max:255'],
            'active'        => ['nullable', 'max:2'],
        ];
    }
}
