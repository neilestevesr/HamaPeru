<?php

namespace App\Http\Requests\Statement;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class StatementUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'   => ['required', 'max:255'],
            'desc'          => ['nullable', 'max:255'],
            'type'          => ['required', 'max:255'],
        ];
    }
}
