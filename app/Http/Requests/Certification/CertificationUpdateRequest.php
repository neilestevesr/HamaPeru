<?php

namespace App\Http\Requests\Certification;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class CertificationUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'assist_user'   => ['nullable', 'max:2'],
            'user_id'       => ['required', 'exists:App\Models\User,id','unique:App\Models\Certification,user_id,' . $this->route->parameter('certification')->id . ',id,event_id,' . $this->get('event_id') . ',evaluation_type,' . $this->get('evaluation_type') ],
            'mining_units'      => ['filled', 'array'],
            'mining_units.*'    => ['filled', 'exists:App\Models\MiningUnit,id'],
            //'exam_id'       => ['required', 'exists:App\Models\Exam,id'],
            'event_id'      => ['required', 'exists:App\Models\Event,id'],
            'company_id'    => ['required', 'exists:App\Models\Company,id'],
        ];
    }
}
