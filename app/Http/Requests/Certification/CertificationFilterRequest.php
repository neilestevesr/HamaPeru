<?php

namespace App\Http\Requests\Certification;

use Illuminate\Foundation\Http\FormRequest;

class CertificationFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateFrom'  => ['nullable', 'date'],
            'dateTo'    => ['nullable', 'date', 'after_or_equal:dateFrom'],
        ];
    }

    public function messages()
    {
        return [
            'dateFrom.date'         => 'El campo Archivo es obligatorio.',
            'dateTo.date'           => 'El campo Archivo es obligatorio.',
            'dateTo.after_or_equal' => 'La fecha debe ser posterior a la fecha "Desde"',
        ];
    }
}
