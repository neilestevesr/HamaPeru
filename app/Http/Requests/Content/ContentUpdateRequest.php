<?php

namespace App\Http\Requests\Content;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class ContentUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'title'     => ['required', 'max:500'],
            'section'   => ['required', 'max:30'],
            'type'      => ['required', 'max:30'],
            'url'       => ['max:1000'],
            'files'     => ['array'],
            #'files.*'   => ['mimetypes:image/jpeg,image/png,video/mp4'],
            'files.*'   => ['file'],
            'video'     => ['file','mimetypes:video/mp4'],
        ];
    }
}
