<?php

namespace App\Http\Requests\Content;

use Illuminate\Foundation\Http\FormRequest;

class ContentStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'     => ['required', 'max:500'],
            'section'   => ['required', 'max:30'],
            'type'      => ['required', 'max:30'],
            'url'       => ['max:1000'],
            'files'     => ['array'],
            #'files.*'  => ['required', 'mimetypes:image/jpeg,image/png,video/mp4'],
            'files.*'   => ['file'],
            'video'     => ['file','mimetypes:video/mp4'],
        ];
    }
/*
    public function messages()
    {
        return [
            'image.required'    => 'El campo imagen es obligatorio.',
            'image.image'       => 'El archivo debe ser una imagen(jpeg, png).',
        ];
    }
*/
}
