<?php

namespace App\Http\Requests\ELearning;

use Illuminate\Foundation\Http\FormRequest;

class ELearningStoreRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'title'         => ['required', 'max:255'],
            'description'   => ['nullable', 'max:255'],
            'image'         => ['nullable', 'image'],
            'active'        => ['nullable', 'max:2'],
        ];
    }

    public function messages()
    {
        return [
            'image.required'    => 'El campo imagen es obligatorio.',
            'image.image'       => 'El archivo debe ser una imagen(jpeg, png).',
        ];
    }
}
