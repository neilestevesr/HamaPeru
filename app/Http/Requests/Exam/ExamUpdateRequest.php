<?php

namespace App\Http\Requests\Exam;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class ExamUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             => ['required', 'max:255'],
            'exam_time'         => ['required', 'Integer', 'max:999999'],
            'active'            => ['nullable', 'max:2'],
            'owner_company_id'  => ['required', 'exists:App\Models\OwnerCompany,id'],
            'course_id'         => ['required', 'exists:App\Models\Course,id'],
        ];
    }
}
