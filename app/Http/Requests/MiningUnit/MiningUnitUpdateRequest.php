<?php

namespace App\Http\Requests\MiningUnit;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class MiningUnitUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => ['required', 'unique:App\Models\MiningUnit,description,' . $this->route->parameter('mining_unit')->id . ',id', 'max:255'],
            'owner' => ['nullable', 'max:255'],
            'district' => ['nullable', 'max:255'],
            'Province' => ['nullable', 'max:255'],
        ];
    }
}
