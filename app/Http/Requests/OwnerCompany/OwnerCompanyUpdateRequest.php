<?php

namespace App\Http\Requests\OwnerCompany;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class OwnerCompanyUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique:App\Models\OwnerCompany,name,' . $this->route->parameter('owner_company')->id . ',id', 'max:50'],
        ];
    }
}
