<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UserUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni'               => ['required', 'unique:App\Models\User,dni,' . $this->route->parameter('user')->id . ',id', 'digits_between:8,11'],
            'name'              => ['required', 'max:255'],
            'paternal'          => ['required', 'max:255'],
            'maternal'          => ['required', 'max:255'],
            'email'             => ['required', 'email', 'max:255'],
            'password'          => ['nullable', 'max:255'],
            'telephone'         => ['nullable', 'max:20'],
            'role'              => ['nullable', 'max:255'],
            'cip'               => ['nullable', 'digits:6'],
            'active'            => ['nullable', 'max:2'],
            'mining_units'      => ['filled', 'array'],
            'mining_units.*'    => ['filled', 'exists:App\Models\MiningUnit,id'],
            'company_id'        => ['nullable', 'exists:App\Models\Company,id'],
            'position'          => ['nullable', 'max:255'],
        ];
    }
}
