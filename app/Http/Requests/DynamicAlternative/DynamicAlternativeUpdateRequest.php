<?php

namespace App\Http\Requests\DynamicAlternative;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class DynamicAlternativeUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => ['required', 'max:500'],
        ];
    }
}
