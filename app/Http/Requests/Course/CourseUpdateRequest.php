<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class CourseUpdateRequest extends FormRequest
{
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'description'   => ['required', 'unique:App\Models\Course,description,' . $this->route->parameter('course')->id . ',id', 'max:255'],
            'description'   => ['required', 'max:255'],
            'subtitle'      => ['nullable', 'max:255'],
            'date'          => ['required','date','date_format:Y-m-d'],
            'hours'         => ['required','numeric', 'max:999.99', 'min:0.5'],
            'time_start'    => ['required','date_format:H:i','before:time_end'],
            'time_end'      => ['required','date_format:H:i','after:time_start'],
            'image'         => ['nullable', 'image'],
            'image'         => ['nullable', 'file'],
            'active'        => ['nullable', 'max:2'],
        ];
    }

    public function messages()
    {
        return [
            'image.required'    => 'El campo imagen es obligatorio.',
            'image.image'       => 'El archivo debe ser una imagen(jpeg, png).',
        ];
    }
}
