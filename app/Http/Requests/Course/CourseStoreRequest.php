<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class CourseStoreRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'description'   => ['required', 'max:255'],
            'subtitle'      => ['nullable', 'max:255'],
            'date'          => ['required', 'date','date_format:Y-m-d'],
            'hours'         => ['required', 'numeric', 'max:999.99', 'min:0.5'],
            'time_start'    => ['required', 'date_format:H:i','before:time_end'],
            'time_end'      => ['required', 'date_format:H:i','after:time_start'],
            'image'         => ['nullable', 'image'],
            'active'        => ['nullable', 'max:2'],
        ];
    }

    public function messages()
    {
        return [
            'image.required'    => 'El campo imagen es obligatorio.',
            'image.image'       => 'El archivo debe ser una imagen(jpeg, png).',
        ];
    }
}
