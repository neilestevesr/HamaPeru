<?php

namespace App\Http\Requests\CertificationOld;

use Illuminate\Foundation\Http\FormRequest;

class CertificationOldFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni'   => ['required', 'digits:8'],
        ];
    }

    public function messages()
    {
        return [
            'dni.required'  => 'El campo DNI es obligatorio.',
        ];
    }
}
