<?php

namespace App\Http\Middleware;

use Closure;

class OnlySeguridad
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( in_array(\Auth::user()->role, ['security_manager','super_admin','admin','security_manager_admin']) )
        {
            return $next($request);
        }
        return redirect('/');
    }
}
