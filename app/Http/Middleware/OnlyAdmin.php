<?php

namespace App\Http\Middleware;

use Closure;

class OnlyAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( in_array(\Auth::user()->role, ['super_admin','admin','supervisor','technical_support']) ){
            return $next($request);
        }
        //return redirect()->route('classroom.home');
        return redirect()->route('classroom.courses.live_list');

    }
}
