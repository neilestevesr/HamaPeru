<?php

namespace App\Http\Middleware;

use Closure;

class OnlyClassroom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( in_array(\Auth::user()->role, ['instructor','participants','admin','super_admin','security_manager','security_manager_admin']) )
        {
            if(in_array(\Auth::user()->role, ['participants','security_manager','security_manager_admin']))
            {
               
                if (\Auth::user()->profile_survey == 'N')
                {
                  
                 
                  return $next($request);
                }
                
                
                else
                {
                   return $next($request);
                
                }
               
               
               
               
            }
            else
            {
               return $next($request);
            }
            
            
        }
        else if (in_array(\Auth::user()->role, ['admin','super_admin','supervisor']) )
        {
            return redirect()->route('admin_home');
        }
        return redirect('/');
    }
}
