<?php

namespace App\Http\Middleware;

use Closure;

class OnlyInstructor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( in_array(\Auth::user()->role, ['instructor','super_admin','admin']) )
        {
            return $next($request);
        }
        return redirect('/');
    }
}
