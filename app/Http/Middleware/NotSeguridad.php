<?php

namespace App\Http\Middleware;

use Closure;

class NotSeguridad
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( in_array(\Auth::user()->role, ['security_manager','security_manager_admin']) )
        {
            return redirect()->route('classroom.profile.index');
        }
        return $next($request);
    }
}
