<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if( in_array(\Auth::user()->role, ['admin','super_admin','technical_support']) ){
                return redirect()->route('admin_home');
            }
            else {
                return redirect()->route('classroom.courses.live_list');
            }
        }

        return $next($request);
    }
}
