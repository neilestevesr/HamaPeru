<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

use App\Policies\CertificationPolicy;
use App\Policies\CompanyPolicy;
use App\Policies\CoursePolicy;
use App\Policies\ELearningPolicy;
use App\Policies\EventPolicy;
use App\Policies\ExamPolicy;
use App\Policies\MiningUnitPolicy;
use App\Policies\OwnerCompanyPolicy;
use App\Policies\RoomPolicy;
use App\Policies\UserPolicy;
use App\Policies\SurveyPolicy;
use App\Policies\GroupPolicy;
use App\Policies\StatementPolicy;

use App\Models\Certification;
use App\Models\Company;
use App\Models\Course;
use App\Models\ELearning;
use App\Models\Event;
use App\Models\Exam;
use App\Models\MiningUnit;
use App\Models\OwnerCompany;
use App\Models\Room;
use App\Models\User;
use App\Models\Survey;
use App\Models\Group;
use App\Models\Statement;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Certification::class => CertificationPolicy::class,
        Company::class => CompanyPolicy::class,
        Course::class => CoursePolicy::class,
        ELearning::class => ELearningPolicy::class,
        Event::class => EventPolicy::class,
        Exam::class => ExamPolicy::class,
        MiningUnit::class => MiningUnitPolicy::class,
        OwnerCompany::class => OwnerCompanyPolicy::class,
        Room::class => RoomPolicy::class,
        User::class => UserPolicy::class,
        Survey::class => SurveyPolicy::class,
        Group::class => GroupPolicy::class,
        Statement::class => StatementPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('onlyAdmin', function ($user) {
            if( in_array($user->role, ['admin','super_admin']) ){
                return true;
            }
            return false;
        });

        Gate::define('adminAndSupervisor', function ($user) {
            if( in_array($user->role, ['super_admin','admin','supervisor']) ){
                return true;
            }
            return false;
        });

       Gate::define('onlySoporte', function ($user) {
            if( in_array($user->role, ['technical_support']) ){
                return true;
            }
            return false;
        });

       
       Gate::define('onlySupervisor', function ($user) {
            if( in_array($user->role, ['security_manager','security_manager_admin']) ){
                return true;
            }
            return false;
        });
       
        Gate::define('onlyInstructor', function ($user) {
            if( in_array($user->role, ['instructor','admin','super_admin']) ){
                return true;
            }
            return false;
        });

        Gate::define('notInstructor', function ($user) {
            if( in_array($user->role, ['instructor']) ){
                return false;
            }
            return true;
        });
        
        Gate::define('notSupervisor', function ($user) {
            if( in_array($user->role, ['security_manager','security_manager_admin']) ){
                return false;
            }
            return true;
        });
        
    }
}
